@extends('layouts.app')
@section('content')
    <div id="main">
        <div class="section section-bg-10 pt-11 pb-17">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="page-title text-center">Detail Produk</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="section border-bottom pt-2 pb-2">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <ul class="breadcrumbs">
                            <li><a href="{{URL::to('/')}}">Home</a></li>
                            <li><a href="{{route('products.index')}}">Belanja</a></li>
                            <li>Detail Produk</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="section pt-7 pb-7">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-md-push-3">
                        <div class="single-product">
                            <div class="col-md-6">
                                <div class="image-gallery" >
                                    <div class="image-gallery-inner">
                                        @foreach($images as $image)
                                            <div>
                                                <div class="image-thumb">

                                                            <img src="{{URL::asset('/storage/'.$image->directory)}}" alt=""/>

                                                </div>
                                            </div>
                                        @endforeach

                                    </div>
                                </div>
                                <div class="image-gallery-nav">
                                    @foreach($images as $image)
                                            <div class="image-nav-item">
                                                <div class="image-thumb">
                                                    <img src="{{URL::asset('/storage/'.$image->directory)}}" class="img-thumbnail" alt=""/>

                                                </div>
                                            </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="summary">
                                    <h1 class="product-title">{{ $product[0]->productName }}</h1>
                                    <!--<div class="product-rating">
                                        <div class="star-rating">
                                            <span style="width:100%"></span>
                                        </div>
                                        <i>(2 customer reviews)</i>
                                    </div>-->
                                    <div class="product-price" >
                                        <ins id="priceSum">
                                            Rp. {{ str_replace(',', '.', number_format($product[0]->price))  }}</ins>
                                        {{--                                        <ins>IDR {{ $product[0]->price }}</ins>--}}
                                    </div>
                                    <form class="cart">
                                        <div class="quantity-chooser">
                                            <div class="quantity">
                                                <span class="qty-minus" data-min="1"><i
                                                            class="ion-ios-minus-outline"></i></span>
                                                <input type="text" name="quantity" value="1" title="Qty"
                                                       class="input-text qty text" size="4" id="multiQty">
                                                <span class="qty-plus" data-max=""><i class="ion-ios-plus-outline"></i></span>
                                            </div>
                                        </div>
                                        <button type="button" class="single-add-to-cart"
                                                onclick="addMultiCartTemp('{{ $product[0]->id }}','{{ $product[0]->productName }}','{{ $product[0]->price }}','{{ $product[0]->image }}')"
                                                {{--ontouchend="addMultiCartTemp('{{ $product[0]->id }}','{{ $product[0]->productName }}','{{ $product[0]->price }}','{{ $product[0]->image }}')"--}}

                                        >
                                            TAMBAH KE CART
                                        </button>

                                    </form>
                                    <!--<div class="product-tool">
                                        <a href="#" data-toggle="tooltip" data-placement="top" title="Add to wishlist"> Browse Wishlist </a>
                                        <a class="compare" href="#" data-toggle="tooltip" data-placement="top" title="Add to compare"> Compare </a>
                                    </div>-->
                                    <div class="product-meta">
                                        <table>
                                            <tbody>

                                            <tr>
                                                <td class="label">Kategori</td>
                                                <td><a href="{{route('categories.show',$product[0]->category_id)}}">{{ $product[0]->categoryName  }}</a></td>
                                            </tr>


                                            <tr>
                                                <td class="label">Petani</td>
                                                <td><a href="{{route('suppliers.show',$product[0]->supplier_id)}}">{{ $product[0]->supplierName  }}</a></td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="commerce-tabs tabs classic">
                                    <ul class="nav nav-tabs tabs">
                                        <li class="active">
                                            <a data-toggle="tab" href="#tab-description" aria-expanded="true">Deskripsi</a>
                                        </li>
                                        <!--<li class="">
                                            <a data-toggle="tab" href="#tab-reviews" aria-expanded="false">Reviews</a>
                                        </li>-->
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade active in" id="tab-description">
                                            <p>
                                                {{ $product[0]->description }}
                                            </p>
                                        </div>
                                        <div id="tab-reviews" class="tab-pane fade">
                                            <div class="single-comments-list mt-0">
                                                <div class="mb-2">
                                                    <h2 class="comment-title">2 reviews for Orange Juice</h2>
                                                </div>
                                                <ul class="comment-list">
                                                    <li>
                                                        <div class="comment-container">
                                                            <div class="comment-author-vcard">
                                                                <img alt=""
                                                                     src="{{URL::asset('images/avatar/avatar.png')}}"/>
                                                            </div>
                                                            <div class="comment-author-info">
                                                                <span class="comment-author-name">admin</span>
                                                                <a href="#" class="comment-date">July 27, 2016</a>
                                                                <p>Far far away, behind the word mountains, far from the
                                                                    countries Vokalia and Consonantia, there live the
                                                                    blind texts. Separated they live in Bookmarksgrove
                                                                    right at the coast of the Semantics, a large
                                                                    language ocean. A small river named Duden flows by
                                                                    their place and supplies it with the necessary
                                                                    regelialia.</p>
                                                            </div>
                                                            <div class="reply">
                                                                <a class="comment-reply-link" href="#">Reply</a>
                                                            </div>
                                                        </div>
                                                        <ul class="children">
                                                            <li>
                                                                <div class="comment-container">
                                                                    <div class="comment-author-vcard">
                                                                        <img alt=""
                                                                             src="{{URL::asset('images/avatar/avatar.png')}}"/>
                                                                    </div>
                                                                    <div class="comment-author-info">
                                                                        <span class="comment-author-name">admin</span>
                                                                        <a href="#" class="comment-date">July 27,
                                                                            2016</a>
                                                                        <p>Far far away, behind the word mountains, far
                                                                            from the countries Vokalia and Consonantia,
                                                                            there live the blind texts. Separated they
                                                                            live in Bookmarksgrove right at the coast of
                                                                            the Semantics, a large language ocean. A
                                                                            small river named Duden flows by their place
                                                                            and supplies it with the necessary
                                                                            regelialia.</p>
                                                                    </div>
                                                                    <div class="reply">
                                                                        <a class="comment-reply-link" href="#">Reply</a>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li>
                                                        <div class="comment-container">
                                                            <div class="comment-author-vcard">
                                                                <img alt=""
                                                                     src="{{URL::asset('images/avatar/avatar.png')}}"/>
                                                            </div>
                                                            <div class="comment-author-info">
                                                                <span class="comment-author-name">admin</span>
                                                                <a href="#" class="comment-date">July 27, 2016</a>
                                                                <p>Far far away, behind the word mountains, far from the
                                                                    countries Vokalia and Consonantia, there live the
                                                                    blind texts. Separated they live in Bookmarksgrove
                                                                    right at the coast of the Semantics, a large
                                                                    language ocean. A small river named Duden flows by
                                                                    their place and supplies it with the necessary
                                                                    regelialia.</p>
                                                            </div>
                                                            <div class="reply">
                                                                <a class="comment-reply-link" href="#">Reply</a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="single-comment-form mt-0">
                                                <div class="mb-2">
                                                    <h2 class="comment-title">LEAVE A REPLY</h2>
                                                </div>
                                                <form class="comment-form">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <textarea id="comment" name="comment" cols="45" rows="5"
                                                                      placeholder="Message *"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input id="author" name="author" type="text" value=""
                                                                   size="30" placeholder="Name *" class="mb-2">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input id="email" name="email" type="email" value=""
                                                                   size="30" placeholder="Email *" class="mb-2">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input id="url" name="url" type="text" value=""
                                                                   placeholder="Website">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <input name="submit" type="submit" id="submit"
                                                                   class="btn btn-alt btn-border" value="Submit">
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-md-pull-9">
                        <div class="sidebar">
                            <div class="widget widget-product-categories">
                                <h3 class="widget-title">Kategori Produk</h3>
                                <ul class="product-categories">
                                    @foreach($countCat as $category)
                                        <li><a href="{{ route('categories.show',$category->id) }}">{{ $category->name }}</a> <span
                                                    class="count">{{ $category->count }}</span></li>
                                    @endforeach
                                </ul>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script type="text/javascript">

    function priceSum(price){
        qty = +$("#multiQty").val() + 1;
        console.log(qty);
        document.getElementById("priceSum").textContent= "Rp. "+ qty * price;
    }

</script>
<script>
        $(".clickable-row").click(function() {
            window.location = $(this).data("href");
        });
</script>
