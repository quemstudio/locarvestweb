<?php
/**
 * Created by PhpStorm.
 * User: black
 * Date: 1/17/2018
 * Time: 4:21 PM
 */?>

<div class="mini-cart">
    <div class="mini-cart-icon" data-count="{{ Cart::count() }}">
        <i style="color: #fff;" class="ion-bag"></i>
    </div>
</div>
<div class="widget-shopping-cart-content">
    <ul class="cart-list">
        @if(Cart::content()->count() === 0)
            <p class="text-center">
            <h2 class="text-center section-bg-1">Tidak ada item</h2>
            </p>
        @else
            @foreach($carts as $row)
                <li>
                    <a class="remove" onclick="deleteCartTemp('{{ $row->id }}')"
                       style="cursor: pointer">×</a>
                    <a href="#">
                        <img src="{{URL::asset($row->options->img)}}" alt=""/>
                        {{ $row->name }}
                    </a>
                    <span class="quantity">  {{ $row->qty }}
                        × Rp. {{ str_replace(',', '.', number_format( $row->price)) }}</span>
                </li>
            @endforeach
    </ul>
    <p class="total">
        <strong>Subtotal:</strong>
        <span class="amount">Rp. {{  Cart::subtotal() }}</span>
    </p>
    <p class="buttons">
        <a href="{{route('carts.index')}}" class="view-cart">Lihat cart</a>
    <!-- {{--  <a href="{{route('checkout')}}" class="checkout">Checkout</a>  --}} -->
    </p>
    @endif
</div>


