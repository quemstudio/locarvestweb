<?php
/**
 * Created by PhpStorm.
 * User: AldiTahir
 * Date: 13/02/2018
 * Time: 3:55
 */
?>

<div class="container">
    <div class="row">
        @if(!empty($joined))
        <div class="table-responsive">
            <table class="table shop-cart">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">No. Order</th>
                    <th scope="col">Status</th>
                    <th scope="col">Total</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($joined as $confirmPayment)
                    @if(!empty($confirmPayment->confirmation_pic))
                        @if($confirmPayment->statusOrder == 'shipping' || $confirmPayment->statusOrder == 'ship now')
                            <tr class="bg-success">
                        @else
                            <tr class="bg-warning">
                        @endif
                    @else
                        <tr class="bg-danger">
                            @endif
                        <td>{{$confirmPayment->order_number}}</td>
                        <td>{{strtoupper($confirmPayment->statusOrder)}}</td>
                        <td>Rp. {{ str_replace(',', '.', number_format($confirmPayment->total_price)) }}</td>
                            @if(!empty($confirmPayment->confirmation_pic))
                                @if($confirmPayment->statusOrder == 'confirmed')
                                    <td>Pembayaran terkonfirmasi</td>
                                @elseif($confirmPayment->statusOrder == 'shipping' || $confirmPayment->statusOrder == 'ship now')
                                    <td>Pengiriman dalam proses</td>

                                @else
                                <td>Bukti transfer terupload</td>
                                @endif
                            @else
                                <td>Upload bukti transfer anda</td>
                            @endif
                        <td>

                            <button onclick="detailPayment('{{ $confirmPayment->order_id }}')" class="ion-eye btn btn-primary"
                                    data-toggle="tooltip" data-placement="top" title="Detail">
                                @if($confirmPayment->statusOrder == 'new-order')
                                <button onclick="uploadPayment('{{ $confirmPayment->order_id }}','{{ $confirmPayment->order_number }}')" class="ion-arrow-up-a btn btn-success"
                                        data-toggle="tooltip" data-placement="top" title="Upload "></button>
                                    @endif
                            {{--<button onclick="deleteAddress('{{ $address->id }}')" class="ion-trash-a btn btn-info"--}}
                                    {{--data-toggle="tooltip" data-placement="top" title="Delete">--}}
                                {{--<button onclick="updateAddress('{{ $address->id }}')" class="ion-edit btn btn-primary"--}}
                                        {{--data-toggle="tooltip" data-placement="top" title="Edit">--}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @else
            <div class="col-md-8 col-md-offset-2 text-center mb-3">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <h1 class="display-4">Tidak Ada Order</h1>
                        <p class="lead">Saat ini tidak ada catatan order dari akun anda</p>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
