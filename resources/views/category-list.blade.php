<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 05/01/18
 * Time: 19.45
 */
?>
@extends('layouts.app')
@section('content')


    <div id="main">
        <div class="section section-bg-10 pt-11 pb-17">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="page-title text-center">Kategori Produk</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="section border-bottom pt-2 pb-2">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <ul class="breadcrumbs">
                            <li><a href="{{URL::to('home')}}">Home</a></li>
                            <li><a href="#">Kategori Produk</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="section pt-7 pb-7">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 ">
                        @foreach($categories as $category)
                            <div class="col-md-3 col-sm-6">
                                <div class="organik-featured-category mb-3">
                                    <a href="{{ route('categories.show', $category['id']) }}">
                                        <div class="image">
                                            <div class="bg" data-bg-color="white"
                                                 style="background-color: white;"></div>
                                            <div class="img">
                                                {{--<img src="{{URL::asset($category->image)}}" alt="">--}}
                                                <img src="{{Storage::url($category->image)}}" alt="">
                                            </div>
                                        </div>
                                        <div class="title">{{ $category['name'] }}</div>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection