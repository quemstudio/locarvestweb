<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 05/01/18
 * Time: 19.56
 */
?>
@extends('layouts.app')
@section('content')
    <div id="main">
        <div class="section border-bottom pt-2 pb-2">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <ul class="breadcrumbs">
                            <li><a href="{{URL::to('/')}}">Home</a></li>
                            <li><a href="{{route('suppliers.index')}}">Partner Petani</a></li>
                            <li>{{$supplier->name}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="section pt-2 pb-7">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-5">
                        <h2>{{$supplier->name}}</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        @if(!empty($supplier->image))
                            <img src="{{URL::asset('/storage/'.$supplier->image)}}" class="img-thumbnail" style="height: 230px">
                        @else
                            <img src="{{URL::asset('/assets/images/test.jpg')}}" class="img-thumbnail" style="height: 230px"/>
                        @endif

                    </div>
                    <div class="col-md-9">
                    {!! $supplier->description !!}
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
