<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 16/01/18
 * Time: 19.33
 */
?>
@extends('layouts.appadmin')
@section('content')
    <div class="header">
        <h2><strong>Petani</strong> Terbaik</h2>
    </div>
    <div class="row">
        <div class="col-lg-12 portlets">
            <div class="panel">
                <div class="panel-content">
                    <div class="filter-left panel-content pagination2 table-responsive">
                        <table class="table table-hover table-dynamic table-tools" data-table-name="Data Suppliers" data-table-orientation="landscape">
                            <thead>
                            <tr>
                                <th style="width: 15px">No</th>
                                <th>Nama Petani</th>
                                <th>Total Produk Terjual</th>
                                <th>Total Pendapatan</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($best as $data)
                                @php
                                    $user = \App\User::find($data->user_id);
                                @endphp
                                <tr id="tr_{{$data->id}}" style="cursor: pointer"
                                    onclick="detailPetani('{{$data->id}}', '{{$user->phone_number}}', '{{$user->email}}','{{$user->gender}}')">
                                    <td>{{$no++}}</td>
                                    <td>{{$data->name}}</td>
                                    <td>{{$data->total_jual}}</td>
                                    <td>{{$data->total_dapat}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="show-detail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content p-0 b-0">
                <div class="panel panel-color panel-primary">
                    <div class="panel-heading">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                    class="icons-office-52"></i></button>
                        <h4 class="modal-title"><strong>Detail</strong> Petani</h4>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-sm-6 control-label">Nama Lengkap</label>
                                <div class="col-sm-6">
                                    <label class="form-control"
                                           placeholder="Announcement"
                                           id="name-detail"></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6 control-label">Email</label>
                                <div class="col-sm-6">
                                    <label class="form-control"
                                           placeholder="Announcement"
                                           id="email-detail"></label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-6 control-label">Nomor Telepon</label>
                                <div class="col-sm-6">
                                    <label class="form-control"
                                           placeholder="Announcement"
                                           id="phone-detail"></label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-6 control-label">Komoditas yang Ditanam</label>
                                <div class="col-sm-6">
                                    <label class="form-control"
                                           placeholder="Announcement"
                                           id="plant-detail"></label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-6 control-label">Luas Lahan</label>
                                <div class="col-sm-6">
                                    <label class="form-control"
                                           placeholder="Announcement"
                                           id="land-detail"></label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-6 control-label">Description</label>
                                <div class="col-sm-6">
                                    <label class="form-control"
                                           placeholder="Announcement"
                                           id="description-detail"></label>
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="col-sm-6 control-label">Alamat</label>
                                <div class="col-sm-6">
                                    <label class="form-control"
                                           placeholder="Announcement"
                                           id="address-detail"></label>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-12" style="margin-top: 20px">
                                    <a onclick="$('#show-detail').modal('hide');">
                                        <button type="button" class="btn btn-primary" style="float:right">Close</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        function detailPetani(id, phone, email, gender) {

            $.ajax({
                url: 'petani/show_detail/' + id,
                beforeSend: function(){
                    $(document.body).css({'cursor' : 'wait'});
                },
                error: function () {
                    $('#nameApp').html('<p>An error has occurred</p>');
                },
                dataType: 'json',
                success: function (data) {
                    $('#name-detail').text(data.name);
                    $('#phone-detail').text(phone);
                    $('#email-detail').text(email);
                    // $('#gender-detail').text(gender);
                    $('#plant-detail').text(data.name_plant);
                    $('#land-detail').text(data.land_area);
                    // $('#planting-detail').text(data.planting_schedule);
                    // $('#harvest-detail').text(data.harvest_schedule);
                    // $('#quantity-detail').text(data.quantity_plant);
                    $('#description-detail').text(data.description);
                    $('#address-detail').text(data.address);

                    $('#show-detail').modal('show');
                    $(document.body).css({'cursor' : 'default'});
                },
                type: 'GET'
            });

        }
    </script>
@endpush