<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::all();
//      $books = Book::all();
        $response = [
            'pages' => $pages
        ];
        return response()->json($response, 200);
//        return view('home' , compact('categories','books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("page.add");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $path = '';
        if ($request->cover)
            $path = Storage::disk('public')->put('page', $request->cover);

        $page = Page::create([
            'title' => $request->title,
            'body' => $request->body,
            'cover' => $path,
            'admin_id' => Auth::user()->id
        ]);

        if ($page != null)
            return redirect()->route('admin.page')->with('success', 'Tambah Page sukses!');

        return redirect()->route('admin.page')->with('error', 'Tambah Page gagal!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Page::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Page::findOrFail($id);

        return view("admin.detail-page", compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $page = Page::findOrFail($id);
        $page->update($request->all());

        return $page;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = Page::findOrFail($id);
        $page->delete();

        return response()->json(null, 204);
    }

    public function pageAdmin()
    {

        $page = Page::all();
        $no = 1;
        return view('admin.page', compact('page', 'no'));
    }

    public function addPageAdmin(Request $request)
    {

        return view('admin.add-page');
    }

    public function updatePageAdmin(Request $request, $id)
    {
        $page = Page::findOrFail($id);
        if ($request->cover) {
            Storage::disk('public')->delete($page->cover);
            $path = Storage::disk('public')->put('page', $request->cover);
            $page->cover = $path;
        }
        $page->title = $request->title;
        $page->body = $request->body;
        $page->admin_id = Auth::user()->id;
        $page->save();

        if ($page != null)
            return redirect()->route('admin.page')->with('success', 'Update Data Success!');

        return redirect()->route('admin.page')->with('error', 'Update Data Failed!');
    }

    public function ourActivities()
    {
        $page = Page::where('title', 'Our Activities')->get();
        return view('our-activities', compact('page'));
    }

    public function ourTeam()
    {
        $team = Page::where('title', 'Our Team')->get();
        return view('our-team', compact('team'));
    }

    public function faq()
    {
        $faq = Page::where('title', 'faq')->get();
        return view('faq', compact('faq'));
    }

    public function customerService()
    {
        $cs = Page::where('title', 'Customer Service')->get();
        return view('customer-service', compact('cs'));
    }

    public function aboutUs()
    {
        $aboutus = Page::where('title', 'About us')->get();
        return view('about-us', compact('aboutus'));
    }

    public function termsCondition()
    {
        $termCondition = Page::where('title', 'Terms & Condition')->get();
        return view('termscondition', compact('termCondition'));
    }
}
