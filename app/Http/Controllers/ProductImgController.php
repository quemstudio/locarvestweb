<?php

namespace App\Http\Controllers;

use App\ProductImg;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ProductImgController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productImgs= PageImg::all();
        $response = [
            'productImgs' => $productImgs
        ];
        return response()->json($response,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("productImg.add");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return ProductImg::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return ProductImg::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $productImg = ProductImg::findOrFail($id);
        return view("productImg.edit", compact('productImg'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $productImg = ProductImg::findOrFail($id);
        $productImg->update($request->all());

        return $productImg;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $productImg = ProductImg::findOrFail($id);
        Storage::disk('public')->delete($productImg->directory);
        $productImg->delete();

        return response()->json(null,204);
    }

    public function uploadImg(Request $request, $id) {

        $path = '';
        if ($request->file) {
//            $path = Storage::disk('public')->put('supplier', $request->image);

            $file = $request->file;
            $path = $file->hashName('productImg');
            // supplier/bf5db5c75904dac712aea27d45320403.jpeg

            $image = Image::make($file);

            $image->resize(null, 350, function ($constraint) {
                $constraint->aspectRatio();
            });

//            $path = Storage::put($path, (string) $image->encode());
            Storage::disk('public')->put($path, (string) $image->encode());
        }

        ProductImg::create([
            'directory' => $path,
            'status' => '0',
            'product_id' => $id,
        ]);
    }
}
