<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 16/01/18
 * Time: 19.33
 */
?>
@extends('layouts.appadmin') @section('content')

    <div class="row">
        <form class=" form-horizontal" method="post" action="{{route('product.add.submit')}}"
              enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="col-md-12">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Nama Produk</label>
                    <div class="col-sm-9">
                        <input class="form-control" type="text" placeholder="" name="name">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Jadwal Tanam</label>
                    <div class="col-sm-9">
                        <input name="planting_schedule" class="b-datepicker form-control" placeholder="Select a date..."
                               type="text">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Luas Lahan  (m<sup>2</sup>)</label>
                    <div class="col-sm-9">
                        <input name="land_area" class="form-control input-sm" placeholder="" type="number">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Kuantitas Benih (kg)</label>
                    <div class="col-sm-9">
                        <input class="form-control input-sm" placeholder="" type="number" name="seed_quantity">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Jadwal Panen</label>
                    <div class="col-sm-9">
                        <input name="harvest_time" class="b-datepicker form-control" placeholder="Select a date..."
                               type="text">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Kuantitas Panen (kg)</label>
                    <div class="col-sm-9">
                        <input class="form-control input-sm" placeholder="" type="number" name="harvest_quantity">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Harga (IDR)</label>
                    <div class="col-sm-9">
                        <input class="form-control input-sm" placeholder="" type="number" name="price">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Kategori Harga </label>
                    <div class="col-sm-9">
                        <select class="form-control" style="width:100%;" name="category">
                            <option value="individu">Individu</option>
                            <option value="wholesale">Wholesale</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Status Ketersediaan</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch2">
                            <input name="status" class="onoffswitch-checkbox" id="myonoffswitch3" type="checkbox"
                                   value="on">
                            <label class="onoffswitch-label" for="myonoffswitch3">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Deskripsi</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" placeholder="" name="description"></textarea>
                    </div>
                </div>
                {{--<div class="form-group">--}}
                    {{--<label class="col-sm-3 control-label">Amount Left</label>--}}
                    {{--<div class="col-sm-9">--}}
                        {{--<input class="form-control input-sm" placeholder="" type="number" name="amount_left">--}}
                    {{--</div>--}}
                {{--</div>--}}
                <div class="form-group">
                    <label class="col-sm-3 control-label">Penawaran Terbaik</label>
                    <div class="col-sm-9">
                        <div class="onoffswitch2">
                            <input name="best_offer" class="onoffswitch-checkbox" id="myonoffswitch4" type="checkbox"
                                   value="ready">
                            <label class="onoffswitch-label" for="myonoffswitch4">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                        {{--
                        <input class="form-control input-sm" placeholder="" type="number" name="best_offer"> --}}
                    </div>
                </div>
                {{--<div class="form-group">--}}
                    {{--<label class="col-sm-3 control-label">Preorder Date</label>--}}
                    {{--<div class="col-sm-9">--}}
                        {{--<input name="preorder_date" class="b-datepicker form-control" placeholder="Select a date..."--}}
                               {{--type="text">--}}
                    {{--</div>--}}
                {{--</div>--}}
                <div class="form-group">
                    <label class="col-sm-3 control-label">Gambar</label>
                    <div class="col-sm-9">
                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                            {{--<div class="form-control" data-trigger="fileinput">--}}
                                {{--<i class="glyphicon glyphicon-file fileinput-exists"></i>--}}
                                {{--<span class="fileinput-filename"></span>--}}
                            {{--</div>--}}
                            <input type="text" class="form-control" id="upload-file-info" readonly>
                            <span class="input-group-addon btn btn-default btn-file">
                                <span class="fileinput-new">Pilih...</span>
                                <span class="fileinput-exists">Ganti</span>
                                <input type="file" name="image" onchange="$('#upload-file-info').val($(this).val().split('\\').pop());">
                            </span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists"
                               data-dismiss="fileinput">Hapus</a>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Kategori </label>
                    <div class="col-sm-9">
                        <select class="form-control" style="width:100%;" name="category_id">
                            @foreach($cat as $dataCat)
                                <option value="{{$dataCat->id}}">{{$dataCat->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Petani </label>
                    <div class="col-sm-9">
                        <select class="form-control" style="width:100%;" name="supplier_id">
                            @foreach($supplier as $dataSupplier)
                                <option value="{{$dataSupplier->id}}">{{$dataSupplier->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-primary" style="float:right">Submit</button>
                        <a href="{{route('admin.product')}}">
                            <button type="button" class="btn btn-danger" style="float:right">Batal</button>
                        </a>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection