
<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 05/01/18
 * Time: 19.44
 */
?>
@extends('layouts.app')
@section('content')
    <div id="main">
    <div class="section section-error pt-12 pb-12">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="text-center">
                        <img src="{{URL::asset('/assets/images/404.png')}}" class="mb-6" alt="" />
                        <h3 class="error-title uppercase">Oops! Halaman ini tidak tersedia</h3>
                        <span class="error-content uppercase">
										Kembali ke
										<a href="{{URL::to('/')}}">Homepage</a>
									</span>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection