<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $users = factory(App\User::class, 3)->create();
        $supplier = factory('App\Supplier', 3)->create();
        $customer = factory('App\Customer', 3)->create();
        $cart = factory('App\Cart', 3)->create();
//        $shipper = factory('App\Shipper', 3)->create();
        $order= factory('App\Order', 3)->create();
        $billing = factory('App\Billing', 3)->create();
        $category = factory('App\Category', 3)->create();
        $product = factory('App\Product', 3)->create();
        $order_detail = factory('App\OrderDetail', 3)->create();
        $post = factory('App\Post', 3)->create();
        $page = factory('App\Page', 3)->create();
        $pageImg = factory('App\PageImg', 3)->create();
        $category_post = factory('App\CategoryPost', 3)->create();
        $productImg = factory('App\ProductImg', 3)->create();
        $admin = factory('App\Admin', 3)->create();
        $address = factory('App\Address', 3)->create();

    }

}
