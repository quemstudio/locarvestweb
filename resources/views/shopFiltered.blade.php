<?php
/**
 * Created by PhpStorm.
 * User: black
 * Date: 1/24/2018
 * Time: 1:38 PM
 */?>

@foreach($products->chunk(3) as $chunk)
    <div class="row">
        @foreach($chunk as $product)
    <div class="col-md-4 col-sm-6 product-item text-center mb-3">
        <div class="product-thumb">
            <a href="{{route('products.show', $product->id)}}">
                <div class="badges">
                    @if($product->best_offer == 'on')
                        <span class="hot">Best</span>
                    @else
                    <!--<span class="hot">Hot</span>
                                                <span class="onsale">Sale!</span>-->
                    @endif
                </div>
                @if(!empty($product->image))
                    <img src="{{URL::asset('/storage/'.$product->image)}}" class="img-thumbnail" style="height: 180px; width: auto" alt=""/>
                @else
                <img src="{{URL::asset('/assets/images/test.jpg')}}" class="img-thumbnail" style="height: 180px; width: auto" alt=""/>
                @endif
                
                </a>
            <div class="product-action">
                {{--<form>--}}
                {{--<input name="id" type="hidden" data-value="{{ $product['id'] }}">--}}
                {{--<input name="name" type="hidden" value="{{ $product['name'] }}">--}}
                {{--<input name="qty" type="hidden" value="1">--}}
                {{--<input name="price" type="hidden" value="{{ $product['price'] }}">--}}
                <span class="add-to-cart" style="cursor: pointer" id="btn-addCart" onclick="addCartTemp('{{ $product->id }}','{{ $product->name }}',1,'{{ $product->price }}','{{ $product->image }}')">
													<a data-toggle="tooltip" data-placement="top" title="Tambah ke cart"
                                                       id="btn-addCart" ></a>

												</span>
                {{--</form>--}}
            </div>
        </div>
        <div class="product-info">
            <a href="{{route('products.show', $product->id)}}">
                <h2 class="title">{{ $product->name }}</h2>
                <span class="price">
													<ins>Rp. {{ str_replace(',', '.', number_format($product->price)) }}</ins>
												</span>
            </a>
        </div>
    </div>
@endforeach
    </div>
        @endforeach


<div class="col-md-8 col-md-offset-4">
    {{ $products->appends(request()->input())->links() }}
</div>

               

