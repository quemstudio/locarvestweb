<?php

namespace App\Http\Controllers;

use App\Http\Controllers\MailController as mail;
use App\Order;
use App\OrderDetail;
use App\Billing;
use App\Customer;
use App\Address;
use App\User;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;
use Auth;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::all();
//      $books = Book::all();
        $response = [
            'orders' => $orders
        ];
        return response()->json($response,200);
//        return view('home' , compact('categories','books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("order.add");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Order::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Order::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::findOrFail($id);
        return view("order.edit", compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::findOrFail($id);
        $order->update($request->all());

        return $order;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::findOrFail($id);
        $order->delete();

        return response()->json(null,204);
    }

    public function orderAdmin() {
        $orders = Order::with('addresses')
            ->with('orderDetails.products')
            ->get();
        $no = 1;
        $area = json_decode($orders, true);
//        $area2 = json_decode($orders, true);
        return view('admin.orders', compact('area','no'));

//        return $orders[23]['order_details'];
//        return $area[23]['order_details'][0]['products']['name'];
//        return $area;
    }

    public function confirmOrder(Request $request)
    {
        $bank = $request->bank;
        $carts = Cart::content();
        $total =  (int)str_replace(',', '', Cart::subtotal()) + Session::get('shipping');
        $random = rand((int)10000000000, (int)99999999999);

//       echo $bank;
//        Session::put('bank',$bank);
        return view('confirm-order',compact('carts', 'total','random','bank'));
    }

    public function finishOrder()
    {
        //update phonenumber-------------------------------------------


        $userUpdate = User::findOrFail(Auth::user()->id);
        $userUpdate->phone_number = Session::get('phonenumber');
        $userUpdate->save();

        //insert order-------------------------------------------
        $address = Address::find(Session::get('addressId'));
        $customer = Customer::where('user_id',Auth::user()->id)->get();
        $orderNumber = rand((int)1000000000, (int)2000000000);
        $total =  (int)str_replace(',', '', Cart::subtotal()) + Session::get('shipping');

        $order = new Order;
        $order->customer_id = $customer[0]->id;
        $order->order_number = $orderNumber;
        $order->distance = $address->distance;
        $order->date = Session::get('shipdate');
        $order->status= 'new-order';
        $order->total_price = $total;
        $order->address_id = Session::get('address');
        $order->save();

        //insert order detail-------------------------------------------
        $lastOrder = Order::orderBy('id', 'desc')->take(1)->get();
        $cartContents = Cart::content();
        foreach ($cartContents as $content){
            $orderDetail = new OrderDetail;
            $orderDetail->price = $content->price;
            $orderDetail->total_price = $content->price * $content->qty;
            $orderDetail->qty = $content->qty;
            $orderDetail->order_number= $lastOrder[0]->order_number;
            $orderDetail->order_id= $lastOrder[0]->id;
            $orderDetail->product_id= $content->id;
            $orderDetail->save();

        }

        //insert billings-------------------------------------------
        $billing = new Billing;
        $billing->order_id = $lastOrder[0]->id;
        $billing->status = 'unpaid';
        $billing->payment_method = 'transfer';
        $billing->save();

        //mail service-------------------------------------------
        $userId = Auth::user()->id;
        $mail = new mail();
        $mail->invoiceMail($lastOrder[0]->id,$userId);

        //empty cart-------------------------------------------
        Cart::destroy();


//        return redirect('/');


    }
    public function orderSucceed(){

        return view('orderSucceed');
    }


    public function updateProgress($id) {
        $order = Order::findOrFail($id);
        if ($order->status === 'confirmed')
            $order->status = 'shipping';
        else
            $order->status = 'completed';
        $order->save();

        if ($order != null)
            return response()->json(null,204);
        return response()->json('error',404);
    }

    public function catatRetur(Request $request) {
        $order = Order::findOrFail($request->id);
        $order->status = 'completed';
        $order->retur_note = $request->retur_note;
        $order->retur_status = 1;
        $order->save();

        if ($order != null)
            return redirect()->route('admin.orders')->with('success', 'Catatan retur disimpan!');

        return redirect()->route('admin.orders')->with('error', 'Gagal!');
    }
}
