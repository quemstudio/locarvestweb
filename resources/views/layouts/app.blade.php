<!doctype html>
<html lang="en-US">
<head>

    <!--[if lt IE 9]>
    
    
    DEVELOPED BY QUEM CREATIVE STUDIO - www.quem-studio.com
    
    
    <![endif]-->
    
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    {{--Google Search Console--}}
    <meta name="google-site-verification" content="6rDZdKgDDXbncpKpwZfFvmv2XXYtZOvopzTgAy8_QkU" />

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-115006310-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-115006310-1');
        @if (Auth::check())
        gtag('set', {'user_id': '{{Auth::user()->id}}'}); // Set the user ID using signed-in user_id.
        @endif
    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <link rel="shortcut icon" href="{{URL::asset('/assets/images/favicon.ico')}}"/>
    <title>Locarvest &#8211; Home</title>
    <link rel="stylesheet" href="{{URL::asset('/assets/css/bootstrap.min.css')}}" type="text/css" media="all"/>
    <link rel="stylesheet" href="{{URL::asset('/assets/css/font-awesome.min.css')}}" type="text/css" media="all"/>
    <link rel="stylesheet" href="{{URL::asset('/assets/css/ionicons.min.css')}}" type="text/css" media="all"/>
    <link rel="stylesheet" href="{{URL::asset('/assets/css/owl.carousel.css')}}" type="text/css" media="all"/>
    <link rel="stylesheet" href="{{URL::asset('/assets/css/owl.theme.css')}}" type="text/css" media="all"/>
    <link rel="stylesheet" href="{{URL::asset('/assets/css/settings.css')}}" type="text/css" media="all"/>
    <link rel="stylesheet" href="{{URL::asset('/assets/css/style.css')}}" type="text/css" media="all"/>
    <link rel="stylesheet" href="{{URL::asset('/assets/css/custom.css')}}" type="text/css" media="all"/>
    <link rel='stylesheet' href="{{URL::asset('/assets/css/prettyPhoto.css')}}" type='text/css' media='all'/>
    <link rel='stylesheet' href="{{URL::asset('/assets/css/slick.css')}}" type='text/css' media='all'/>
    <link rel='stylesheet' href="{{URL::asset('/assets/css/loaderbar.css')}}" type='text/css' media='all'/>
    <link rel='stylesheet' href="{{URL::asset('/assets/css/jquery.toast.min.css')}}" type='text/css' media='all'/>
    <link rel='stylesheet' href="{{URL::asset('/assets/css/nouislider.css')}}" type='text/css' media='all'/>
   <!--  <link rel="stylesheet" href="{{URL::asset('quemstudio.css')}}" type="text/css" media="all"/> -->
    <link href="https://fonts.googleapis.com/css?family=Great+Vibes%7CLato:100,100i,300,300i,400,400i,700,700i,900,900i"
          rel="stylesheet"/>
    {{--<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">--}}

    <link rel='stylesheet' href="{{URL::asset('/assets/css/bootstrap-datepicker.css')}}" type='text/css' media='all'/>
    <link rel='stylesheet' href="{{URL::asset('/assets/css/bootstrap-toggle.css')}}" type='text/css' media='all'/>
    <link rel='stylesheet' href="{{URL::asset('/assets/css/font-awesome-animation.min.css')}}" type='text/css' media='all'/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


    <!-- DEVELOPED BY QUEM CREATIVE STUDIO - www.quem-studio.com -->


    {{--map search style--}}
    {{--end map search style--}}
    <style>
        /*modal add address geolocation*/
        .modal-backdrop.in {
            z-index: 1000;
        }

        .modal-open {
            overflow: scroll;
        }

        #ui-id-1 {
            z-index: 10000;
            font-size: 15px;
            max-width: 50%;
        }

        /*background color*/
        .jumbotron{
            background-color: #7fca90;
        }

        /*always show mobile scrollbar*/
        ::-webkit-scrollbar {
            -webkit-appearance: none;
        }

        ::-webkit-scrollbar:vertical {
            width: 12px;
        }

        ::-webkit-scrollbar:horizontal {
            height: 12px;
        }

        ::-webkit-scrollbar-thumb {
            background-color: rgba(0, 0, 0, .5);
            border-radius: 10px;
            border: 2px solid #ffffff;
        }

        ::-webkit-scrollbar-track {
            border-radius: 10px;
            background-color: #ffffff;
        }
    </style>
<style>
    {{--maksa--}}
    header.header-1 nav.menu .main-menu > li > a, header.header-2 nav.menu .main-menu > li > a, header.header-3 nav.menu .main-menu > li > a, header.header-4 nav.menu .main-menu > li > a {

        padding: 23px 5px !important;

    }
    header.header-1 nav.menu .main-menu > li > a, header.header-2 nav.menu .main-menu > li > a, header.header-3 nav.menu .main-menu > li > a, header.header-4 nav.menu .main-menu > li > a {
        text-transform: uppercase;
        padding: 23px 5px !important;
    }
    header.header-2 #logo, header.header-3 #logo, header.header-4 #logo {

        padding-top: 8px !important;

    }

    .menu .main-menu .sub-menu {
        z-index: 2;

    }
    header.header-mobile {

        padding: 0 !important;

    }
    .w3-border {
        border: 1px solid #ccc !important;
        border-top-color: rgb(204, 204, 204);
        border-right-color: rgb(204, 204, 204);
        border-bottom-color: rgb(204, 204, 204);
        border-left-color: rgb(204, 204, 204);
    }


    .w3-white, .w3-hover-white:hover {
        color: #000 !important;
    }
    .w3-btn, .w3-btn:link, .w3-btn:visited {
        color: #FFFFFF;
    }

    .w3-round-xlarge {
        border-radius: 16px;
    }
    w3-btn, .w3-button {
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .w3-btn, .w3-button {
        border: none;
        display: inline-block;
        padding: 8px 16px;
        vertical-align: middle;
        overflow: hidden;
        text-decoration: none;
        color: inherit;
        background-color: inherit;
        text-align: center;
        cursor: pointer;
        white-space: nowrap;
    }

    .section-pretitle {

        font-size: 46px !important;
        font-family: roboto !important;

    }
    .page-title {

        font-family: roboto !important;
        text-transform: uppercase !important;

    }
    .menu .main-menu > li:hover > .sub-menu {
        z-index: 100 !important;
    }
    </style>

</head>
<body>
<!-- Modal -->
@if(Auth::check())
    @if (Request::is('checkout') || Request::is('user-profile'))
        <div class="modal fade bd-example-modal-lg" id="exampleModalAddress" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambah Alamat Baru</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="commerce">
                            <form class="commerce-login-form" action="{{route('addresses.store')}}" method="post">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-wrap">
                                            Kota
                                            <input type="text" name="city" placeholder="Kota" value="Bandung" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-wrap">
                                            Kecamatan
                                            <input type="text" name="kecamatan" placeholder="Kecamatan" value="" size="40"  required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-wrap">
                                            Kelurahan
                                            <input type="text" name="kelurahan" placeholder="Kelurahan" value="" size="40" required>
                                            <input type="hidden" name="rtrw" value="00/00">
                                            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-wrap">
                                            Kode Pos
                                            <input type="text" name="zip_code" id="account_number"  placeholder="Kode Pos" value="" size="40" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-wrap">
                                            Alamat Lengkap
                                            <textarea name="address" placeholder="Alamat Lengkap" value="" required></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="form-wrap">
                                            <!-- copy dari sini -->
                                            <div class="form-group input-group">
                                                <input type="text" id="search_location" class="form-control" placeholder="Nama Tempat/Jalan/Daerah">
                                                <div class="input-group-btn">
                                                    <button class="btn btn-default get_map" type="submit">
                                                        Cari Lokasi
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="col-md-8 col-md-offset-4">
                                                <p class="account-copyright">
                                                    <span style="">Tarik dan pindahkan </span>
                                                    <img style="vertical-align:middle; height: 25px;background-color: transparent;
border: none;" src="https://www.urltarget.com/images/location-pointer-pin-google-map-red.png">
                                                    <span style="">ke lokasi anda</span>
                                                </p>
                                            </div>
                                            <div id="myMap" class="col-md-12"></div>
                                            <input id="address" class="form-control input-sm" type="text"/><br/>
                                            <input type="hidden" id="latitude" name="lat" placeholder="Latitude" value="-6.902342" required/>
                                            <input type="hidden" id="longitude" name="long" placeholder="Longitude" value="107.62180699999999"
                                                   required/>
                                            <!-- copy sampe sini -->
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-wrap">
                                            <input type="submit" class="col-md-12" value="Tambah">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--End Modal -->
    @endif
@endif
<div id="menu-slideout" class="slideout-menu hidden-md-up">
    <div class="mobile-menu">
        <ul id="mobile-menu" class="menu">
            <li class="dropdown">
                <a href="#">Belanja</a>
                <i class="sub-menu-toggle fa fa-angle-down"></i>
                <ul class="sub-menu">
                    <li><a href="{{route('shop')}}">Semua Produk</a></li>
                    <li><a href="{{URL::to('category-list')}}">Kategori Produk</a></li>
                </ul>
            </li>
            <li>
                <a href="{{route('posts.index')}}">Berita</a>
            </li>
            <li class="dropdown">
                <a href="#">Tentang</a>
                <i class="sub-menu-toggle fa fa-angle-down"></i>
                <ul class="sub-menu">
                    <li><a href="{{route('about-us')}}">Tentang Locarvest</a></li>
                    <li><a href="{{route('our-activities')}}">Aktivitas Kami</a></li>
                    <li><a href="{{route('our-team')}}">Tim Kami</a></li>
                </ul>
            </li>
            <li>
                <a href="{{URL::to('partner-petani')}}">Partner</a>
            </li>
            <li class="dropdown">
                <a href="#">Kontak</a>
                <i class="sub-menu-toggle fa fa-angle-down"></i>
                <ul class="sub-menu">
                    <li><a href="{{route('customer-service')}}">Customer Service</a></li>
                    <li><a href="{{route('faq')}}">F.A.Q</a></li>
                    <li><a href="{{route('terms-and-condition')}}">Terms & Condition</a></li>
                </ul>
            </li>
            @if (Auth::check())
                @if(Session::get('statusCat') == 'individu')
                    <li data-toggle="tooltip" data-placement="bottom"
                        title="Belanja sebagai grosir">
                        <a href="{{ route('userWholesale') }}" >
                            <span class="fa fa-shopping-bag faa-tada animated faa-slow"></span>  Belanja sebagai GROSIR  <span class="fa fa-shopping-bag faa-tada animated faa-slow"></span>
                        </a>
                    </li>
                @else
                    <li data-toggle="tooltip" data-placement="bottom"
                        title="Belanja sebagai individu">
                        <div>
                        <a href="{{ route('switchwholesale') }}">
                            <span class="fa fa-refresh"></span>  Belanja sebagai INDIVIDU
                        </a>
                        </div>
                    </li>
                @endif
                {{--<li>Hello! {{Auth::user()->first_name ." ". Auth::user()->last_name}}</li>--}}
                <li>
                    <a href="{{ route('logoutGet') }}" style="color: #fff;padding: 10px 10px !important;background: #a5212c;border-color: #000000 !important;margin-left: 20px;" class="w3-btn w3-white w3-border w3-border-green w3-round-xlarge">
                        <font color="white">LOGOUT</font>
                    </a>
                </li>
            @else
                <li>
                    <a >Belum Login ?</a>
                </li>
                <li>
                    <a href="{{URL::to('login')}}" style="color: #fff !important;padding: 10px 10px !important;background: #5fbd74;border-color: #5fbd74 !important;margin-left: 5%;margin-right: 5%" class="w3-btn w3-white w3-border w3-border-green w3-round-xlarge">
                        LOGIN
                    </a>
                </li>
                <li>
                    <a href="{{URL::to('register')}}" style="color: #5fbd74 !important;border-color: #5fbd74 !important;margin-left: 5%;margin-right: 5%;padding: 10px 10px !important;" class="w3-btn w3-white w3-border w3-border-green w3-round-xlarge">
                        REGISTER
                    </a>
                </li>
            @endif
            @if (Auth::check())

                <li>
                    <a href="{{ route('userProfile') }}">
                        <div class="top-search-btn-wrap">
                            <i class="ion-person"></i> PROFIL
                        </div>
                    </a>
                </li>
            @endif
        </ul>
    </div>
</div>
<div class="site">
    {{--<div class="topbar">--}}
    {{--<div class="container">--}}
    {{--<div class="row">--}}
    {{--<div class="col-md-6">--}}
    {{--<div class="topbar-text">--}}

    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="col-md-6">--}}
    {{--<div class="topbar-menu">--}}
    {{--<ul class="topbar-menu">--}}
    {{--@if (Auth::check())--}}
    {{--@if(Session::get('statusCat') == 'individu')--}}
    {{--<a href="{{ route('userWholesale') }}">--}}
    {{--<li class="" style="cursor: pointer;"><span class="fa fa-shopping-bag faa-tada animated faa-slow"></span>  GROSIR  <span class="fa fa-shopping-bag faa-tada animated faa-slow"></span></li>--}}
    {{--</a>--}}
    {{--@else--}}
    {{--<a href="{{ route('switchwholesale') }}">--}}
    {{--<li class="" style="cursor: pointer;"><span class="fa fa-refresh"></span>  INDIVIDU  </li>--}}
    {{--</a>--}}
    {{--@endif--}}
    {{--<li>Hello! {{Auth::user()->first_name ." ". Auth::user()->last_name}}</li>--}}
    {{--<a href="{{ route('logoutGet') }}">--}}
    {{--<li class="ion-log-in"> LOGOUT</li>--}}
    {{--</a>--}}
    {{--@else--}}
    {{--<a href="{{URL::to('login')}}">--}}
    {{--<li class="ion-log-in"> LOGIN</li>--}}
    {{--</a>--}}
    {{--<a href="{{URL::to('register')}}">--}}
    {{--<li class="ion-person-add"> REGISTER</li>--}}
    {{--</a>--}}
    {{--@endif--}}

    {{--</ul>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    <header id="header" class="header header-desktop header-2" style="background: #4B382A;">

        <div class="container" style="padding-bottom: 5px">
            <div class="row">
                <div class="col-md-2">
                    <a href="{{URL::to('/')}}" id="logo">
                        <img class="logo-image" src="{{URL::asset('/assets/images/LOCARVEST.png')}}" alt="Organik Logo"
                             style="width:85px"/>
                    </a>
                </div>
                <div class="col-md-10">
                    <div class="header-right">
                        <nav class="menu">
                            <ul class="main-menu">
                                <li class="dropdown">
                                    <a href="#" style="color: #fff;">Belanja</a>
                                    <ul class="sub-menu" style="background: #4B382A">
                                        <li><a href="{{URL::to('products')}}" style="color: #fff;">Semua Produk</a></li>
                                        <li><a href="{{URL::to('categories')}}" style="color: #fff;">Kategori Produk</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="{{route('posts.index')}}" style="color: #fff;">Berita</a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" style="color: #fff;">Tentang</a>
                                    <ul class="sub-menu " style="background: #4B382A">
                                        <li><a href="{{route('about-us')}}" style="color: #fff;">Tentang Locarvest</a></li>
                                        <li><a href="{{route('our-activities')}}" style="color: #fff;">Aktivitas Kami</a></li>
                                        <li><a href="{{route('our-team')}}" style="color: #fff;">Tim Kami</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="{{URL::to('partner-petani')}}" style="color: #fff;">Partner</a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" style="color: #fff;">Kontak</a>
                                    <ul class="sub-menu" style="background: #4B382A">
                                        <li><a href="{{route('customer-service')}}" style="color: #fff;">Customer Service</a></li>
                                        <li><a href="{{route('faq')}}" style="color: #fff;">F.A.Q</a></li>
                                        <li><a href="{{route('terms-and-condition')}}" style="color: #fff;">Terms & Condition</a></li>
                                    </ul>
                                </li>
                                @if (Auth::check())
                                    @if(Session::get('statusCat') == 'individu')
                                        <li>
                                            <a href="{{ route('userWholesale') }}" style="color: #fff;" data-toggle="tooltip" data-placement="bottom"
                                               title="Belanja sebagai grosir">
                                                <span class="fa fa-shopping-bag faa-tada animated faa-slow"></span>  GROSIR  <span class="fa fa-shopping-bag faa-tada animated faa-slow"></span>
                                            </a>
                                        </li>
                                    @else
                                        <li>
                                            <a href="{{ route('switchwholesale') }}" style="color: #fff;"  data-toggle="tooltip" data-placement="bottom"
                                               title="Belanja sebagai individu">
                                                <span class="fa fa-refresh"></span>  INDIVIDU
                                            </a>
                                        </li>
                                    @endif
                                    {{--<li>Hello! {{Auth::user()->first_name ." ". Auth::user()->last_name}}</li>--}}
                                    <li>
                                        <a href="{{ route('logoutGet') }}" style="color: #fff;padding: 10px 10px !important;background: #a5212c;border-color: #000000 !important;margin-left: 20px;" class="w3-btn w3-white w3-border w3-border-green w3-round-xlarge">
                                            <font color="white">LOGOUT</font>
                                        </a>
                                    </li>
                                @else
                                    <li>
                                        <a href="{{URL::to('login')}}" style="color: #fff;padding: 10px 10px !important;background: #5fbd74;border-color: #5fbd74 !important;margin-left: 20px;" class="w3-btn w3-white w3-border w3-border-green w3-round-xlarge">
                                            LOGIN
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{URL::to('register')}}" style="color: #5fbd74 !important;border-color: #5fbd74 !important;margin-left: 20px;padding: 10px 10px !important;" class="w3-btn w3-white w3-border w3-border-green w3-round-xlarge">
                                            REGISTER
                                        </a>
                                    </li>
                                @endif

                            </ul>
                        </nav>
                        <div class="btn-wrap">
                            @if (!Request::is('checkout') && !Request::is('carts') && !Request::is('user-payment') && !Request::is('user-wholesale') && !Request::is('confirm-order'))
                                <div class="mini-cart-wrap" id="addCart" data-toggle="tooltip" data-placement="bottom"
                                     title="Cart">
                                    <div class="mini-cart">
                                        <div class="mini-cart-icon" data-count="{{ Cart::count() }}">
                                            <i style="color: #fff;" class="ion-bag"></i>
                                        </div>
                                    </div>

                                    <div class="widget-shopping-cart-content">
                                        <ul class="cart-list">
                                            @if(Cart::content()->count() === 0)
                                                <p class="text-center">
                                                <h2 class="text-center section-bg-1">Tidak ada item</h2>
                                                </p>
                                            @else
                                                @foreach($carts as $row)
                                                    <li>
                                                        <a class="remove" onclick="deleteCartTemp('{{ $row->id }}')"
                                                           style="cursor: pointer">�</a>
                                                        <a href="#">
                                                            <img src="{{URL::asset($row->options->img)}}" alt=""/>
                                                            {{ $row->name }}
                                                        </a>
                                                        <span class="quantity">  {{ $row->qty }}
                                                            � Rp. {{ str_replace(',', '.', number_format( $row->price)) }}</span>
                                                    </li>
                                                @endforeach
                                        </ul>
                                        <p class="total">
                                            <strong>Subtotal:</strong>
                                            <span class="amount">Rp. {{  Cart::subtotal() }}</span>
                                        </p>
                                        <p class="buttons">
                                            <a href="{{route('carts.index')}}" class="view-cart">Lihat Cart</a>
                                        <!-- {{--  <a href="{{route('checkout')}}" class="checkout">Checkout</a>  --}} -->
                                        </p>
                                        @endif
                                    </div>

                                </div>
                            @endif
                            @if (Auth::check())
                                <a href="{{ route('userProfile') }}">
                                    <div class="top-search-btn-wrap">
                                        <i class="ion-person" style="color: #fff;"></i> <font style="color: #fff;">PROFIL</font>
                                    </div>
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="load-bar" style="background-color: #4B382A">
        </div>
    </header>
    <header class="header header-mobile" style="background: #4B382A">
        <div class="container">
            <div class="row">
                <div class="col-xs-2">
                    <div class="header-left">
                        <div id="open-left"><i class="ion-navicon" style="color: white;"></i></div>
                    </div>
                </div>
                <div class="col-xs-8">
                    <div class="header-center">
                        <a href="#" id="logo-2">
                            <img class="logo-image" src="{{URL::asset('/assets/images/LOCARVEST.png')}}"
                                 alt="Organik Logo" style="width: 35%;padding-bottom: 8px;padding-top: 8px;"/>
                        </a>
                    </div>
                </div>
                <div class="col-xs-2">
                    <div class="header-right">
                        <div class="mini-cart-wrap">
                            <a href="{{route('carts.index')}}">
                                <div class="mini-cart">
                                    <div class="mini-cart-icon">
                                        <i class="ion-bag" style="color: white;"></i>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </header>
    @yield('content')
    <footer class="footer" style="background: #4B382A">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="{{URL::asset('/assets/images/LOCARVEST.png')}}" class="footer-logo" alt=""
                         style="width:30%"/>
                    <p>
                        Halo locarbuddy, selamat berbelanja di locarvest! Locarvest menyediakan berbagai macam produk pertanian segar dan bersih yang dipanen petani H-1  sebelum dikirim ke rumah anda.
                    </p>
                </div>
                <div class="col-md-3">
                    <div class="widget">
                        <h3 class="widget-title">Informasi</h3>
                        <ul>
                            <li><a href="{{route('shop')}}">Belanja</a></li>
                            <li><a href="{{route('faq')}}">F.A.Q</a></li>
                            <li><a href="{{route('terms-and-condition')}}">Terms & Condition</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="widget">
                        <h3 class="widget-title">Link Berguna</h3>
                        <ul>
                            <li><a href="{{route('about-us')}}">Tentang</a></li>
                            <li><a href="{{URL::to('partner-petani')}}">Partner</a></li>
                            <li><a href="{{route('customer-service')}}">Customer Service</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </footer>
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <p class="account-copyright">
                        <span style="">Copyright � 2018    </span>
                        {{--<a href="http://quem-studio.com">--}}
                        {{--<img style="vertical-align:middle; height: 20px;margin-bottom: 3px" src="{{URL::asset('/assets/images/LogoPanjang-White.png')}}">--}}
                        {{--</a>--}}
                        {{--<span style="">    and <a style="color: #5fbd74" href="http://itik.tech">ITIK labs</a></span>.<span> All rights reserved.--}}
                        {{--</span>--}}
                        <a style="color: #5fbd74" href="https://locarvest.com">Locarvest.com</a></span>.<span>  All rights reserved.</span>
                    </p>
                </div>
            </div>
        </div>
        <div class="backtotop" id="backtotop"></div>
    </div>
</div>

<script>

</script>
@if (Request::is('checkout') || Request::is('user-profile'))
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB5EksjNLZM377mz5PcHWXG1rTHusDPF4o&libraries=places"></script>
@endif
<script type="text/javascript" src="{{URL::asset('/assets/js/jquery-3.3.1.min.js')}}"></script>
<script src="{{URL::asset('/assets_admin/global/plugins/jquery/jquery-migrate-3.0.0.min.js')}}"></script>
<script src="{{URL::asset('/assets/js/jquery-ui.min.js')}}"></script>
{{--<script src="https://apis.google.com/js/platform.js" async defer></script>--}}
{{--<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>--}}
{{--<script type="text/javascript" src="{{URL::asset('/assets/js/jquery-migrate.min.js')}}"></script>--}}
@if (Request::is('checkout') || Request::is('user-profile'))
    <script type="text/javascript" src="{{URL::asset('/assets/js/quem-maps-setting.js')}}"></script>
@endif
<script type="text/javascript" src="{{URL::asset('/assets/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/assets/js/modernizr-2.7.1.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/assets/js/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/assets/js/jquery.countdown.min.js')}}"></script>
<script type='text/javascript' src='{{URL::asset('/assets/js/jquery.prettyPhoto.js')}}'></script>
<script type='text/javascript' src='{{URL::asset('/assets/js/jquery.prettyPhoto.init.min.js')}}'></script>
<script type="text/javascript" src="{{URL::asset('/assets/js/imagesloaded.pkgd.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/assets/js/isotope.pkgd.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/assets/js/jquery.isotope.init.js')}}"></script>
<script type='text/javascript' src='{{URL::asset('/assets/js/slick.min.js')}}'></script>
<script type="text/javascript" src="{{URL::asset('/assets/js/script.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/assets/js/widget.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/assets/js/mouse.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/assets/js/slider.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/assets/js/price-slider.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/assets/js/jquery.toast.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/assets/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/assets/js/bootstrap-toggle.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/assets/js/nouislider.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/assets/js/wNumb.js')}}"></script>
<!--Start Maps -->
{{--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>--}}

<!-- END MAPS -->

<script type="text/javascript" src="{{URL::asset('/assets/js/jquery.themepunch.tools.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/assets/js/jquery.themepunch.revolution.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/assets/js/extensions/revolution.extension.video.min.js')}}"></script>
<script type="text/javascript"
        src="{{URL::asset('/assets/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
<script type="text/javascript"
        src="{{URL::asset('/assets/js/extensions/revolution.extension.actions.min.js')}}"></script>
<script type="text/javascript"
        src="{{URL::asset('/assets/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
<script type="text/javascript"
        src="{{URL::asset('/assets/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
<script type="text/javascript"
        src="{{URL::asset('/assets/js/extensions/revolution.extension.navigation.min.js')}}"></script>
<script type="text/javascript"
        src="{{URL::asset('/assets/js/extensions/revolution.extension.migration.min.js')}}"></script>
<script type="text/javascript"
        src="{{URL::asset('/assets/js/extensions/revolution.extension.parallax.min.js')}}"></script>

        
@stack('scripts')
{{--<script type="text/javascript" src="{{URL::asset('/assets/js/quem-studio.min.js')}}"></script>--}}
<script type="text/javascript">
$('#exampleModalAddress').on('shown.bs.modal', function () {
    google.maps.event.trigger(map, 'resize');
    map.setCenter(new google.maps.LatLng(-6.902273,107.621686));
});
$('#exampleModal').on('shown.bs.modal', function () {
    google.maps.event.trigger(map, 'resize');
    map.setCenter(new google.maps.LatLng(-6.902273,107.621686));
});
</script>

<script type="text/javascript">
    //Cart--------------------------------------------------------------------------------
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function addCartTemp(id, name, qty, price, img) {
        $('.load-bar').html('<div class="bar"></div>');
        $.ajax({
            url: '{{URL::to("/cart/addTemp")}}',
            type: 'POST',
            data: {id: id, name: name, qty: qty, price: price, img: img},
            success: function (data) {
                if ($.isEmptyObject(data.error)) {
                    $('#addCart').html(data);
                    $.toast({
                        heading: 'Sukses',
                        text: 'Produk ditambahkan ke cart',
                        showHideTransition: 'fade',
                        icon: 'success',
                        hideAfter: 2000,
                        position: 'bottom-center',
                        bgColor: '#5fbd74',
                        loader: false,
                    });

                } else {
                    printErrorMsg(data.error);
                }
            }
        });
    };

    function addMultiCartTemp(id, name, price, img) {
        var qty = $("#multiQty").val();
//        console.log(id);
//        console.log(name);
//        console.log(price);
//        console.log(img);
        $('.load-bar').html('<div class="bar"></div>');
        $.ajax({
            url: '{{URL::to("/cart/addTemp")}}',
            type: 'POST',
            data: {id: id, name: name, qty: qty, price: price, img: img},
            success: function (data) {
                if ($.isEmptyObject(data.error)) {
                    $('#addCart').html(data);
                     $.toast({
                        heading: 'Sukses',
                        text: 'Produk ditambahkan ke cart',
                        showHideTransition: 'fade',
                        icon: 'success',
                        hideAfter: 2000,
                        position: 'bottom-center',
                        bgColor: '#5fbd74',
                        loader: false,
                    });
                } else {
                    printErrorMsg(data.error);
                }
            }
        });
    };

    function deleteCartTemp(id) {
        console.log(id);
        $('.load-bar').html('<div class="bar"></div>');
        $.ajax({
            url: '{{URL::to("/cart/delTemp")}}',
            type: 'POST',
            data: {id: id},
            success: function (data) {
                if ($.isEmptyObject(data.error)) {
                    $('#addCart').html(data);
                    $.toast({
                        heading: 'Sukses',
                        text: 'Produk terhapus dari cart',
                        showHideTransition: 'fade',
                        icon: 'success',
                        hideAfter: 2000,
                        position: 'bottom-center',
                        bgColor: '#5fbd74',
                        loader: false,
                    });
                } else {
                    printErrorMsg(data.error);
                }
            }
        });
    };

    function deleteCartDetailTemp(id) {
        console.log(id);
        $('.load-bar').html('<div class="bar"></div>');
        $.ajax({
            url: '{{URL::to("/cart/delDetailTemp")}}',
            type: 'POST',
            data: {id: id},
            success: function (data) {
                if ($.isEmptyObject(data.error)) {
                    $('#deleteCart').html(data);
                    $.toast({
                        heading: 'Sukses',
                        text: 'Produk terhapus dari cart',
                        showHideTransition: 'fade',
                        icon: 'success',
                        hideAfter: 2000,
                        position: 'bottom-center',
                        bgColor: '#5fbd74',
                        loader: false,
                    });
                } else {
                    printErrorMsg(data.error);
                }
            }
        });
    };

    var timeout = null;

    function addQty(id, idCart, price) {
        document.getElementById("amount-" + id).textContent = "Rp. " + $("#" + idCart).val() * price;
        if (timeout) {
            $('#loading').html('<img src="{{ URL::asset('/assets/images/ajax-loader.gif') }}">');
            clearTimeout(timeout);
        }
        timeout = setTimeout(function () {
            $('#loading').html('<img src="{{ URL::asset('/assets/images/ajax-loader.gif') }}">');
            var qty = $("#" + idCart).val();
            $.ajax({
                url: '{{URL::to("/cart/addQtyTemp")}}',
                type: 'POST',
                data: {qty: qty, id: id},
                success: function (data) {
                    if ($.isEmptyObject(data.error)) {
                        $('#updateCart').html(data);
                    } else {
                        printErrorMsg(data.error);
                    }
                }
            });
        }, 300);
    };

    function deleteAddress(id) {
        {{--$('.load-bar').html('<div class="bar"></div>');--}}
        {{--if (timeout) {--}}
            {{--$('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');--}}
            {{--clearTimeout(timeout);--}}
        {{--}--}}
        {{--timeout = setTimeout(function () {--}}
        {{--$('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');--}}
        $.ajax({
            url: '{{URL::to("/address/delAddress")}}',
            type: 'POST',
            data: {id: id},
            success: function (data) {
                if ($.isEmptyObject(data.error)) {
                    $('#user').html(data);
                    $.toast({
                        heading: 'Sukses',
                        text: 'Alamat terhapus',
                        showHideTransition: 'fade',
                        icon: 'success',
                        hideAfter: 2000,
                        position: 'bottom-center',
                        bgColor: '#5fbd74',
                        loader: false,
                    });
                } else {
                    printErrorMsg(data.error);
                }
            },
            error: function (data) {
                $.toast({
                    heading: 'Error',
                    text: 'Tidak dapat menghapus alamat',
                    showHideTransition: 'fade',
                    icon: 'error',
                    hideAfter: 3000,
                    position: 'bottom-center',
                    loader: false,
                });
            }
        });
    // }, 300);
    };

    function updateAddressSubmit() {
        var city = $('#city').val();
        var id = $('#id').val();
        var kecamatan = $('#kecamatan').val();
        var kelurahan = $('#kelurahan').val();
        var address = $('#addressUpdate').val();
        var zip_code = $('#zip_code').val();

        if (timeout) {
            $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            clearTimeout(timeout);
        }
        timeout = setTimeout(function () {
            $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            $.ajax({
                url: '{{URL::to("/address/updateAddressSubmit")}}',
                type: 'POST',
                data: {zip_code: zip_code, city: city, id: id, kecamatan: kecamatan, kelurahan: kelurahan, address: address},
                success: function (data) {
                    if ($.isEmptyObject(data.error)) {
                        $('#user').html(data);
                        $.toast({
                            heading: 'Sukses',
                            text: 'Alamat terbarui',
                            showHideTransition: 'fade',
                            icon: 'success',
                            hideAfter: 2000,
                            position: 'bottom-center',
                            bgColor: '#5fbd74',
                            loader: false,
                        });
                    } else {
                        printErrorMsg(data.error);
                    }
                }
            });
        }, 300);
    };

    function addAddress() {
        $('.load-bar').html('<div class="bar"></div>');
        var city = $('#city').val();
        var id = $('#id').val();
        var kecamatan = $('#kecamatan').val();
        var kelurahan = $('#kelurahan').val();
        var address = $('#address').val();
        var zip_code = $('#zip_code').val();

        if (timeout) {
            $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            clearTimeout(timeout);
        }
        timeout = setTimeout(function () {
            $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            $.ajax({
                url: '{{URL::to("/address/updateAddressSubmit")}}',
                type: 'POST',
                data: {zip_code: zip_code, city: city, id: id, kecamatan: kecamatan, kelurahan: kelurahan, address: address},
                success: function (data) {
                    if ($.isEmptyObject(data.error)) {
                        $('#user').html(data);
                        $.toast({
                            heading: 'Sukses',
                            text: 'Alamat terbarui',
                            showHideTransition: 'fade',
                            icon: 'success',
                            hideAfter: 2000,
                            position: 'bottom-center',
                            bgColor: '#5fbd74',
                            loader: false,
                        });
                    } else {
                        printErrorMsg(data.error);
                    }
                }
            });
        }, 300);
    };

    function updateAddress(id) {
        if (timeout) {
            $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            clearTimeout(timeout);
        }
        timeout = setTimeout(function () {
            $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            $.ajax({
                url: '{{URL::to("/address/updateAddress")}}',
                type: 'POST',
                data: {id: id},
                success: function (data) {
                    if ($.isEmptyObject(data.error)) {
                        $('#user').html(data);
                    } else {
                        printErrorMsg(data.error);
                    }
                }
            });
        }, 300);
    };

    function addAddressProfile(id) {
        if (timeout) {
            $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            clearTimeout(timeout);
        }
        timeout = setTimeout(function () {
        $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
        $.ajax({
            url: '{{URL::to("/address/delAddress")}}',
            type: 'POST',
            data: {id: id},
            success: function (data) {
                if ($.isEmptyObject(data.error)) {
                    $('#user').html(data);
                    $.toast({
                        heading: 'Sukses',
                        text: 'Alamat terhapus',
                        showHideTransition: 'fade',
                        icon: 'success',
                        hideAfter: 2000,
                        position: 'bottom-center',
                        bgColor: '#5fbd74',
                        loader: false,
                    });
                } else {
                    printErrorMsg(data.error);
                }
            }
        });
    }, 300);
    };

    function myProfile() {
        if (timeout) {
            $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            clearTimeout(timeout);
        }
        timeout = setTimeout(function () {
            $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            $.ajax({
                url: '{{URL::to("/user-profile-form")}}',
                type: 'GET',
                data: {},
                success: function (data) {
                    if ($.isEmptyObject(data.error)) {
                        $('#user').html(data);
                    } else {
                        printErrorMsg(data.error);
                    }
                }
            });
        }, 300);
    };

    function myAddress() {
        console.log('test');
        if (timeout) {
            $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            clearTimeout(timeout);
        }
        timeout = setTimeout(function () {
            $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            $.ajax({
                url: '{{URL::to("/user-address")}}',
                type: 'GET',
                data: {},
                success: function (data) {
                    if ($.isEmptyObject(data.error)) {
                        $('#user').html(data);
                    } else {
                        printErrorMsg(data.error);
                    }
                }
            });
        }, 300);
    };

    function myHistory() {
        if (timeout) {
            $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            clearTimeout(timeout);
        }
        timeout = setTimeout(function () {
            $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            $.ajax({
                url: '{{URL::to("/user-history")}}',
                type: 'GET',
                data: {},
                success: function (data) {
                    if ($.isEmptyObject(data.error)) {
                        $('#user').html(data);
                    } else {
                        printErrorMsg(data.error);
                    }
                }
            });
        }, 300);
    };

    function myConfirmPayment() {
        if (timeout) {
            $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            clearTimeout(timeout);
        }
        timeout = setTimeout(function () {
            $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            $.ajax({
                url: '{{URL::to("/user-confirmPayment")}}',
                type: 'GET',
                data: {},
                success: function (data) {
                    if ($.isEmptyObject(data.error)) {
                        $('#user').html(data);
                    } else {
                        printErrorMsg(data.error);
                    }
                }
            });
        }, 300);
    };

    function detailPayment(order_id) {
        if (timeout) {
            $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            clearTimeout(timeout);
        }
        timeout = setTimeout(function () {
            $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            $.ajax({
                url: '{{URL::to("/user-confirmPayment/detail")}}',
                type: 'POST',
                data: {order_id: order_id},
                success: function (data) {
                    if ($.isEmptyObject(data.error)) {
                        $('#user').html(data);
                    } else {
                        printErrorMsg(data.error);
                    }
                }
            });
        }, 300);
    };

    function uploadPayment(order_id,order_number) {
        if (timeout) {
            $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            clearTimeout(timeout);
        }
        timeout = setTimeout(function () {
            $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            $.ajax({
                url: '{{URL::to("/user-confirmPayment/upload")}}',
                type: 'POST',
                data: {order_id: order_id, order_number: order_number},
                success: function (data) {
                    if ($.isEmptyObject(data.error)) {
                        $('#user').html(data);
                    } else {
                        printErrorMsg(data.error);
                    }
                }
            });
        }, 300);
    };

    function updateBillingSubmit(order_id,order_number) {
        if (timeout) {
            $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            clearTimeout(timeout);
        }
        var bank = $('#bank').val();
        var account_number = $('#account_number').val();
        timeout = setTimeout(function () {
            $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            $.ajax({
                url: '{{URL::to("/user-confirmPayment/uploadPaymentFile")}}',
                type: 'POST',
                data: {order_id: order_id, order_number: order_number, bank: bank, account_number: account_number},
                success: function (data) {
                    if ($.isEmptyObject(data.error)) {
                        $('#user').html(data);
                    } else {
                        printErrorMsg(data.error);
                    }
                }
            });
        }, 300);
    };

    function wholesale() {
        if (timeout) {
            $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            clearTimeout(timeout);
        }
        timeout = setTimeout(function () {
            $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            $.ajax({
                url: '{{URL::to("/user-wholesale")}}',
                type: 'GET',
                data: {},
                success: function (data) {
                    if ($.isEmptyObject(data.error)) {
                        $('#user').html(data);
                    } else {
                        printErrorMsg(data.error);
                    }
                }
            });
        }, 300);
    };

    function switchWholesale() {
        if (timeout) {
            $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            clearTimeout(timeout);
        }
        timeout = setTimeout(function () {
            $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            $.ajax({
                url: '{{URL::to("/switchwholesale")}}',
                type: 'GET',
                data: {},
                success: function (data) {
                    if ($.isEmptyObject(data.error)) {
                        $.toast({
                        heading: 'Sukses',
                        text: 'Status dirubah',
                        showHideTransition: 'fade',
                        icon: 'success',
                        hideAfter: 2000,
                        position: 'bottom-center',
                        bgColor: '#5fbd74',
                        loader: false,

                    });
                    var url = '{{URL::to('/')}}';
                    window.location.href=url;
                    } else {
                        printErrorMsg(data.error);
                    }
                }
            });
        }, 300);
    };

    function filterBestOffer() {
        $('.load-bar').html('<div class="bar"></div>');
        if (timeout) {
            $('.product-grid').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            clearTimeout(timeout);
        }
        timeout = setTimeout(function () {
            $('.product-grid').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            $.ajax({
                url: '{{URL::to("/shop/filterBestOffer")}}',
                type: 'GET',
                data: {},
                success: function (data) {
                    if ($.isEmptyObject(data.error)) {
                        $('.product-grid').html(data);
                    } else {
                        printErrorMsg(data.error);
                    }
                }
            });
        }, 300);
    };

    function filterPriceAllProduct() {
        $('.load-bar').html('<div class="bar"></div>');
        if (timeout) {
            $('.product-grid').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            clearTimeout(timeout);
        }
        timeout = setTimeout(function () {
            $('.product-grid').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            var minPrice = $("#min_price").text();
            var maxPrice = $("#max_price").text();

            console.log(minPrice);
            console.log(maxPrice);
            $.ajax({
                url: '{{URL::to("/shop/filterPriceAll")}}',
                type: 'POST',
                data: {minPrice: minPrice, maxPrice: maxPrice},
                success: function (data) {
                    if ($.isEmptyObject(data.error)) {
                        $('.product-grid').html(data);
                    } else {
                        printErrorMsg(data.error);
                    }
                }
            });
        }, 300);
    };

    function sortBy() {
        $('.load-bar').html('<div class="bar"></div>');
        var order = document.getElementById("orderby").value;
        console.log(order);
        if (timeout) {
            $('.product-grid').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            clearTimeout(timeout);
        }
        timeout = setTimeout(function () {
            $('.product-grid').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            $.ajax({
                url: '{{URL::to("/shop/sortBy")}}',
                type: 'POST',
                data: {order: order},
                success: function (data) {
                    if ($.isEmptyObject(data.error)) {
                        $('.product-grid').html(data);
                    } else {
                        printErrorMsg(data.error);
                    }
                }
            });
        }, 300);
    };

    function catSortBy(id) {
        $('.load-bar').html('<div class="bar"></div>');
        var order = document.getElementById("orderby").value;
        if (timeout) {
            $('.product-grid').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            clearTimeout(timeout);
        }
        timeout = setTimeout(function () {
            $('.product-grid').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            $.ajax({
                url: '{{URL::to("/shop/catSortBy")}}',
                type: 'POST',
                data: {order: order, id: id},
                success: function (data) {
                    if ($.isEmptyObject(data.error)) {
                        $('.product-grid').html(data);
                    } else {
                        printErrorMsg(data.error);
                    }
                }
            });
        }, 300);
    };

    function filterPriceCategoryProduct(catId) {
        $('.load-bar').html('<div class="bar"></div>');
        if (timeout) {
            $('.product-grid').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            clearTimeout(timeout);
        }
        timeout = setTimeout(function () {
            $('.product-grid').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            var minPrice = $("#min_price").text();
            var maxPrice = $("#max_price").text();

            console.log(minPrice);
            console.log(maxPrice);
            $.ajax({
                url: '{{URL::to("/category/filterPriceCategory")}}',
                type: 'POST',
                data: {minPrice: minPrice, maxPrice: maxPrice, catId: catId},
                success: function (data) {
                    if ($.isEmptyObject(data.error)) {
                        $('.product-grid').html(data);
                    } else {
                        printErrorMsg(data.error);
                    }
                }
            });
        }, 300);
    };

    function updateProfile(){
        timeout = setTimeout(function () {
            var first_name = $("#first_name").val();
            var last_name = $("#last_name").val();
            var phone_number = $("#phone_number").val();
            // var image = $('input[type=file]')[0].files[0];
            var gender = $('input[name=gender]:checked').val();


            $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');


            console.log(first_name);
            console.log(last_name);
            console.log(phone_number);
            console.log(gender);
            // console.log(image);


            $.ajax({
                url: '{{URL::to("/profile/updateProfile")}}',
                type: 'PUT',
                data: {first_name: first_name, last_name: last_name, phone_number: phone_number, gender: gender},
                success: function (data) {
                    if ($.isEmptyObject(data.error)) {
                        $('#user').html(data);
                        $.toast({
                        heading: 'Sukses',
                        text: 'Profil terbarui',
                        showHideTransition: 'fade',
                        icon: 'success',
                        hideAfter: 2000,
                        position: 'bottom-center',
                        bgColor: '#5fbd74',
                        loader: false,
                    });
                    } else {
                        printErrorMsg(data.error);
                    }
                }
            });
        }, 300);
    }

    function calculateShipping(){
        $('.load-bar').html('<div class="bar"></div>');
        var id = document.getElementById("addressId").value;
        console.log(id);
        if (timeout) {
            $('#shippingLoading').html('<img src="{{ URL::asset('/assets/images/ajax-loader.gif') }}">');
            $('#dstLoading').html('');
            $('#totalShippingLoading').html('<img src="{{ URL::asset('/assets/images/ajax-loader.gif') }}">');
            clearTimeout(timeout);
        }
        timeout = setTimeout(function () {
            $('#shippingLoading').html('<img src="{{ URL::asset('/assets/images/ajax-loader.gif') }}">');
            $('#dstLoading').html('');
            $('#totalShippingLoading').html('<img src="{{ URL::asset('/assets/images/ajax-loader.gif') }}">');
           $.ajax({
                url: '{{URL::to("/checkout/calculateShipping")}}',
                type: 'POST',
                data: {id: id},
                success: function (data) {
                    if ($.isEmptyObject(data.error)) {
                        $('#shippingDetail').html(data);
                    } else {
                        printErrorMsg(data.error);
                    }
                }
            });
        }, 300);
    }

    function paymentConfirm(){
        if($('input:radio:checked').length > 0){
            var bank = $('input[name=pilih]:checked').val();
            var url = '{{ route("confirm-order") }}';
            $.ajax({
                url: '{{URL::to("/confirm-order")}}',
                type: 'POST',
                data: {bank: bank},
                success: function (data) {
                    if ($.isEmptyObject(data.error)) {
                    } else {
                        printErrorMsg(data.error);
                    }
                }
            });
            window.location.href=url;

            }else{
                $.toast({
                            heading: 'Error',
                            text: 'Pilih bank pembayaran',
                            showHideTransition: 'fade',
                            icon: 'error',
                            hideAfter: 2000,
                            position: 'bottom-center',
                            loader: false,
                        });
            }
    }

    function finishingOrder() {
            var url2 = '{{URL::to('/order-succeed')}}';
            $('#finishOrder').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            $.ajax({
                url: '{{URL::to("/finish-order")}}',
                type: 'GET',
                data: {},
                success: function (data) {
                    if ($.isEmptyObject(data.error)) {
                        window.location.href=url2;
                    } else {
                        printErrorMsg(data.error);
                    }
                },
                error: function(data){
                    $.toast({
                        heading: 'Error',
                        text: 'Order tidak dapat diproses. Data tidak valid ',
                        showHideTransition: 'fade',
                        icon: 'error',
                        hideAfter: 4000,
                        position: 'bottom-center',
                        loader: false,
                    });
                }
            });
    };

    function printErrorMsg(msg) {
        $(".print-error-msg").find("ul").html('');
        $(".print-error-msg").css('display', 'block');
        $.each(msg, function (key, value) {
            $(".print-error-msg").find("ul").append('<li>' + value + '</li>');
        });
    }

    //Cart--------------------------------------------------------------------------------

    $("[type='number']").keypress(function (evt) {
        evt.preventDefault();
    });



    $(function() {

        $('.search-field').keypress(function (e) {
            var key = e.which;
            if(key == 13)  // the enter key code
            {
                load_product_serial(e);
            }
        });
        $('#searchBtn').on('click',function(e) { load_product_serial(e); });

        function load_product_serial(event){
            $('.load-bar').html('<div class="bar"></div>');
            var search = document.getElementById("searchProduct").value;
            console.log(search);
            if (timeout) {
                $('.product-grid').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
                clearTimeout(timeout);
            }
            timeout = setTimeout(function () {
                $('.product-grid').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
                $.ajax({
                    url: '{{URL::to("/shop/searchProduct")}}',
                    type: 'POST',
                    data: {search: search},
                    success: function (data) {
                        if ($.isEmptyObject(data.error)) {
                            $('.product-grid').html(data);
                        } else {
                            printErrorMsg(data.error);
                        }
                    }
                });
            }, 300);
        }

        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            // some code..
        }else
        {

            //tooltip Cart--------------------------------

            $('#qtyTooltip').tooltip('show');
            $('#removeTooltip').tooltip('show');
            setTimeout(function(){
                $('.removeTooltip').tooltip('destroy');
                $('.qtyTooltip').tooltip('destroy');
            }, 4000);

            $('.slick-arrow').hide();
        }

        //Pagination--------------------------
        $('body').on('click', '.pagination a', function(e) {
            e.preventDefault();
            $('#product-grid').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
            $('.pagination a').css('color', '#dfecf6');

            var url = $(this).attr('href');
            getArticles(url);
            window.history.pushState("", "", url);
        });

        function getArticles(url) {
            $.ajax({
                url : url
            }).done(function (data) {
                $('#product-grid').html(data);
            }).fail(function () {
                alert('Products could not be loaded.');
            });
        }
        //END//Pagination--------------------------
    });
    @if (Request::is('checkout') || Request::is('user-profile'))
    $(function () {
        //load google map
        initialize();
        // var infowindow = new google.maps.InfoWindow();
        /*
         * autocomplete location search
         */
        var PostCodeid = '#search_location';
        $(function () {
            $(PostCodeid).autocomplete({
                source: function (request, response) {
                    geocoder.geocode({
                        'address': request.term,
                        /*componentRestrictions: {country: "id"}*/
                    }, function (results, status) {
                        response($.map(results, function (item) {
                            return {
                                label: item.formatted_address,
                                value: item.formatted_address,
                                lat: item.geometry.location.lat(),
                                lon: item.geometry.location.lng()
                            };
                        }));
                    });
                },
                select: function (event, ui) {

                    $('#address').val(ui.item.value);
                    $('#latitude').val(ui.item.lat);
                    $('#longitude').val(ui.item.lon);

                    var latlng = new google.maps.LatLng(ui.item.lat, ui.item.lon);
                    marker.setPosition(latlng);
                    initialize();
                }
            });
        });

        /*
         * Point location on google map
         */
        $('.get_map').click(function (e) {
            var address = $(PostCodeid).val();
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    marker.setPosition(results[0].geometry.location);

                    $('#address').val(results[0].formatted_address);
                    $('#latitude').val(marker.getPosition().lat());
                    $('#longitude').val(marker.getPosition().lng());
                    infowindow.setContent(null);
                    infowindow.open(null);
                } else {
                    $.toast({
                        heading: 'Error',
                        text: 'Lokasi tidak ditemukan',
                        showHideTransition: 'fade',
                        icon: 'error',
                        hideAfter: 4000,
                        position: 'bottom-center',
                        loader: false,
                    });
                }
            });
            e.preventDefault();
        });

        //Add listener to marker for reverse geocoding
        google.maps.event.addListener(marker, 'drag', function () {
            geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        $('#address').val(results[0].formatted_address);
                        $('#latitude').val(marker.getPosition().lat());
                        $('#longitude').val(marker.getPosition().lng());
                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, marker);
                    }
                }
            });
        });
    });
    @endif
</script>
<!--Start of Tawk.to Script-->
<script type = "text/javascript" >
    var Tawk_API = Tawk_API || {},
        Tawk_LoadStart = new Date();
(function () {
    var s1 = document.createElement("script"),
        s0 = document.getElementsByTagName("script")[0];
    s1.async = true;
    s1.src = 'https://embed.tawk.to/5b13f46a10b99c7b36d49546/default';
    s1.charset = 'UTF-8';
    s1.setAttribute('crossorigin', '*');
    s0.parentNode.insertBefore(s1, s0);
})(); </script>
<!--End of Tawk.to Script-->

</body>
</html>
