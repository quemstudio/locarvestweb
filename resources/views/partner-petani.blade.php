<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 05/01/18
 * Time: 19.56
 */
?>
@extends('layouts.app')
@section('content')
    <div id="main">
        <div class="section section-bg-10 pt-11 pb-17">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="page-title text-center">Partner Petani</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="section border-bottom pt-2 pb-2">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <ul class="breadcrumbs">
                            <li><a href="{{URL::to('/')}}">Home</a></li>
                            <li><a href="#">Partner Petani</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="section pt-8 pb-8">
            <div class="container">
                @foreach($suppliers->chunk(4) as $chunk)
                    <div class="row">
                        @foreach($chunk as $supplier)
                    <div class="col-md-3 col-sm-6">
                        <a href="{{route('suppliers.show',$supplier->supplierId)}}">
                        <div class="team-member">
                            <div class="image">
                                @if(!empty($supplier->image))
                                    <img src="{{URL::asset('/storage/'.$supplier->image)}}" style="height: 200px" >
                                @else
                                    <img src="{{URL::asset('/assets/images/test.jpg')}}" style="height: 200px"/>
                                @endif
                            </div>
                            <div class="team-info">
                                <h5 class="name">{{ $supplier->first_name.' '.$supplier->last_name }}</h5>
                            </div>
                        </div>
                        </a>
                    </div>
                    @endforeach
                </div>
                    @endforeach
            </div>
        </div>
    </div>
@endsection
