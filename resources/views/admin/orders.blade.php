<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 16/01/18
 * Time: 19.31
 */
?>
@extends('layouts.appadmin')
@section('content')
    <!-- BEGIN PAGE CONTENT -->
    <div class="header">
        <h2>Data <strong>Pesanan</strong></h2>
    </div>
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif(session('error'))
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    @endif
    <div class="row">
        <div class="col-lg-12 portlets">
            <div class="panel">
                <div class="panel-content">
                    <div class="filter-left panel-content pagination2 table-responsive">
                        <table class="table table-hover table-dynamic table-tools" data-table-name="Data Orders">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th class='hidden-350'>Nomor Order</th>
                                <th class='hidden-1024'>Tanggal Pengiriman</th>
                                <th class='hidden-480'>Alamat</th>
                                <th>Kategori Order</th>
                                <th class='hidden-480'>Detail</th>
                                {{--<th class='hidden-480'>Produk</th>--}}
                                {{--<th class='hidden-480'>Total Harga</th>--}}
                                <th class='hidden-480'>Status</th>
                                <th class='hidden-480'>Retur</th>
                                <th class='hidden-480'></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($area as $data)
                                @php
                                    $cus = \App\Customer::find($data['customer_id']);
                                    $user = \App\User::find($cus->user_id);
                                @endphp
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td class='hidden-350'>{{$data['order_number']}}</td>
                                    {{--<td class='hidden-1024'>{{$data['date']}}</td>--}}
                                    <td class='hidden-1024'>{{date('d/m/Y', strtotime($data['date']))}}</td>
                                    {{--<td class='hidden-480'>{{$data->order_details[0]['price']}}</td>--}}
                                    <td class='hidden-480'>{{$data['addresses']['address']}}</td>
                                    {{--<td>{{$user->first_name}} {{$user->last_name}}</td>--}}
                                    <td>{{$data['order_details'][0]['products']['category']}}</td>
                                    <td class='hidden-480'>

                                        {{--DISINI ALDI--}}
                                        @php $productText = "" @endphp
                                        @foreach($data['order_details'] as $order )
                                            @php
                                                $productText = $productText.$order['products']['name'].' : '.$order['qty'];
                                            @endphp

                                            @if(!$loop->last)
                                                @php
                                                    $productText = $productText.', ';
                                                @endphp
                                            @endif
                                        @endforeach
                                        <label style="display: none">{{$productText}}</label>
                                        <button class="btn btn-blue" type="button" data-toggle="modal" data-target="#show-detail{{$data['id']}}">
                                            <span class="fa fa-eye"></span>
                                        </button>
                                    </td>
                                    {{--<td class='hidden-480'>Rp. {{ str_replace(',', '.', number_format($data->total_price)) }}</td>--}}
                                    <td class='hidden-480'>{{$data['status']}}</td>

                                    @if($data['retur_status'] === 0)
                                        <td></td>
                                    @else
                                        <td class='hidden-480'>
                                            <label style="display: none">{{$data['retur_note']}}</label>
                                            <button class="btn btn-dark" type="button" data-toggle="modal" data-target="#show-retur{{$data['id']}}">
                                                <span class="fa fa-eye"></span>
                                            </button>
                                        </td>
                                    @endif

                                    @if($data['status'] === 'confirmed')
                                        <td class='hidden-480'>
                                            <button class="btn btn-blue" type="button"
                                                    id="danger-alert"
                                                    data-tr="tr_{{$data['id']}}"
                                                    onclick="updateProgress('{{$data['id']}}', 'Shipping')">
                                                <span class="fa fa-truck"></span>
                                            </button>
                                        </td>
                                    @elseif($data['status'] === 'shipping')
                                        <td class='hidden-480'>
                                            <button class="btn btn-success" type="button"
                                                    id="danger-alert"
                                                    data-tr="tr_{{$data['id']}}"
                                                    onclick="completeOrder('{{$data['id']}}')">
                                                <span class="fa fa-check"></span>
                                            </button>
                                        </td>
                                    @else
                                        <td></td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @foreach($area as $data)
        @php
            $cus = \App\Customer::find($data['customer_id']);
            $user = \App\User::find($cus->user_id);
        @endphp
        <div id="show-detail{{$data['id']}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content p-0 b-0">
                    <div class="panel panel-color panel-primary">
                        <div class="panel-heading">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                        class="icons-office-52"></i></button>
                            <h4 class="modal-title"><strong>Detail</strong></h4>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                {{--<td>{{$user->first_name}} {{$user->last_name}}</td>--}}
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" style="vertical-align: middle">Nama Pelanggan</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control"
                                               placeholder="" id="np"
                                               name="np">{{$user->first_name}} {{$user->last_name}}</label>
                                    </div>
                                </div>
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Nama Produk</th>
                                        <th>QTY</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data['order_details'] as $order )
                                        <tr>
                                            <td>
                                                <img style="width: 50px;" src="{{URL::asset('/storage/'.$order['products']['image'])}}" alt="picture" class="img-responsive">
                                            </td>
                                            <td>{{ $order['products']['name'] }}</td>
                                            <td>{{ $order['qty'] }}</td>
                                        </tr>
                                        <br />
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{--modal detail retur--}}
        <div id="show-retur{{$data['id']}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content p-0 b-0">
                    <div class="panel panel-color panel-primary">
                        <div class="panel-heading">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                        class="icons-office-52"></i></button>
                            <h4 class="modal-title">Detail <strong>Retur</strong></h4>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Catatan</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>{{ $data['retur_note'] }}</td>
                                    </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{--Modal Catat Retur--}}
        <div class="modal fade" id="modal-retur{{$data['id']}}" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form class="form-horizontal" id="catatRetur" action="{{route('orders.retur.submit')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}} {{method_field('PUT')}}
                        <div class="modal-header bg-primary">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                        class="icons-office-52"></i></button>
                            <h4 class="modal-title">Catatan <strong>Retur</strong></h4>
                        </div>
                        <div class="modal-body">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">No. Order</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="text" name="order_number" id="order_number" placeholder="No. Order" value="{{$data['order_number']}}" readonly>
                                        <input type="hidden" name="id" id="id" placeholder="Order Id" value="{{$data['id']}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Catatan</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" type="text" name="retur_note" id="retur_note" placeholder="" value="{{$data['retur_note']}}" required></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary btn-embossed" type="submit">Confirm</button>
                            {{--<button onclick="updateBillingSubmit()" class="col-md-12">Confirm</button>--}}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
@endsection

@push('script')
    <script>
        function updateProgress(id, msg) {
            swal({
                title: "Ganti status order menjadi " + msg + " ?",
                type: "warning",
                showCancelButton: true,
                titleFontStyle: "Bold",
                confirmButtonClass: 'btn btn-danger btn-custom waves-effect waves-light',
                confirmButtonText: "Ya, ganti!",
                cancelButtonText: "Tidak",
                closeOnConfirm: false,
                cancelButtonClass: 'btn',
                buttonsStyling: false,
                reverseButtons: true
            }, function () {
                setTimeout(function () {
                    $.ajax({
                        url: 'orders/update/' + id,
                        type: 'PUT',
                        dataType: 'json',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        success: function (data) {
                            if (data == null) {
                                swal("Berhasil!", "Status order berhasil diubah", "success");
                                location.reload();
                            }
                            else
                                swal("Gagal", "Gagal update status order", "error");
                        }, error: function () {
                            swal("Error!", "Failed to update.", "error");
                        }
                    });
                }, 2000)
            });
        }


        function completeOrder(id) {
            swal({
                title: "Apakah ada produk yang diretur ?",
                text: "Status order akan diubah menjadi Completed",
                type: "warning",
                showCancelButton: true,
                titleFontStyle: "Bold",
                confirmButtonClass: 'btn btn-danger btn-custom waves-effect waves-light',
                confirmButtonText: "Ya, buat catatan retur",
                cancelButtonText: "Tidak",
                cancelButtonClass: 'btn',
                buttonsStyling: false,
                reverseButtons: true,
                closeOnCancel: false,
                allowOutsideClick: true
            }, function (isConfirm) {
                if (isConfirm) {
                    $('#modal-retur' + id).modal('show');
                } else {
                    setTimeout(function () {
                        $.ajax({
                            url: 'orders/update/' + id,
                            type: 'PUT',
                            dataType: 'json',
                            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            success: function (data) {
                                if (data == null) {
                                    swal("Completed!", "Order completed!", "success");
                                    location.reload();
                                }
                                else
                                    swal("Not Updated!", "Failed to update.", "error");
                            }, error: function () {
                                swal("Error!", "Failed to update.", "error");
                            }
                        });
                    }, 2000)
                }
            });
        }

    </script>
@endpush