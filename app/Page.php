<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $primaryKey="id";
    protected $guarded = [];

    public function pageImages()
    {
        return $this->hasMany('App\PageImage','id');
    }
}
