
        <div class="cart-totals">
            <table>
                <tbody>
                <tr class="cart-subtotal">
                    <th>Subtotal</th>
                    <td id="loading">Rp. <?php echo Cart::subtotal(); ?></td>
                </tr>
                </tbody>
            </table>
            <div class="proceed-to-checkout" onclick="cartCheck('{{Cart::count()}}')" style="cursor:pointer">
                <a >Lanjut ke Pengiriman</a>
            </div>
        </div>

        <script type="text/javascript">
            $('[data-toggle="tooltip"]').tooltip({
                trigger: 'manual'
            });

            function cartCheck(cart) {
                if (cart > 0) {
                    var url = '{{URL::to('/checkout')}}';
                    window.location.href = url;
                } else {
                    $.toast({
                        heading: 'Error',
                        text: 'Cart Kosong',
                        showHideTransition: 'fade',
                        icon: 'error',
                        hideAfter: 3000,
                        position: 'bottom-center',
                        loader: false,
                    });
                }
            }

        </script>
