<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 05/01/18
 * Time: 19.56
 */
?>
@extends('layouts.app')
@section('content')
    <div id="main">
        <div class="section section-bg-10 pt-11 pb-17">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="page-title text-center">F.A.Q</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="section border-bottom pt-2 pb-2">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <ul class="breadcrumbs">
                            <li><a href="{{URL::to('/')}}">Home</a></li>
                            <li>F.A.Q</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="section pt-7 pb-7">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="single-blog">
                            <div class="entry-content">
                                {!! $faq[0]->body !!}
                            </div>
                            <!-- <div class="entry-footer">
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="share">
                                            <span> <i class="ion-android-share-alt"></i> Share this post </span>
                                            <span> <a target="_blank" href="#"><i class="fa fa-facebook"></i></a> </span>
                                            <span> <a target="_blank" href="#"><i class="fa fa-twitter"></i></a> </span>
                                            <span> <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a> </span>
                                        </div>
                                    </div>
                                </div>
                            </div> -->

                            <!-- <div class="entry-nav">
                                <div class="row">
                                    <div class="col-md-5 left">
                                        <i class="fa fa-angle-double-left"></i>
                                        <a href="#">How can salmon be raised organically in fish farms?</a>
                                    </div>
                                    <div class="col-md-2 text-center">
                                        <i class="ion-grid"></i>
                                    </div>
                                    <div class="col-md-5 right">
                                        <a href="#">How to steam &amp; purée your sugar pie pumkin</a>
                                        <i class="fa fa-angle-double-right"></i>
                                    </div>
                                </div>
                            </div> -->

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
