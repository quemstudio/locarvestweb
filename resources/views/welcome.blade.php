@extends('layouts.app')
@section('content')
    <div id="main">
        <div class="section">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12 p-0">
                        @if (session('success'))
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="alert alert-success" style="text-align:center">
                                        <span class="fa fa-check-circle"></span> {{ session('success') }}
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div id="rev_slider" class="rev_slider fullscreenbanner">
                            <ul>
                                @foreach($slides as $slide)

                                <li data-transition="fade" data-slotamount="default" data-hideafterloop="0"
                                    data-hideslideonmobile="off" data-easein="default" data-easeout="default"
                                    data-masterspeed="300" data-rotate="0" data-saveperformance="off"
                                    data-title="" >
                                    <img src="{{URL::asset('/storage/'.$slide->cover)}}" alt=""
                                         data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat"
                                         data-bgparallax="off" class="rev-slidebg"/>

                                    {{--<div class="tp-caption rs-parallaxlevel-1"--}}
                                         {{--data-x="center" data-hoffset=""--}}
                                         {{--data-y="center" data-voffset="-80"--}}
                                         {{--data-width="['none','none','none','none']"--}}
                                         {{--data-height="['none','none','none','none']"--}}
                                         {{--data-type="image" data-responsive_offset="on"--}}
                                         {{--data-frames='[{"delay":0,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'--}}
                                         {{--data-textAlign="['inherit','inherit','inherit','inherit']"--}}
                                         {{--data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]"--}}
                                         {{--data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]">--}}
                                        {{--<img src="{{URL::asset('/assets/images/slider/slide_'.array_random($array).'.png')}}" alt=""/>--}}
                                    {{--</div>--}}
                                    <a class="tp-caption btn-2 hidden-xs" href="{{route('posts.show', $slide->id)}}"
                                       data-x="['center','center','center','center']"
                                       data-y="['center','center','center','center']" data-voffset="['260','260','260','260']"
                                       data-width="['auto']" data-height="['auto']"
                                       data-type="button" data-responsive_offset="on"
                                       data-responsive="off"
                                       data-frames='[{"delay":0,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Power0.easeIn","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(95,189,116);bg:rgba(51, 51, 51, 0);"}]'
                                       data-textAlign="['inherit','inherit','inherit','inherit']"
                                       data-paddingtop="[16,16,16,16]" data-paddingright="[30,30,30,30]"
                                       data-paddingbottom="[16,16,16,16]" data-paddingleft="[30,30,30,30]">LIHAT DETAIL
                                    </a>

                                </li>
                                @endforeach
                                {{--<li data-transition="fade" data-slotamount="default" data-hideafterloop="0"--}}
                                    {{--data-hideslideonmobile="off" data-easein="default" data-easeout="default"--}}
                                    {{--data-masterspeed="300" data-rotate="0" data-saveperformance="off"--}}
                                    {{--data-title="Slide">--}}
                                    {{--<img src="{{URL::asset('/assets/images/slider/slide_bg_5.jpg')}}" alt=""--}}
                                         {{--data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat"--}}
                                         {{--data-bgparallax="off" class="rev-slidebg"/>--}}
                                    {{--<div class="tp-caption rs-parallaxlevel-1"--}}
                                         {{--data-x="center" data-hoffset=""--}}
                                         {{--data-y="center" data-voffset="-120"--}}
                                         {{--data-width="['none','none','none','none']"--}}
                                         {{--data-height="['none','none','none','none']"--}}
                                         {{--data-type="image" data-responsive_offset="on"--}}
                                         {{--data-frames='[{"delay":0,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'--}}
                                         {{--data-textAlign="['inherit','inherit','inherit','inherit']"--}}
                                         {{--data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]"--}}
                                         {{--data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]">--}}
                                        {{--<img src="{{URL::asset('/assets/images/slider/slide_8.png')}}" alt=""/>--}}
                                    {{--</div>--}}

                                {{--</li>--}}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="text-center mt-3 section-pretitle">Produk Kami</div>
        <div style="padding-top:40px" class="category-carousel-2 mb-3" data-auto-play="true" data-desktop="6"
             data-laptop="5" data-tablet="4" data-mobile="2">
            @foreach($countCat as $category)
                <div class="cat-item">
                    <div class="cats-wrap" data-bg-color="#fff">
                        <a href="{{ route('categories.show',$category->id)}}">

                            <img src="{{URL::asset('/storage/'.$category->image)}}" alt=""/>
                            <h2 style="color: #180E07" class="category-title">
                                {{ $category->name }}
                            </h2>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
        <!--<div class="section section-bg-1 section-cover pt-17 pb-3">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="mt-3 mb-3">
                            <img src="images/oranges.png" alt="" />
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="mb-1 section-pretitle default-left">Welcome to</div>
                        <h2 class="section-title mtn-2 mb-3">Organik Store</h2>
                        <p class="mb-4">
                            Organic store opened its doors in 1990, it was Renée Elliott's dream to offer the best and widest range of organic foods available, and her mission to promote health in the community and to bring a sense of discovery and adventure into food shopping.
                        </p>
                        <a class="organik-btn arrow" href="#">Tentang Kami</a>
                    </div>
                </div>
            </div>
        </div>-->
        <div class="section bg-light pt-10 pb-10">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center mb-1 section-pretitle">Locarvest</div>
                        <h2 class="text-center section-title mtn-2">Proses Pengiriman</h2>
                        <div class="organik-seperator center mb-3">
                            <span class="sep-holder"><span class="sep-line"></span></span>
                            <div class="sep-icon"><i class="organik-flower"></i></div>
                            <span class="sep-holder"><span class="sep-line"></span></span>
                        </div>
                        <div class="organik-steps">
                            <div class="step">
                                <div class="step-icon">
                                    <i class="organik-lemon"></i>
                                    <span class="order">1</span>
                                </div>
                                <div class="step-title">Langkah 01:</div>
                                <div class="step-text">Pilih produk pertanian lokalmu</div>
                            </div>
                            <div class="step-line"></div>
                            <div class="step">
                                <div class="step-icon">
                                    <i class="organik-map-location"></i>
                                    <span class="order">2</span>
                                </div>
                                <div class="step-title">Langkah 02:</div>
                                <div class="step-text">Tulis dan Sertakan alamatmu</div>
                            </div>
                            <div class="step-line"></div>
                            <div class="step">
                                <div class="step-icon">
                                    <i class="organik-credit-card"></i>
                                    <span class="order">3</span>
                                </div>
                                <div class="step-title">Langkah 03:</div>
                                <div class="step-text">Pilih metode pembayaran</div>
                            </div>
                            <div class="step-line"></div>
                            <div class="step">
                                <div class="step-icon">
                                    <i class="organik-delivery"></i>
                                    <span class="order">4</span>
                                </div>
                                <div class="step-title">Langkah 04:</div>
                                <div class="step-text">Dapatkan produk depan rumahmu</div>
                            </div>
                        </div>
                        <div class="text-center">
                            <a class="organik-btn brown mt-4" href="{{route('shop')}}">Belanja Sekarang</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="section section-bg-3 pt-6 pb-6">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center mb-1 section-pretitle">A friendly</div>
                        <h2 class="text-center section-title mtn-2">Organic Store</h2>
                        <div class="organik-seperator mb-9 center">
                            <span class="sep-holder"><span class="sep-line"></span></span>
                            <div class="sep-icon"><i class="organik-flower"></i></div>
                            <span class="sep-holder"><span class="sep-line"></span></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="organik-special-title">
                            <div class="number">01</div>
                            <div class="title">Agriservices</div>
                        </div>
                        <p>Unit bisnis penyedia jasa (selain jasa niaga). Termasuk di dalam komponen ini antara lain kegiatan Riset dan Pengembangan, penyuluhan, informasi, perkreditan, asuransi, pendidikan dan pelatihan, dan lain-lain.</p>
                        <div class="mb-8"></div>
                        <div class="organik-special-title">
                            <div class="number">02</div>
                            <div class="title">Agribusiness</div>
                        </div>
                        <p>Subsistem usahatani ini adalah usaha tanaman pangan, usaha tanaman hortikultura, usaha tanaman obat-obatan, usaha perkebunan, usaha perikanan, usaha peternakan, dan kehutanan.</p>
                    </div>
                    <div class="col-md-4">
                        <div class="mb-8"></div>
                    </div>
                    <div class="col-md-4">
                        <div class="organik-special-title align-right">
                            <div class="number">03</div>
                            <div class="title">Agriprocess</div>
                        </div>
                        <p>Mengolah produk usahatani menjadi produk olahan baik produk awal maupun produk akhir.</p>
                        <div class="mb-8"></div>
                        <div class="organik-special-title align-right">
                            <div class="number">04</div>
                            <div class="title">Agriproduct</div>
                        </div>
                        <p>Cur tantas regiones barbarorum peat dibus obiit, tregiones barbarorum peat dibus obiit, tot mariata</p>
                    </div>
                </div>
            </div>
        </div>-->
        <!--<div class="section border-bottom mt-6 mb-5">
            <div class="container">
                <div class="row">
                    <div class="organik-process">
                        <div class="col-md-3 col-sm-6 organik-process-small-icon-step">
                            <div class="icon">
                                <i class="organik-delivery"></i>
                            </div>
                            <div class="content">
                                <div class="title">Free shipping</div>
                                <div class="text">All order over $100</div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 organik-process-small-icon-step">
                            <div class="icon">
                                <i class="organik-hours-support"></i>
                            </div>
                            <div class="content">
                                <div class="title">Customer support</div>
                                <div class="text">Support 24/7</div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 organik-process-small-icon-step">
                            <div class="icon">
                                <i class="organik-credit-card"></i>
                            </div>
                            <div class="content">
                                <div class="title">Secure payments</div>
                                <div class="text">Confirmed</div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 organik-process-small-icon-step">
                            <div class="icon">
                                <i class="organik-lettuce"></i>
                            </div>
                            <div class="content">
                                <div class="title">Made with love</div>
                                <div class="text">Best services</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <hr class="mt-8 mb-4" />
                    </div>
                </div>
            </div>
        </div>-->
        <div class="section section-bg-6 section-cover pt-8 pb-8">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center mt-2 section-pretitle" style="color:#392a25">Apa kata mereka mengenai kami</div>
                        <div class="organik-seperator mb-4 center">
                            <span class="sep-holder"><span class="sep-line"></span></span>
                            <div class="sep-icon"><i class="organik-flower" style="color:#392a25"></i></div>
                            <span class="sep-holder"><span class="sep-line"></span></span>
                        </div>
                        <div class="testimonial-carousel organik-testimonial style-2" data-auto-play="true" data-desktop="1" data-laptop="1" data-tablet="1" data-mobile="1">
                            @foreach($testimonies as $testimony)
                            <div class="testi-item">
                                <div class="text">
                                {!! $testimony->body !!}
                                </div>
                                <div class="info">
                                    <div class="photo">
                                        <img src="{{URL::asset('/storage/'.$testimony->cover)}}" alt="" />
                                    </div>
                                    <div class="author">
                                        <span class="name">@php $splitName = explode('-',$testimony->title); echo($splitName[0]); @endphp</span>
                                        <span class="tagline">@php echo($splitName[1]); @endphp</span>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section pt-12">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center mb-1 section-pretitle">Terbaru</div>
                        <h2 class="text-center section-title mtn-2">Dari berita Kami</h2>
                        <div class="organik-seperator center mb-6">
                            <span class="sep-holder"><span class="sep-line"></span></span>
                            <div class="sep-icon"><i class="organik-flower"></i></div>
                            <span class="sep-holder"><span class="sep-line"></span></span>
                        </div>
                    </div>
                </div>

                @foreach($posts->chunk(3) as $chunk)
                    <div class="row">
                        @foreach($chunk as $post)
                            <div class="col-md-4">
                                <div class="blog-grid-item">
                                    <div class="post-thumbnail">
                                        <a href="{{route('posts.show', $post['id'])}}">
                                            <img src="{{URL::asset('/storage/'.$post->cover)}}" alt=""/>
                                        </a>
                                    </div>
                                    <div class="post-content">
                                        <div class="entry-meta">
											<span class="posted-on">
												<i class="ion-calendar"></i>
												<span>{{ $post['updated_at']->format('F j, Y') }}</span>
											</span>
                                        </div>
                                        <a href="{{route('posts.show', $post['id'])}}">
                                            <h1 class="entry-title">{{ $post['title'] }}</h1>
                                        </a>
                                        <div class="entry-content">
                                            {!! str_limit( $post['body'], $limit = 150, $end = '...') !!}
                                        </div>
                                        <div class="entry-more">
                                            <a href="{{route('posts.show', $post['id'])}}">Baca Selengkapnya</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
                <div class="row">
                    <div class="col-sm-12">
                        <hr class="mt-7 mb-3"/>
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="section pt-2 pb-4">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="client-carousel" data-auto-play="true" data-desktop="5" data-laptop="3" data-tablet="3" data-mobile="2">
                            <div class="client-item">
                                <a href="#" target="_blank">
                                    <img src="images/client/client_1.png" alt="" />
                                </a>
                            </div>
                            <div class="client-item">
                                <a href="#" target="_blank">
                                    <img src="images/client/client_2.png" alt="" />
                                </a>
                            </div>
                            <div class="client-item">
                                <a href="#" target="_blank">
                                    <img src="images/client/client_3.png" alt="" />
                                </a>
                            </div>
                            <div class="client-item">
                                <a href="#" target="_blank">
                                    <img src="images/client/client_4.png" alt="" />
                                </a>
                            </div>
                            <div class="client-item">
                                <a href="#" target="_blank">
                                    <img src="images/client/client_5.png" alt="" />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
    </div>

@endsection
