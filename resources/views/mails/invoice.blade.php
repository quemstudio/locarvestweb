<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns:v="urn:schemas-microsoft-com:vml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />
    <meta name="viewport" content="width=600,initial-scale = 2.3,user-scalable=no">
    <!--[if !mso]><!-- -->
    <link href='https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700' rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Quicksand:300,400,700' rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
    <!-- <![endif]-->

    <title>Material Design for Bootstrap</title>

    <style type="text/css">
        body {
            width: 100%;
            background-color: #ffffff;
            margin: 0;
            padding: 0;
            -webkit-font-smoothing: antialiased;
            mso-margin-top-alt: 0px;
            mso-margin-bottom-alt: 0px;
            mso-padding-alt: 0px 0px 0px 0px;
        }

        p,
        h1,
        h2,
        h3,
        h4 {
            margin-top: 0;
            margin-bottom: 0;
            padding-top: 0;
            padding-bottom: 0;
        }

        span.preheader {
            display: none;
            font-size: 1px;
        }

        html {
            width: 100%;
        }

        table {
            font-size: 14px;
            border: 0;
        }

        /* ----------- responsivity ----------- */

        @media only screen and (max-width: 640px) {

            /*------ top header ------ */
            .main-header {
                font-size: 20px !important;
            }

            .main-section-header {
                font-size: 28px !important;
            }

            .show {
                display: block !important;
            }

            .hide {
                display: none !important;
            }

            .align-center {
                text-align: center !important;
            }

            .no-bg {
                background: none !important;
            }

            /*----- main image -------*/
            .main-image img {
                width: 440px !important;
                height: auto !important;
            }

            /* ====== divider ====== */
            .divider img {
                width: 440px !important;
            }

            /*-------- container --------*/
            .container590 {
                width: 440px !important;
            }

            .container580 {
                width: 400px !important;
            }

            .main-button {
                width: 220px !important;
            }

            /*-------- secions ----------*/
            .section-img img {
                width: 320px !important;
                height: auto !important;
            }

            .team-img img {
                width: 100% !important;
                height: auto !important;
            }
        }

        @media only screen and (max-width: 479px) {

            /*------ top header ------ */
            .main-header {
                font-size: 18px !important;
            }

            .main-section-header {
                font-size: 26px !important;
            }

            /* ====== divider ====== */
            .divider img {
                width: 280px !important;
            }

            /*-------- container --------*/
            .container590 {
                width: 280px !important;
            }

            .container590 {
                width: 280px !important;
            }

            .container580 {
                width: 260px !important;
            }

            /*-------- secions ----------*/
            .section-img img {
                width: 280px !important;
                height: auto !important;
            }
        }
    </style>
    <!-- [if gte mso 9]><style type=”text/css”>
        body {
            font-family: arial, sans-serif!important;
        }
    </style>
    <![endif]-->
</head>


<body class="respond" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
    <!-- pre-header -->
    <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="#7fca90" class="bg_color">
        <tr>
            <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
        </tr>
    </table>
    <!-- pre-header end -->
    <!-- header -->
    <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff">

        <tr>
            <td align="center">
                <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590">

                    <tr>
                        <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                    </tr>

                    <tr>
                        <td align="center">

                            <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590">

                                <tr>
                                    <td align="center" height="70" style="height:70px;">
                                        <a href="http://locarvest.com" style="display: block; border-style: none !important; border: 0 !important;"><img
                                                width="100" border="0" style="display: block; width: 100px;" src="http://locarvest.com/assets/images/LOCARVEST.png"
                                                alt="" /></a>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="center">
                                        <table width="360 " border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"
                                            class="container590 hide">
                                            <tr>
                                                <td width="120" align="center" style="font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;">
                                                    <a href="" style="color: #312c32; text-decoration: none;">LOCAL</a>
                                                </td>
                                                <td width="120" align="center" style="font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;">
                                                    <a href="" style="color: #312c32; text-decoration: none;">HARVEST</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                    </tr>

                </table>
            </td>
        </tr>
    </table>
    <!-- end header -->
   
                
   
    <!-- big image section -->
    <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="#7fca90" class="bg_color">

        <tr>
            <td align="center">
                <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590">
                    <tr>
                        <td colspan="4" height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center" style="color: #343434; font-size: 24px; font-family: Quicksand, Calibri, sans-serif; font-weight:700;letter-spacing: 3px; line-height: 35px;"
                            class="main-header">
                            <div style="line-height: 35px">
                                INFORMASI <span style="color: #3b7795;">INVOICE</span> ANDA

                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">                                                                 
                            <table border="0" width="40" align="center" cellpadding="0" cellspacing="0" bgcolor="eeeeee">
                                <tr>
                                    <td height="2" style="font-size: 2px; line-height: 2px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td  style="font-size: 15px; line-height: 20px;">Tanggal </td>
                        <td  style="font-size: 15px; line-height: 20px;">: {{$date}} </td>
                        <td>
                        <td>
                        <td>
                    </tr>
                    <tr>
                        <td height="20" style="font-size: 15px; line-height: 20px;">Dari </td>
                        <td height="20" style="font-size: 15px; line-height: 20px;"> : {{$sender}} </td>
                        <td>
                        <td>
                        <td>
                    </tr>
                    <tr>
                        <td  height="20" style="width:100px; font-size: 15px; line-height: 20px;">Kepada </td>
                        <td  height="20" style="width:70%; font-size: 15px; line-height: 20px;">: {{$receiver}} </td>
                        <td>
                        <td>
                        <td>
                    </tr>
                    <tr>
                        <td height="20" style="font-size: 15px; line-height: 20px;">Alamat </td>
                        <td height="20" style="font-size: 15px; line-height: 20px;">: {{$dataProducts[0]->address}} </td>
                        <td>
                        <td>
                        <td>
                    </tr>
                    <tr>
                        <td height="20" style="font-size: 15px; line-height: 20px;">Hp </td>
                        <td height="20" style="font-size: 15px; line-height: 20px;">: {{$phone}} </td>
                        <td>
                        <td>
                            <td>
                    </tr>
                    <tr>
                        <td height="20" style="font-size: 15px; line-height: 20px;">Pembayaran </td>
                        <td height="20" style="font-size: 15px; line-height: 20px;">: {{ $dataProducts[0]->payment_method}} </td>
                        <td>
                        <td>
                            <td>
                    </tr>
					
					 <tr>
                        <td colspan="4" height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                    </tr>
					 <tr>
                        <td colspan="4" height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                    </tr>
					<tr>
                        <td colspan="5" height="20" style="font-size: 17px; line-height: 20px;">Pembelian Sayuran Dengan Rincian </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <table border="0" width="600" align="center" cellpadding="0" cellspacing="0" class="container590">
                                {{-- <!-- <tr>
                                    <td align="justify" style="color: #ffffff; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;">

                                        <div style="line-height: 24px">
                                            Halo, <font color="#3b7795"><b>{{  $demo->receiver }}</b></font>.
                                            Terimakasih telah melakukan pembelian produk-produk hasil panen segar di
                                            Locarvest.
                                            Berikut ditampilkan detail produk order anda dengan no. order : <font color="#ff5358"><b>{{
                                                    $dataProducts[0]->order_number }}</b></font>.
                                        </div>
                                    </td>
                                </tr> --> --}}


                                <tr>
                                    <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                                </tr>


                            <table cellpadding="0" cellspacing="0" width="100%" align="left" style="border-spacing: 10px; border-collapse: separate; border: 1px solid #ddd;">
                        <th align="left">Produk</th>
                        <th align="left">Harga</th>
                        <th align="left">Volume</th>
                        <th align="left">Satuan</th>
                        <th align="left">Jumlah</th>
                        <tbody>
                            @foreach($dataProducts as $row)
                            <tr class="cart_item">
                                <td>
                                    <a>
                                        <img align="center" src="http://locarvest.com/storage/{{$row->image}}" alt=""
                                            width="75px" height="50px">
                                    </a>
                                    <a>{{ $row->name }}</a>
                                </td>
                                <td>
                                    <span class="amount" value="">Rp. {{ str_replace(',', '.',
                                        number_format($row->price )) }},00</span>
                                </td>
                                <td>
                                    <div class="quantity">
                                        {{ $row->qty }}
                                    </div>
                                </td>
                                <td>
                                    <span class="amount">Rp. {{ str_replace(',', '.',
                                        number_format($row->orderDetailTotal )) }},00</span>
                                </td>
                            </tr>
                            @endforeach
                            <tr>
                                <td colspan="4" height="15" style="font-size: 15px; line-height: 15px;">
                                    <hr>
                                </td>
                            </tr>
                            <tr style="border-top: 1px solid #e0e0e0;">
                                <th colspan="4">Subtotal</th>
                                <td>Rp. {{str_replace(',', '.', number_format($total_price)) }},00</td>
                            </tr>
                            <tr>
                                <th colspan="4">Pengiriman</th>
                                <td>Rp. {{ str_replace(',', '.', number_format($dataProducts[0]->orderTotal -
                                    $total_price)) }},00</td>
                            </tr>
                            <tr>
                                <th colspan="4">Total</th>
                                <td><strong>Rp. {{ str_replace(',', '.', number_format($dataProducts[0]->orderTotal))
                                        }},00</strong></td>
                            </tr>
                            <tr>
                                <td colspan="4" height="15" style="font-size: 15px; line-height: 15px;">
                                    <hr>
                                </td>
                            </tr>
                        </tbody>
                </table>

        <tr>
            <td colspan="4" height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
        </tr>

        <table align="center">

            <tr>
                <td colspan="4" style="color: #ffffff; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;">
                    <div style="line-height: 24px">
                        <p align="center">Segera lakukan pembayaran dan upload bukti transfer dengan cara : </p><br />
                    </div>
                </td>
            </tr>

            <tr>
                <td colspan="4" style="color: #ffffff; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;">
                    <div style="line-height: 24px">
                        1. Login dan masuk ke profile anda, atau klik <a href="http://locarvest.com/user-profile">
                            <font color="#3b7795"><b>disini</b></font>
                        </a>
                    </div>
                </td>
            </tr>

            <tr>
                <td style="color: #ffffff; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif;">
                    2. Pilih tab
                </td>
                <td>
                    <img style="display: block; height: 20px;" src="https://locarvest.com/assets/images/konfirmasiPembayaran.png"
                        alt="" />
                </td>
            </tr>

            <tr>
                <td style="color: #ffffff; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;">
                    <div style="line-height: 24px">
                        3. Tekan tombol
                    </div>
                </td>
                <td>
                    <img style="display: block; height: 20px;" src="http://locarvest.com/assets/images/tombolUpload.png"
                        alt="" />
                </td>
                <td>

                </td>
            </tr>

            <tr>
                <td colspan="4" style="color: #ffffff; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;">
                    <div style="line-height: 24px">
                        4. Upload gambar dan masukkan informasi bank dan no. rekening pengirim.
                    </div>
                </td>
            </tr>

        </table>

        </tr>
    </table>

    </td>
    </tr>

    <tr>
        <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
    </tr>



    </table>

    </td>
    </tr>


    </table>
		<table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590">
                    <tr>
                        <td colspan="4" height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center" style="color: #343434; font-size: 24px; font-family: Quicksand, Calibri, sans-serif; font-weight:700;letter-spacing: 3px; line-height: 35px;"
                            class="main-header">
                            <div style="line-height: 35px">
                                TANDA TERIMA

                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">                                                                 
                            <table border="0" width="40" align="center" cellpadding="0" cellspacing="0" bgcolor="eeeeee">
                                <tr>
                                    <td height="2" style="font-size: 2px; line-height: 2px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td  style="font-size: 15px; line-height: 20px;">Tanggal </td>
                        <td  style="font-size: 15px; line-height: 20px;">: {{$date}} </td>
                        <td>
                        <td>
                        <td>
                    </tr>
                    <tr>
                        <td height="20" style="font-size: 15px; line-height: 20px;">Dari </td>
                        <td height="20" style="font-size: 15px; line-height: 20px;"> : {{$sender}} </td>
                        <td>
                        <td>
                        <td>
                    </tr>
                    <tr>
                        <td  height="20" style="width:100px; font-size: 15px; line-height: 20px;">Kepada </td>
                        <td  height="20" style="width:70%; font-size: 15px; line-height: 20px;">: {{$receiver}} </td>
                        <td>
                        <td>
                        <td>
                    </tr>
                    <tr>
                        <td height="20" style="font-size: 15px; line-height: 20px;">Alamat </td>
                        <td height="20" style="font-size: 15px; line-height: 20px;">: {{$dataProducts[0]->address}} </td>
                        <td>
                        <td>
                        <td>
                    </tr>
                    <tr>
                        <td height="20" style="font-size: 15px; line-height: 20px;">Hp </td>
                        <td height="20" style="font-size: 15px; line-height: 20px;">: {{$phone}} </td>
                        <td>
                        <td>
                            <td>
                    </tr>
                    <tr>
                        <td height="20" style="font-size: 15px; line-height: 20px;">Pembayaran </td>
                        <td height="20" style="font-size: 15px; line-height: 20px;">: {{ $dataProducts[0]->payment_method}} </td>
                        <td>
                        <td>
                         <td>
                    </tr>
					<tr>
                        <td height="20" style="font-size: 15px; line-height: 20px;">Total </td>
                        <td height="20" style="font-size: 15px; line-height: 20px;">: <strong>Rp. {{ str_replace(',', '.', number_format($dataProducts[0]->orderTotal))
                                        }},00</strong> </td>
                        <td>
                        <td>
                         <td>
                    </tr>
					<tr>
                        <td colspan="4" height="20" style="font-size: 15px; line-height: 20px;">Nama Penerima </td>
                     
                                 <td>
                    
                    </tr>
					 <tr>
                        <td colspan="4" height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                    </tr>
					 <tr>
                        <td colspan="4" height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                    </tr>
					<tr>
                        <td height="20" style="font-size: 15px; line-height: 20px;">TTD </td>
                        <td height="20" style="font-size: 15px; line-height: 20px;"> </td>
                        <td>
                        <td>
                         <td>
                    </tr>
	</table>

    <!-- end section -->
<hr>
    <!-- contact section -->
    <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="bg_color">

        <tr>
            <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
        </tr>

        <tr>
            <td align="center">
                <table border="0" align="center" width="590" cellpadding="0" cellspacing="0" class="container590 bg_color">

                    <tr>
                        <td>
                            <table border="0" width="300" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"
                                class="container590">

                                <tr>
                                    <!-- logo -->
                                    <td align="left">
                                        <a href="" style="display: block; border-style: none !important; border: 0 !important;"><img
                                                width="80" border="0" style="display: block; width: 80px;" src="http://locarvest.com/assets/images/LOCARVEST.png"
                                                alt="" /></a>
                                    </td>
                                </tr>

                                <tr>
                                    <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td align="left" style="color: #888888; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 23px;"
                                        class="text_color">
                                        <div style="color: #333333; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; font-weight: 600; mso-line-height-rule: exactly; line-height: 23px;">

                                            Email us: <br /> <a href="mailto:" style="color: #888888; font-size: 14px; font-family: 'Hind Siliguri', Calibri, Sans-serif; font-weight: 400;">locarvest@gmail.com</a>

                                        </div>
                                    </td>
                                </tr>

                            </table>

                            <table border="0" width="2" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"
                                class="container590">
                                <tr>
                                    <td width="2" height="10" style="font-size: 10px; line-height: 10px;"></td>
                                </tr>
                            </table>

                            <table border="0" width="200" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"
                                class="container590">

                                <tr>
                                    <td class="hide" height="45" style="font-size: 45px; line-height: 45px;">&nbsp;</td>
                                </tr>



                                <tr>
                                    <td height="15" style="font-size: 15px; line-height: 15px;">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td>
                                        <table border="0" align="right" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <a href="#" style="display: block; border-style: none !important; border: 0 !important;"><img
                                                            width="24" border="0" style="display: block;" src="http://i.imgur.com/Qc3zTxn.png"
                                                            alt=""></a>
                                                </td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                <td>
                                                    <a href="#" style="display: block; border-style: none !important; border: 0 !important;"><img
                                                            width="24" border="0" style="display: block;" src="http://i.imgur.com/RBRORq1.png"
                                                            alt=""></a>
                                                </td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                <td>
                                                    <a href="#" style="display: block; border-style: none !important; border: 0 !important;"><img
                                                            width="24" border="0" style="display: block;" src="http://i.imgur.com/Wji3af6.png"
                                                            alt=""></a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
        </tr>

    </table>
    <!-- end section -->

    <!-- footer ====== -->
    <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="#7fca90" class="bg_color">
        <tr>
            <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
        </tr>
    </table>
    <!-- end footer ====== -->

</body>

</html>