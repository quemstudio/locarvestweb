<table>
                                <tbody>
                                <tr class="cart-subtotal">
                                    <th colspan="2">Subtotal</th>
                                    <td id="loading">Rp. <?php echo Cart::subtotal(); ?></td>
                                </tr>
                                <tr class="shipping">
                                    <th>Shipping</th>
                                    @if(empty ( $shipping ))
                                        <td>Choose Delivery Address</td>
                                        @else
                                        <th id="dstLoading">{{$addresses[0]->distance/1000}} Km</th>
                                        <td id="shippingLoading">Rp. {{ str_replace(',', '.', number_format($shipping)) }}</td>
                                    @endif

                                </tr>
                                <tr class="order-total">
                                    <th colspan="2">Total</th>
                                    @if(empty ( $shipping ))
                                        <td>----</td>
                                    @else
                                        <td id="totalShippingLoading"><strong>Rp. {{ str_replace(',', '.', number_format($total)) }}</strong></td>
                                    @endif

                                </tr>
                                </tbody>
                            </table>