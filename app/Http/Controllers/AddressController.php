<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Address;
use App\User;
use GoogleDistanceMatrix;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $addresses = Address::all();
        $response = [
            'addresses' => $addresses
        ];
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("address.add");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        // $distanceMatrix = new GoogleDistanceMatrix('YOUR API KEY');
        // $distance = $distanceMatrix->setLanguage('cs')
        // ->addOrigin('-6.902273,107.621686')
        // ->addDestination('-6.90059012642745,107.6085109893188')
        // ->sendRequest();
        
        $api = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=-6.933240,107.635499&destinations=".$request->lat."%2C".$request->long."&key=AIzaSyC5cOgK1Qryl6gOwf-tdoN2-R5ThKYjwmU";
        $distance = file_get_contents($api);
        $details = json_decode($distance, TRUE);

        $dstValue = $details['rows'][0]['elements'][0]['distance']['value'];
        $request->request->add(['distance' => $dstValue]);
        Address::create($request->all());

//        return $details;
        // return $request->long;

        return redirect()->back()->with('success', 'Tambah Alamat Sukses!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Address::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $address = Address::findOrFail($id);
        return view("address.edit", compact('address'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $address = Address::findOrFail($id);
        $address->update($request->all());

        return 'asdf';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $address = Address::findOrFail($id);
        $address->delete();

        return response()->json(null, 204);
    }

    public function updateAddress(Request $request)
    {
        $address = Address::find($request->id);

        return view('updateAddress', compact('address'));
    }


}
