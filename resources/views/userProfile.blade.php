<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 05/01/18
 * Time: 19.54
 */
?>
@extends('layouts.app')
@section('content')<div id="main">
    <div class="section section-bg-10 pt-11 pb-17">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="page-title text-center">MY Account</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="section border-bottom pt-2 pb-2">
        <div class="container">
            <div class="row">
                <div class="col-sm-12" style="text-align:center">
                    <ul class="breadcrumbs">
                        <li><a href="index.html">My Profile</a></li>
                        <li><a href="index.html">My Address</a></li>
                        <li><a href="index.html">Confirm Payment</a></li>
                        <li><a href="index.html">History</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="section pt-7 pb-7">
        <div class="container">
            <div class="row">
                <h2>My Address</h2>
                <div class="col-sm-3">
                    <div class="commerce-login-form">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="entry-content">
                                    <font style="font-weight: bold;">Adman Dewi</font>
                                    <br/>
                                    <hr style="height: 1px;width: 100%;color: black;background: black;padding: 0px;margin: 0px;">
                                    <br>
                                    Pondok mutiara indah PMI 3 no 2, RT. 09/RW. 23 - 40513
                                    <br/>

                                    KAB TAPANULI TENGAH
                                    <br/>
                                    Indonesia
                                    <br/>
                                    085720506556
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-wrap" style="text-align:right">
                                    <a href="" class="view-cart btn btn-success">Edit</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="commerce-login-form">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="entry-content">
                                    <font style="font-weight: bold;">Adman Dewi</font>
                                    <br/>
                                    <hr style="height: 1px;width: 100%;color: black;background: black;padding: 0px;margin: 0px;">
                                    <br>
                                    Pondok mutiara indah PMI 3 no 2, RT. 09/RW. 23 - 40513
                                    <br/>

                                    KAB TAPANULI TENGAH
                                    <br/>
                                    Indonesia
                                    <br/>
                                    085720506556
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-wrap" style="text-align:right">
                                    <a href="" class="view-cart btn btn-success">Edit</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="commerce-login-form">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="entry-content">
                                    <font style="font-weight: bold;">Adman Dewi</font>
                                    <br/>
                                    <hr style="height: 1px;width: 100%;color: black;background: black;padding: 0px;margin: 0px;">
                                    <br>
                                    Pondok mutiara indah PMI 3 no 2, RT. 09/RW. 23 - 40513
                                    <br/>

                                    KAB TAPANULI TENGAH
                                    <br/>
                                    Indonesia
                                    <br/>
                                    085720506556
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-wrap" style="text-align:right">
                                    <a href="" class="view-cart btn btn-success">Edit</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="commerce-login-form">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="entry-content">
                                    <font style="font-weight: bold;">Adman Dewi</font>
                                    <br/>
                                    <hr style="height: 1px;width: 100%;color: black;background: black;padding: 0px;margin: 0px;">
                                    <br>
                                    Pondok mutiara indah PMI 3 no 2, RT. 09/RW. 23 - 40513
                                    <br/>

                                    KAB TAPANULI TENGAH
                                    <br/>
                                    Indonesia
                                    <br/>
                                    085720506556
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-wrap" style="text-align:right">
                                    <a href="" class="view-cart btn btn-success">Edit</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="commerce-login-form">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="entry-content">
                                    <font style="font-weight: bold;">Adman Dewi</font>
                                    <br/>
                                    <hr style="height: 1px;width: 100%;color: black;background: black;padding: 0px;margin: 0px;">
                                    <br>
                                    Pondok mutiara indah PMI 3 no 2, RT. 09/RW. 23 - 40513
                                    <br/>

                                    KAB TAPANULI TENGAH
                                    <br/>
                                    Indonesia
                                    <br/>
                                    085720506556
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-wrap" style="text-align:right">
                                    <a href="" class="view-cart btn btn-success">Edit</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="commerce-login-form">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="entry-content">
                                    <font style="font-weight: bold;">Adman Dewi</font>
                                    <br/>
                                    <hr style="height: 1px;width: 100%;color: black;background: black;padding: 0px;margin: 0px;">
                                    <br>
                                    Pondok mutiara indah PMI 3 no 2, RT. 09/RW. 23 - 40513
                                    <br/>

                                    KAB TAPANULI TENGAH
                                    <br/>
                                    Indonesia
                                    <br/>
                                    085720506556
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-wrap" style="text-align:right">
                                    <a href="" class="view-cart btn btn-success">Edit</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection