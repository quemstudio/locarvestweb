@extends('layouts.app')
@section('content')
<div id="main">
    <div class="section section-bg-10 pt-11 pb-17">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                @if(!empty($products[0]))
                    <h2 class="page-title text-center">{{$products[0]->categoryName}}</h2>
                @else
                    <h2 class="page-title text-center">Tidak Ada Produk Dalam Kategori Ini :(</h2>
                @endif
                    
                </div>
            </div>
        </div>
    </div>
    <div class="section border-bottom pt-2 pb-2">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                @if(!empty($products[0]))
                    <ul class="breadcrumbs">
                        <li><a href="{{URL::to('/')}}">Home</a></li>
                        <li><a href="{{route('categories.index')}}">Kategori Produk</a></li>
                        <li>{{$products[0]->categoryName}}</li>
                    </ul>
                @else
                    <ul class="breadcrumbs">
                        <li><a href="{{URL::to('/')}}">Home</a></li>
                        <li><a href="{{route('categories.index')}}">Kategori Produk</a></li>
                    </ul>
                @endif
                </div>
            </div>
        </div>
    </div>
    <div class="section pt-7 pb-7">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-md-push-3">
                    <div class="shop-filter">
                        <div class="col-md-6">
                            
                        </div>
                        <div class="col-md-6">
                            <div class="shop-filter-right">
                            @if(!empty($products[0]))
                                <form class="commerce-ordering">
                                    <select name="orderby" id="orderby" class="orderby" onchange="catSortBy('{{ $products[0]->category_id }}')">
                                        <option selected="true" disabled="disabled">Urut berdasarkan</option>
                                        <option value="new">Produk baru</option>
                                        <option value="lh">Harga: rendah ke tinggi</option>
                                        <option value="hl">Harga: tinggi ke rendah</option>
                                    </select>
                                </form>
                            @endif
                            </div>
                        </div>
                    </div>
                    <br><br>

                    <div class="product-grid" id="product-grid">
                        {{--<div id="addCart"></div>--}}
                        @if(!empty($products[0]))
                        @foreach($products as $product)
                        <div class="col-md-4 col-sm-6 product-item text-center mb-3">
                            <div class="product-thumb">
                                <a href="{{route('products.show', $product->id)}}">
                                    <div class="badges">
                                        @if($product->best_offer == 'on')
                                            <span class="hot">Best</span>
                                        @else
                                        <!--<span class="hot">Hot</span>
                                                <span class="onsale">Sale!</span>-->
                                        @endif
                                    </div>
                                    @if(!empty($product->image))
                                        <img src="{{URL::asset('/storage/'.$product->image)}}" class="img-thumbnail" style="height: 200px" alt=""/>
                                    @else
                                        <img src="{{URL::asset('/assets/images/test.jpg')}}" class="img-thumbnail" style="height: 200px" alt=""/>
                                    @endif
                                </a>
                                <div class="product-action">
                                    {{--<form>--}}
                                        {{--<input name="id" type="hidden" data-value="{{ $product['id'] }}">--}}
                                        {{--<input name="name" type="hidden" value="{{ $product['name'] }}">--}}
                                        {{--<input name="qty" type="hidden" value="1">--}}
                                        {{--<input name="price" type="hidden" value="{{ $product['price'] }}">--}}
                                        <span class="add-to-cart" style="cursor: pointer" id="btn-addCart" onclick="addCartTemp('{{ $product->id }}','{{ $product->name }}',1,'{{ $product->price }}','{{ $product->image }}')">
													<a data-toggle="tooltip" data-placement="top" title="Tambah ke cart"
                                                       id="btn-addCart" ></a>

												</span>
                                        {{--</form>--}}
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="{{route('products.show', $product->id)}}">
                                    <h2 class="title">{{ $product->name }}</h2>
                                    <span class="price">
													<ins>Rp. {{ str_replace(',', '.', number_format($product->price)) }}</ins>
												</span>
                                </a>
                            </div>
                        </div>
                        @endforeach
                        <div class="col-md-4 col-md-offset-4">
                                {{ $products->appends(request()->input())->links() }}
                        </div> 
                        @else
                        <div class="col-md-8 col-md-offset-2 text-center mb-3">
                        <div class="jumbotron jumbotron-fluid">
                            <div class="container">
                                <h1 class="display-4">Tidak Ada Produk</h1>
                                <p class="lead">Saat ini tidak ada produk dalam kategori ini</p>
                            </div>
                        </div>
                        </div>
                        @endif
                        
                    </div>
                </div>
                <div class="col-md-3 col-md-pull-9">
                    <div class="sidebar">
                        <div class="widget widget-product-search">
                        <div class="form-search">
                            <input type="text" class="search-field" placeholder="Search products…" value="" id="searchProduct"
                               name="s"/>
                            <input type="submit" value="Search" id="searchBtn"/>
                        </div>
                        </div>
                        @if(!empty($products[0]))
                            <div class="widget widget_price_filter">
                                <h3 class="widget-title">Filter berdasarkan harga</h3>
                                <div class="price_slider_wrapper" style="padding-left: 15px; padding-right: 15px;">
                                    <div id="html5"></div>
                                    <div class="price_slider_amount">
                                        <button type="submit" class="button" onclick="filterPriceCategoryProduct('{{$products[0]->category_id}}')">Filter</button>
                                        <div class="price_label" style="display:block;">
                                            {{--IDR <input type="text" id="min_price"/> - IDR <input type="text" id="max_price"/>--}}
                                            IDR <span id="min_price"></span> &mdash; IDR <span id="max_price"></span>
                                        </div>
                                        <div class="clear"></div>
                                    </div>

                                </div>
                            </div>
                        @endif
                        <div class="widget widget-product-categories">
                            <h3 class="widget-title">Kategori Lainnya</h3>
                            <ul class="product-categories">
                                @foreach($countCat as $category)
                                    <li><a href="{{ route('categories.show',$category->id) }}">>&nbsp&nbsp{{ $category->name }}</a></li>
                                @endforeach
                            </ul>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
    <script type="text/javascript">

        var html5Slider = document.getElementById('html5');
        if(html5Slider){
            noUiSlider.create(html5Slider, {
                start: [ 0, 50000 ],
                connect: true,
                range: {
                    'min': 0,
                    'max': 50000
                },
                format: wNumb({
                    decimals: 0,
                })
            });

            var snapValues = [
                document.getElementById('min_price'),
                document.getElementById('max_price')
            ];
            html5Slider.noUiSlider.on('update', function( values, handle ) {
                snapValues[handle].innerHTML = values[handle];
            });
        }




    </script>
@endpush

