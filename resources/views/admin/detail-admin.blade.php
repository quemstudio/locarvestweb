<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 16/01/18
 * Time: 19.32
 */
?>
@extends('layouts.appadmin')
@section('content')
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif(session('error'))
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-header">
                    <h3><i class="icon-target"></i> <strong>Data</strong> Admin</h3>
                </div>
                <div class="panel-content">
                    <div class="row">
                        <form class=" form-horizontal" method="post" action="{{ route('admin.update.submit', $admin->id)}}">
                            {{ csrf_field() }} {{method_field('PUT')}}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Email</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="email" placeholder="" id="email"
                                               name="email"
                                               required="required" value="{{$admin->email}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Nama Depan</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="text" placeholder="" id="first_name"
                                               name="first_name"
                                               required="required" value="{{$admin->first_name}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Nama Belakang</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="text" placeholder="" id="last_name" name="last_name" value="{{$admin->last_name}}">
                                    </div>
                                </div>
                                @if($admin->role == 'superadmin')
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Role </label>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <select class="form-control" name="role" id="role" style="width:100%;">
                                                {{--@if($admin->role == 'admin')--}}
                                                    <option value="admin" selected>Admin</option>
                                                    {{--<option value="superadmin">Super Admin</option>--}}
                                                {{--@else--}}
                                                    {{--<option value="admin">Admin</option>--}}
                                                    {{--<option value="superadmin" selected>Super Admin</option>--}}
                                                {{--@endif--}}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                @endif

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary btn-square" style="float:right">Update Data</button>
                                        <a href="{{route('admin.admin.page')}}"><button type="button" class="btn btn-danger" style="float:right">Batal</button></a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
    {{--<div class="row">--}}
        {{--<div class="col-md-12">--}}
            {{--<div class="panel">--}}
                {{--<div class="panel-header">--}}
                    {{--<h3><i class="icon-target"></i> <strong>Update </strong> Password</h3>--}}
                {{--</div>--}}
                {{--<div class="panel-content">--}}
                    {{--<div class="row">--}}
                        {{--<form class=" form-horizontal" method="post" action="{{ route('admin.add.submit') }}">--}}
                            {{--{{ csrf_field() }}--}}
                            {{--<div class="col-md-12">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label class="col-sm-3 control-label">Password</label>--}}
                                    {{--<div class="col-sm-9">--}}
                                        {{--<input class="form-control" type="password" placeholder="" id="password"--}}
                                               {{--name="password"--}}
                                               {{--required="required">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="form-group">--}}
                                    {{--<label class="col-sm-3 control-label">Repassword</label>--}}
                                    {{--<div class="col-sm-9">--}}
                                        {{--<input class="form-control" type="password" placeholder="" id="password_confirmation"--}}
                                               {{--name="password_confirmation"--}}
                                               {{--required="required">--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="form-group">--}}
                                    {{--<div class="col-sm-12">--}}
                                        {{--<button type="submit" class="btn btn-primary btn-square" style="float:right">Update Password</button>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</form>--}}
                    {{--</div>--}}

                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
@endsection