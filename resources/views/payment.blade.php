<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 05/01/18
 * Time: 19.56
 */
?>
@extends('layouts.app')
@section('content')
<div id="main">
<div class="section pt-7 pb-7">
    <div class="container">
        @if(Cart::count() && !empty(Session::get('shipping')) && !empty( Session::get('addressId')) )
        <div class="row">
            <form>	
            <div class="col-md-8 col col-md-offset-2">
                
                <div class="commerce">
                    <div class="table-responsive">
                            <table class="table table-striped" id="tableSelect" style="cursor:pointer">
                                <thead>
                                <tr>
                                    <th style="text-align: center;"></th>
                                    <th>Bank</th>
                                    <th>Nama Akun</th>
                                    <th>No. Rekening</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td style="text-align: center;"><input type="radio" name="pilih" value="BNI"></td>
                                    <td><img src="{{URL::asset('/assets/images/bnilogo.jpg')}}" style="max-width: 75px;" /></td>
                                    <td>Ajeng Sesy Nur Pratiwi</td>
                                    <td>0283064914</td>
                                </tr>
                                </tbody>

                            </table>  
                          </div>
                </div>
            </div>
            
            
            <div class="col-md-8 col-md-offset-2">
                    <div class="commerce">
                                    <div class="row">
                                        <div class="md-col-12">
                                            <div class="cart-totals">
                                            <div class="proceed-to-checkout" id="payment" onclick="paymentConfirm()" style="cursor:pointer">
                                                <a id="fontresponsive" style="letter-spacing:0;">LANJUT KE KONFIRMASI ORDER</a>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                    </div>
            </div>
        </form>
        </div>
            @else
            <div class="row">
                <div class="md-col-12">
                    <div class="col-md-8 col-md-offset-2 text-center mb-3">
                        <div class="jumbotron jumbotron-fluid">
                            <div class="container">
                                <h1 class="display-4">Order tidak dapat diproses</h1>
                                <p class="lead">Tidak ada produk dalam keranjang belanja dan/atau informasi order anda tidak valid</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
    </div>
</div>
</div>

@endsection
@push('scripts')
<script type="text/javascript">
$('.table tr').click(function() {
    $(this).children('td').children('input').prop('checked', true);
     
     $('.table tr').removeClass('selected');
     $(this).toggleClass('selected');
   
   });
</script>
@endpush

<script type="text/javascript">

</script>