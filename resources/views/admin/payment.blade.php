<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 16/01/18
 * Time: 19.35
 */
?>
@extends('layouts.appadmin')
@section('content')
    <style>
        .modal-content {
            width: 100%;
        }
    </style>
    <!-- BEGIN PAGE CONTENT -->
    <div class="header">
        <h2>Data <strong>Pembayaran</strong></h2>
    </div>
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif(session('error'))
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    @endif
    <div class="row">
        <div class="col-lg-12 portlets">
            <div class="panel">
                {{--<div class="panel-header ">--}}
                {{--<h3><a href="add-payment.html"><button type="button" class="btn btn-primary">+ Add Payment</button></a></h3>--}}
                {{--</div>--}}
                <div class="panel-content">
                    <div class="filter-left panel-content pagination2 table-responsive">
                        <table class="table table-hover table-dynamic table-tools" data-table-name="Data Payments">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Nomor Order</th>
                                <th>Bank</th>
                                <th class='hidden-350'>Nomor Rekening</th>
                                <th class='hidden-1024'>Gambar</th>
                                <th class='hidden-480'>Total Harga</th>
                                <th class='hidden-480'>Status</th>
                                <th class='hidden-480' style="min-width: 150px"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($billings as $data)
                                @php
                                    $order = \App\Order::findOrFail($data->order_id);
                                @endphp
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td>{{$order->order_number}}</td>
                                    <td>{{$data->bank}}</td>
                                    <td class='hidden-350'>{{$data->account_number}}</td>
                                    {{--<td class='hidden-1024' style="cursor: pointer">--}}
                                    <td>
                                        <img style="width: 50px;" data-toggle="modal" data-target="#modal-image{{$data->id}}"
                                             src="{{URL::asset('/storage/'.$data->confirmation_pic)}}" alt="image"
                                             class="img-responsive modal-img basic-modal-img">
                                    </td>
                                    <td class='hidden-1024'>{{$order->total_price}}</td>
                                    <td class='hidden-480'>{{$data->status}}</td>
                                    @if($data->status == 'unpaid')
                                        <td class='hidden-480'>
                                            <div class="btn-group">
                                                {{--<a class="btn btn-success" href="{{route('payment.change.submit', $data['id'])}}">--}}
                                                {{--<i style="padding:0px; margin:0px" class="fa fa-edit"></i>--}}
                                                {{--</a>--}}
                                                <button class="btn btn-success" type="button"
                                                        id="danger-alert"
                                                        data-tr="tr_{{$data->id}}"
                                                        data-toggle="modal" data-target="#modal-edit{{$data->id}}">
                                                    <i style="padding:0px; margin:0px" class="fa fa-upload"></i>
                                                </button>
                                                <button class="btn btn-danger" type="button"
                                                        id="danger-alert"
                                                        data-tr="tr_{{$data->id}}"
                                                        onclick="changeStatus('{{$data->id}}', '{{$data->order_id}}', '{{$order->order_number}}')">
                                                    <i style="padding:0px; margin:0px" class="fa fa-edit"></i>
                                                </button>
                                            </div>
                                        </td>
                                    @else
                                        <td>
                                            <button class="btn btn-success" type="button"
                                                    id="danger-alert"
                                                    data-tr="tr_{{$data->id}}"
                                                    data-toggle="modal" data-target="#modal-edit{{$data->id}}">
                                                <i style="padding:0px; margin:0px" class="fa fa-upload"></i>
                                            </button>
                                        </td>
                                    @endif
                                </tr>

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @foreach($billings as $data)
        @php
            $order = \App\Order::findOrFail($data->order_id);
        @endphp

        {{-- Modal Detail Gambar --}}
        <div class="modal fade modal-image" id="modal-image{{$data->id}}" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            <i class="icons-office-52"></i></button>
                    </div>
                    <div class="modal-body">
                        <img style="width: 100%" src="{{URL::asset('/storage/'.$data->confirmation_pic)}}" alt="picture 1" class="img-responsive">
                    </div>
                    <div class="modal-footer">
                        <p><strong>Order Number : {{$order->order_number}}</strong></p>
                    </div>
                </div>
            </div>
        </div>

        {{--Modal Edit Payment--}}
        <div class="modal fade" id="modal-edit{{$data->id}}" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form class="form-horizontal" id="updateOrder" action="{{route('payment.upload.submit')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}} {{method_field('PUT')}}
                        <div class="modal-header bg-primary">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                        class="icons-office-52"></i></button>
                            <h4 class="modal-title"><strong>Edit</strong> Pembayaran</h4>
                        </div>
                        <div class="modal-body">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">No. Order</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="text" name="order_number" id="order_number" placeholder="No. Order" value="{{ $order->order_number }}" readonly>
                                        <input type="hidden" name="order_id" id="order_id" placeholder="Order Id" value="{{ $data->order_id }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Bank</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="text" name="bank" id="bank" placeholder="" value="{{$data->bank}}" size="40" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">No. Rekening</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="text" name="account_number" id="account_number" placeholder="" value="{{$data->account_number}}" size="40" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Bukti Transfer</label>
                                    <div class="col-sm-9">
                                        {{--<input type="file" class="form-control custom-file-input" id="transferFile" name="image" value="{{$data->confirmation_pic}}" required>--}}

                                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                            <input type="text" class="form-control" id="upload-file-info{{$data->id}}" readonly
                                                   value="{{$data->confirmation_pic}}">
                                            <span class="input-group-addon btn btn-default btn-file"><span
                                                        class="fileinput-new">Pilih...</span><span
                                                        class="fileinput-exists">Ganti</span>
                                                {{--<input type="file" name="image" onchange="$('#upload-file-info').val($(this).val().split('\\').pop());">--}}
                                                <input type="file" id="transferFile" name="image"
                                                       onchange="$('#upload-file-info{{$data->id}}').val($(this).val().split('\\').pop());">
                                            </span>
                                            <a href="#" class="input-group-addon btn btn-default fileinput-exists"
                                               data-dismiss="fileinput">Hapus</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary btn-embossed" type="submit">Confirm</button>
                            {{--<button onclick="updateBillingSubmit()" class="col-md-12">Confirm</button>--}}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
@endsection
@push('script')
    <script>
        function changeStatus(id, order_id, no_order) {
            swal({
                title: "Anda yakin ingin mengganti status order " + no_order + " ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn btn-danger btn-custom waves-effect waves-light',
                confirmButtonText: "Ya, ganti!",
                cancelButtonText: "Tidak",
                closeOnConfirm: false,
                cancelButtonClass: 'btn',
                buttonsStyling: false,
                reverseButtons: true
            }, function () {
                setTimeout(function () {
                    $.ajax({
                        url: 'payment/edit/' + id,
                        type: 'PUT',
                        dataType: 'json',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        success: function (data) {
                            if (data == null) {
                                swal("Sukses!", "Status order " + no_order + " berhasil diganti.", "success");
                                location.reload();
                            }
                            else
                                swal("Gagal!", "Gagal mengganti status.", "error");
                        }, error: function () {
                            swal("Error!", "Network error.", "error");
                        }
                    });
                }, 2000)
            });
        }
    </script>
@endpush