<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 05/01/18
 * Time: 19.47
 */
?>
@extends('layouts.app')
@section('content')
        <div class="section section-checkout pt-7 pb-7">
            <div class="container" id="finishOrder">
                @if(Cart::count() && !empty(Session::get('shipping')) && !empty( Session::get('addressId')) )
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="table-responsive pre-scrollable">
                        <table class="table shop-cart">
                            <th colspan="2">Produk</th>
                            <th>Qty {{$bank}}</th>
                            <th>Harga</th>
                            <tbody>
                            @foreach($carts as $row)
                            <tr class="cart_item">
                                <td class="product-thumbnail">
                                    <a>
                                        <img src="{{URL::asset($row->options->img)}}" alt="" style="max-width: 50px;">
                                    </a>
                                </td>
                                <td class="product-info">
                                    <a>{{ $row->name }}</a>
                                    {{--<span class="sub-title">Faucibus Tincidunt</span>--}}
                                    <span class="amount" value="">Rp. {{ str_replace(',', '.', number_format($row->price )) }}</span>
                                </td>
                                <td class="product-quantity">
                                    <div class="quantity">
                                    {{ $row->qty }}
                                    </div>
                                </td>
                                <td class="product-subtotal">
                                    <span class="amount" id="amount-{{ $row->id }}">Rp. {{ str_replace(',', '.', number_format($row->price*$row->qty )) }}</span>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                    </table>
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12"><hr></hr></div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="table-responsive">
                        <table class="table table-hover text-center">
                            <thead>
                                <tr class="table-active">
                                        <th colspan="3" class="text-center">Pembayaran</th>
                                </tr>
                            </thead>
                                <tbody>
                                <tr >
                                    <th colspan="2">Subtotal</th>
                                    <td>Rp. <?php echo Cart::subtotal(); ?></td>
                                </tr>
                                <tr >
                                    <th colspan="2">Pengiriman</th>
                                    @if(empty ( Session::get('shipping')))
                                        <td></td>
                                        <td>Choose Delivery Address</td>
                                        @else
                                        <td id="shippingLoading">Rp. {{ str_replace(',', '.', number_format(Session::get('shipping'))) }}</td>
                                    @endif

                                </tr>
                                <tr >
                                    <th colspan="2">Total</th>
                                    @if(empty ( Session::get('shipping')))
                                        <td>----</td>
                                    @else
                                        <td><strong>Rp. {{ str_replace(',', '.', number_format($total)) }}</strong></td>
                                    @endif

                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="commerce">
                            <div class="table-responsive">
                                <table class="table table-hover text-center">
                                <thead>
                                <tr class="table-active">
                                        <th colspan="3" class="text-center">Metode Pembayaran</th>
                                </tr>
                                </thead>
                                    <tr>
                                        <td><img src="{{URL::asset('/assets/images/bnilogo.jpg')}}" style="max-width: 75px;" /></td>
                                        <td>Ajeng Sesy Nur Pratiwi</td>
                                        <td>0283064914</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="md-col-12">
                        <div class="cart-totals">
                        <div class="proceed-to-checkout" id="payment" onclick="finishingOrder()" style="cursor:pointer">
                            <a>KONFIRMASI ORDER</a>
                        </div>
                        </div>
                    </div>
                </div>
                    @else
                    <div class="row">
                        <div class="md-col-12">
                            <div class="col-md-8 col-md-offset-2 text-center mb-3">
                                <div class="jumbotron jumbotron-fluid">
                                    <div class="container">
                                        <h1 class="display-4">Order tidak dapat diproses</h1>
                                        <p class="lead">Tidak ada produk dalam keranjang belanja dan/atau informasi order anda tidak valid</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
