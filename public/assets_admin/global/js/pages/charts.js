$(function () {


    /**** Line Charts: ChartJs ****/
    var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
    var lineChartData = {
        labels : ["January","February","March","April","May","June","July"],
        datasets : [
            {
                label: "My First dataset",
                backgroundColor : "rgba(220,220,220,0.2)",
                borderColor : "rgba(220,220,220,1)",
                pointBackgroundColor : "rgba(220,220,220,1)",
                pointBorderColor : "#fff",
                pointHoverBackgroundColor : "#fff",
                pointHoverBorderColor : "rgba(220,220,220,1)",
                data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
            },
            {
                label: "My Second dataset",
                backgroundColor : "rgba(49, 157, 181,0.2)",
                borderColor : "#319DB5",
                pointBackgroundColor : "#319DB5",
                pointBorderColor : "#fff",
                pointHoverBackgroundColor : "#fff",
                pointHoverBorderColor : "#319DB5",
                data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
            }
        ]
    }
    var ctx = document.getElementById("line-chart").getContext("2d");
    var myLineChart = new Chart(ctx, {
        type: 'line',
        data: lineChartData,
        options: {
            scales : {
                xAxes : [{
                    gridLines : {
                        color : 'rgba(0,0,0,0.05)'
                    }
                }],
                yAxes : [{
                    gridLines : {
                        color : 'rgba(0,0,0,0.05)'
                    }
                }]
            },
            legend:{
                display: false
            },
            tooltips : {
                cornerRadius: 0
            }
        }
    });


    /**** Line Charts: ChartJs ****/
    var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
    var lineChartData = {
        labels : ["January","February","March","April","May","June","July"],
        datasets : [
            {
                label: "My First dataset",
                backgroundColor : "rgba(220,220,220,0.2)",
                borderColor : "rgba(220,220,220,1)",
                pointBackgroundColor : "rgba(220,220,220,1)",
                pointBorderColor : "#fff",
                pointHoverBackgroundColor : "#fff",
                pointHoverBorderColor : "rgba(220,220,220,1)",
                data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
            },
            {
                label: "My Second dataset",
                backgroundColor : "rgba(49, 157, 181,0.2)",
                borderColor : "#319DB5",
                pointBackgroundColor : "#319DB5",
                pointBorderColor : "#fff",
                pointHoverBackgroundColor : "#fff",
                pointHoverBorderColor : "#319DB5",
                data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
            }
        ]
    }
    var ctx = document.getElementById("line-charts").getContext("2d");
    var myLineChart = new Chart(ctx, {
        type: 'line',
        data: lineChartData,
        options: {
            scales : {
                xAxes : [{
                    gridLines : {
                        color : 'rgba(0,0,0,0.05)'
                    }
                }],
                yAxes : [{
                    gridLines : {
                        color : 'rgba(0,0,0,0.05)'
                    }
                }]
            },
            legend:{
                display: false
            },
            tooltips : {
                cornerRadius: 0
            }
        }
    });


});