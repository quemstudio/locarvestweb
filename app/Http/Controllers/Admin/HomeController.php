<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Category;
use App\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        $posts = Post::all();
        $countCat = DB::table('products')
            ->join('categories','categories.id','=','products.category_id')
            ->select(DB::raw('count(products.name) as count, categories.name'))
            ->groupBy('products.category_id')
            ->get();

        return view('admin/login',compact('countCat','posts'));

    }
}
