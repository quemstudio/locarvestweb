<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 05/01/18
 * Time: 19.57
 */
?>

@extends('layouts.app')
@section('content')
    <div id="main">
        <div class="section section-bg-10 pt-11 pb-17">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="page-title text-center">Shipping</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="section border-bottom pt-2 pb-2">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <ul class="breadcrumbs">
                            <li><a href="index.html">Home</a></li>
                            <li>Shipping</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="section pt-7 pb-7">
            <div class="container">
                <div class="row">
                    <form>
                        <div class="col-sm-4">

                            <div class="commerce">
                                <h2>Billing Address</h2>
                                <div class="commerce-login-form">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Address <span class="required">*</span></label>
                                            <div class="form-wrap">
                                                <select name="country" id="country">
                                                    <option value="">Pondok mutiara indah PMI 3 no 2, RT. 09/RW. 23</option>
                                                    <option value="">Pondok mutiara indah PMI 3 no 2, RT. 09/RW. 23</option>
                                                    <option value="">Pondok mutiara indah PMI 3 no 2, RT. 09/RW. 23</option>
                                                    <option value="">Pondok mutiara indah PMI 3 no 2, RT. 09/RW. 23</option>
                                                    <option value="">Pondok mutiara indah PMI 3 no 2, RT. 09/RW. 23 </option>
                                                    <option value="">Pondok mutiara indah PMI 3 no 2, RT. 09/RW. 23</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="entry-content">
                                                <font style="font-weight: bold;">Adman Dewi</font>
                                                <br/>
                                                <hr style="height: 1px;width: 100%;color: black;background: black;padding: 0px;margin: 0px;">
                                                <br>
                                                Pondok mutiara indah PMI 3 no 2, RT. 09/RW. 23 - 40513
                                                <br/>

                                                KAB TAPANULI TENGAH
                                                <br/>
                                                Indonesia
                                                <br/>
                                                085720506556
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="commerce">
                                <h2>Delivery Address</h2>

                                <div class="commerce-login-form">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Address <span class="required">*</span></label>
                                            <div class="form-wrap">
                                                <select name="country" id="country">
                                                    <option value="">Pondok mutiara indah PMI 3 no 2, RT. 09/RW. 23</option>
                                                    <option value="">Pondok mutiara indah PMI 3 no 2, RT. 09/RW. 23</option>
                                                    <option value="">Pondok mutiara indah PMI 3 no 2, RT. 09/RW. 23</option>
                                                    <option value="">Pondok mutiara indah PMI 3 no 2, RT. 09/RW. 23</option>
                                                    <option value="">Pondok mutiara indah PMI 3 no 2, RT. 09/RW. 23 </option>
                                                    <option value="">Pondok mutiara indah PMI 3 no 2, RT. 09/RW. 23</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="entry-content">
                                                <font style="font-weight: bold;">Adman Dewi</font>
                                                <br/>
                                                <hr style="height: 1px;width: 100%;color: black;background: black;padding: 0px;margin: 0px;">
                                                <br>
                                                Pondok mutiara indah PMI 3 no 2, RT. 09/RW. 23 - 40513
                                                <br/>

                                                KAB TAPANULI TENGAH
                                                <br/>
                                                Indonesia
                                                <br/>
                                                085720506556
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="commerce">
                                <h2>Etc.</h2>

                                <div class="commerce-login-form">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-wrap" style="float:right;width:100%">
                                                <input type="submit" value="Add Address" style="width: 100%;" class="organik-btn brown">

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="commerce">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-wrap" style="float:right">
                                            <input type="submit" value="Next to Payment Method">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
