<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $primaryKey="id";
    protected $guarded = [];

    public function billings()
    {
        return $this->hasMany('App\Billing','order_id');
    }

    public function products()
    {
        return $this->belongsTo('App\Product','product_id');
    }

}
