<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 05/01/18
 * Time: 19.44
 */
?>
@extends('layouts.app')
@section('content')
    <div id="main">
        <div class="section section-bg-10 pt-11 pb-17">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="page-title text-center">Berita</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="section border-bottom pt-2 pb-2">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <ul class="breadcrumbs">
                            <li><a href="{{URL::to('/')}}">Home</a></li>
                            <li>Berita</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="section pt-7 pb-7">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        @foreach($posts as $post)
                            <div class="blog-list-item">
                                <div class="col-md-6">
                                    <div class="post-thumbnail">
                                        <a href="{{route('posts.show', $post['id'])}}">
                                            <img src="{{URL::asset('/storage/'.$post->cover)}}" alt=""/>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="post-content">
                                        <div class="entry-meta">
													<span class="posted-on">
														<i class="ion-calendar"></i>
														<span>{{ $post['updated_at']->format('F j, Y') }}</span>
													</span>
                                        </div>
                                        <a href="{{route('posts.show', $post['id'])}}">
                                            <h1 class="entry-title">{{ $post['title'] }}</h1>
                                        </a>
                                        <div class="entry-content">
                                            {!!  str_limit( $post['body'], $limit = 150, $end = '...') !!}  
                                        </div>
                                        <div class="entry-more">
                                            <a href="{{route('posts.show', $post['id'])}}">Baca Selengkapnya</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        {{--<div class="pagination">--}}
                            {{--<a class="prev page-numbers" href="#">Prev</a>--}}
                            {{--<a class="page-numbers" href="#">1</a>--}}
                            {{--<span class="page-numbers current">2</span>--}}
                            {{--<a class="page-numbers" href="#">3</a>--}}
                            {{--<a class="next page-numbers" href="#">Next</a>--}}
                        {{--</div>--}}
                    </div>
                    <div class="col-md-3">
                        <div class="sidebar">
                            <!--<div class="widget widget-product-search">
                                <form class="form-search">
                                    <input type="text" class="search-field" placeholder="Search products…" value="" name="s" />
                                    <input type="submit" value="Search" />
                                </form>
                            </div>-->
                            <div class="widget widget-product-categories">
                                <h3 class="widget-title">Kategori</h3>
                                <ul class="product-categories">
                                    @foreach($countCat as $count)
                                        <li><a href="#">{{ $count->name }}</a>
                                            <span
                                                    class="count">{{ $count->count }}</span></li>
                                    @endforeach
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
