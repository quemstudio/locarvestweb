<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'gender', 'role', 'phone_number', 'username', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $primaryKey="id";
//    protected $guarded = [];

    public function suppliers()
    {
        return $this->hasMany('App\Supplier','id');
    }

    public function customers()
    {
        return $this->hasMany('App\Customer','user_id');
    }

    public function addresses()
    {
        return $this->hasMany('App\Address','user_id');
    }

//    public function orders()
//    {
//        return $this->hasManyThrough('App\Order', 'App\Customer');
//    }
//
//    public function billings()
//    {
//        return $this->hasManyThrough('App\Billing', 'App\Order');
//    }
}
