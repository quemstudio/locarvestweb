<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 16/01/18
 * Time: 19.35
 */
?>
@extends('layouts.appadmin')
@section('content')
<!-- BEGIN PAGE CONTENT -->
    <div class="header">
        <h2>Data <strong>Coupon</strong></h2>
    </div>
    <div class="row">
        <div class="col-lg-12 portlets">
            <div class="panel">
                <div class="panel-header ">
                    <h3><a href="add-coupon.html"><button type="button" class="btn btn-primary">+ Add Coupon</button></a></h3>
                </div>
                <div class="panel-content">
                    <div class="filter-left table-responsive">
                        <table class="table table-dynamic table-tools" data-table-name="Data Coupons">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name Category Product</th>
                                <th class='hidden-480'>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Trident</td>
                                <td>
                                    Internet Explorer 4.0
                                </td>
                                <td class='hidden-480'>
                                    <div class="btn-group">
                                        <button class="btn btn-success" type="button"><i class="fa fa-edit"></i>
                                        </button>
                                        <button class="btn btn-danger" type="button"><i class="fa fa-trash-o"></i>
                                        </button>
                                    </div>

                                </td>
                            </tr>

                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection