<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Customer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Controllers\MailController as mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    //protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
//            'role' => 'required|string|max:255',
//            'gender' => 'required|string|max:1',
//            'phone_number' => 'required|string|max:255',
//            'username' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function create(Request $request)
    {
        $user =  User::create([
           'first_name' => $request->first_name,
           'last_name' => $request->last_name,
           'role' => 'customer',
           'email' => $request->email,
           'gender' => 'T',
           'phone_number' => null,
           'username' => $request->email,
           'password' => bcrypt($request->password),
        ]);
        $request->classification = 'individu';
        $userdetail =  Customer::create([
            'image' => '/assets/images/test.jpg',
            'classification' => $request->classification,
            'user_id' => $user->id,
         ]);



        return $user;


    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request)));

//        $this->guard()->login($user);

        //mail service-------------------------------------------
        $mail = new mail();
        $mail->registerMail($user);

//        return $this->registered($request, $user)
//            ?: redirect($this->redirectPath());

        return redirect('/')->with('success', 'Link Aktivasi Akun telah dikirim ke email anda');

    }
}
