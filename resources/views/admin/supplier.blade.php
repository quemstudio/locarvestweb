<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 16/01/18
 * Time: 19.27
 */
?>
@extends('layouts.appadmin')
@section('content')
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <style>
        #myMap {
            height: 350px;
        }
    </style>
    <div class="header">
        <h2>Data <strong>Petani</strong></h2>
    </div>
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif(session('error'))
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    @endif
    <div class="row">
        <div class="col-lg-12 portlets">
            <div class="panel">
                <div class="panel-header ">
                    <h3><a href="{{route('supplier.add.show')}}">
                            <button type="button" class="btn btn-primary">+ Tambah Petani
                            </button>
                        </a></h3>
                </div>
                <div class="panel-content">
                    <div class="filter-left panel-content pagination2 table-responsive">
                        <table class="table table-hover table-dynamic table-tools" data-table-name="Data Suppliers"
                               data-table-orientation="landscape">
                            <thead>
                            <tr>
                                <th style="width: 15px">No</th>
                                <th>Nama</th>
                                <th style="display: none">
                                    Email
                                </th>
                                <th style="display: none">
                                    Nomor Telepon
                                </th>
                                {{--<th style="display: none">--}}
                                {{--Jenis Kelamin--}}
                                {{--</th>--}}
                                <th style="display: none">
                                    Komoditas
                                </th>
                                <th style="display: none">
                                    Luas Lahan
                                </th>
                                {{--<th style="display: none">--}}
                                {{--Jadwal Tanam--}}
                                {{--</th>--}}
                                {{--<th style="display: none">--}}
                                {{--Jadwal Panen--}}
                                {{--</th>--}}
                                {{--<th style="display: none">--}}
                                {{--Kuantitas--}}
                                {{--</th>--}}
                                <th>Lokasi</th>
                                <th class='hidden-480' style="width: 150px"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($suppliers as $data)
                                @php
                                    $user = \App\User::find($data->user_id);
                                @endphp
                                <tr id="tr_{{$data->id}}">
                                    <td onclick="detailPetani('{{$data->id}}', '{{$user->phone_number}}', '{{$user->email}}','{{$user->gender}}')"
                                        style="cursor: pointer">{{$no++}}</td>
                                    <td onclick="detailPetani('{{$data->id}}', '{{$user->phone_number}}', '{{$user->email}}','{{$user->gender}}')"
                                        style="cursor: pointer">{{$data->name}}</td>
                                    <td style="display: none">
                                        {{$user->email}}
                                    </td>
                                    <td style="display: none">
                                        {{$user->phone_number}}
                                    </td>
                                    {{--<td style="display: none">--}}
                                    {{--{{$user->gender}}--}}
                                    {{--</td>--}}
                                    <td style="display: none">
                                        {{$data->name_plant}}
                                    </td>
                                    <td style="display: none">
                                        {{$data->land_area}}
                                    </td>
                                    {{--<td style="display: none">--}}
                                    {{--{{$data->planting_schedule}}--}}
                                    {{--</td>--}}
                                    {{--<td style="display: none">--}}
                                    {{--{{$data->harvest_schedule}}--}}
                                    {{--</td>--}}
                                    {{--<td style="display: none">--}}
                                    {{--{{$data->quantity_plant}}--}}
                                    {{--</td>--}}
                                    <td onclick="detailPetani('{{$data->id}}', '{{$user->phone_number}}', '{{$user->email}}','{{$user->gender}}')"
                                        style="cursor: pointer">{{$data->address}}</td>
                                    <td class='hidden-480'>
                                        <div class="btn-group">
                                            <a class="btn btn-success"
                                               href="{{route('supplier.edit.show' ,$data['id'])}}">
                                                <i style="padding:0px; margin:0px" class="fa fa-edit"></i>
                                            </a>
                                            <button class="btn btn-danger" type="button"
                                                    id="danger-alert"
                                                    data-tr="tr_{{$data->id}}"
                                                    onclick="deleteUser('{{$data->id}}', '{{$data->user_id}}', '{{$data->name}}')">
                                                <i style="padding:0px; margin:0px" class="fa fa-trash-o"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="add-petani" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form class=" form-horizontal" method="post" action="{{route('supplier.add.submit')}}"
                      enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="modal-header bg-primary">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                    class="icons-office-52"></i></button>
                        <h4 class="modal-title"><strong>Tambah</strong> Petani</h4>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Nama Depan</label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="text" placeholder="" name="first_name"
                                           required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Nama Belakang</label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="text" placeholder="" name="last_name"
                                           required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="email" placeholder="" name="email"
                                           required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Nomor Telepon</label>
                                <div class="col-sm-9">
                                    <input class="form-control input-sm" placeholder="" type="number"
                                           name="phone_number" required>
                                </div>
                            </div>

                            {{--<div class="form-group">--}}
                            {{--<label class="col-sm-3 control-label">Jenis Kelamin</label>--}}
                            {{--<div class="radio radio-inline">--}}
                            {{--<label><input type="radio" name="gender" data-radio="iradio_square-blue"--}}
                            {{--value="M" checked> Laki-laki</label>--}}
                            {{--<label><input type="radio" name="gender" data-radio="iradio_square-blue"--}}
                            {{--value="F"> Perempuan</label>--}}
                            {{--</div>--}}
                            {{--</div>--}}

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Komoditas yang Ditanam</label>
                                <div class="col-sm-9">
                                    <input class="form-control input-sm" placeholder="" type="text"
                                           name="name_plant" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Luas Lahan</label>
                                <div class="col-sm-9">
                                    <input class="form-control input-sm" placeholder="" type="number"
                                           name="land_area" required>
                                </div>
                            </div>

                            {{--<div class="form-group">--}}
                            {{--<label class="col-sm-3 control-label">Jadwal Tanam (per bulan)</label>--}}
                            {{--<div class="col-sm-9">--}}
                            {{--<input name="planting_schedule" class="b-datepicker form-control"--}}
                            {{--placeholder="Select a date..."--}}
                            {{--type="text">--}}
                            {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group">--}}
                            {{--<label class="col-sm-3 control-label">Perkiraan Tanggal Panen (per--}}
                            {{--bulan)</label>--}}
                            {{--<div class="col-sm-9">--}}
                            {{--<input name="harvest_schedule" class="b-datepicker form-control"--}}
                            {{--placeholder="Select a date..."--}}
                            {{--type="text">--}}
                            {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group">--}}
                            {{--<label class="col-sm-3 control-label">Akumulasi Perkiraan Kuantitas Panen (per--}}
                            {{--bulan)</label>--}}
                            {{--<div class="col-sm-9">--}}
                            {{--<input class="form-control input-sm" placeholder="" type="number"--}}
                            {{--name="quantity_plant" required>--}}
                            {{--</div>--}}
                            {{--</div>--}}

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Gambar</label>
                                <div class="col-sm-9">
                                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                        {{--<div class="form-control" data-trigger="fileinput">--}}
                                        {{--<i class="glyphicon glyphicon-file fileinput-exists"></i><span--}}
                                        {{--class="fileinput-filename"></span>--}}
                                        {{--</div>--}}
                                        <input type="text" class="form-control" id="upload-file-info" readonly>
                                        <span class="input-group-addon btn btn-default btn-file"><span
                                                    class="fileinput-new">Pilih...</span><span
                                                    class="fileinput-exists">Ganti</span>
                                                <input type="file" name="image"
                                                       onchange="$('#upload-file-info').val($(this).val().split('\\').pop());">
                                            </span>
                                        <a href="#" class="input-group-addon btn btn-default fileinput-exists"
                                           data-dismiss="fileinput">Hapus</a>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Description</label>
                                <div class="col-sm-9">
                                    <h3>Deskripsi <strong>Petani</strong></h3>
                                    <textarea style="height: 100px;" class="summernote bg-white"
                                              name="description"></textarea>
                                </div>

                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Alamat</label>
                                <div class="col-sm-9">
                                        <textarea class="form-control" placeholder="" name="address"
                                                  required></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="form-wrap">
                                        <!-- copy dari sini -->
                                        <div class="form-group input-group">
                                            <input type="text" id="search_location" class="form-control"
                                                   placeholder="Search location">
                                            <div class="input-group-btn">
                                                <button class="btn btn-default get_map" type="submit">
                                                    Search Lokasi
                                                </button>
                                            </div>
                                        </div>
                                        <div id="myMap" class="col-md-12"></div>
                                        <input id="address" class="form-control input-sm" type="text"/><br/>
                                        <input type="hidden" id="latitude" name="lat" placeholder="Latitude"
                                               required/>
                                        <input type="hidden" id="longitude" name="long" placeholder="Longitude"
                                               required/>
                                        <!-- copy sampe sini -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-embossed">Save changes
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="show-detail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content p-0 b-0">
                <div class="panel panel-color panel-primary">
                    <div class="panel-heading">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                    class="icons-office-52"></i></button>
                        <h4 class="modal-title"><strong>Detail</strong> Petani</h4>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-sm-6 control-label">Nama Lengkap</label>
                                <div class="col-sm-6">
                                    <label class="form-control"
                                           placeholder="Announcement"
                                           id="name-detail"></label>
                                </div>
                            </div>

                            {{--<div class="form-group">--}}
                                {{--<label class="col-sm-6 control-label">Email</label>--}}
                                {{--<div class="col-sm-6">--}}
                                    {{--<label class="form-control"--}}
                                           {{--placeholder="Announcement"--}}
                                           {{--id="email-detail"></label>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group">--}}
                                {{--<label class="col-sm-6 control-label">Nomor Telepon</label>--}}
                                {{--<div class="col-sm-6">--}}
                                    {{--<label class="form-control"--}}
                                           {{--placeholder="Announcement"--}}
                                           {{--id="phone-detail"></label>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group">--}}
                            {{--<label class="col-sm-6 control-label">Jenis Kelamin</label>--}}
                            {{--<div class="col-sm-6">--}}
                            {{--<label class="form-control"--}}
                            {{--placeholder="Announcement"--}}
                            {{--id="gender-detail"></label>--}}
                            {{--</div>--}}
                            {{--</div>--}}

                            <div class="form-group">
                                <label class="col-sm-6 control-label">Komoditas yang Ditanam</label>
                                <div class="col-sm-6">
                                    <label class="form-control"
                                           placeholder="Announcement"
                                           id="plant-detail"></label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-6 control-label">Luas Lahan (m<sup>2</sup>)</label>
                                <div class="col-sm-6">
                                    <label class="form-control"
                                           placeholder="Announcement"
                                           id="land-detail"></label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-6 control-label">Sertifikasi</label>
                                <div class="col-sm-6">
                                    <label class="form-control"
                                           placeholder=""
                                           id="certification"></label>
                                </div>
                            </div>

                            {{--<div class="form-group">--}}
                            {{--<label class="col-sm-6 control-label">Jadwal Tanam (per bulan)</label>--}}
                            {{--<div class="col-sm-6">--}}
                            {{--<label class="form-control"--}}
                            {{--placeholder="Announcement"--}}
                            {{--id="planting-detail"></label>--}}
                            {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group">--}}
                            {{--<label class="col-sm-6 control-label">Perkiraan Tanggal Panen (per--}}
                            {{--bulan)</label>--}}
                            {{--<div class="col-sm-6">--}}
                            {{--<label class="form-control"--}}
                            {{--placeholder="Announcement"--}}
                            {{--id="harvest-detail"></label>--}}
                            {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group">--}}
                            {{--<label class="col-sm-6 control-label">Kuantitas Panen (per--}}
                            {{--bulan)</label>--}}
                            {{--<div class="col-sm-6">--}}
                            {{--<label class="form-control"--}}
                            {{--placeholder="Announcement"--}}
                            {{--id="quantity-detail"></label>--}}
                            {{--</div>--}}
                            {{--</div>--}}

                            <div class="form-group">
                                <label class="col-sm-6 control-label">Deskripsi</label>
                                <div class="col-sm-6">
                                    <label class="form-control"
                                           placeholder=""
                                           id="description-detail"></label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-6 control-label">Alamat</label>
                                <div class="col-sm-6">
                                    <label class="form-control"
                                           placeholder=""
                                           id="address-detail"></label>
                                </div>
                            </div>

                            {{--<div class="form-group">--}}
                            {{--<div class="col-md-12">--}}
                            {{--<div class="form-wrap">--}}
                            {{--<!-- copy dari sini -->--}}
                            {{--<div class="form-group input-group">--}}
                            {{--<input type="text" id="search_location" class="form-control" placeholder="Search location">--}}
                            {{--<div class="input-group-btn">--}}
                            {{--<button class="btn btn-default get_map" type="submit">--}}
                            {{--Search Lokasi--}}
                            {{--</button>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--<div id="myMap" class="col-md-12"></div>--}}
                            {{--<input id="address" class="form-control input-sm" type="text"/><br/>--}}
                            {{--<input type="hidden" id="latitude" name="lat" placeholder="Latitude" required/>--}}
                            {{--<input type="hidden" id="longitude" name="long" placeholder="Longitude"--}}
                            {{--required/>--}}
                            {{--<!-- copy sampe sini -->--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}


                            {{--<div class="form-group">--}}
                            {{--<div class="col-md-12">--}}
                            {{--<div class="form-wrap">--}}
                            {{--<!-- copy dari sini -->--}}
                            {{--<div id="myMap" class="col-md-12"></div>--}}
                            {{--<input id="address" class="form-control input-sm" type="text"/><br/>--}}
                            {{--<input type="hidden" id="latitude" name="lat" placeholder="Latitude"--}}
                            {{--required/>--}}
                            {{--<input type="hidden" id="longitude" name="long" placeholder="Longitude"--}}
                            {{--required/>--}}
                            {{--<!-- copy sampe sini -->--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}


                            <div class="form-group">
                                <div class="col-sm-12" style="margin-top: 20px">
                                    {{--<a href="{{route('product.edit.show' , $data->id)}}">--}}
                                    {{--<button type="button" class="btn btn-primary"--}}
                                    {{--style="float:right">Edit</button>--}}
                                    {{--</a>--}}
                                    <a onclick="$('#show-detail').modal('hide');">
                                        <button type="button" class="btn btn-primary" style="float:right">Close</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>--}}
    {{--<script src="https://apis.google.com/js/platform.js" async defer></script>--}}
    {{--<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>--}}
    {{--<script type="text/javascript"--}}
            {{--src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC5cOgK1Qryl6gOwf-tdoN2-R5ThKYjwmU"></script>--}}
    {{--<script src="{{URL::asset('/assets/js/quem-maps-setting.js')}}"></script>--}}

    {{--<script type="application/javascript">--}}
        {{--google.maps.event.trigger(map, 'resize');--}}
        {{--map.setCenter(new google.maps.LatLng(-6.902273, 107.621686));--}}
    {{--</script>--}}
@endsection
@push('script')
    <script>
        function deleteUser(id, user_id, name) {
            swal({
                title: "Anda yakin ingin menghapus petani " + name + " ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn btn-danger btn-custom waves-effect waves-light',
                confirmButtonText: "Ya, hapus!",
                cancelButtonText: "Tidak",
                closeOnConfirm: false,
                cancelButtonClass: 'btn',
                buttonsStyling: false,
                reverseButtons: true
            }, function () {
                setTimeout(function () {
                    $.ajax({
                        url: 'supplier/' + id + '/' + user_id,
                        type: 'DELETE',
                        dataType: 'json',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        success: function (data) {
                            if (data == null) {
                                swal("Terhapus!", "Petani berhasil dihapus.", "success");
                                location.reload();
                            }
                            else
                                swal("Gagal!", "Gagal menghapus data.", "error");
                        }, error: function () {
                            swal("Error!", "Foreign key constraint fails.", "error");
//                            swal("Error!", "Failed to delete.", "error");
                        }
                    });
                }, 2000)
            });
        }

        function detailPetani(id, phone, email, gender) {

            $.ajax({
                url: 'petani/show_detail/' + id,
                beforeSend: function () {
                    $(document.body).css({'cursor': 'wait'});
                },
                error: function () {
                    $('#nameApp').html('<p>An error has occurred</p>');
                },
                dataType: 'json',
                success: function (data) {
                    $('#name-detail').text(data.name);
                    $('#phone-detail').text(phone);
                    // $('#email-detail').text(email);
                    // $('#gender-detail').text(gender);
                    $('#plant-detail').text(data.name_plant);
                    $('#land-detail').text(data.land_area);
                    if (data.certification != null)
                        $('#certification').text(data.certification);
                    else
                        $('#certification').text('-');
                    // $('#planting-detail').text(data.planting_schedule);
                    // $('#harvest-detail').text(data.harvest_schedule);
                    // $('#quantity-detail').text(data.quantity_plant);
                    if (data.description != null)
                        $('#description-detail').text(data.description);
                    else
                        $('#description-detail').text('-');
                    $('#address-detail').text(data.address);

                    $('#show-detail').modal('show');
                    $(document.body).css({'cursor': 'default'});
                },
                type: 'GET'
            });

        }
    </script>
@endpush