<div class="container">
                <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >      
                            <div class="panel panel-info">
                            <div class="panel-heading">
                              <h3 class="panel-title">{{$user[0]->first_name}} {{$user[0]->last_name}}</h3>
                            </div>
                            <div class="panel-body">
                              <div class="row">
                                {{--<div class="col-md-3 col-lg-3 " align="center">--}}
                                  {{--<img alt="User Pic" src="{{URL::asset($user[0]->customers[0]->image)}}" class="img-circle img-responsive">--}}
                                  {{--<div class="custom-file">--}}
                                    {{--<input type="file" id="profile_pic" name="image" required  class="hidden"/>--}}
                                    {{--<label  style="cursor: pointer;" class="custom-file-label" for="profile_pic">Ubah <i class="fa fa-image"></i></label>--}}
                                  {{--</div>--}}
                                {{--</div>--}}
                                
                                <!--<div class="col-xs-10 col-sm-10 hidden-md hidden-lg"> <br>
                                  <dl>
                                    <dt>DEPARTMENT:</dt>
                                    <dd>Administrator</dd>
                                    <dt>HIRE DATE</dt>
                                    <dd>11/12/2013</dd>
                                    <dt>DATE OF BIRTH</dt>
                                       <dd>11/12/2013</dd>
                                    <dt>GENDER</dt>
                                    <dd>Male</dd>
                                  </dl>
                                </div>-->
                                <div class=" col-md-12 col-lg-12 ">
                                  <table class="table table-user-information">
                                    <tbody>
                                      <tr>
                                        <td>First Name</td>
                                        <td><input type="text" id="first_name" name="first_name" value="{{$user[0]->first_name}}"></td>
                                      </tr>
                                      <tr>
                                        <td>Last Name</td>
                                        <td><input type="text" id="last_name" name="last_name" value="{{$user[0]->last_name}}"></td>
                                      </tr>
                                      <tr>
                                        <td>Phone Number</td>
                                        <td><input type="text" id="phone_number" name="phone_number" value="{{$user[0]->phone_number}}" onkeypress='return event.charCode >= 48 && event.charCode <= 57'></td>
                                      </tr>
                                      <tr>
                                      <td>Gender</td>
                                      <td>
                                      <div class="col-md-6">
                                      @if($user[0]->gender == 'M')
                                        <input class="form-check-input" type="radio" name="gender" id="gender" value="M" checked>
                                        <label class="form-check-label" for="inlineRadio1">M</label>&nbsp &nbsp
                                        
                                        <input class="form-check-input" type="radio" name="gender" id="gender" value="F">
                                        <label class="form-check-label" for="inlineRadio2">F</label>
                                        @elseif($user[0]->gender == 'F')
                                        <input class="form-check-input" type="radio" name="gender" id="gender" value="M">
                                        <label class="form-check-label" for="inlineRadio1">M</label>&nbsp &nbsp
                                        
                                        <input class="form-check-input" type="radio" name="gender" id="gender" value="F" checked>
                                        <label class="form-check-label" for="inlineRadio2">F</label>
                                        @else
                                        <input class="form-check-input" type="radio" name="gender" id="gender" value="M">
                                        <label class="form-check-label" for="inlineRadio1">M</label>&nbsp &nbsp
                                        
                                        <input class="form-check-input" type="radio" name="gender" id="gender" value="F">
                                        <label class="form-check-label" for="inlineRadio2">F</label>
                                        
                                        @endif
                                        </div>
                                      </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <button class="col-md-12" type="button" class="btn btn-primary" onclick="updateProfile()">Update Profile
                                    </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
            </div>