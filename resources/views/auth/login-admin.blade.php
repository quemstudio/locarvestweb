<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Locarvest Administrator</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="" name="description" />
    <meta content="quemstudio" name="author" />
    <link rel="shortcut icon" href="{{URL::asset('/assets/global/images/favicon.png')}}">
    <link href="{{URL::asset('/assets_admin/global/css/style.css')}}" rel="stylesheet">
    <link href="{{URL::asset('/assets_admin/global/css/ui.css')}}" rel="stylesheet">
    <link href="{{URL::asset('/assets_admin/global/plugins/bootstrap-loading/lada.min.css')}}" rel="stylesheet">
</head>
<body class="account separate-inputs" data-page="login">
<!-- BEGIN LOGIN BOX -->
<div class="container" id="login-block">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <div class="account-wall"style="text-align: center">
                <img src="http://locarvest.com/assets/images/LOCARVEST.png" style="width: 100px"/>
                <h2 style="color: #fff;padding-bottom: 10px;font-weight: bold">Locarvest Administrator</h2>
                <form class="form-signin" action="{{route('admin.login.submit')}}" method="post" role="form">
                    {{csrf_field()}}
                    <div class="append-icon">
                        <input type="text" name="email" id="username" class="form-control form-white username" placeholder="Email" required>
                        <i class="icon-user"></i>
                    </div>
                    <div class="append-icon m-b-20">
                        <input type="password" name="password" class="form-control form-white password" placeholder="Password" required>
                        <i class="icon-lock"></i>
                    </div>
                    <button type="submit" id="submit-form" class="btn btn-lg btn-danger btn-block ladda-button" data-style="expand-left">Sign In</button>
                    @if (session('success'))
                        <h5 class="alert alert-success" data-textAlign="center">
                            {{ session('success') }}
                        </h5>
                    @elseif(session('error'))
                        <h5 class="alert alert-error" data-textAlign="center" style="color: #f3e97a;font-weight: bold">
                            {{ session('error') }}
                        </h5>
                    @endif
                </form>

            </div>
        </div>
    </div>
    <p class="account-copyright">
        <span  class="align-middle">Copyright © 2018
            {{--<a style="color: #5fbd74" href="http://quem-studio.com">--}}
                {{--<img src="{{URL::asset('/assets/images/LogoPanjang-White.png')}}" style="height: 30px;background-color: transparent;vertical-align:middle;margin-bottom: 3px;border: none;" class="img-thumbnail" alt=""/>--}}
            {{--</a>--}}
            {{--and --}}
            {{--<a href="http://itik.tech">ITIK labs</a></span>.<span> All rights reserved.</span>--}}
            <a style="color: #5fbd74" href="http://locarvest.com">Locarvest.com</a></span>.<span>  All rights reserved.</span>
    </p>
</div>

{{--<script src="{{URL::asset('/assets_admin/global/plugins/jquery/jquery-3.1.0.min.js')}}"></script>--}}
{{--<script src="{{URL::asset('/assets_admin/global/plugins/jquery/jquery-migrate-3.0.0.min.js')}}'"></script>--}}
{{--<script src="{{URL::asset('/assets_admin/global/plugins/gsap/main-gsap.min.js')}}"></script>--}}
{{--<script src="{{URL::asset('/assets_admin/global/plugins/tether/js/tether.min.js')}}'"></script>--}}
{{--<script src="{{URL::asset('/assets_admin/global/plugins/bootstrap/js/bootstrap.min.js')}}"></script>--}}
{{--<script src="{{URL::asset('/assets_admin/global/plugins/backstretch/backstretch.min.js')}}'"></script>--}}
{{--<script src="{{URL::asset('/assets_admin/global/plugins/bootstrap-loading/lada.min.js')}}"></script>--}}
{{--<script src="{{URL::asset('/assets_admin/global/plugins/jquery-validation/jquery.validate.min.js')}}'"></script>--}}
{{--<script src="{{URL::asset('/assets_admin/global/plugins/jquery-validation/additional-methods.min.js')}}"></script>--}}
{{--<script src="{{URL::asset('/assets_admin/global/js/pages/login-v1.js')}}'"></script>--}}
</body>
</html>