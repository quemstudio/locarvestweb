<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 05/01/18
 * Time: 19.54
 */
?>
@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <style>
        #myMap {
            height: 350px;
        }
        #myMap2 {
            height: 350px;
        }
    </style>

    <div id="main">
        <div class="section section-bg-10 pb-17">
            <div class="container">
                @if (session('success'))
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="alert alert-success" style="text-align:center">
                                <span class="fa fa-check-circle"></span> {{ session('success') }}
                            </div>
                        </div>
                    </div>
                @endif
                <div class="row pt-11">
                    <div class="col-sm-12">
                        <h2 class="page-title text-center">My Account</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="section border-bottom pt-2 pb-2">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12" style="text-align:center">
                        <ul class="breadcrumbs">
                            <li style="cursor: pointer;"><a onclick="myProfile()">Profil</a></li>
                            <li style="cursor: pointer;"><a onclick="myAddress()">Alamat</a></li>
                            <li style="cursor: pointer;"><a onclick="myConfirmPayment()">Konfirmasi Pembayaran</a></li>
                            <li style="cursor: pointer;"><a onclick="myHistory()">History</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="section pt-4 pb-7" id="user">
            <div class="container">
                <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >      
                            <div class="panel panel-info">
                            <div class="panel-heading">
                              <h3 class="panel-title">{{$user[0]->first_name}} {{$user[0]->last_name}}</h3>
                            </div>
                            <div class="panel-body">
                              <div class="row">
                                {{--<div class="col-md-3 col-lg-3 " align="center">--}}
                                    {{--<img alt="User Pic" src="{{URL::asset($user[0]->customers[0]->image)}}" class="img-circle img-responsive">--}}
                                    {{--<div class="custom-file">--}}
                                        {{--<input type="file" id="profile_pic" name="image" required  class="hidden"/>--}}
                                        {{--<label  style="cursor: pointer;" class="custom-file-label" for="profile_pic">Ubah <i class="fa fa-image"></i></label>--}}
                                    {{--</div>--}}


                                {{--</div>--}}
                                {{----}}
                                <!--<div class="col-xs-10 col-sm-10 hidden-md hidden-lg"> <br>
                                  <dl>
                                    <dt>DEPARTMENT:</dt>
                                    <dd>Administrator</dd>
                                    <dt>HIRE DATE</dt>
                                    <dd>11/12/2013</dd>
                                    <dt>DATE OF BIRTH</dt>
                                       <dd>11/12/2013</dd>
                                    <dt>GENDER</dt>
                                    <dd>Male</dd>
                                  </dl>
                                </div>-->
                                <div class=" col-md-12 col-lg-12 ">
                                  <table class="table table-user-information">
                                    <tbody>
                                      <tr>
                                        <td>Name Depan</td>
                                        <td><input type="text" id="first_name" name="first_name" value="{{$user[0]->first_name}}"></td>
                                      </tr>
                                      <tr>
                                        <td>Nama Belakang</td>
                                        <td><input type="text" id="last_name" name="last_name" value="{{$user[0]->last_name}}"></td>
                                      </tr>
                                      <tr>
                                        <td>Nomor Telepon</td>
                                        <td><input type="text" id="phone_number" name="phone_number" value="{{$user[0]->phone_number}}" onkeypress='return event.charCode >= 48 && event.charCode <= 57'></td>
                                      </tr>
                                      <tr>
                                      <td>Gender</td>
                                      <td>
                                      <div class="col-md-6">
                                      @if($user[0]->gender == 'M')
                                        <input class="form-check-input" type="radio" name="gender" id="gender" value="M" checked>
                                        <label class="form-check-label" for="inlineRadio1">M</label>&nbsp &nbsp
                                        
                                        <input class="form-check-input" type="radio" name="gender" id="gender" value="F">
                                        <label class="form-check-label" for="inlineRadio2">F</label>
                                        @elseif($user[0]->gender == 'F')
                                        <input class="form-check-input" type="radio" name="gender" id="gender" value="M">
                                        <label class="form-check-label" for="inlineRadio1">M</label>&nbsp &nbsp
                                        
                                        <input class="form-check-input" type="radio" name="gender" id="gender" value="F" checked>
                                        <label class="form-check-label" for="inlineRadio2">F</label>
                                        @else
                                        <input class="form-check-input" type="radio" name="gender" id="gender" value="M">
                                        <label class="form-check-label" for="inlineRadio1">M</label>&nbsp &nbsp
                                        
                                        <input class="form-check-input" type="radio" name="gender" id="gender" value="F">
                                        <label class="form-check-label" for="inlineRadio2">F</label>
                                        
                                        @endif
                                        </div>
                                      </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <button class="col-md-12" type="button" class="btn btn-primary" onclick="updateProfile()">Perbarui Profil
                                    </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
            </div>
        </div>
      </div>

@endsection
<style>
    #myMap {
        height: 350px;
    }
</style>

{{--<script src="{{URL::asset('/assets/js/quem-maps-setting.js')}}"></script>--}}


<style>
        .user-row {
            margin-bottom: 14px;
        }
        
        .user-row:last-child {
            margin-bottom: 0;
        }
        
        .dropdown-user {
            margin: 13px 0;
            padding: 5px;
            height: 100%;
        }
        
        .dropdown-user:hover {
            cursor: pointer;
        }
        
        .table-user-information > tbody > tr {
            border-top: 1px solid rgb(221, 221, 221);
        }
        
        .table-user-information > tbody > tr:first-child {
            border-top: 0;
        }
        
        
        .table-user-information > tbody > tr > td {
            border-top: 0;
        }
        .toppad
        {
        }
</style>
