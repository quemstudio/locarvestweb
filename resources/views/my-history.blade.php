<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 27/01/18
 * Time: 23.29
 */?>
<div class="container">
    <div class="row">
        @if(!empty($joined))
            <div class="table-responsive">
                <table class="table shop-cart">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">No. Order</th>
                        <th scope="col">Status</th>
                        <th scope="col">Total</th>
                        <th scope="col">Detail</th>
                    </tr>
                    </thead>
                    <tbody class="bg-success">
                    @foreach($joined as $confirmPayment)
                        <tr>
                            <td>{{$confirmPayment->order_number}}</td>
                            <td>{{strtoupper($confirmPayment->statusOrder)}}</td>
                            <td>Rp. {{ str_replace(',', '.', number_format($confirmPayment->total_price)) }}</td>
                            <td>
                                <button onclick="detailPayment('{{ $confirmPayment->order_id }}')" class="ion-eye btn btn-primary"
                                        data-toggle="tooltip" data-placement="top" title="Detail"></button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="col-md-8 col-md-offset-2 text-center mb-3">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <h1 class="display-4">Tidak Ada Riwayat Pembelian</h1>
                        <p class="lead">Saat ini tidak ada catatan riwayat pembelian dari akun anda</p>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>