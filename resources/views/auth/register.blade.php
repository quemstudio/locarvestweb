@extends('layouts.app')

@section('content')
    <div id="main">
        <div class="section section-bg-9 pt-1 pb-1">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">

                            <div class="commerce-login-form">
                                <form class="form-horizontal" method="POST" action="{{URL::to('register')}}">
                                    {{ csrf_field() }}

                                    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                        <label for="name" class="col-md-4 control-label">Nama Depan</label>

                                        <div class="col-md-6">
                                            <input id="name" type="text" class="form-control" name="first_name"
                                                   value="{{ old('first_name') }}" required autofocus>

                                            @if ($errors->has('first_name'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                        <label for="name" class="col-md-4 control-label">Nama Belakang</label>

                                        <div class="col-md-6">
                                            <input id="name" type="text" class="form-control" name="last_name"
                                                   value="{{ old('last_name') }}" required autofocus>

                                            @if ($errors->has('last_name'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email" class="col-md-4 control-label">E-Mail</label>

                                        <div class="col-md-6">
                                            <input id="email" type="email" class="form-control" name="email"
                                                   value="{{ old('email') }}" required>

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password" class="col-md-4 control-label">Password</label>

                                        <div class="col-md-6">
                                            <input id="password" type="password" class="form-control" name="password"
                                                   required>

                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="password-confirm" class="col-md-4 control-label">Konfirmasi Password</label>

                                        <div class="col-md-6">
                                            <input id="password-confirm" type="password" class="form-control"
                                                   name="password_confirmation" required>
                                        </div>
                                    </div>

                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-5">
                                                <button type="submit">
                                                    Register
                                                </button>
                                            </div>
                                        </div>



                                    @if($errors->any())
                                        <div class="row collapse">
                                            <ul class="alert-box warning radius">
                                                @foreach($errors->all() as $error)
                                                    <li> {{ $error }} </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                </form>
                            </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
