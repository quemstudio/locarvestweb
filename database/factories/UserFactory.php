<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    static $password;

    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'role' => implode("", $faker->randomElements(array('supplier', 'customer'))),
        'gender' => implode("", $faker->randomElements(array('M', 'F'))),
        'phone_number' => $faker->phoneNumber,
        'username' => $faker->userName,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

//factory(App\User::class, 50)->create()->each(function($u) {
//    $u->posts()->save(factory(App\Post::class)->make());
//});

$factory->define(App\Supplier::class, function (Faker $faker) {

    return [
        'address' => $faker->address,
        'land_area' => $faker->numberBetween($min = 500, $max = 2000),
        'name' => $faker->name,
        'image' => '/assets/images/test.jpg',
        'user_id' => factory(App\User::class)->create()->id,
    ];
});

$factory->define(App\Customer::class, function (Faker $faker) {

    return [
        'classification' => implode("", $faker->randomElements(array('wholesale', 'individu'))),
        'image' => '/assets/images/test.jpg',
        'user_id' => factory(App\User::class)->create()->id,
    ];
});

$factory->define(App\Cart::class, function (Faker $faker) {

    return [
        'customer_id' => factory(App\Customer::class)->create()->id,
        'product_id' => factory(App\Product::class)->create()->id,
        'status' => implode("", $faker->randomElements(array('status1', 'status2'))),
        'qty' => $faker->numberBetween($min = 0, $max = 50),
    ];
});

//$factory->define(App\Shipper::class, function (Faker $faker) {
//
//    return [
//        'first_name' => $faker->firstName,
//        'last_name' => $faker->lastName,
//        'gender' => implode("", $faker->randomElements(array('M', 'F'))),
//        'phone_number' => $faker->phoneNumber,
//        'email' => $faker->unique()->safeEmail,
//    ];
//});

$factory->define(App\Order::class, function (Faker $faker) {

    return [
        'customer_id' => factory(App\Customer::class)->create()->id,
//        'shipper_id' => factory(App\Shipper::class)->create()->id,
        'order_number' => $faker->randomNumber($nbDigits = NULL, $strict = false),
        'date' => $faker->dateTimeBetween($startDate = '-20 days', $endDate = 'now', $timezone = null),
        'distance' => $faker->numberBetween($min = 100, $max = 10000),
        'status' => implode("", $faker->randomElements(array('sent', 'onProgress'))),


    ];
});

$factory->define(App\Billing::class, function (Faker $faker) {

    return [

        'status' => implode("", $faker->randomElements(array('payed', 'unpayed'))),
        'payment_method' => 'transfer',
        'bank' => implode("", $faker->randomElements(array('BCA', 'BRI', 'Mandiri', 'BNI'))),
        'account_number' => $faker->creditCardNumber,
        'confirmation_pic' => '/assets/images/test.jpg',
        'order_id' => factory(App\Order::class)->create()->id,

    ];
});

$factory->define(App\Category::class, function (Faker $faker) {

    return [
        'name' => implode("", $faker->randomElements(array('Sayuran', 'Buah', 'Beras', 'Daging', 'Ikan', 'Bumbu dapur', 'Produk olahan', 'Siap santap'))),
    ];
});

$factory->define(App\Product::class, function (Faker $faker) {

    $planting_schedule = $faker->dateTimeBetween('+10 days', '50 days');

    return [
        'name' => $faker->word,
        'planting_schedule' => $planting_schedule,
        'land_area' => $faker->numberBetween($min = 500, $max = 2000),
        'seed_quantity' => $faker->numberBetween($min = 100, $max = 1000),
        'harvest_time' => $faker->dateTimeBetween($planting_schedule, '+100 days'),
        'harvest_quantity' => $faker->numberBetween($min = 1000, $max = 5000),
        'wholesale_price' => $faker->numberBetween($min = 20000, $max = 200000),
        'individu_price' => $faker->numberBetween($min = 20000, $max = 200000),
        'status' => implode("", $faker->randomElements(array('ready', 'not ready'))),
        'description' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
        'amount_left' => $faker->biasedNumberBetween(0, 5, function ($x) {
            return 1 - sqrt($x);
        }),
        'best_offer' => implode("", $faker->randomElements(array(1, 0))),
        'preorder_date' => $faker->date($format = 'Y-m-d', $min = 'now'),
        'image' => '/assets/images/test.jpg',


        'category_id' => factory(App\Category::class)->create()->id,
        'supplier_id' => factory(App\Supplier::class)->create()->id,
    ];
});

$factory->define(App\OrderDetail::class, function (Faker $faker) {

    $qty = $faker->numberBetween($min = 1, $max = 20);
    $price = factory(App\Product::class)->create()->wholesale_price;

    return [
        'qty' => $qty,
        'price' => $price,
        'total_price' => $price * $qty,
        'order_number' => factory(App\Order::class)->create()->order_number,
        'order_id' => factory(App\Order::class)->create()->id,
        'product_id' => factory(App\Product::class)->create()->id,
    ];
});

$factory->define(App\Post::class, function (Faker $faker) {

    return [
        'title' => $faker->sentence($nbWords = 4, $variableNbWords = true),
        'body' => $faker->paragraph($nbSentences = 20, $variableNbSentences = true),
        'cover' => '/assets/images/cover.jpg',
        'admin_id' => factory(App\Admin::class)->create()->id,
        'categoryPost_id' => factory(App\CategoryPost::class)->create()->id,
    ];
});

$factory->define(App\Page::class, function (Faker $faker) {

    return [
        'title' => $faker->sentence($nbWords = 4, $variableNbWords = true),
        'body' => $faker->paragraph($nbSentences = 20, $variableNbSentences = true),
        'cover' => '/assets/images/cover.jpg',
        'admin_id' => factory(App\Admin::class)->create()->id,
    ];
});

$factory->define(App\PageImg::class, function (Faker $faker) {

    return [
        'directory' => '/assets/images/test.jpg',
        'page_id' => factory(App\Page::class)->create()->id,
    ];
});

$factory->define(App\CategoryPost::class, function (Faker $faker) {

    return [
        'picture' => '/assets/images/test.jpg',
        'name' => $faker->name(),
        'description' => $faker->sentence($nbWords = 4, $variableNbWords = true),
    ];
});

$factory->define(App\ProductImg::class, function (Faker $faker) {

    return [
        'directory' => '/assets/images/test.jpg',
        'status' => implode("", $faker->randomElements(array(1, 0))),
        'product_id' => factory(App\Product::class)->create()->id,
    ];
});

$factory->define(App\Admin::class, function (Faker $faker) {
    static $password;

    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'role' => implode("", $faker->randomElements(array('admin', 'superadmin'))),
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
    ];
});


$factory->define(App\Address::class, function (Faker $faker) {
    static $password;

    return [
        'address' => $faker->address,
        'city' => $faker->city,
        'kecamatan' => $faker->city,
        'kelurahan' => $faker->city,
        'zip_code' => $faker->numberBetween($min = 4000, $max = 4200),
        'rtrw' => '01/09',
        'user_id' => factory(App\User::class)->create()->id
    ];
});