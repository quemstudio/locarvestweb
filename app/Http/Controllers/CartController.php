<?php

namespace App\Http\Controllers;


use App\Cart;
use App\Product;
use App\User;
use App\Address;
use App\Customer;
use Gloudemans\Shoppingcart\Facades\Cart as CartTemp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Session;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $carts = Cart::all();
//      $books = Book::all();
        $carts = CartTemp::content();
        $response = [
            'carts' => $carts
        ];
//        return response()->json($response,200);
       
        return view('cart' , compact('carts'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("cart.add");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Cart::create($request->all());
//        $category = new Category();
//        $category->namaCategory = $request->input('namaCategory');
//        $category->save();
//
//        return response()->json(['category' => $category], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Cart::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cart= Cart::findOrFail($id);
        return view("cart.edit", compact('cart'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cart= Cart::findOrFail($id);
        $cart->update($request->all());

        return $cart;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cart = Cart::findOrFail($id);
        $cart->delete();

        return response()->json(null,204);
    }

    public function addCartTemp(Request $request)
    {
        if($request->ajax()){
            CartTemp::add($request->id, $request->name, $request->qty, $request->price, ['img' => '/storage/'.$request->img]);
        }
        $carts = CartTemp::content();
        return view('cartbar',compact('carts'));
    }

    public function deleteCartTemp(Request $request)
    {
        if($request->ajax()) {
            $rows  = CartTemp::content();
            $rowId = $rows->where('id', $request->id)->first()->rowId;
            CartTemp::remove($rowId);
        }
        $carts = CartTemp::content();
        return view('cartbar',compact('carts'));
    }

    public function deleteCartDetailTemp(Request $request)
    {
        if($request->ajax()) {
            $rows  = CartTemp::content();
            $rowId = $rows->where('id', $request->id)->first()->rowId;
            CartTemp::remove($rowId);
        }
        $carts = CartTemp::content();
        return view('cartDetailDelete',compact('carts'));
    }

    public function addQtyCartTemp(Request $request)
    {
        if($request->ajax()) {
            $rows  = CartTemp::content();
            $rowId = $rows->where('id', $request->id)->first()->rowId;
            CartTemp::update($rowId,$request->qty);
        }
        $carts = CartTemp::content();
        return view('cartDetail',compact('carts'));
    }

    public function checkout()
    {
        $status = Customer::select('classification')->where('user_id',Auth::user()->id)->get();
       
        $user = User::select('phone_number')->where('id', Auth::user()->id)->get();
        $carts = CartTemp::content();
        $addresses = DB::table('addresses')
            ->where('user_id',Auth::user()->id)
            ->get();
        if($addresses->isEmpty()){
            $shipping = null;
        }else{
            if($addresses[0]->distance <= 20000){
                $shipping = 10000;
            }else{
               // $shipping = 10000 + ( ceil(($addresses[0]->distance - 10000)/1000) * 3000);
                $shipping = 15000;
            }   
            Session::put('shipping',$shipping);
            Session::put('addressId',$addresses[0]->id);
        }
        
        $total =  (int)str_replace(',', '', CartTemp::subtotal()) + $shipping;
        return view('checkout',compact('carts','addresses','shipping','total', 'user', 'status'));
    }

    public function calculateShipping(Request $request)
    {
        // $carts = CartTemp::content();
        $addresses = DB::table('addresses')
            ->where('id',$request->id)
            ->get();
        if($addresses->isEmpty()){
            $shipping = null;
        }else{
            if($addresses[0]->distance <= 10000){
                $shipping = 10000;
            }else{
//                $shipping = 10000 + ( ceil(($addresses[0]->distance - 10000)/1000) * 3000);
                $shipping = 15000;
            }
        }
        $total =  (int)str_replace(',', '', CartTemp::subtotal()) + $shipping;
        // return 'asdf';
        Session::put('shipping',$shipping);
        Session::put('addressId',$addresses[0]->id);
        return view('shippDetail',compact('shipping','total','addresses'));
    }
}
