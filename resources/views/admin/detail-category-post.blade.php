<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 16/01/18
 * Time: 19.32
 */
?>
@extends('layouts.appadmin')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-header">
                    <h3><i class="icon-target"></i> <strong>Data</strong> Kategori Post</h3>
                </div>
                <div class="panel-content">
                    <div class="row">
                        <form class=" form-horizontal" method="post" action="{{route('category_post.update.submit', $categoryPost->id)}}"
                              enctype="multipart/form-data">
                            {{csrf_field()}} {{method_field('PUT')}}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Nama</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="text" placeholder="" name="name" value="{{$categoryPost->name}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Deskripsi</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" placeholder="" name="description">{{$categoryPost->description}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Gambar</label>
                                    <div class="col-sm-9">
                                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                            {{--<div class="form-control" data-trigger="fileinput">--}}
                                                {{--<i class="glyphicon glyphicon-file fileinput-exists"></i>--}}
                                                {{--<span class="fileinput-filename">{{$categoryPost->picture}}</span>--}}
                                            {{--</div>--}}
                                            <input type="text" class="form-control" id="upload-file-info" readonly value="{{$categoryPost->picture}}">
                                            <span class="input-group-addon btn btn-default btn-file">
                                                <span class="fileinput-new">Pilih...</span>
                                                <span class="fileinput-exists">Ganti</span>
                                                <input type="file" name="picture" onchange="$('#upload-file-info').val($(this).val().split('\\').pop());">
                                            </span>
                                            <a href="#" class="input-group-addon btn btn-default fileinput-exists"
                                               data-dismiss="fileinput">Hapus</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary" style="float:right">
                                            Update Data
                                        </button>
                                        <a href="{{route('admin.category.post')}}"><button type="button" class="btn btn-danger" style="float:right">Batal</button></a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection