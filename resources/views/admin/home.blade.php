@extends('layouts.appadmin')
@section('content')
    {{--
<div class="container">--}} {{--
    <div class="row">--}} {{--
        <div class="col-md-8 col-md-offset-2">--}} {{--
            <div class="panel panel-default">--}} {{--
                <div class="panel-heading">Admin Dashboard</div>--}} {{--

                <div class="panel-body">--}} {{--@if (session('status'))--}} {{--
                    <div class="alert alert-success">--}} {{--{{ session('status') }}--}} {{--
                    </div>--}} {{--@endif--}} {{--You are logged in!--}} {{--
                </div>--}} {{--
            </div>--}} {{--
        </div>--}} {{--
    </div>--}} {{--
</div>--}}

    <div class="header">
        <h2>Admin
            <strong>Dashboard</strong>
        </h2>
    </div>
    <div class="row">
        <div class="col-xlg-12 col-lg-12 col-sm-12">
            <div class="col-xlg-2 col-small-stats">
                <div class="row">
                    <div class="col-xlg-12 col-lg-12 col-sm-12">
                        <div class="panel">
                            <div class="panel-content widget-small bg-blue-light">
                                <div class="title" style="text-align: center;">
                                    <a href="https://drive.google.com/open?id=17McUWIVXOMHqPhxXfX6eiStGY1DVa0RT">USER MANUAL</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xlg-2 col-small-stats">
            <div class="row">
                <div class="col-xlg-12 col-lg-6 col-sm-6">
                    <div class="panel">
                        <div class="panel-content widget-small bg-green">
                            <div class="title">
                                <h1>Jumlah Product</h1>
                                <p>{{$product}}</p>
                            </div>
                            <div class="content">
                                <div id="stock-virgin-sm"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xlg-12 col-lg-6 col-sm-6">
                    <div class="panel">
                        <div class="panel-content widget-small bg-purple">
                            <div class="title">
                                <h1>Jumlah Petani</h1>
                                <p>{{$petani}}</p>
                            </div>
                            <div class="content">
                                <div id="stock-ebay-sm"></div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--<div class="col-xlg-12 col-lg-4 col-sm-4">--}}
                {{--<div class="panel">--}}
                {{--<div class="panel-content widget-small bg-primary">--}}
                {{--<div class="title">--}}
                {{--<h1>Jumlah Order</h1>--}}
                {{--<p>{{$order}}</p>--}}
                {{--</div>--}}
                {{--<div class="content">--}}
                {{--<div id="stock-facebook-sm"></div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
    <div class="row" style="display: none;">
        <div class="col-xlg-12 col-lg-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel bg-dark widget-map">
                        <div class="panel-header">
                            <h3><i class="icon-globe-alt"></i> <strong>Interactif</strong> Map</h3>
                        </div>
                        <div class="panel-content widget-full">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="map"></div>
                                </div>
                                <div class="col-md-3 c-white">
                                    <div id="listdiv"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{--TESTTTT--}}
    <div class="row">
        <div class="col-lg-12 portlets">
            <div class="panel">
                <div class="panel-content">
                    <div class="row">
                        <div class="col-sm-6">
                            <h3>
                                <strong>Jumlah Order</strong>(per jenis)</h3>

                            {{--
                            <p>A line chart is a way of plotting data points on a line.
                                <br>--}} {{--Often, it is used to show trend data, and the comparison of two data sets.--}} {{--
                        </p>--}}
                            <canvas id="pie-chart" class="full" height="140"></canvas>
                        </div>
                        <div class="col-sm-6">
                            <h3>
                                <strong>jumlah Kategori yang Terjual</strong></h3>

                            {{--
                            <p>A line chart is a way of plotting data points on a line.
                                <br>--}} {{--Often, it is used to show trend data, and the comparison of two data sets.--}} {{--
                        </p>--}}
                            <canvas id="pie2-chart" class="full" height="140"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 portlets">
            <div class="panel">
                <div class="panel-content">
                    <div class="row">
                        <div class="col-sm-12">
                            <h3>
                                <strong>Jumlah Order</strong> (per bulan)
                            </h3>
                            <canvas id="bar-chart" class="full" height="140"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>--}}
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js"></script>--}}
    <script>
        var ctx = document.getElementById("bar-chart").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
                datasets: [{
                    label: 'Order',
                    data: [{{$orderMonth[1]}}, {{$orderMonth[2]}}, {{$orderMonth[3]}}, {{$orderMonth[4]}}, {{$orderMonth[5]}}, {{$orderMonth[6]}}, {{$orderMonth[7]}}, {{$orderMonth[8]}}, {{$orderMonth[9]}}, {{$orderMonth[10]}}, {{$orderMonth[11]}}, {{$orderMonth[12]}}],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(255, 206, 86, 0.2)',

                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 99, 132, 1)',
                        'rgba(255, 206, 86, 1)',
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script>
    <script>
        var ctx1 = document.getElementById("pie-chart").getContext('2d');
        var myPieChart = new Chart(ctx1, {
            type: 'pie',
            data: {
                datasets: [{
                    data: [{{$neworder}}, {{$shipp}}, {{$confirm}},{{$comp}}],
                    backgroundColor: [
                        'rgba(255, 0, 0, 0.6)',
                        'rgba(54, 162, 235, 0.6)',
                        'rgba(255, 206, 86, 0.6)',
                        'rgba(128,0,128,0.6)'
                    ]
                }],

                // These labels appear in the legend and in the tooltips when hovering different arcs
                labels: [
                    'New Order',
                    'Shipping',
                    'Confirmed',
                    'Complete'
                ]
            },
            options: {
                legend: false
            }
        });
    </script>
    <script>
        var ctx1 = document.getElementById("pie2-chart").getContext('2d');
        var myPieChart = new Chart(ctx1, {
            type: 'pie',
            data: {
                datasets: [{
                    data: [{{$grafikCat[1]}}, {{$grafikCat[2]}}, {{$grafikCat[3]}},{{$grafikCat[4]}},{{$grafikCat[5]}},{{$grafikCat[6]}},{{$grafikCat[7]}},{{$grafikCat[8]}},{{$grafikCat[9]}}],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.6)',
                        'rgba(54, 162, 235, 0.6)',
                        'rgba(255, 206, 86, 0.6)',
                        'rgba(192,192,192,0.6)',
                        'rgba(128,0,128,0.6)',
                        'rgba(255,255,0,0.6)',
                        'rgba(0,0,0,0.6)',
                        'rgba(0,255,0,0.6)',
                        'rgba(0,0,128,0.6)'
                    ]
                }],

                // These labels appear in the legend and in the tooltips when hovering different arcs
                labels: [
                    'Daging',
                    'Siap Santap',
                    'Ikan',
                    'Sayuran',
                    'Buah',
                    'Bumbu Dapur',
                    'Beras',
                    'Bunga',
                    'Sayur Hijau'
                ]
            },
            options: {
              legend: false
            }
        });
    </script>
@endpush
