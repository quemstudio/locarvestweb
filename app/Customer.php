<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $primaryKey="id";
    protected $guarded = [];

    public function carts()
    {
        return $this->hasMany('App\Cart','id');
    }

    public function orders()
    {
        return $this->hasMany('App\Order','customer_id');
    }

//    public function billings()
//    {
//        return $this->hasManyThrough('App\Billing', 'App\Order');
//    }

}
