<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 16/01/18
 * Time: 19.32
 */
?>
@extends('layouts.appadmin')
@section('content')
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif(session('error'))
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-header">
                    <h3><i class="icon-target"></i> <strong>Tambah</strong> Admin</h3>
                </div>
                <div class="panel-content">
                    <div class="row">
                        <form class=" form-horizontal" method="post" action="{{ route('admin.add.submit') }}">
                            {{ csrf_field() }}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Email</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="email" placeholder="" id="email"
                                               name="email"
                                               required="required">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Nama Depan</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="text" placeholder="" id="first_name"
                                               name="first_name"
                                               required="required">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Nama Belakang</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="text" placeholder="" id="last_name"
                                               name="last_name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Role </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="role" id="role" style="width:100%;">
                                            <option value="admin">Admin</option>
                                            {{--<option value="superadmin">Super Admin</option>--}}
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Password</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="password" placeholder="" id="password"
                                               name="password" minlength="6"
                                               required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Repassword</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="password" placeholder=""
                                               id="password_confirmation"
                                               name="password_confirmation"
                                               required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary" style="float:right">Submit
                                        </button>
                                        <a href="{{route('admin.admin.page')}}">
                                            <button type="button" class="btn btn-danger" style="float:right">Batal
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>

        var password = document.getElementById("password")
            , confirm_password = document.getElementById("password_confirmation");

        function validatePassword(){
            if(password.value !== confirm_password.value) {
                confirm_password.setCustomValidity("Passwords Don't Match");
            } else {
                confirm_password.setCustomValidity('');
            }
        }

        password.onchange = validatePassword;
        confirm_password.onkeyup = validatePassword;
    </script>
@endpush