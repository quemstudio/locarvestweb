<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 16/01/18
 * Time: 19.25
 */
?>
@extends('layouts.appadmin')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-header">
                    <h3><i class="icon-target"></i> <strong>Data</strong> Page</h3>
                </div>
                <div class="panel-content">
                    <div class="row">
                        <form class=" form-horizontal" method="post" action="{{route('page.edit.submit', $page->id)}}"
                              enctype="multipart/form-data">
                            {{csrf_field()}} {{method_field('PUT')}}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Judul</label>
                                    <div class="col-sm-9">
                                        <input class="form-control input-lg" type="text" placeholder="" name="title"
                                               value="{{$page->title}}" readonly>
                                    </div>
                                </div>
                                {{--<div class="form-group">--}}
                                    {{--<label class="col-sm-3 control-label">Cover</label>--}}
                                    {{--<div class="col-sm-9">--}}
                                        {{--<div class="fileinput fileinput-new input-group" data-provides="fileinput">--}}
                                            {{--<div class="form-control" data-trigger="fileinput">--}}
                                                {{--<i class="glyphicon glyphicon-file fileinput-exists"></i><span--}}
                                                        {{--class="fileinput-filename">{{$page->cover}} </span>--}}
                                            {{--</div>--}}
                                            {{--<input type="text" class="form-control" id="upload-file-info" readonly value="{{$page->cover}}">--}}
                                            {{--<span class="input-group-addon btn btn-default btn-file"><span--}}
                                                        {{--class="fileinput-new">Pilih...</span><span--}}
                                                        {{--class="fileinput-exists">Ganti</span>--}}
                                                {{--<input type="file" name="cover" onchange="$('#upload-file-info').val($(this).val().split('\\').pop());">--}}
                                            {{--</span>--}}
                                            {{--<a href="#" class="input-group-addon btn btn-default fileinput-exists"--}}
                                               {{--data-dismiss="fileinput">Hapus</a>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <h3>Konten <strong>Editor</strong></h3>
                                        <textarea class="summernote bg-white" name="body">{{$page->body}}</textarea>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary" style="float:right">
                                            Update Data
                                        </button>
                                        <a href="{{route('admin.page')}}"><button type="button" class="btn btn-danger" style="float:right">Batal</button></a>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
    {{--<div class="row">--}}
    {{--<div class="col-md-12">--}}
    {{--<div class="panel">--}}
    {{--<div class="panel-header panel-controls">--}}
    {{--<h3><i class="icon-target"></i> <strong>Input</strong> Images</h3>--}}
    {{--</div>--}}
    {{--<div class="panel-content">--}}
    {{--<div class="row">--}}
    {{--<div class="col-md-12">--}}
    {{--<p>Add image if the page have galleries.</p>--}}
    {{--<form action="#" class="dropzone">--}}

    {{--<div class="fallback">--}}
    {{--<input name="file" type="file" multiple />--}}
    {{--</div>--}}
    {{--</form>--}}
    {{--</div>--}}
    {{--<div class="col-lg-12 portlets">--}}
    {{--<div class="panel">--}}
    {{--<div class="panel-content">--}}
    {{--<div class="filter-left">--}}
    {{--<table class="table table-dynamic table-tools" data-table-name="Total users">--}}
    {{--<thead>--}}
    {{--<tr>--}}
    {{--<th>Rendering engine</th>--}}
    {{--<th>Browser</th>--}}
    {{--<th class='hidden-350'>Platform(s)</th>--}}
    {{--<th class='hidden-1024'>Engine version</th>--}}
    {{--<th class='hidden-480'>CSS grade</th>--}}
    {{--</tr>--}}
    {{--</thead>--}}
    {{--<tbody>--}}
    {{--<tr>--}}
    {{--<td>Trident</td>--}}
    {{--<td>--}}
    {{--Internet Explorer 4.0--}}
    {{--</td>--}}
    {{--<td class='hidden-350'>Win 95+</td>--}}
    {{--<td class='hidden-1024'>4</td>--}}
    {{--<td class='hidden-480'>X</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>Presto</td>--}}
    {{--<td>Nokia N800</td>--}}
    {{--<td class='hidden-350'>N800</td>--}}
    {{--<td class='hidden-1024'>1.8</td>--}}
    {{--<td class='hidden-480'>A</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>Misc</td>--}}
    {{--<td>NetFront 3.4</td>--}}
    {{--<td class='hidden-350'>Embedded devices</td>--}}
    {{--<td class='hidden-1024'>1.7</td>--}}
    {{--<td class='hidden-480'>A</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>Misc</td>--}}
    {{--<td>Dillo 0.8</td>--}}
    {{--<td class='hidden-350'>Embedded devices</td>--}}
    {{--<td class='hidden-1024'>1.5</td>--}}
    {{--<td class='hidden-480'>X</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>Misc</td>--}}
    {{--<td>Links</td>--}}
    {{--<td class='hidden-350'>Text only</td>--}}
    {{--<td class='hidden-1024'>1.8</td>--}}
    {{--<td class='hidden-480'>X</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>Misc</td>--}}
    {{--<td>Lynx</td>--}}
    {{--<td class='hidden-350'>Text only</td>--}}
    {{--<td class='hidden-1024'>1.9</td>--}}
    {{--<td class='hidden-480'>X</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>Misc</td>--}}
    {{--<td>IE Mobile</td>--}}
    {{--<td class='hidden-350'>Windows Mobile 6</td>--}}
    {{--<td class='hidden-1024'>1.4</td>--}}
    {{--<td class='hidden-480'>C</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>Misc</td>--}}
    {{--<td>PSP browser</td>--}}
    {{--<td class='hidden-350'>PSP</td>--}}
    {{--<td class='hidden-1024'>1.6</td>--}}
    {{--<td class='hidden-480'>C</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>Other browsers</td>--}}
    {{--<td>All others</td>--}}
    {{--<td class='hidden-350'>-</td>--}}
    {{--<td class='hidden-1024'>1.5</td>--}}
    {{--<td class='hidden-480'>U</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>Trident</td>--}}
    {{--<td>--}}
    {{--Internet Explorer 4.0--}}
    {{--</td>--}}
    {{--<td class='hidden-350'>Win 95+</td>--}}
    {{--<td class='hidden-1024'>4</td>--}}
    {{--<td class='hidden-480'>X</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>Presto</td>--}}
    {{--<td>Nokia N800</td>--}}
    {{--<td class='hidden-350'>N800</td>--}}
    {{--<td class='hidden-1024'>-</td>--}}
    {{--<td class='hidden-480'>A</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>Misc</td>--}}
    {{--<td>NetFront 3.4</td>--}}
    {{--<td class='hidden-350'>Embedded devices</td>--}}
    {{--<td class='hidden-1024'>-</td>--}}
    {{--<td class='hidden-480'>A</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>Misc</td>--}}
    {{--<td>Dillo 0.8</td>--}}
    {{--<td class='hidden-350'>Embedded devices</td>--}}
    {{--<td class='hidden-1024'>1.5</td>--}}
    {{--<td class='hidden-480'>X</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>Misc</td>--}}
    {{--<td>Links</td>--}}
    {{--<td class='hidden-350'>Text only</td>--}}
    {{--<td class='hidden-1024'>1.6</td>--}}
    {{--<td class='hidden-480'>X</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>Misc</td>--}}
    {{--<td>Lynx</td>--}}
    {{--<td class='hidden-350'>Text only</td>--}}
    {{--<td class='hidden-1024'>1.7</td>--}}
    {{--<td class='hidden-480'>X</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>Misc</td>--}}
    {{--<td>IE Mobile</td>--}}
    {{--<td class='hidden-350'>Windows Mobile 6</td>--}}
    {{--<td class='hidden-1024'>1.4</td>--}}
    {{--<td class='hidden-480'>C</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>Misc</td>--}}
    {{--<td>PSP browser</td>--}}
    {{--<td class='hidden-350'>PSP</td>--}}
    {{--<td class='hidden-1024'>1.6</td>--}}
    {{--<td class='hidden-480'>C</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>Other browsers</td>--}}
    {{--<td>All others</td>--}}
    {{--<td class='hidden-350'>-</td>--}}
    {{--<td class='hidden-1024'>1.9</td>--}}
    {{--<td class='hidden-480'>U</td>--}}
    {{--</tr>--}}
    {{--</tbody>--}}

    {{--</table>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
@endsection