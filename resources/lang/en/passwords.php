<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Panjang password minimal 6 karakter dan sama dengan konfirmasi password.',
    'reset' => 'Password anda telah di reset!',
    'sent' => 'Cek e-mail anda untuk link reset password.',
    'token' => 'This password reset token is invalid.',
    'user' => "Tidak ditemukan user dengan e-mail tersebut.",

];
