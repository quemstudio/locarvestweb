<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 16/01/18
 * Time: 19.33
 */
?>
@extends('layouts.appadmin')
@section('content')
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <style>
        #myMap {
            height: 350px;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-header ">
                    <h3><i class="icon-target"></i> <strong>Edit</strong> Petani</h3>
                </div>
                <div class="panel-content">
                    <div class="row">
                        <form class=" form-horizontal" method="post" action="{{route('supplier.update.submit',$supplier->id)}}"
                              enctype="multipart/form-data">
                            {{csrf_field()}} {{method_field('PUT')}}
                            <div class="col-md-12">

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Nama Depan</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="text" placeholder="" value="{{$user->first_name}}" name="first_name" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Name Belakang</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="text" placeholder="" value="{{$user->last_name}}" name="last_name" >
                                    </div>
                                </div>

                                {{--<div class="form-group">--}}
                                    {{--<label class="col-sm-3 control-label">Email</label>--}}
                                    {{--<div class="col-sm-9">--}}
                                        {{--<input class="form-control" type="email" placeholder="" value="{{$user->email}}" name="email" required>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="form-group">--}}
                                    {{--<label class="col-sm-3 control-label">Nomor Telepon</label>--}}
                                    {{--<div class="col-sm-9">--}}
                                        {{--<input class="form-control input-sm" placeholder="" type="text" value="{{$user->phone_number}}" name="phone_number" required>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="form-group">--}}
                                    {{--<label class="col-sm-3 control-label">Jenis Kelamin</label>--}}
                                    {{--<div class="radio radio-inline">--}}
                                        {{--@if($user->gender == 'M')--}}
                                            {{--<label><input type="radio" name="gender" data-radio="iradio_square-blue" value="M" checked> Laki-laki</label>--}}
                                            {{--<label><input type="radio" name="gender" data-radio="iradio_square-blue" value="F"> Perempuan</label>--}}
                                        {{--@else--}}
                                            {{--<label><input type="radio" name="gender" data-radio="iradio_square-blue" value="M"> Laki-laki</label>--}}
                                            {{--<label><input type="radio" name="gender" data-radio="iradio_square-blue" value="F" checked> Perempuan</label>--}}
                                        {{--@endif--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Komoditas yang Ditanam</label>
                                    <div class="col-sm-9">
                                        <input class="form-control input-sm" value="{{$supplier->name_plant}}" type="text"
                                               name="name_plant" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Luas Lahan  (m<sup>2</sup>)</label>
                                    <div class="col-sm-9">
                                        <input class="form-control input-sm" value="{{$supplier->land_area}}" type="number"
                                               name="land_area" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Sertifikasi</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" value="{{$supplier->certification}}" type="text"
                                                  name="certification">{{$supplier->certification}}</textarea>
                                    </div>
                                </div>

                                {{--<div class="form-group">--}}
                                    {{--<label class="col-sm-3 control-label">Jadwal Tanam (per bulan)</label>--}}
                                    {{--<div class="col-sm-9">--}}
                                        {{--<input name="planting_schedule" class="b-datepicker form-control" placeholder="Select a date..." value="{{$supplier->planting_schedule}}"--}}
                                               {{--type="text">--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="form-group">--}}
                                    {{--<label class="col-sm-3 control-label">Perkiraan Tanggal Panen</label>--}}
                                    {{--<div class="col-sm-9">--}}
                                        {{--<input name="harvest_schedule" class="b-datepicker form-control" placeholder="Select a date..." value="{{$supplier->harvest_schedule}}"--}}
                                               {{--type="text">--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="form-group">--}}
                                    {{--<label class="col-sm-3 control-label">Akumulasi Perkiraan Kuantitas Panen</label>--}}
                                    {{--<div class="col-sm-9">--}}
                                        {{--<input class="form-control input-sm" placeholder="" type="number" value="{{$supplier->quantity_plant}}"--}}
                                               {{--name="quantity_plant" required>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Gambar</label>
                                    <div class="col-sm-9">
                                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                            {{--<div class="form-control" data-trigger="fileinput">--}}
                                                {{--<i class="glyphicon glyphicon-file fileinput-exists" ></i>--}}
                                                {{--<span class="fileinput-filename">{{$supplier->image}}</span>--}}
                                            {{--</div>--}}
                                            <input type="text" class="form-control" id="upload-file-info" readonly value="{{$supplier->image}}">
                                            <span class="input-group-addon btn btn-default btn-file">
                                                <span class="fileinput-new">Pilih...</span>
                                                <span class="fileinput-exists">Ganti</span>
                                            <input type="file" name="image" id="image" onchange="$('#upload-file-info').val($(this).val().split('\\').pop());">
                                            </span>
                                            <a href="#" class="input-group-addon btn btn-default fileinput-exists"
                                               data-dismiss="fileinput">Hapus</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Alamat</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" placeholder=""  name="address" required>{{$supplier->address}}</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Deskripsi</label>
                                    <div class="col-sm-9">
                                        {{--<h3>Deskripsi <strong>Petani</strong></h3>--}}
                                        <textarea class="summernote bg-white" name="description">{{$supplier->description}}</textarea>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="form-wrap">
                                            <!-- copy dari sini -->
                                            <div class="form-group input-group">
                                                <input type="text" id="search_location" class="form-control" placeholder="Search location">
                                                <div class="input-group-btn">
                                                    <button class="btn btn-default get_map" type="submit">
                                                        Cari Lokasi
                                                    </button>
                                                </div>
                                            </div>
                                            <div id="myMap" class="col-md-12"></div>
                                            <input id="address" class="form-control input-sm" type="text"/><br/>
                                            <input type="hidden" id="latitude" name="lat" placeholder="Latitude" required/>
                                            <input type="hidden" id="longitude" name="long" placeholder="Longitude"
                                                   required/>
                                            <!-- copy sampe sini -->
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary btn-square" style="float:right">Submit</button>
                                        <a href="{{route('admin.petani')}}"><button type="button" class="btn btn-danger" style="float:right">Batal</button></a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

    {{--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB5EksjNLZM377mz5PcHWXG1rTHusDPF4o&callback=initMap&libraries=places"></script>--}}
    {{--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>--}}
    {{--<script src="{{URL::asset('/assets/js/quem-maps-setting.js')}}"></script>--}}

    <script>
        $(document).ready(function () {
            //load google map
            initialize();
            var infowindow = new google.maps.InfoWindow();
            /*
             * autocomplete location search
             */
            var PostCodeid = '#search_location';
            $(function () {
                $(PostCodeid).autocomplete({
                    source: function (request, response) {
                        geocoder.geocode({
                            'address': request.term,
                            /*componentRestrictions: {country: "id"}*/
                        }, function (results, status) {
                            response($.map(results, function (item) {
                                return {
                                    label: item.formatted_address,
                                    value: item.formatted_address,
                                    lat: item.geometry.location.lat(),
                                    lon: item.geometry.location.lng()
                                };
                            }));
                        });
                    },
                    select: function (event, ui) {

                        $('#address').val(ui.item.value);
                        $('#latitude').val(ui.item.lat);
                        $('#longitude').val(ui.item.lon);

                        var latlng = new google.maps.LatLng(ui.item.lat, ui.item.lon);
                        marker.setPosition(latlng);
                        initialize();
                    }
                });
            });

            /*
             * Point location on google map
             */
            $('.get_map').click(function (e) {
                var address = $(PostCodeid).val();
                geocoder.geocode({'address': address}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        map.setCenter(results[0].geometry.location);
                        marker.setPosition(results[0].geometry.location);

                        $('#address').val(results[0].formatted_address);
                        $('#latitude').val(marker.getPosition().lat());
                        $('#longitude').val(marker.getPosition().lng());
                        // infowindow.setContent(results[0].formatted_address);
                        // infowindow.open(map, marker);
                    } else {
                        alert("Geocode was not successful for the following reason: " + status);
                    }
                });
                e.preventDefault();
            });

            //Add listener to marker for reverse geocoding

        });
    </script>


@endsection