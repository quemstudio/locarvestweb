<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 16/01/18
 * Time: 19.32
 */
?>
@extends('layouts.appadmin')
@section('content')
    <!-- BEGIN PAGE CONTENT -->

    <div class="header">
        <h2><strong>Kategori</strong> Terbaik</h2>
    </div>
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif(session('error'))
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    @endif
    <div class="row">
        <div class="col-lg-12 portlets">
            <div class="panel">
                <div class="panel-content">
                    <div class="filter-left">
                        <table class="table table-dynamic table-tools" data-table-name="Total users">
                            <thead>
                            <tr>
                                <th style="width: 20px">No</th>
                                <th>Nama Kategori</th>
                                <th>Total Produk Terjual Per Kategori</th>
                                <th>Total Pendapatan Per Kategori</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($best as $data)
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td>
                                        {{$data->name}}
                                    </td>
                                    <td>
                                        {{$data->total_jual}}
                                    </td>
                                    <td>
                                        {{$data->total_dapat}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection