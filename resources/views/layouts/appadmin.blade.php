<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 16/01/18
 * Time: 19.21
 */
?>
        <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">

    {{--Google Search Console--}}
    <meta name="google-site-verification" content="6rDZdKgDDXbncpKpwZfFvmv2XXYtZOvopzTgAy8_QkU" />

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-115006310-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-115006310-1');

        @if (Auth::check())
            gtag('set', {'user_id': '{{Auth::user()->id}}'}); // Set the user ID using signed-in user_id.
        @endif
    </script>


    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="admin-themes-lab">
    <meta name="author" content="themes-lab">
    {{--<link rel="shortcut icon" href="{{URL::asset('/assets_admin/global/images/favicon.png')}}" type="image/png">--}}
    <link rel="shortcut icon" href="{{URL::asset('/assets/images/favicon.ico')}}"/>
    <title>Administrator</title>
    <!-- BEGIN PAGE STYLE -->
    <style>
        #ui-id-1 {
            z-index: 10000;
            font-size: 13px;
        }
    </style>
    <link href="{{URL::asset('/assets_admin/global/plugins/datatables/dataTables.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('/assets_admin/global/plugins/metrojs/metrojs.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('/assets_admin/global/plugins/maps-amcharts/ammap/ammap.css')}}" rel="stylesheet">
    <link href="{{URL::asset('/assets_admin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('/assets_admin/global/plugins/rateit/rateit.css')}}" rel="stylesheet">
    <link href="{{URL::asset('/assets_admin/global/plugins/dropzone/dropzone.min.css')}}" rel="stylesheet">

    <!-- Sweet Alert -->
    <link href="{{URL::asset('/plugins/bootstrap-sweetalert/sweet-alert.css')}}" rel="stylesheet" type="text/css">

    <link href="{{URL::asset('/assets_admin/global/plugins/summernote/summernote.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('/assets_admin/global/plugins/metrojs/metrojs.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('/assets_admin/global/plugins/maps-amcharts/ammap/ammap.css')}}" rel="stylesheet">
    <!-- END PAGE STYLE -->


    {{--TES GAMBAR PRODUCT--}}
    <link href="{{URL::asset('/assets_admin/global/plugins/magnific/magnific-popup.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('/assets_admin/global/plugins/hover-effects/hover-effects.min.css')}}" rel="stylesheet">
    {{--END--}}

    <link href="{{URL::asset('/assets_admin/global/css/style.css')}}" rel="stylesheet">
    <link href="{{URL::asset('/assets_admin/global/css/theme.css')}}" rel="stylesheet">
    <link href="{{URL::asset('/assets_admin/global/css/ui.css')}}" rel="stylesheet">
    <link href="{{URL::asset('/assets_admin/admin/layout1/css/layout.css')}}" rel="stylesheet">
    <script src="{{URL::asset('/assets_admin/global/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js')}}"></script>
    <link href="https://fonts.googleapis.com/css?family=Great+Vibes%7CLato:100,100i,300,300i,400,400i,700,700i,900,900i"
          rel="stylesheet" />

    <style>
        .select2-container {
            z-index: initial;
        }
    </style>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body class="fixed-topbar fixed-sidebar theme-sltd color-green bg-light-dark dashboard">
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<section>
    <!-- BEGIN SIDEBAR -->
    <div class="sidebar">
        <div class="logopanel">
            <h1>
                <a href="dashboard.html"></a>
            </h1>
        </div>
        <div class="sidebar-inner">

            <div class="menu-title">
                Navigasi

            </div>
            <ul class="nav nav-sidebar">
                <li class="">
                    <a href="{{route('admin.dashboard')}}">
                        <i class="icon-home"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="nav-parent">
                    <a href="#">
                        <i class="icon-tag"></i>
                        <span>Katalog</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="children collapse">
                        <li>
                            <a href="{{route('admin.product')}}"> Produk</a>
                        </li>
                        <li>
                            <a href="{{route('admin.category')}}"> Kategori</a>
                        </li>
                        <li>
                            <a href="{{route('admin.import.product')}}"> Import Produk</a>
                        </li>
                    </ul>
                </li>
                <li class=" ">
                    <a href="{{route('admin.petani')}}">
                        <i class="fa fa-cubes"></i>
                        <span>Petani</span>
                    </a>
                </li>
                <li class=" ">
                    <a href="{{route('admin.customer')}}">
                        <i class="icon-user"></i>
                        <span>Pelanggan</span>
                    </a>
                </li>
                <li class=" ">
                    <a href="{{route('admin.payment')}}">
                        <i class="icon-credit-card"></i>
                        <span>Pembayaran</span>
                    </a>
                </li>
                <li class=" ">
                    <a href="{{route('admin.orders')}}">
                        <i class="icon-basket"></i>
                        <span>Pesanan</span>
                    </a>
                </li>
                <li class="nav-parent">
                    <a href="#">
                        <i class="icon-bar-chart"></i>
                        <span>Status</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="children collapse">
                        <li>
                            <a href="{{route('admin.best.product')}}"> Produk Terbaik</a>
                        </li>
                        <li>
                            <a href="{{route('admin.best.categories')}}"> Kategori Terbaik</a>
                        </li>
                        <li>
                            <a href="{{route('admin.best.supplier')}}"> Petani Terbaik</a>
                        </li>
                        <li>
                            <a href="{{route('admin.best.customer')}}"> Pelanggan Terbaik</a>
                        </li>
                    </ul>
                </li>
                <li class="nav-parent">
                    <a href="#">
                        <i class="icon-docs"></i>
                        <span>Konten</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="children collapse">
                        <li>
                            <a href="{{route('admin.page')}}"> Page</a>
                        </li>
                        <li>
                            <a href="{{route('admin.post')}}"> Post</a>
                        </li>
                        <li>
                            <a href="{{route('admin.category.post')}}"> Kategori Post</a>
                        </li>
                        <li>
                            <a href="{{route('admin.testimony')}}"> Testimoni</a>
                        </li>
                    </ul>
                </li>
                {{--
                <li class=" ">
                    <a href="{{route('admin.coupon')}}">
                        <i class="icon-present"></i>
                        <span>Coupon</span>
                    </a>--}} {{--
                        </li>--}}
                <li class=" ">
                    <a href="{{route('admin.admin.page')}}">
                        <i class="icon-users"></i>
                        <span>Administrator</span>
                    </a>
                </li>
                {{--  <li class=" ">
                    <a href="{{route('admin.configuration')}}">
                        <i class="icon-settings"></i>
                        <span>Configuration</span>
                    </a>
                </li>  --}}
            </ul>
        </div>
    </div>
    <!-- END SIDEBAR -->
    <div class="main-content">
        <!-- BEGIN TOPBAR -->
        <div class="topbar">
            <div class="header-left">
                <div class="topnav">
                    <a class="menutoggle" href="#" data-toggle="sidebar-collapsed">
                                <span class="menu__handle">
                                    <span>Menu</span>
                                </span>
                    </a>
                </div>
            </div>
            <div class="header-right">
                <ul class="header-menu nav navbar-nav">


                    <!-- BEGIN USER DROPDOWN -->
                    <li class="dropdown" id="user-header">
                        <a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <img src="{{URL::asset('/assets_admin/global/images/user1.png')}}" alt="user image">
                            <span class="username">{{Auth::user()->first_name ." ". Auth::user()->last_name}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#">
                                    <i class="icon-user"></i>
                                    <span>Profil Saya</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('admin.logout.submit')}}" onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">
                                    <i class="icon-logout"></i>
                                    <span>Keluar</span>
                                </a>
                                <form id="logout-form" action="{{route('admin.logout.submit')}}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                                {{--
                                <a href="{{route('admin.logout.submit')}}">
                                    <i class="icon-logout"></i>
                                    <span>Logout</span>
                                </a>--}}
                            </li>
                        </ul>
                    </li>
                    <!-- END USER DROPDOWN -->
                </ul>
            </div>
            <!-- header-right -->
        </div>
        <!-- END TOPBAR -->
        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            @yield('content')
            <div class="footer">
                <div class="copyright">
                    <p class="pull-left sm-pull-reset">
                        <span class="align-middle">Copyright © 2018
                            {{--<a style="color: #5fbd74" href="http://quem-studio.com">--}}
                                {{--<img src="{{URL::asset('/assets/images/LogoPanjang-Black.png')}}"--}}
                                     {{--style="vertical-align:middle; height: 24px;margin-bottom: 1px;background-color: transparent;border: none;"--}}
                                     {{--class="img-thumbnail" alt="" />--}}
                            {{--</a>and <a href="http://itik.tech">ITIK labs</a>--}}
                        {{--</span>--}}
                        <a style="color: #5fbd74" href="http://locarvest.com">Locarvest.com</a></span>.<span>  All rights reserved.</span>
                    </p>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END MAIN CONTENT -->

</section>


<!-- BEGIN PRELOADER -->
<div class="loader-overlay">
    <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>
<!-- END PRELOADER -->
<a href="#" class="scrollup">
    <i class="fa fa-angle-up"></i>
</a>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC5cOgK1Qryl6gOwf-tdoN2-R5ThKYjwmU"></script>

<script src="{{URL::asset('/assets_admin/global/plugins/jquery/jquery-3.1.0.min.js')}}"></script>
<script src="{{URL::asset('/assets_admin/global/plugins/jquery/jquery-migrate-3.0.0.min.js')}}"></script>
<script src="{{URL::asset('/assets_admin/global/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<script src="{{URL::asset('/assets_admin/global/plugins/gsap/main-gsap.min.js')}}"></script>
<script src="{{URL::asset('/assets/js/quem-maps-setting.js')}}"></script>
<script src="{{URL::asset('/assets_admin/global/plugins/tether/js/tether.min.js')}}"></script>
<script src="{{URL::asset('/assets_admin/global/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{URL::asset('/assets_admin/global/plugins/appear/jquery.appear.js')}}"></script>
<script src="{{URL::asset('/assets_admin/global/plugins/jquery-cookies/jquery.cookies.min.js')}}"></script>
<!-- Jquery Cookies, for theme -->
<script src="{{URL::asset('/assets_admin/global/plugins/jquery-block-ui/jquery.blockUI.min.js')}}"></script>
<!-- simulate synchronous behavior when using AJAX -->
<script src="{{URL::asset('/assets_admin/global/plugins/bootbox/bootbox.min.js')}}"></script>
<!-- Modal with Validation -->
<script src="{{URL::asset('/assets_admin/global/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<!-- Custom Scrollbar sidebar -->
<script src="{{URL::asset('/assets_admin/global/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js')}}"></script>
<!-- Show Dropdown on Mouseover -->
<script src="{{URL::asset('/assets_admin/global/plugins/charts-sparkline/sparkline.min.js')}}"></script>
<!-- Charts Sparkline -->
<script src="{{URL::asset('/assets_admin/global/plugins/retina/retina.min.js')}}"></script>
<!-- Retina Display -->
<script src="{{URL::asset('/assets_admin/global/plugins/select2/dist/js/select2.full.min.js')}}"></script>
<!-- Select Inputs -->
<script src="{{URL::asset('/assets_admin/global/plugins/backstretch/backstretch.min.js')}}"></script>
<!-- Background Image -->
<script src="{{URL::asset('/assets_admin/global/plugins/bootstrap-progressbar/bootstrap-progressbar.min.js')}}"></script>
<!-- Animated Progress Bar -->
<script src="{{URL::asset('/assets_admin/global/js/builder.js')}}"></script>
<!-- Theme Builder -->
<script src="{{URL::asset('/assets_admin/global/js/application.js')}}"></script>
<!-- Main Application Script -->
<script src="{{URL::asset('/assets_admin/global/js/plugins.js')}}"></script>
<!-- Main Plugin Initialization Script -->

<!-- BEGIN PAGE SCRIPT -->
<script src="{{URL::asset('/assets_admin/global/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<!-- Tables Filtering, Sorting & Editing -->
<script src="{{URL::asset('/assets_admin/global/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{URL::asset('/assets_admin/global/js/pages/table_dynamic.js')}}"></script>
<script src="{{URL::asset('/assets_admin/global/plugins/switchery/switchery.min.js')}}"></script>
<!-- IOS Switch -->
<script src="{{URL::asset('/assets_admin/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.min.js')}}"></script>
<!-- Select Inputs -->
<script src="{{URL::asset('/assets_admin/global/plugins/dropzone/dropzone.min.js')}}"></script>
<!-- Upload Image & File in dropzone -->
<script src="{{URL::asset('/assets_admin/global/js/pages/form_icheck.js')}}"></script>
<!-- Change Icheck Color - DEMO PURPOSE - OPTIONAL -->
<script src="{{URL::asset('/assets_admin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<!-- >Bootstrap Date Picker -->
<script src="{{URL::asset('/assets_admin/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js')}}"></script>

<script src="{{URL::asset('/assets_admin/global/plugins/touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>
<!-- A mobile and touch friendly input spinner component for Bootstrap -->
<script src="{{URL::asset('/assets_admin/global/plugins/timepicker/jquery-ui-timepicker-addon.min.js')}}"></script>
<!-- Time Picker -->
<script src="{{URL::asset('/assets_admin/global/plugins/multidatepicker/multidatespicker.min.js')}}"></script>
<!-- Multi dates Picker -->
<script src="{{URL::asset('/assets_admin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<!-- >Bootstrap Date Picker -->
<script src="{{URL::asset('/assets_admin/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js')}}"></script>
<!-- >Bootstrap Date Picker in Spanish (can be removed if not use) -->
<script src="{{URL::asset('/assets_admin/global/plugins/colorpicker/spectrum.min.js')}}"></script>
<!-- Color Picker -->
<script src="{{URL::asset('/assets_admin/global/plugins/rateit/jquery.rateit.min.js')}}"></script>
<!-- Rating star plugin -->
<script src="{{URL::asset('/assets_admin/global/js/pages/form_plugins.js')}}"></script>

<script src="{{URL::asset('/assets_admin/global/plugins/summernote/summernote.min.js')}}"></script>
<!-- Simple HTML Editor -->
<script src="{{URL::asset('/assets_admin/global/plugins/cke-editor/ckeditor.js')}}"></script>
<!-- Advanced HTML Editor -->
<script src="{{URL::asset('/assets_admin/global/plugins/typed/typed.min.js')}}"></script>
<!-- Animated Typing -->
<script src="{{URL::asset('/assets_admin/global/js/pages/editor.js')}}"></script>
<script src="{{URL::asset('/assets_admin/global/plugins/dropzone/dropzone.min.js')}}"></script>

<script src="{{URL::asset('/assets_admin/global/plugins/charts-chartjs/Chart.js')}}"></script>
<script src="{{URL::asset('/assets_admin/global/plugins/charts-chartjs/Chart.min.js')}}"></script>
<!-- ChartJS Chart -->
<script src="{{URL::asset('/assets_admin/global/js/pages/charts.js')}}"></script>
<!-- END PAGE SCRIPT -->
<script src="{{URL::asset('/assets_admin/admin/layout1/js/layout.js')}}"></script>
@stack('script')
<script src="{{URL::asset('/assets_admin/global/plugins/metrojs/metrojs.min.js')}}"></script> <!-- Flipping Panel -->
<script src="{{URL::asset('/assets_admin/global/plugins/bootstrap-context-menu/bootstrap-contextmenu.min.js')}}"></script>
<!-- Context Menu -->
{{--  <script src="{{URL::asset('/assets_admin/global/plugins/char/highstock.js')}}"></script> <!-- financial Charts -->  --}}
<script src="{{URL::asset('/assets_admin/global/plugins/charts-highstock/js/modules/exporting.js')}}"></script>
<!-- Financial Charts Export Tool -->
<script src="{{URL::asset('/assets_admin/global/plugins/maps-amcharts/ammap/ammap.js')}}"></script> <!-- Vector Map -->
<script src="{{URL::asset('/assets_admin/global/plugins/maps-amcharts/ammap/maps/js/worldLow.js')}}"></script>
<!-- Vector World Map  -->
<script src="{{URL::asset('/assets_admin/global/plugins/maps-amcharts/ammap/themes/black.js')}}"></script>
<!-- Vector Map Black Theme -->
<script src="{{URL::asset('/assets_admin/global/plugins/skycons/skycons.min.js')}}"></script>
<!-- Animated Weather Icons -->
<script src="{{URL::asset('/assets_admin/global/js/pages/dashboard.js')}}"></script>

<script type="application/javascript">
    google.maps.event.trigger(map, 'resize');
    map.setCenter(new google.maps.LatLng(-6.902273, 107.621686));
</script>


<!-- BARU -->

{{--<script src="{{URL::asset('/assets_admin/admin/layout1/js/layout.js')}}"></script>--}}
<!-- BEGIN PAGE SCRIPTS -->
<script src="{{URL::asset('/assets_admin/global/plugins/magnific/jquery.magnific-popup.min.js')}}"></script>  <!-- Image Popup -->
{{--<script src="{{URL::asset('/assets_admin/global/plugins/isotope/isotope.pkgd.min.js')}}"></script>  <!-- Filter & sort magical Gallery -->--}}
<!-- END PAGE SCRIPTS -->


{{--
<!-- jQuery  -->--}} {{--
        <script src="{{URL::asset('/js/jquery.min.js')}}"></script>--}} {{--
        <script src="{{URL::asset('/js/bootstrap.min.js')}}"></script>--}} {{--
        <script src="{{URL::asset('/js/detect.js')}}"></script>--}} {{--
        <script src="{{URL::asset('/js/fastclick.js')}}"></script>--}} {{--
        <script src="{{URL::asset('/js/jquery.slimscroll.js')}}"></script>--}} {{--
        <script src="{{URL::asset('/js/jquery.blockUI.js')}}"></script>--}} {{--
        <script src="{{URL::asset('/js/waves.js')}}"></script>--}} {{--
        <script src="{{URL::asset('/js/wow.min.js')}}"></script>--}} {{--
        <script src="{{URL::asset('/js/jquery.nicescroll.js')}}"></script>--}} {{--
        <script src="{{URL::asset('/js/jquery.scrollTo.min.js')}}"></script>--}} {{--
        <script src="{{URL::asset('/plugins/peity/jquery.peity.min.js')}}"></script>--}} {{--

        <!-- jQuery  -->--}} {{--
        <script src="{{URL::asset('/plugins/waypoints/lib/jquery.waypoints.js')}}"></script>--}} {{--
        <script src="{{URL::asset('/plugins/counterup/jquery.counterup.min.js')}}"></script>--}} {{--
        <script src="{{URL::asset('/plugins/raphael/raphael-min.js')}}"></script>--}} {{--
        <script src="{{URL::asset('/plugins/jquery-knob/jquery.knob.js')}}"></script>--}} {{--

        <!-- Bootstrap plugin js -->--}} {{--
        <script src="{{URL::asset('/plugins/bootstrap-table/js/bootstrap-table.min.js')}}"></script>--}} {{--
        <script src="{{URL::asset('/pages/jquery.bs-table.js')}}"></script>--}}

<!-- Modal-Effect -->
<script src="{{URL::asset('/plugins/custombox/js/custombox.min.js')}}"></script>
<script src="{{URL::asset('/plugins/custombox/js/legacy.min.js')}}"></script>

{{--
<!-- Date Picker !-->--}} {{--
        <script src="{{URL::asset('/plugins/timepicker/bootstrap-timepicker.js')}}"></script>--}} {{--
        <script src="{{URL::asset('/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')}}"></script>--}} {{--
        <script src="{{URL::asset('/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>--}} {{--

        <!-- Multi Select !-->--}} {{--
        <script src="{{URL::asset('/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js')}}"></script>--}} {{--
        <script src="{{URL::asset('/plugins/switchery/js/switchery.min.js')}}"></script>--}} {{--
        <script src="{{URL::asset('/plugins/multiselect/js/jquery.multi-select.js')}}"></script>--}} {{--
        <script src="{{URL::asset('/plugins/jquery-quicksearch/jquery.quicksearch.js')}}"></script>--}} {{--
        <script src="{{URL::asset('/plugins/select2/js/select2.min.js')}}"></script>--}} {{--
        <script src="{{URL::asset('/pages/jquery.form-advanced.init.js')}}"></script>--}} {{--
        <!-- DatePicker !-->--}} {{--
        <script src="{{URL::asset('/plugins/moment/moment.js')}}"></script>--}} {{--
        <script src="{{URL::asset('/plugins/timepicker/bootstrap-timepicker.js')}}"></script>--}} {{--
        <script src="{{URL::asset('/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')}}"></script>--}} {{--
        <script src="{{URL::asset('/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>--}} {{--
        <script src="{{URL::asset('/plugins/clockpicker/js/bootstrap-clockpicker.min.js')}}"></script>--}} {{--
        <script src="{{URL::asset('/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>--}}

<!-- Sweet Alert !-->
<script src="{{URL::asset('/plugins/bootstrap-sweetalert/sweet-alert.js')}}"></script>
<script src="{{URL::asset('/plugins/bootstrap-sweetalert/swalExtend.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{URL::asset('/plugins/bootstrap-sweetalert/swalExtend.css')}}">
{{--<link rel="stylesheet" type="text/css" href="swalExtend.css">--}}
{{--<link href="{{URL::asset('/assets_admin/global/plugins/dropzone/dropzone.min.css')}}" rel="stylesheet">--}}

{{--POP UP--}}
{{--  <script src="{{URL::asset('/assets_admin/global/plugins/magnific/jquery.magnific-popup.min.js')}}"></script>
<script src="{{URL::asset('/assets/global/plugins/isotope/isotope.pkgd.min.js')}}"></script>
<script src="{{URL::asset('/assets/global/plugins/bootbox/bootbox.min.js')}}"></script>  --}}

<!-- Modal with Validation -->

{{--<script src="{{URL::asset('/assets/js/sweetalert2.all.js')}}"></script>--}}
{{--<script src="sweetalert2.all.min.js"></script>--}}

<!-- Optional: include a polyfill for ES6 Promises for IE11 and Android browser -->
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>--}}

{{--<script src="{{URL::asset('/plugins/chart.js/chart.min.js')}}"></script>--}}

{{--
<!--Chartist Chart-->--}} {{--
        <script src="{{URL::asset('/plugins/chartist/js/chartist.min.js')}}"></script>--}} {{--
        <script src="{{URL::asset('/plugins/chartist/js/chartist-plugin-tooltip.min.js')}}"></script>--}} {{--

        <!-- App Core JS !-->--}} {{--
        <script src="{{URL::asset('/js/jquery.core.js')}}"></script>--}} {{--
        <script src="{{URL::asset('/js/jquery.app.js')}}"></script>--}} {{--

        <script src="{{URL::asset('/pages/jquery.form-pickers.init.js')}}"></script>--}}

<!-- END BARU -->
</body>

<script>
    $(function () {

        ///maps
        //load google map----------------------------------------------------------
        initialize();
        // var infowindow = new google.maps.InfoWindow();
        /*
         * autocomplete location search
         */
        var PostCodeid = '#search_location';
        $(function () {
            $(PostCodeid).autocomplete({
                source: function (request, response) {
                    geocoder.geocode({
                        'address': request.term,
                        /*componentRestrictions: {country: "id"}*/
                    }, function (results, status) {
                        response($.map(results, function (item) {
                            return {
                                label: item.formatted_address,
                                value: item.formatted_address,
                                lat: item.geometry.location.lat(),
                                lon: item.geometry.location.lng()
                            };
                        }));
                    });
                },
                select: function (event, ui) {

                    $('#address').val(ui.item.value);
                    $('#latitude').val(ui.item.lat);
                    $('#longitude').val(ui.item.lon);

                    var latlng = new google.maps.LatLng(ui.item.lat, ui.item.lon);
                    marker.setPosition(latlng);
                    initialize();
                }
            });
        });

        /*
         * Point location on google map
         */
        $('.get_map').click(function (e) {
            var address = $(PostCodeid).val();
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    marker.setPosition(results[0].geometry.location);

                    $('#address').val(results[0].formatted_address);
                    $('#latitude').val(marker.getPosition().lat());
                    $('#longitude').val(marker.getPosition().lng());
                    infowindow.setContent(null);
                    infowindow.open(null);
                } else {
                    alert("Geocode was not successful for the following reason: " + status);
                }
            });
            e.preventDefault();
        });

        //Add listener to marker for reverse geocoding
        google.maps.event.addListener(marker, 'drag', function () {
            geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        $('#address').val(results[0].formatted_address);
                        $('#latitude').val(marker.getPosition().lat());
                        $('#longitude').val(marker.getPosition().lng());
                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, marker);
                    }
                }
            });
        });
    });


</script>
</html>