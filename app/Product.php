<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $primaryKey="id";
    protected $guarded = [];

    public function orderDetails()
    {
        return $this->hasMany('App\OrderDetail','id');
    }

    public function productImages()
    {
        return $this->hasMany('App\ProductImg','id');
    }

    public function categories() {

        return $this->belongsTo('App\Category','category_id');
    }
}
