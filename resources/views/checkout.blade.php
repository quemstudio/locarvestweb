<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 05/01/18
 * Time: 19.46
 */
?>
@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <style>
        #myMap {
            height: 350px;
        }
    </style>
    {{--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB5EksjNLZM377mz5PcHWXG1rTHusDPF4o&callback=initMap&libraries=places"></script>--}}
    {{--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>--}}
    {{--<script src="{{URL::asset('/assets/js/quem-maps-setting.js')}}"></script>--}}


    <div id="main">
        <div class="section section-checkout pt-7 pb-7">

            <div class="container" id="loadingtopayment">
                @if(Cart::count())
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <form id="form1">
                            <div class="row">
                                <div class="col-md-6">
                                    <label id="phonenotif">NOMOR TELEPON <span class="required">*</label>
                                    <div class="form-wrap">
                                        <input type="text" name="phonenumber" id="phone_number" value="{{$user[0]->phone_number}}" size="40" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label id="shippingnotif">TANGGAL PENGIRIMAN <span class="required" >*</span></label>
                                    <div id="datepicker-group" class="input-group date" data-provide="datepicker">
                                        <input type="text" id="shipping" class="form-control" placeholder="yyyy-mm-dd">
                                        <div class="input-group-addon">
                                            <span class="ion-calendar"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label id="addressnotif">ALAMAT TUJUAN <span class="required" >*</span></label>
                                    <div class="form-wrap">
                                        <select name="address" id="addressId" onchange="calculateShipping()">
                                            @foreach($addresses as $address)
                                            <option value="{{ $address->id }}">{{ $address->address }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="col-md-12" type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#exampleModalAddress">Tambah Alamat Baru
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">

                        <div class="cart-totals table-responsive" id="shippingDetail">
                            <table>
                                <tbody>
                                <tr class="cart-subtotal">
                                    <th colspan="2">Subtotal</th>
                                    <td id="loading">Rp. <?php echo Cart::subtotal(); ?></td>
                                </tr>
                                <tr class="shipping">
                                    <th>Pengiriman</th>
                                    @if(empty ( $shipping ))
                                        <td></td>
                                        <td>Pilih Alamat Tujuan</td>
                                        @else
                                        <th id="dstLoading">{{$addresses[0]->distance/1000}} Km</th>
                                        <td id="shippingLoading">Rp. {{ str_replace(',', '.', number_format($shipping)) }}</td>
                                    @endif

                                </tr>
                                <tr class="order-total">
                                    <th colspan="2">Total</th>
                                    @if(empty ( $shipping ))
                                        <td>----</td>
                                    @else
                                        <td id="totalShippingLoading"><strong>Rp. {{ str_replace(',', '.', number_format($total)) }}</strong></td>
                                    @endif

                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="md-col-12">
                        <div class="cart-totals">
                        <div class="proceed-to-checkout" id="payment" onclick="payment()" style="cursor:pointer">
                            <a >LANJUT KE PEMBAYARAN</a>
                        </div>
                        </div>
                    </div>
                </div>
                    @else
                    <div class="row">
                        <div class="md-col-12">
                    <div class="col-md-8 col-md-offset-2 text-center mb-3">
                        <div class="jumbotron jumbotron-fluid">
                            <div class="container">
                                <h1 class="display-4">Order tidak dapat diproses</h1>
                               <p class="lead">Tidak ada produk dalam keranjang belanja dan/atau informasi order anda tidak valid</p>
                            </div>
                        </div>
                    </div>
                        </div>
                    </div>
                    @endif

            </div>
        </div>
    </div>


@endsection
<script>
    $(document).ready(function () {
        //load google map
        initialize();
        // var infowindow = new google.maps.InfoWindow();
        /*
         * autocomplete location search
         */
        var PostCodeid = '#search_location';
        $(function () {
            $(PostCodeid).autocomplete({
                source: function (request, response) {
                    geocoder.geocode({
                        'address': request.term,
                        /*componentRestrictions: {country: "id"}*/
                    }, function (results, status) {
                        response($.map(results, function (item) {
                            return {
                                label: item.formatted_address,
                                value: item.formatted_address,
                                lat: item.geometry.location.lat(),
                                lon: item.geometry.location.lng()
                            };
                        }));
                    });
                },
                select: function (event, ui) {

                    $('#address').val(ui.item.value);
                    $('#latitude').val(ui.item.lat);
                    $('#longitude').val(ui.item.lon);

                    var latlng = new google.maps.LatLng(ui.item.lat, ui.item.lon);
                    marker.setPosition(latlng);
                    initialize();
                }
            });
        });

        /*
         * Point location on google map
         */
        $('.get_map').click(function (e) {
            var address = $(PostCodeid).val();
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    marker.setPosition(results[0].geometry.location);

                    $('#address').val(results[0].formatted_address);
                    $('#latitude').val(marker.getPosition().lat());
                    $('#longitude').val(marker.getPosition().lng());
                    infowindow.setContent(results[0].formatted_address);
                    infowindow.open(map, marker);
                } else {
                    alert("Geocode was not successful for the following reason: " + status);
                }
            });
            e.preventDefault();
        });

        //Add listener to marker for reverse geocoding
        google.maps.event.addListener(marker, 'drag', function () {
            geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        $('#address').val(results[0].formatted_address);
                        $('#latitude').val(marker.getPosition().lat());
                        $('#longitude').val(marker.getPosition().lng());
                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, marker);
                    }
                }
            });
        });
    });
</script>
<style>
    .modal-backdrop.in {
        z-index: 1000;
    }

    .modal-open {
        overflow: scroll;
    }

    .pac-container {
        z-index: 10000 !important;
    }

</style>

{{--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB5EksjNLZM377mz5PcHWXG1rTHusDPF4o&callback=initMap&libraries=places"></script>--}}
{{--<script src="{{URL::asset('/assets/js/quem-maps-setting.js')}}"></script>--}}
@push('scripts')
<script type="text/javascript">
$(document).ready(function() {
    if("{{$status[0]->classification}}" === "individu"){
        $("#datepicker-group").datepicker({
        startDate:'+1d',
            endDate:'+30d',
            format: 'yyyy-mm-dd',
            daysOfWeekDisabled: [0,1,3,5]

    });
    }else{
        $("#datepicker-group").datepicker({
            startDate:'+1d',
            endDate:'+30d',
            format: 'yyyy-mm-dd',
            daysOfWeekDisabled: [0,1,3,5]
        });
    }


  });

</script>

@endpush

{{--<script src="https://apis.google.com/js/platform.js" async defer></script>--}}
{{--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC5cOgK1Qryl6gOwf-tdoN2-R5ThKYjwmU"></script>--}}
{{--<script src="{{URL::asset('/assets/js/quem-maps-setting.js')}}"></script>--}}

<script type="text/javascript">

 function payment(){
    console.log('test')
         if (!$('#phone_number').val() || !$('#addressId').val() || !$('#shipping').val()){
            
            $.toast({
                        heading: 'Error',
                        text: 'Lengkapi Data',
                        showHideTransition: 'fade',
                        icon: 'error',
                        hideAfter: 2000,
                        position: 'bottom-center',
                        loader: false,
                    });
                    if(!$('#phone_number').val()){
                        $('#phonenotif').html('<span class="required" >MASUKKAN NOMOR TELEPON *</span>');
                    }else{
                        $('#phonenotif').html('NOMOR TELEPON <span class="required" >*</span>');
                    }

                    if(!$('#addressId').val()){
                        $('#addressnotif').html('<span class="required" >*PILIH/TAMBAH ALAMAT *</span>');
                    }else{
                        $('#addressnotif').html('ALAMAT TUJUAN <span class="required" >*</span>');
                    }

                    if(!$('#shipping').val()){
                        $('#shippingnotif').html('<span class="required" >PILIH TANGGAL PENGIRIMAN *</span>');
                    }else{
                        $('#shippingnotif').html('TANGGAL PENGIRIMAN <span class="required" >*</span>');
                    }
         }else{
             var phonenumber = $("#phone_number").val();
             var shipdate = $("#shipping").val();
             var address = $("#addressId").val();
            $.ajax({
                 url: '{{URL::to("/user-phoneshipping")}}',
                 type: 'POST',
                 data: {phonenumber: phonenumber, shipdate:shipdate, address:address},
                 success: function (data) {
                     if ($.isEmptyObject(data.error)) {
                         var url = '{{URL::to('/user-payment')}}';
                         window.location.href=url;
                     } else {
                         printErrorMsg(data.error);
                     }
                 }
             });
         }
 }
</script>
