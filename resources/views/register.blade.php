<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 05/01/18
 * Time: 19.55
 */
?>
@extends('layouts.app')
@section('content')
    <div id="main">
        <div class="section section-bg-10 pt-0 pb-0">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="page-title text-center">Register</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="section border-bottom pt-2 pb-2">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <ul class="breadcrumbs">
                            <li><a href="{{URL::to('/')}}">Home</a></li>
                            <li>Register</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="section pt-7 pb-7">
            <div class="container">
                <div class="row">
                    <form>
                        <div class="col-sm-4">

                            <div class="commerce">
                                <h2>Profile</h2>
                                <div class="commerce-login-form">
                                    <div class="row ">
                                        <div class="col-md-12">
                                            <label>Username or email address <span class="required">*</span></label>
                                            <div class="form-wrap">
                                                <input type="text" name="your-name" value="" size="40">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Password <span class="required">*</span></label>
                                            <div class="form-wrap">
                                                <input type="password" name="your-pass" value="" size="40">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="commerce">
                                <h2>Address</h2>

                                <div class="commerce-login-form">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Username or email address <span class="required">*</span></label>
                                            <div class="form-wrap">
                                                <input type="text" name="your-name" value="" size="40">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Password <span class="required">*</span></label>
                                            <div class="form-wrap">
                                                <input type="password" name="your-pass" value="" size="40">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="commerce">
                                <h2>Etc</h2>

                                <div class="commerce-login-form">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Username or email address <span class="required">*</span></label>
                                            <div class="form-wrap">
                                                <input type="text" name="your-name" value="" size="40">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Password <span class="required">*</span></label>
                                            <div class="form-wrap">
                                                <input type="password" name="your-pass" value="" size="40">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="commerce">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-wrap" style="float:right">
                                            <input type="submit" value="REGISTER NOW">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
