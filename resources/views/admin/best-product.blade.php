<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 16/01/18
 * Time: 19.32
 */
?>
@extends('layouts.appadmin')
@section('content')
    <div class="header">
        <h2><strong>Produk Terbaik</strong></h2>
    </div>
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif(session('error'))
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    @endif
    <div class="row">
        <div class="col-lg-12 portlets">
            <div class="panel">
                {{--<div class="panel-header ">--}}
                    {{--<h3><a href="{{route('product.add.show')}}">--}}
                            {{--<button type="button" class="btn btn-primary">+ Add Product</button>--}}
                        {{--</a></h3>--}}
                {{--</div>--}}
                <div class="panel-content">
                    <div class="filter-left panel-content pagination2 table-responsive">
                        <table class="table table-hover table-dynamic table-tools" data-table-name="Data Products" data-table-orientation="landscape">
                            <thead>
                            <tr>
                                <th style="width: 20px">No</th>
                                <th>Nama Produk</th>
                                {{--<th class='hidden-1024' style="display:none">Description</th>--}}
                                {{--<th class='hidden-1024' style="display:none">Land Area</th>--}}
                                {{--<th class='hidden-350' style="display:none">Planting Schedule</th>--}}
                                {{--<th class='hidden-480' style="display:none">Seed Quantity</th>--}}
                                {{--<th class='hidden-480' style="display:none">Harvest Time</th>--}}
                                {{--<th class='hidden-480' style="display:none">Harvest Quantity</th>--}}
                                {{--<th class='hidden-480'>Price</th>--}}
                                {{--<th class='hidden-480'>Price Category</th>--}}
                                {{--<th class='hidden-480'>Product Category</th>--}}
                                {{--<th class='hidden-480'>Supplier</th>--}}
                                {{--<th class='hidden-480' style="display:none">Best Offer</th>--}}
                                {{--<th class='hidden-480' style="display:none">Status</th>--}}
                                <th class='hidden-480'>Total Terjual</th>
                                <th class='hidden-480'>Total Pendapatan</th>
                                {{--<th class='hidden-480'></th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($best as $data)
                                @php
                                    $product = \App\Product::find($data->product_id);
                                    $cat = \App\Category::find($product->category_id);
                                    $supplier = \App\Supplier::find($product->supplier_id);
                                @endphp
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td onclick="detailProduct('{{$product->id}}', '{{$cat->name}}', '{{$supplier->name}}')" style="cursor: pointer;">
                                        {{$product->name}}
                                    </td>
                                    {{--<td class='hidden-350' style="display:none">--}}
                                        {{--{{$product->description}}--}}
                                    {{--</td>--}}
                                    {{--<td class='hidden-350' style="display:none">{{$product->land_area}}</td>--}}
                                    {{--<td class='hidden-350' style="display:none">{{$product->planting_schedule}}</td>--}}
                                    {{--<td class='hidden-350' style="display:none">{{$product->seed_quantity}}</td>--}}
                                    {{--<td class='hidden-350' style="display:none">{{$product->harvest_time}}</td>--}}
                                    {{--<td class='hidden-350' style="display:none">{{$product->harvest_quantity}}</td>--}}
                                    {{--<td class='hidden-350' onclick="detailProduct('{{$product->id}}', '{{$cat->name}}', '{{$supplier->name}}')" style="cursor: pointer;">--}}
                                        {{--{{$product->price}}--}}
                                    {{--</td>--}}
                                    {{--<td class='hidden-350' onclick="detailProduct('{{$product->id}}', '{{$cat->name}}', '{{$supplier->name}}')" style="cursor: pointer;">--}}
                                        {{--{{$product->category}}--}}
                                    {{--</td>--}}
                                    {{--<td class='hidden-350' onclick="detailProduct('{{$product->id}}', '{{$cat->name}}', '{{$supplier->name}}')" style="cursor: pointer;">--}}
                                        {{--{{$cat->name}}--}}
                                    {{--</td>--}}
                                    {{--<td class='hidden-1024' onclick="detailProduct('{{$product->id}}', '{{$cat->name}}', '{{$supplier->name}}')" style="cursor: pointer;">--}}
                                        {{--{{$supplier->name}}--}}
                                    {{--</td>--}}
                                    {{--@if($product->best_offer == 'on')--}}
                                        {{--<td class='hidden-350' style="display:none">Yes</td>--}}
                                    {{--@else--}}
                                        {{--<td class='hidden-350' style="display:none">No</td>--}}
                                    {{--@endif--}}
                                    {{--@if($product->status == 'on')--}}
                                        {{--<td class='hidden-350' onclick="detailProduct('{{$product->id}}', '{{$cat->name}}', '{{$supplier->name}}')" style="display:none">--}}
                                            {{--Ready--}}
                                        {{--</td>--}}
                                    {{--@else--}}
                                        {{--<td class='hidden-350' onclick="detailProduct('{{$product->id}}', '{{$cat->name}}', '{{$supplier->name}}')" style="display:none">--}}
                                            {{--Not Ready--}}
                                        {{--</td>--}}
                                    {{--@endif--}}
                                    <td class='hidden-350' onclick="detailProduct('{{$product->id}}', '{{$cat->name}}', '{{$supplier->name}}')" style="cursor: pointer;">
                                        {{$data->total}}
                                    </td>
                                    <td class='hidden-350' onclick="detailProduct('{{$product->id}}', '{{$cat->name}}', '{{$supplier->name}}')" style="cursor: pointer;">
                                        {{$data->total_dapat}}
                                        {{--Rp. {{ str_replace(',', '.', number_format($data->total_dapat))  }},---}}

                                        {{--{{setlocale(LC_MONETARY,"en_US")}}--}}
                                        {{--{{money_format("%i", $data->total_dapat)}}--}}
                                    </td>
                                    {{--<td class='hidden-480' style="min-width:150px">--}}
                                        {{--<div class="btn-group">--}}
                                            {{--<a class="btn btn-success" href="{{route('product.edit.show' ,$data['id'])}}">--}}
                                                {{--<i style="padding:0px; margin:0px" class="fa fa-edit"></i>--}}
                                            {{--</a>--}}
                                            {{--<button class="btn btn-danger" type="button"--}}
                                                    {{--id="danger-alert"--}}
                                                    {{--data-tr="tr_{{$data->id}}"--}}
                                                    {{--onclick="deleteProduct('{{$data->id}}')">--}}
                                                {{--<i class="fa fa-trash-o"></i>--}}
                                            {{--</button>--}}
                                        {{--</div>--}}
                                    {{--</td>--}}
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="edit-info" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content p-0 b-0">
                <div class="panel panel-color panel-primary">
                    <div class="panel-heading">
                        <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 class="panel-title">Product Detail</h3>
                    </div>
                    <form method="post">
                        {{ csrf_field() }}
                        <div class="panel-body">
                            <span id="id"></span>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">ID</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="id-detail" name="id-detail" placeholder="" readonly>--}}
                                        <label class="form-control"
                                               placeholder="Announcement" id="id-detail"
                                               name="id-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Name</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control"
                                               placeholder="Announcement" id="name-detail"
                                               name="name-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Planting Schedule</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control" id="ps-detail"
                                               name="ps-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Land Area</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control" id="la-detail"
                                               name="la-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Seed Quantity</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control" id="sq-detail"
                                               name="sq-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Harvest Time</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control" id="ht-detail"
                                               name="ht-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Harvest Quantity</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control" id="hq-detail"
                                               name="hq-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Price</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control" id="price-detail"
                                               name="price-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Price Category</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control" id="pc-detail"
                                               name="pc-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Description</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control" id="description-detail"
                                               name="description-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Amount Left</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control" id="al-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Best Offer</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control" id="bo-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Preorder Date</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control" id="pd-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Image</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control" id="image-detail"></label>
                                    </div>
                                </div>
                            </div>

                            {{--//Product Category--}}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Product Category</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control" id="category-detail"></label>
                                    </div>
                                </div>
                            </div>

                            {{--//Supplier--}}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Supplier</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control" id="supplier-detail"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Status</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control" id="status-detail"
                                               name="status-detail"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12" style="margin-top: 20px">
                                    {{--<a href="{{route('product.edit.show' , $data->id)}}">--}}
                                    {{--<button type="button" class="btn btn-primary"--}}
                                    {{--style="float:right">Edit</button>--}}
                                    {{--</a>--}}
                                    <a onclick="$('#edit-info').modal('hide');">
                                        <button type="button" class="btn btn-primary" style="float:right">Close</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection
@push('script')
    <script>
        function detailProduct(id, catName, supName) {
            $.ajax({
                url: 'product/show_detail/' + id,
                error: function () {
                    $('#nameApp').html('<p>An error has occurred</p>');
                },
                dataType: 'json',
                success: function (data) {
                    $('#id').html('<input type="hidden" value="' + data.id + '" name="id" id="id">');
                    $('#id-detail').text(data.id);
                    $('#name-detail').text(data.name);
                    $('#ps-detail').text(data.planting_schedule);
                    $('#la-detail').text(data.land_area);
                    $('#sq-detail').text(data.seed_quantity);
                    $('#ht-detail').text(data.harvest_time);
                    $('#hq-detail').text(data.harvest_quantity);
                    $('#price-detail').text(data.price);
                    $('#pc-detail').text(data.category);
                    // $('#status-detail').text(data.status);
                    if (data.status === 'on')
                        $('#status-detail').text('Ready');
                    else
                        $('#status-detail').text('Not Ready');
                    $('#description-detail').text(data.description);
                    $('#al-detail').text(data.amount_left);
                    // $('#bo-detail').text(data.best_offer);
                    if (data.best_offer === 'on')
                        $('#bo-detail').text('Yes');
                    else
                        $('#bo-detail').text('No');
                    $('#pd-detail').text(data.preorder_date);
                    $('#image-detail').text(data.image);
                    $('#category-detail').text(catName);
                    $('#supplier-detail').text(supName);

                    $('#edit-info').modal('show');
                },
                type: 'GET'
            });

        }
    </script>
@endpush