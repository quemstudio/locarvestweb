<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageImg extends Model
{
    protected $primaryKey="id";
    protected $guarded = [];
}
