<?php

namespace App\Http\Controllers;

use App\CategoryPost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CategoryPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categoryPosts= CategoryPost::all();
        $response = [
            'categoryPosts' => $categoryPosts
        ];
        return response()->json($response,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("categoryPost.add");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return CategoryPost::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return CategoryPost::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categoryPost = CategoryPost::findOrFail($id);
        return view("admin.detail-category-post", compact('categoryPost'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $categoryPost = CategoryPost::findOrFail($id);
        $categoryPost ->update($request->all());

        return $categoryPost ;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categoryPost= CategoryPost::findOrFail($id);
        $categoryPost->delete();

        return response()->json(null,204);
    }

    public function categoryPostAdmin() {

        $cat_post = CategoryPost::where('id', '!=' , 21)->where('id', '!=' , 22)->get();
        $no=1;
        return view('admin.category-post',compact('cat_post','no'));
    }

    public function addCategoryPostAdmin(Request $request) {

        if ($request->name) {
            $path = '';
            if ($request->picture)
                $path = Storage::disk('public')->put('categoryPost', $request->picture);

            $cat_post = CategoryPost::create([
                'name' => $request->name,
                'description' => $request->description,
                'picture' => $path,
            ]);
            if ($cat_post != null)
                return redirect()->route('admin.category.post')->with('success', 'Insert Data Success!');
            else
                return redirect()->route('admin.category.post')->with('error', 'Insert Data Failed!');
        }


        return view('admin.add-category-post');
    }

    public function updateCategoryPost(Request $request, $id)
    {
        $categoryPost = CategoryPost::findOrFail($id);
        if ($request->picture) {
            Storage::disk('public')->delete($categoryPost->picture);
            $path = Storage::disk('public')->put('categoryPost', $request->picture);
            $categoryPost->picture = $path;
        }
        $categoryPost->name = $request->name;
        $categoryPost->description = $request->description;
        $categoryPost->save();

        if ($categoryPost != null)
            return redirect()->route('admin.category.post')->with('success', 'Update data sukses!');

        return redirect()->route('admin.category.post')->with('error', 'Update data gagal!');
    }
}
