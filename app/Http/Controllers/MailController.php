<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Mail\DemoEmail;
use Illuminate\Support\Facades\Mail;
use App\OrderDetail;
use Illuminate\Support\Facades\DB;
use App\Product;
use Auth;
use App\Order;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;


class MailController extends Controller
{
   
    public function send($lastOrder,$userId)
    {

        $joined = DB::table('customers')
        ->join('orders', 'customers.id', '=', 'orders.customer_id')
        ->join('order_details', 'orders.id', '=', 'order_details.order_id')
        ->join('products', 'order_details.product_id', '=', 'products.id')
        ->join('billings', 'orders.id', '=', 'billings.order_id')
        ->join('addresses', 'orders.address_id', '=', 'addresses.id')
        ->select(DB::raw('orders.total_price orderTotal, order_details.total_price as orderDetailTotal, orders.*, order_details.*, customers.*, products.*,billings.*,addresses.*'))
        ->where('customers.user_id', $userId)
        ->where('orders.id',$lastOrder)
        ->get();

        $total_price = null;
        foreach($joined as $join){
            $total_price = $total_price + $join->total_price;
        }

        $user = User::find($userId);
        
        $objDemo = new \stdClass();
        $objDemo->emailType = 'invoice';
        $objDemo->sender = 'Locarvest';
        $objDemo->receiver = $user->first_name." ".$user->last_name;
        $objDemo->products = $joined;
        $objDemo->total = $total_price;
        $objDemo->phone =  $user->phone_number;
        $objDemo->date = Carbon::today();
        //return new DemoEmail($objDemo);

        Mail::to("faqih.salban@gmail.com")->send(new DemoEmail($objDemo));
    }

    public function invoiceMail($lastOrder,$userId)
    {

        $joined = DB::table('customers')
            ->join('orders', 'customers.id', '=', 'orders.customer_id')
            ->join('order_details', 'orders.id', '=', 'order_details.order_id')
            ->join('products', 'order_details.product_id', '=', 'products.id')
            ->join('billings', 'orders.id', '=', 'billings.order_id')
            ->join('addresses', 'orders.address_id', '=', 'addresses.id')
            ->select(DB::raw('orders.total_price orderTotal, order_details.total_price as orderDetailTotal, orders.*, order_details.*, customers.*, products.*,billings.*,addresses.*'))
            ->where('customers.user_id', $userId)
            ->where('orders.id',$lastOrder)
            ->get();

        $total_price = null;
        foreach($joined as $join){
            $total_price = $total_price + $join->total_price;
        }

        $user = User::find($userId);
        
        $objDemo = new \stdClass();
        $objDemo->emailType = 'invoice';
        $objDemo->sender = 'Locarvest';
        $objDemo->receiver = $user->first_name." ".$user->last_name;
        $objDemo->products = $joined;
        $objDemo->total = $total_price;
        $objDemo->phone =  $user->phone_number;
        $objDemo->date = Carbon::today();
        //return new DemoEmail($objDemo);
        Mail::to($user->email)->send(new DemoEmail($objDemo));
    }

    public function paymentThanksMail($orderId, $bank, $norek)
    {

        $joined = DB::table('customers')
            ->join('orders', 'customers.id', '=', 'orders.customer_id')
            ->join('users', 'users.id', '=', 'customers.user_id')
            ->where('orders.id',$orderId)
            ->get();

        $total_price = null;
        foreach($joined as $join){
            $total_price = $total_price + $join->total_price;
        }

        $objDemo = new \stdClass();
        $objDemo->emailType = 'paymentThanks';
        $objDemo->sender = 'SenderUserName';
        $objDemo->orderNo = $joined[0]->order_number;
        $objDemo->nominal = $joined[0]->total_price;
        $objDemo->bank = $bank;
        $objDemo->accountNumber = $norek;
        $objDemo->receiver = $joined[0]->first_name." ".$joined[0]->last_name;

        Mail::to($joined[0]->email)->send(new DemoEmail($objDemo));
    }

    public function registerMail($user)
    {

        $objDemo = new \stdClass();
        $objDemo->emailType = 'register';
        $objDemo->sender = 'SenderUserName';
        $objDemo->email = $user->email;
        $objDemo->receiver = $user->first_name." ".$user->last_name;
        $objDemo->password = $user->password;
        $objDemo->userId= Crypt::encrypt($user->id);

        Mail::to($user->email)->send(new DemoEmail($objDemo));
    }
}