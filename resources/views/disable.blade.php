<?php
/**
 * Created by PhpStorm.
 * User: najibalkhala
 * Date: 3/3/2018
 * Time: 5:06 PM
 */
?>
@extends('layouts.app')
@section('content')
    <div id="main">
        <div class="section section-error pt-12 pb-12">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center">
                            <img src="{{URL::asset('/assets/images/disable.png')}}" class="mb-6" alt=""/>
                            <h3 class="error-title uppercase">Oops! situs ini tidak tesedia!</h3>
                            <h1 class="error-title uppercase">
                                Segera selesaikan pembayaran anda untuk dapat membukanya :)
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
