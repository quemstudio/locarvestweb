<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 16/01/18
 * Time: 19.33
 */

?>
@extends('layouts.appadmin')
@section('content')
    <div class="header">
        <h2><strong>Pelanggan</strong> Terbaik</h2>
    </div>
    <div class="row">
        <div class="col-lg-12 portlets">
            <div class="panel">
                <div class="panel-content">
                    <div class="filter-left panel-content pagination2 table-responsive">
                        <table class="table table-hover table-dynamic table-tools" data-table-name="Data Customers">
                            <thead>
                            <tr>
                                <th style="width: 20px">No</th>
                                <th>Nama Lengkap</th>
                                <th>Total Pembelian Produk</th>
                                <th>Total Pengeluaran</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($best as $data)
                                @php
                                    $user = \App\User::find($data->user_id);
                                    $cust = \App\Customer::find($data->id);
                                @endphp
                                <tr onclick="detailCustomer('{{$user}}', '{{$cust}}')" style="cursor: pointer;">
                                    <td>{{$no++}}</td>
                                    <td>
                                        {{$data->first_name." ".$data->last_name}}</td>
                                    <td>
                                        {{$data->total_beli}}
                                    </td>
                                    <td>
                                        {{$data->total_keluar}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="edit-info" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content p-0 b-0">
                <div class="panel panel-color panel-primary">
                    <div class="panel-heading">
                        <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 class="panel-title">Customer Detail</h3>
                    </div>
                    <form method="post">
                        {{ csrf_field() }}
                        <div class="panel-body">
                            {{--<span id="id"></span>--}}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Nama</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control"
                                               placeholder="Announcement" id="name-detail"
                                               name="name-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Username</label>
                                    <div class="col-sm-9">
                                        <label class="form-control" id="username-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Email</label>
                                    <div class="col-sm-9">
                                        <label class="form-control" id="email-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Jenis Kelamin</label>
                                    <div class="col-sm-9">
                                        <label class="form-control" id="gender-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Nomor Telepon</label>
                                    <div class="col-sm-9">
                                        <label class="form-control" id="phone-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Klasifikasi</label>
                                    <div class="col-sm-9">
                                        <label class="form-control" id="klas-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Gambar</label>
                                    <div class="col-sm-9">
                                        <label class="form-control" id="image-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12" style="margin-top: 20px">
                                    <a onclick="$('#edit-info').modal('hide');">
                                        <button type="button" class="btn btn-primary" style="float:right">Close</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection
@push('script')
    <script>
        function detailCustomer(tmpUser, tmpCust) {

            var user = JSON.parse(tmpUser);
            var cust = JSON.parse(tmpCust);

            $('#name-detail').text(user.first_name + ' ' + user.last_name);
            $('#username-detail').text(user.username);
            $('#email-detail').text(user.email);
            $('#gender-detail').text(user.gender);
            $('#phone-detail').text(user.phone_number);
            $('#klas-detail').text(cust.classification);
            $('#image-detail').text(cust.image);

            $('#edit-info').modal('show');

        }
    </script>
@endpush