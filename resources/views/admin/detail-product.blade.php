<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 16/01/18
 * Time: 19.27
 */
?>
@extends('layouts.appadmin') @section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-header">
                    <h3>
                        <i class="icon-target"></i>
                        <strong>Detail </strong>Produk</h3>
                </div>
                <div class="panel-content">
                    <div class="nav-tabs2">
                        <ul class="nav nav-tabs nav-red">
                            <li class="nav-item active">
                                <a class="nav-link" href="#tab-datepicker" data-toggle="tab">
                                    <i class="icon-home"></i>Detail</a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" href="#tab-checkbox" data-toggle="tab">
                                    <i class="fa fa-file-photo-o"></i>Gambar</a>
                            </li>

                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab-datepicker">
                                <div class="row">
                                    <form class=" form-horizontal" method="post"
                                          action="{{route('product.update.submit',$product->id)}}"
                                          enctype="multipart/form-data">
                                        {{csrf_field()}} {{method_field('PUT')}}
                                        <div class="col-md-12">

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Nama Produk</label>
                                                <div class="col-sm-9">
                                                    <input class="form-control" type="text" name="name"
                                                           value="{{$product->name}}" placeholder="">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Jadwal Tanam</label>
                                                <div class="col-sm-9">
                                                    <input class="b-datepicker form-control" name="planting_schedule"
                                                           value="{{$product->planting_schedule}}"
                                                           placeholder="Select a date..."
                                                           type="text">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Luas Lahan (m<sup>2</sup>)</label>
                                                <div class="col-sm-9">
                                                    <input class="form-control input-sm" placeholder="" name="land_area"
                                                           value="{{$product->land_area}}" type="number">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Kuantitas Benih (kg)</label>
                                                <div class="col-sm-9">
                                                    <input class="form-control input-sm" placeholder=""
                                                           name="seed_quantity" value="{{$product->seed_quantity}}"
                                                           type="number">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Jadwal Panen</label>
                                                <div class="col-sm-9">
                                                    <input name="harvest_time" class="b-datepicker form-control"
                                                           name="harvest_time" value="{{$product->harvest_time}}"
                                                           placeholder="Select a date..."
                                                           type="text">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Kuantitas Panen (kg)</label>
                                                <div class="col-sm-9">
                                                    <input class="form-control input-sm" placeholder=""
                                                           name="harvest_quantity"
                                                           value="{{$product->harvest_quantity}}" type="number">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Harga (IDR)</label>
                                                <div class="col-sm-9">
                                                    <input class="form-control input-sm" placeholder="" name="price"
                                                           value="{{$product->price}}" type="number">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Kategori Harga </label>
                                                <div class="col-sm-9">
                                                    {{--<div class="form-group">--}}
                                                    <select class="form-control" style="width:100%;"
                                                            name="category">
                                                        @if($product->category == 'wholesale')
                                                            <option value="individu">Individu</option>
                                                            <option value="wholesale" selected>Wholesale</option>
                                                        @else
                                                            <option value="individu" selected>Individu</option>
                                                            <option value="wholesale">Wholesale</option>
                                                        @endif
                                                    </select>
                                                    {{--</div>--}}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Status Ketersediaan</label>
                                                <div class="col-sm-9">
                                                    <div class="onoffswitch2">
                                                        @if($product->status == 'on')
                                                            <input name="status" class="onoffswitch-checkbox"
                                                                   id="status" type="checkbox" value="on" checked>
                                                            <label class="onoffswitch-label" for="status">
                                                                <span class="onoffswitch-inner"></span>
                                                                <span class="onoffswitch-switch"></span>
                                                            </label>
                                                        @else
                                                            <input name="status" class="onoffswitch-checkbox"
                                                                   id="status" value="on" type="checkbox">
                                                            <label class="onoffswitch-label" for="status">
                                                                <span class="onoffswitch-inner"></span>
                                                                <span class="onoffswitch-switch"></span>
                                                            </label>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Deskripsi</label>
                                                <div class="col-sm-9">
                                                    <textarea class="form-control" placeholder="" name="description"
                                                              value="{{$product->description}}">{{$product->description}}</textarea>
                                                </div>
                                            </div>
                                            {{--<div class="form-group">--}}
                                                {{--<label class="col-sm-3 control-label">Amount Left</label>--}}
                                                {{--<div class="col-sm-9">--}}
                                                    {{--<input class="form-control input-sm" placeholder=""--}}
                                                           {{--name="amount_left" value="{{$product->amount_left}}"--}}
                                                           {{--type="number">--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Penawaran Terbaik</label>
                                                <div class="col-sm-9">
                                                    <div class="onoffswitch2">
                                                        @if($product->best_offer == 'on')
                                                            <input name="best_offer" class="onoffswitch-checkbox"
                                                                   id="best_offer" value="on" checked type="checkbox">
                                                            <label class="onoffswitch-label" for="best_offer">
                                                                <span class="onoffswitch-inner"></span>
                                                                <span class="onoffswitch-switch"></span>
                                                            </label>
                                                        @else
                                                            <input name="best_offer" class="onoffswitch-checkbox"
                                                                   id="best_offer" value="on" type="checkbox">
                                                            <label class="onoffswitch-label" for="best_offer">
                                                                <span class="onoffswitch-inner"></span>
                                                                <span class="onoffswitch-switch"></span>
                                                            </label>
                                                        @endif
                                                    </div>
                                                    {{--
                                                    <input class="form-control input-sm" placeholder="" name="best_offer"
                                                        value="{{$product->best_offer}}" type="number"> --}}
                                                </div>
                                            </div>
                                            {{--<div class="form-group">--}}
                                                {{--<label class="col-sm-3 control-label">Preorder Date</label>--}}
                                                {{--<div class="col-sm-9">--}}
                                                    {{--<input class="b-datepicker form-control" name="preorder_date"--}}
                                                           {{--value="{{$product->preorder_date}}"--}}
                                                           {{--placeholder="Select a date..."--}}
                                                           {{--type="text">--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Gambar</label>
                                                <div class="col-sm-9">
                                                    <div class="fileinput fileinput-new input-group"
                                                         data-provides="fileinput">
                                                        {{--<div class="form-control" data-trigger="fileinput">--}}
                                                        {{--<i class="glyphicon glyphicon-file fileinput-exists"></i>--}}
                                                        {{--<span class="fileinput-filename">{{$product->image}}</span>--}}
                                                        {{--</div>--}}
                                                        <input type="text" class="form-control" id="upload-file-info" readonly value="{{$product->image}}">
                                                        <span class="input-group-addon btn btn-default btn-file">
                                                            <span class="fileinput-new">Pilih...</span>
                                                            <span class="fileinput-exists">Ganti</span>
                                                            <input type="file" name="image" onchange="$('#upload-file-info').val($(this).val().split('\\').pop());">
                                                        </span>
                                                        <a href="#"
                                                           class="input-group-addon btn btn-default fileinput-exists"
                                                           data-dismiss="fileinput">Hapus</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Kategori </label>
                                                <div class="col-sm-9">
                                                    {{--<div class="form-group">--}}
                                                    <select class="form-control" style="width:100%;"
                                                            name="category_id">
                                                        @foreach($categories as $data) @if($data->id == $product->category_id)
                                                            <option value="{{$data->id}}"
                                                                    selected>{{$data->name}}</option>
                                                        @else
                                                            <option value="{{$data->id}}">{{$data->name}}</option>
                                                        @endif @endforeach
                                                    </select>
                                                    {{--</div>--}}
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Petani </label>
                                                <div class="col-sm-9">
                                                    {{--<div class="form-group">--}}
                                                    <select class="form-control" name="supplier_id"
                                                            style="width:100%;">
                                                        @foreach($suppliers as $data) @if($data->id == $product->supplier_id)
                                                            <option value="{{$data->id}}"
                                                                    selected>{{$data->name}}</option>
                                                        @else
                                                            <option value="{{$data->id}}">{{$data->name}}</option>
                                                        @endif @endforeach
                                                    </select>
                                                    {{--</div>--}}
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <button type="submit" class="btn btn-success"
                                                            style="float:right">Update Produk
                                                    </button>
                                                    <a href="{{route('admin.product')}}">
                                                        <button type="button" class="btn btn-danger" style="float:right">
                                                            Batal
                                                        </button>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab-checkbox">
                                <form action="{{route('productImg.upload.submit', $product->id)}}"
                                      method="post" class="dropzone"
                                      id="my-dropzone" name="file" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <h3><strong>Gambar Produk</strong>
                                            </h3>
                                            <p>Maks ukuran gambar: <strong>5 MB</strong></p>
                                        </div>
                                    </div>
                                </form>
                                <div class="col-sm-12">
                                    <a href="{{route('admin.product')}}">
                                        <button type="button" class="btn btn-success" style="float:right; margin-top: 20px">
                                            Done
                                        </button>
                                    </a>
                                </div>
                                @php
                                    $imgs = DB::table('product_imgs')->where('product_id', $product->id)->get();
                                @endphp
                                <div class="row">
                                    <div class="col-lg-12 portlets">
                                        {{--<div class="panel panel-transparent">--}}
                                            {{--<div class="panel-content">--}}
                                                <div class="portfolioContainer grid">
                                                    @foreach($imgs as $img)
                                                        <div class="col-md-4">
                                                            <div class="panel">
                                                                <div class="panel-header">
                                                                    <h3>Gambar <strong>Produk</strong></h3>
                                                                    <div class="control-btn">
                                                                        <a class="panel-toggle"><i class="fa fa-angle-down"></i></a>
                                                                        <a class="panel-close" id="rp{{$img->id}}" style="display: none"><i class="icon-trash"></i></a>
                                                                        <a onclick="deleteImg('{{$img->id}}')"><i class="icon-trash"></i></a>
                                                                    </div>
                                                                </div>
                                                                <div class="panel-content">
                                                                    <img src="{{URL::asset('/storage/'.$img->directory)}}" alt="Image" style="width: 100%"
                                                                         class="animal effect-zoe magnific" data-mfp-src="{{URL::asset('/storage/'.$img->directory)}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--<script type="text/javascript">--}}
    {{--var myDropzone = new Dropzone("#my-dropzone", {--}}
    {{--parallelUploads: 20,--}}
    {{--maxFilesize: 2,--}}
    {{--maxFiles: 20,--}}
    {{--addRemoveLinks: true,--}}
    {{--paramName: "file" // The name that will be used to transfer the file--}}
    {{--});--}}
    {{--</script>--}}
@endsection
@push('script')
    {{--<script src="{{URL::asset('/assets_admin/admin/layout1/js/layout.js')}}"></script>--}}
    <script type="text/javascript">
        var myDropzone = new Dropzone("#my-dropzone", {
            parallelUploads: 20,
            maxFilesize: 5,
            maxFiles: 20,
            acceptedFiles: '.jpg, .jpeg, .png',
            paramName: "file" // The name that will be used to transfer the file
            // addRemoveLinks: true,
        });

        // $('a#ts').on('confirm', function(event) {
        //     alert('closed');
        // });

        // $('a#deleteImg').click(function() {
        //     alert('tesss')
        // });

        function deleteImg(id) {
            swal({
                title: "Anda yakin ingin menghapus?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn btn-danger btn-custom waves-effect waves-light',
                confirmButtonText: "Ya, hapus!",
                cancelButtonText: "Tidak",
                closeOnConfirm: false,
                cancelButtonClass: 'btn',
                buttonsStyling: false,
                reverseButtons: true
            }, function () {
                window.setTimeout(function () {
                    $.ajax({
                        {{--url: '{{ URL::route('steps.destroy') }}/'+stepId,--}}   //contoh kalo pake route
                        url: '../edit/delete/' + id,
                        type: 'DELETE',
                        dataType: 'json',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        success: function (data) {
                            if (data == null) {
                                swal("Deleted!", "Gambar dihapus.", "success");
                                $('a#rp'+id).click();
                                $('.btn-primary').click();
                            }
                            else
                                swal("Not Deleted!", "Gagal.", "error");
                        }, error: function () {
                            swal("Error!", "Gagal menghapus.", "error");
                        }
                    });
                }, 3000)
            });
        }

        // $(window).load(function () {
        //     var $container = $('.portfolioContainer');
        //     $container.isotope();
        //     $('.portfolioFilter a').click(function () {
        //         $('.portfolioFilter .current').removeClass('current');
        //         $(this).addClass('current');
        //         var selector = $(this).attr('data-filter');
        //         $container.isotope({
        //             filter: selector
        //         });
        //         return false;
        //     });
        // });
    </script>
@endpush