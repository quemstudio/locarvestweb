<?php

namespace App\Http\Controllers;

use App\User;
use App\Address;
use App\Customer;
use App\Billing;
use App\Order;
use Session;
use Illuminate\Http\Request;
use Auth;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Crypt;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usersasdf = User::all();
        $response = [
            'users' => $usersasdf
        ];
        return response()->json($response,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("user.add");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return User::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return User::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view("user.edit", compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->update($request->all());

        return $user;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return response()->json(null,204);
    }

    public function editUser()
    {
        $user = User::where('id',Auth::user()->id)->with(['customers'])->get();
        // return $user;
        return view('my-profile',compact('user'));
    }

    public function userProfile()
    {
        
        $user = User::where('id',Auth::user()->id)->with(['customers'])->get();
        // return $user;
        return view('my-profile-form',compact('user'));
    }

    public function updateProfile(Request $request)
    {
        $userUpdate = User::findOrFail(Auth::user()->id);
        $userUpdate->update($request->all());


        $customer = Customer::where('user_id',Auth::user()->id);
//        $supplier->update($request->all());

        $path = '';
        if ($request->image) {
//            $path = Storage::disk('public')->put('supplier', $request->image);

            $file = $request->image;
            $path = $file->hashName('user');
            // supplier/bf5db5c75904dac712aea27d45320403.jpeg

            $image = Image::make($file);

            $image->resize(null, 200, function ($constraint) {
                $constraint->aspectRatio();
            });

//            $path = Storage::put($path, (string) $image->encode());
            Storage::disk('public')->delete($customer->image);
            Storage::disk('public')->put($path, (string) $image->encode());

            $customer->image = $path;
            $customer->save();
        }



        $user = User::where('id',Auth::user()->id)->with(['customers'])->get();
        return view('my-profile-form',compact('user'));
    }

    public function userAddress()
    {
        $users = User::where('id',Auth::user()->id)->with(['addresses'])->get();
        // return $users;
        return view('my-address',compact('users'));
        // return view('my-address');
    }

    public function userHistory()
    {
        $joined = DB::table('customers')
            ->join('orders', 'customers.id', '=', 'orders.customer_id')
            ->join('billings', 'orders.id', '=', 'billings.order_id')
            ->select(DB::raw('orders.status statusOrder, billings.status as statusBilling, orders.*, billings.*, customers.*'))
            ->where('customers.user_id', Auth::user()->id)
            ->where('orders.status','completed')
            ->get();
        return view('my-history',compact('joined'));
        // return view('my-address');
    }

    public function userConfirmPayment()
    {
//        $joined = Customer::where('user_id',Auth::user()->id)->with(['orders'])->get();
//        $test = Customer::where('user_id',Auth::user()->id)
//                ->join('orders', 'customer.id', '=', 'orders.customer_id')
////                ->join('billings', 'billings.order_id', '=', 'orders.id')
//                ->get();
        $joined = DB::table('customers')
            ->join('orders', 'customers.id', '=', 'orders.customer_id')
//            ->join('order_details', 'orders.id', '=', 'order_details.order_id')
            ->join('billings', 'orders.id', '=', 'billings.order_id')
            ->select(DB::raw('orders.status statusOrder, billings.status as statusBilling, orders.*, billings.*, customers.*'))
            ->where('customers.user_id', Auth::user()->id)
            ->whereIn('orders.status', ['new-order', 'confirmed','shipping'])
            ->get();
//        $total_price = null;
//        foreach($joined as $join){
//            $total_price = $total_price + $join->total_price;
//        }
//         return $joined;
        return view('my-confirmPayment',compact('joined'));
        // return view('my-address');
    }

    public function userDetailPayment(Request $request)
    {
        $joined = DB::table('customers')
            ->join('orders', 'customers.id', '=', 'orders.customer_id')
            ->join('order_details', 'orders.id', '=', 'order_details.order_id')
            ->join('products', 'order_details.product_id', '=', 'products.id')
            ->select(DB::raw('orders.total_price orderTotal, order_details.total_price as orderDetailTotal, orders.*, order_details.*, customers.*, products.*'))
//            ->join('billings', 'orders.id', '=', 'billings.order_id')
            ->where('customers.user_id', Auth::user()->id)
            ->where('orders.id',$request->order_id)
            ->get();

        $total_price = null;
        foreach($joined as $join){
            $total_price = $total_price + $join->total_price;
        }

//        return $joined;
        return view('my-confirmPayment-detail',compact('joined','total_price'));
    }
    public function userUploadPaymentPage(Request $request)
    {
        $orderId = $request->order_id;
        $orderNumber = $request->order_number;

//        return $joined;
        return view('my-confirmPayment-upload',compact('orderId','orderNumber'));
    }

    public function uploadPaymentFile(Request $request)
    {
//        $order = Order::findOrFail($request->order_id);
//        $billing = Billing::where('order_id',$request->order_id);

//        Billing::where('order_id',$request->order_id)->update(array('bank' =>  $request->bank, 'account_number' => $request->account_number));

        $billing = Billing::where('order_id',$request->order_id)->get();
        $path = '';
        if ($request->image) {
//            $path = Storage::disk('public')->put('supplier', $request->image);

            $file = $request->image;
            $path = $file->hashName('product');
            // supplier/bf5db5c75904dac712aea27d45320403.jpeg

            $image = Image::make($file);

            $image->resize(null, 800, function ($constraint) {
                $constraint->aspectRatio();
            });

//            $path = Storage::put($path, (string) $image->encode());
            Storage::disk('public')->put($path, (string) $image->encode());
        }

        Billing::where('order_id',$request->order_id)
            ->update(['bank' => $request->bank,
                'account_number' => $request->account_number,
                'confirmation_pic' => $path,
                ]);



//        $billing->bank = $request->bank;
//        $billing->account_number = $request->account_number;
//        $billing->confirmation_pic = 'asdf';
//        $billing->save();

//        return redirect()->back();
//        return $request;
        return redirect()->route('userProfile')->with('success', 'Upload Informasi Transfer Sukses!');;
    }

    public function userWholesale()
    {
        // $classification = DB::table('users')
        // ->join('customers', 'customers.user_id', '=', 'users.id')
        // ->select('classification')
        // ->where('users.id',Auth::user()->id)
        // ->get();
        $classification = Customer::select('classification')->where('user_id',Auth::user()->id)->get();
        // return $users;
        // return $classification;
        return view('wholesale-page',compact('classification'));
        // return view('my-address');
    }

    public function switchwholesale()
    {
        $status = Customer::select('classification')->where('user_id',Auth::user()->id)->get();
        if($status[0]->classification == 'individu'){
            $classificationUpdate = Customer::where('user_id',Auth::user()->id);
            $classificationUpdate->update(array('classification' => 'wholesale'));
            Cart::destroy();
            $statusChanged = Customer::select('classification')->where('user_id',Auth::user()->id)->get();
            Session::put('statusCat', $statusChanged[0]->classification);
        }else{
            $classificationUpdate = Customer::where('user_id',Auth::user()->id);
            $classificationUpdate->update(array('classification' => 'individu'));
            Cart::destroy();
            $statusChanged = Customer::select('classification')->where('user_id',Auth::user()->id)->get();
            Session::put('statusCat', $statusChanged[0]->classification);
        }
        // $classification = Customer::select('classification')->where('user_id',Auth::user()->id)->get();
        
        return redirect('/');
        // return view('wholesale',compact('classification'));

        // return redirect()->route('userProfile');
    }


    public function user_customer() {

        $user = User::with(['customers'])->get();
    }

    public function delAddress(Request $request)
    {
        $address = Address::findOrFail($request->id);
        $address->delete();

        $users = User::where('id',Auth::user()->id)->with(['addresses'])->get();
        // return $users;
        return view('my-address',compact('users'));
        // return view('my-address');
    }

    public function updateAddressSubmit(Request $request)
    {

        $addressUpdate = Address::findOrFail($request->id);
        // $userUpdate->update($request->all());
        if($addressUpdate){
            $addressUpdate->address = $request->address;
            $addressUpdate->kecamatan = $request->kecamatan;
            $addressUpdate->kelurahan = $request->kelurahan;
            $addressUpdate->city = $request->city;
            $addressUpdate->zip_code = $request->zip_code;
            // $addressUpdate->rtrw = 4050;
            $addressUpdate->save();
        }

        // return $request->addressUpdate;
       
        $users = User::where('id',Auth::user()->id)->with(['addresses'])->get();
        // return $users;
        return view('my-address',compact('users'));
        // return view('my-address');
    }

    public function changeStatus($id){
        $idUser = Crypt::decrypt($id);
        $user = User::findOrFail($idUser);
        $user->status = 1;
        $user->save();

        return redirect('/')->with('success', 'Akun Anda Telah Teraktivasi dan Dapat Digunakan Untuk Login');
    }



}
