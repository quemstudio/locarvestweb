<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Order;
use App\OrderDetail;
use App\Product;
use App\Supplier;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //return Admin::all();
        $product = Product::all()->count();
        $petani = Supplier::all()->count();
        $order = Order::all()->count();

        $neworder = Order::where('status','new-order')->count();
        $shipp = Order::where('status','shipping')->count();
        $confirm = Order::where('status','confirmed')->count();
        $comp = Order::where('status','completed')->count();

        $orderMonth = array();
        for($i=1;$i<13;$i++) {

            $data = Order::whereMonth('created_at','=',$i)->whereYear('created_at','=',date('Y'))->count();
            $orderMonth[$i]= $data;
        }

        $orderStat = Order::where('status', 'completed')->with(['orderDetails.products.categories'])->get();
        $new=json_decode($orderStat,true);

        $grafikCat = array();
        for ($i = 1; $i < 10; $i++)
            $grafikCat[$i] = 0;

        foreach ($new as $order) {
           // echo $order['order_details'][0]['id'];
            foreach ( $order['order_details'] as $detail) {
               //karna 1 detail 1 produk jadi
              // echo $detail['products']['categories']['name'];
                if($detail['products']['categories']['name'] == 'Daging')
                    $grafikCat[1]+=1;
                elseif ($detail['products']['categories']['name'] == 'Siap Santap')
                    $grafikCat[2]+=1;
                elseif ($detail['products']['categories']['name'] == 'Ikan')
                    $grafikCat[3]+=1;
                elseif ($detail['products']['categories']['name'] == 'Sayuran')
                    $grafikCat[4]+=1;
                elseif ($detail['products']['categories']['name'] == 'Buah')
                    $grafikCat[5]+=1;
                elseif ($detail['products']['categories']['name'] == 'Bumbu Dapur')
                    $grafikCat[6]+=1;
                elseif ($detail['products']['categories']['name'] == 'Beras')
                    $grafikCat[7]+=1;
                elseif ($detail['products']['categories']['name'] == 'Bunga')
                    $grafikCat[8]+=1;
                else
                    $grafikCat[9]+=1;
            }
        }

        return view('admin.home', compact('product', 'petani', 'order', 'neworder', 'shipp', 'confirm', 'comp', 'orderMonth','grafikCat'));
        //return $orderStat;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $admin = Admin::create([
            'email' => $request->email,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'role' => $request->role,
            'password' => bcrypt($request->password),
            'password_confirmation' => bcrypt($request->password_confirmation)
        ]);

        if ($admin != null)
            return redirect()->route('admin.admin.page')->with('success', 'Insert Data Success!');

        return redirect()->route('admin.admin.page')->with('error', 'Insert Data Failed!');

//        return Admin::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Admin::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin = Admin::findOrFail($id);
//        return $supplier;
        return view('admin.detail-admin', compact('admin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $admin = Admin::findOrFail($id);
        $admin->update($request->all());

        return $admin;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin = Admin::findOrFail($id);
        $admin->delete();

//        return response()->json(['success'=>"Admin Deleted successfully.", 'tr'=>'tr_'.$id]);
        return response()->json(null, 204);
    }

    public function adminPage()
    {

//        $admins = Admin::all();
        $admins = Admin::where('role', '!=' ,'superadmin')->get();
        $no = 1;

        return view('admin.admin', compact('admins', 'no'));
    }

    public function addAdminPage(Request $request)
    {

        return view('admin.add-admin');
    }

    public function configurationAdmin()
    {

        return view('admin.configuration');
    }

    public function updateAdmin(Request $request, $id)
    {
        $admin = Admin::findOrFail($id);
        $admin->update($request->all());

        if ($admin != null)
            return redirect()->route('admin.admin.page')->with('success', 'Update admin sukses!');

        return redirect()->route('admin.admin.page')->with('error', 'Update admin gagal!');
    }
}
