<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 05/01/18
 * Time: 19.57
 */
?>

@extends('layouts.app')
@section('content')
    <div id="main">
        <div class="section section-bg-10 pt-11 pb-17">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="page-title text-center">Profile</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="section border-bottom pt-2 pb-2">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <ul class="breadcrumbs">
                            <li><a href="index.html">Home</a></li>
                            <li>Profile</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="section pt-10 pb-10">
            <div class="container">
                <div class="row">
                    <div class="about-main-img col-lg-6">
                        <img src="./assets/images/about_1.jpg" alt="" />
                    </div>
                    <div class="about-content col-lg-6">
                        <div class="about-content-title">
                            <h4>Title</h4>
                            <div class="about-content-title-line"></div>
                        </div>
                        <div class="about-content-text">
                            <p>Organic store opened its doors in 1990, it was Renée Elliott’s dream to offer the best and widest range of organic foods available, and her mission to promote health in the community and to bring a sense of discovery and adventure into food shopping.</p>
                            <p>Visit our site for a complete list of exclusive and premium food brands we are stocking.<br></p>
                        </div>
                        <div class="about-carousel" data-auto-play="true" data-desktop="4" data-laptop="4" data-tablet="4" data-mobile="2">
                            <a href="./assets/images/carousel/img_large_1.jpg" data-rel="prettyPhoto[gallery]">
                                <img src="./assets/images/carousel/img_1.jpg" alt="" />
                                <span class="ion-plus-round"></span>
                            </a>
                            <a href="./assets/images/carousel/img_large_2.jpg" data-rel="prettyPhoto[gallery]">
                                <img src="./assets/images/carousel/img_2.jpg" alt="" />
                                <span class="ion-plus-round"></span>
                            </a>

                            <a href="./assets/images/carousel/img_large_4.jpg" data-rel="prettyPhoto[gallery]">
                                <img src="./assets/images/carousel/img_4.jpg" alt="" />
                                <span class="ion-plus-round"></span>
                            </a>
                            <a href="./assets/images/carousel/img_large_5.jpg" data-rel="prettyPhoto[gallery]">
                                <img src="./assets/images/carousel/img_5.jpg" alt="" />
                                <span class="ion-plus-round"></span>
                            </a>
                            <a href="./assets/images/carousel/img_large_6.jpg" data-rel="prettyPhoto[gallery]">
                                <img src="./assets/images/carousel/img_6.jpg" alt="" />
                                <span class="ion-plus-round"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>



@endsection
