<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 16/01/18
 * Time: 19.26
 */
?>
@extends('layouts.appadmin')
@section('content')
    <div class="header">
        <h2>Data <strong>Produk</strong></h2>
    </div>
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif(session('error'))
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    @endif
    <div class="row">
        <div class="col-lg-12 portlets">
            <div class="panel">
                <div class="panel-header ">
                    <h3><a href="{{route('product.add.show')}}">
                            <button type="button" class="btn btn-primary">+ Tambah Produk</button>
                        </a></h3>
                </div>
                <div class="panel-content">
                    <div class="filter-left panel-content pagination2 table-responsive">
                        <table class="table table-hover table-dynamic table-tools" data-table-name="Data Products" data-table-orientation="landscape">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th class='hidden-1024' style="display:none">Deskripsi</th>
                                <th class='hidden-1024' style="display:none">Luas Lahan</th>
                                <th class='hidden-350' style="display:none">Jadwal Tanam</th>
                                <th class='hidden-480' style="display:none">Kuantitas Benih</th>
                                <th class='hidden-480' style="display:none">Jadwal Panen</th>
                                <th class='hidden-480' style="display:none">Kuantitas Panen</th>
                                <th class='hidden-480'>Harga</th>
                                <th class='hidden-480'>Kategori Harga</th>
                                <th class='hidden-480'>Kategori Produk</th>
                                <th class='hidden-480'>Petani</th>
                                <th class='hidden-480' style="display:none">Penawaran Terbaik</th>
                                <th class='hidden-480'>Status</th>
                                <th class='hidden-480'></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($product as $data)
                                @php
                                    $cat = \App\Category::find($data->category_id);
                                    $supplier = \App\Supplier::find($data->supplier_id);
                                @endphp
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td onclick="detailProduct('{{$data->id}}', '{{$cat->name}}', '{{$supplier->name}}')" style="cursor: pointer;">
                                        {{$data->name}}
                                    </td>
                                    <td class='hidden-350' style="display:none">
                                        {{$data->description}}
                                    </td>
                                    <td class='hidden-350' style="display:none">{{$data->land_area}}</td>
                                    <td class='hidden-350' style="display:none">{{$data->planting_schedule}}</td>
                                    <td class='hidden-350' style="display:none">{{$data->seed_quantity}}</td>
                                    <td class='hidden-350' style="display:none">{{$data->harvest_time}}</td>
                                    <td class='hidden-350' style="display:none">{{$data->harvest_quantity}}</td>
                                    <td class='hidden-350' onclick="detailProduct('{{$data->id}}', '{{$cat->name}}', '{{$supplier->name}}')" style="cursor: pointer;">
                                        {{$data->price}}
                                    </td>
                                    <td class='hidden-350' onclick="detailProduct('{{$data->id}}', '{{$cat->name}}', '{{$supplier->name}}')" style="cursor: pointer;">
                                        {{$data->category}}
                                    </td>
                                    <td class='hidden-350' onclick="detailProduct('{{$data->id}}', '{{$cat->name}}', '{{$supplier->name}}')" style="cursor: pointer;">
                                        {{$cat->name}}
                                    </td>
                                    <td class='hidden-1024' onclick="detailProduct('{{$data->id}}', '{{$cat->name}}', '{{$supplier->name}}')" style="cursor: pointer;">
                                        {{$supplier->name}}
                                    </td>
                                    @if($data->best_offer == 'on')
                                        <td class='hidden-350' style="display:none">Yes</td>
                                    @else
                                        <td class='hidden-350' style="display:none">No</td>
                                    @endif
                                    @if($data->status == 'on')
                                        <td class='hidden-350' onclick="detailProduct('{{$data->id}}', '{{$cat->name}}', '{{$supplier->name}}')" style="cursor: pointer;">
                                            Ready
                                        </td>
                                    @else
                                        <td class='hidden-350' onclick="detailProduct('{{$data->id}}', '{{$cat->name}}', '{{$supplier->name}}')" style="cursor: pointer;">
                                            Not Ready
                                        </td>
                                    @endif
                                    <td class='hidden-480' style="min-width:150px">
                                        <div class="btn-group">
                                            <a class="btn btn-success" href="{{route('product.edit.show' ,$data['id'])}}">
                                                <i style="padding:0px; margin:0px" class="fa fa-edit"></i>
                                            </a>
                                            <button class="btn btn-danger" type="button"
                                                    id="danger-alert"
                                                    data-tr="tr_{{$data->id}}"
                                                    onclick="deleteProduct('{{$data->id}}', '{{$data->name}}')">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="edit-info" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content p-0 b-0">
                <div class="panel panel-color panel-primary">
                    <div class="panel-heading">
                        <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 class="panel-title">Product Detail</h3>
                    </div>
                    <form method="post">
                        {{ csrf_field() }}
                        <div class="panel-body">
                            <span id="id"></span>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">ID</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="id-detail" name="id-detail" placeholder="" readonly>--}}
                                        <label class="form-control"
                                               placeholder="Announcement" id="id-detail"
                                               name="id-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Name</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control"
                                               placeholder="Announcement" id="name-detail"
                                               name="name-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Planting Schedule</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control" id="ps-detail"
                                               name="ps-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Land Area (m<sup>2</sup>)</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control" id="la-detail"
                                               name="la-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Seed Quantity (kg)</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control" id="sq-detail"
                                               name="sq-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Harvest Time</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control" id="ht-detail"
                                               name="ht-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Harvest Quantity (kg)</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control" id="hq-detail"
                                               name="hq-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Price (IDR)</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control" id="price-detail"
                                               name="price-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Price Category</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control" id="pc-detail"
                                               name="pc-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Description</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control" id="description-detail"
                                               name="description-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Amount Left</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control" id="al-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Best Offer</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control" id="bo-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Preorder Date</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control" id="pd-detail"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Image</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control" id="image-detail"></label>
                                    </div>
                                </div>
                            </div>

                            {{--//Product Category--}}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Product Category</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control" id="category-detail"></label>
                                    </div>
                                </div>
                            </div>

                            {{--//Supplier--}}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Supplier</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control" id="supplier-detail"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Status</label>
                                    <div class="col-sm-9">
                                        {{--<input class="form-control" type="text" id="name-detail" name="name-detail" placeholder="" readonly>--}}
                                        <label class="form-control" id="status-detail"
                                               name="status-detail"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12" style="margin-top: 20px">
                                    {{--<a href="{{route('product.edit.show' , $data->id)}}">--}}
                                    {{--<button type="button" class="btn btn-primary"--}}
                                    {{--style="float:right">Edit</button>--}}
                                    {{--</a>--}}
                                    <a onclick="$('#edit-info').modal('hide');">
                                        <button type="button" class="btn btn-primary" style="float:right">Close</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection

@push('script')
    <script>
        function deleteProduct(id, name) {
            swal({
                title: "Anda yakin ingin menghapus produk " + name + " ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn btn-danger btn-custom waves-effect waves-light',
                confirmButtonText: "Ya, hapus!",
                cancelButtonText: "Tidak",
                closeOnConfirm: false,
                cancelButtonClass: 'btn',
                buttonsStyling: false,
                reverseButtons: true
            }, function () {
                setTimeout(function () {
                    $.ajax({
                        url: 'product/delete/' + id,
                        type: 'DELETE',
                        dataType: 'json',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        success: function (data) {
                            if (data == null) {
                                swal("Deleted!", "Product has been deleted.", "success");
                                location.reload();
                            }
                            else
                                swal("Not Deleted!", "Failed to delete.", "error");
                        }, error: function () {
                            swal("Error!", "Failed to delete.", "error");
                        }
                    });
                }, 2000)
            });
        }

        function detailProduct(id, catName, supName) {

            $.ajax({
                url: 'product/show_detail/' + id,
                error: function () {
                    $('#nameApp').html('<p>An error has occurred</p>');
                },
                dataType: 'json',
                success: function (data) {
                    $('#id').html('<input type="hidden" value="' + data.id + '" name="id" id="id">');
                    $('#id-detail').text(data.id);
                    $('#name-detail').text(data.name);
                    $('#ps-detail').text(data.planting_schedule);
                    $('#la-detail').text(data.land_area);
                    $('#sq-detail').text(data.seed_quantity);
                    $('#ht-detail').text(data.harvest_time);
                    $('#hq-detail').text(data.harvest_quantity);
                    $('#price-detail').text(data.price);
                    $('#pc-detail').text(data.category);
                    // $('#status-detail').text(data.status);
                    if (data.status === 'on')
                        $('#status-detail').text('Ready');
                    else
                        $('#status-detail').text('Not Ready');
                    $('#description-detail').text(data.description);
                    $('#al-detail').text(data.amount_left);
                    // $('#bo-detail').text(data.best_offer);
                    if (data.best_offer === 'on')
                        $('#bo-detail').text('Yes');
                    else
                        $('#bo-detail').text('No');
                    $('#pd-detail').text(data.preorder_date);
                    $('#image-detail').text(data.image);
                    $('#category-detail').text(catName);
                    $('#supplier-detail').text(supName);

                    $('#edit-info').modal('show');
                },
                type: 'GET'
            });

        }
    </script>
@endpush