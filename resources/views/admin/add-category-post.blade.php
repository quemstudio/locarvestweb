<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 16/01/18
 * Time: 19.32
 */
?>
@extends('layouts.appadmin')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-header">
                    <h3><i class="icon-target"></i> <strong>Tambah</strong> Kategori Post</h3>
                </div>
                <div class="panel-content">
                    <div class="row">
                        <form class=" form-horizontal" action="{{route('category_post.add.submit')}}" method="post"
                              enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Nama</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="text" placeholder="" name="name" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Deskripsi</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" placeholder="" name="description" required></textarea>
                                    </div>
                                </div>
                                {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>--}}
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Gambar</label>
                                    <div class="col-sm-9">
                                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                            {{--<div class="form-control" data-trigger="fileinput">--}}
                                                {{--<i class="glyphicon glyphicon-file fileinput-exists"></i>--}}
                                                {{--<span class="fileinput-filename"></span>--}}
                                            {{--</div>--}}
                                            <input type="text" class="form-control" id="upload-file-info" readonly>
                                            <span class="input-group-addon btn btn-default btn-file">
                                                <span class="fileinput-new">Pilih...</span>
                                                <span class="fileinput-exists">Ganti</span>
                                                <input type="file" name="picture" onchange="$('#upload-file-info').val($(this).val().split('\\').pop());">
                                            </span>
                                            <a href="#" class="input-group-addon btn btn-default fileinput-exists"
                                               data-dismiss="fileinput">Hapus</a>
                                        </div>
                                    </div>
                                </div>
                                {{--<div class="input-group">--}}
                                    {{--<label class="input-group-btn">--}}
                                        {{--<span class="btn btn-primary">--}}
                                            {{--Browse&hellip; <input id="my-file-selector" type="file" style="display:none;" onchange="$('#upload-file-info').val($(this).val());">--}}
                                        {{--</span>--}}
                                    {{--</label>--}}
                                    {{--<input type="text" class="form-control" id="upload-file-info" readonly>--}}
                                {{--</div>--}}
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary" style="float:right">
                                            Submit
                                        </button>
                                        <a href="{{route('admin.category.post')}}"><button type="button" class="btn btn-danger" style="float:right">Batal</button></a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection