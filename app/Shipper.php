<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipper extends Model
{
    protected $primaryKey="id";
    protected $guarded = [];

    public function orders()
    {
        return $this->hasMany('App\Order','id');
    }
}
