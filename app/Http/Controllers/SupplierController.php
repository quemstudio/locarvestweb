<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suppliers = DB::table('suppliers')
            ->join('users', 'users.id', '=', 'suppliers.user_id')
            ->select(DB::raw('users.id as userId, suppliers.id as supplierId, users.*, suppliers.*'))
            ->get();
//        $response = [
//            'supplier' => $supplier
//        ];
//        return $suppliers;
        return view('partner-petani', compact('suppliers'));
//        return response()->json($response,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("supplier.add");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Supplier::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $supplier = Supplier::findOrFail($id);

//        return $supplier;
        return view('partner-petaniDetail',compact('supplier'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $supplier = Supplier::findOrFail($id);
        $user = User::findOrFail($supplier->user_id);
//        return $supplier;
        return view('admin.detail-supplier', compact('user', 'supplier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $supplier = Supplier::findOrFail($id);
        $supplier->update($request->all());

        return $supplier;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $supplier = Supplier::findOrFail($id);
        $supplier->delete();

        return response()->json(null, 204);
    }

    public function supplierAdmin()
    {

        $suppliers = Supplier::all();
        $no = 1;

        return view('admin.supplier', compact('suppliers', 'no'));
    }

    public function showAddSupplierAdmin()
    {

        return view('admin.add-supplier');
    }

    public function bestSupplierAdmin()
    {

        $best = DB::table('order_details')
            ->join('products', 'products.id', '=', 'order_details.product_id')
            ->join('categories', 'categories.id', '=', 'products.category_id')
            ->join('suppliers', 'suppliers.id', '=', 'products.supplier_id')
            ->join('users', 'users.id', '=', 'suppliers.user_id')
            ->selectRaw('suppliers.id, suppliers.user_id, suppliers.name, SUM(order_details.total_price) as total_dapat, SUM(order_details.qty) as total_jual')
            ->groupBy('suppliers.id')
            ->orderBy('total_jual', 'DESC')->get();

        $no = 1;

        return view('admin.best-supplier', compact('best', 'no'));
    }

    public function addSupplierAdmin(Request $request)
    {

        $path = '';
        if ($request->image) {
//            $path = Storage::disk('public')->put('supplier', $request->image);

            $file = $request->image;
            $path = $file->hashName('supplier');
            // supplier/bf5db5c75904dac712aea27d45320403.jpeg

            $image = Image::make($file);

            $image->resize(null, 350, function ($constraint) {
                $constraint->aspectRatio();
            });

//            $path = Storage::put($path, (string) $image->encode());
            Storage::disk('public')->put($path, (string) $image->encode());
        }

        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
//            'username' => $request->email,
//            'email' => $request->email,
            'username' => 'petani@locarvest.com',
            'email' => 'petani@locarvest.com',
//            'phone_number' => $request->phone_number,
            'password' => bcrypt('ukm12345*'),
            'password_confirmation' => bcrypt('ukm12345*'),
            'role' => 'supplier',
//            'gender' => $request->gender,
            'gender' => 'M',
        ]);

        $supplier = Supplier::create([
            'address' => $request->address,
            'image' => $path,
            'name' => $request->first_name . ' ' . $request->last_name,
            'land_area' => $request->land_area,
            'user_id' => $user->id,
            'name_plant' => $request->name_plant,
            'planting_schedule' => date('Y-m-d',strtotime($request->planting_schedule)),
            'harvest_schedule' => date('Y-m-d',strtotime($request->harvest_schedule))   ,
            'quantity_plant' => $request->quantity_plant,
            'long' => $request->long,
            'lat' => $request->lat,
            'description' => $request->description,
            'certification' => $request->certification
        ]);

        if ($user != null && $supplier != null)
            return redirect()->route('admin.supplier')->with('success', 'Tambah petani sukses!');

        return redirect()->route('admin.supplier')->with('error', 'Tambah petani gagal!');
    }

    public function deleteSupplier($id, $user_id)
    {
        $supplier = Supplier::findOrFail($id);
        $supplier->delete();

        $user = User::findOrFail($user_id);
        $user->delete();

        return response()->json(null, 204);
    }

    public function updateSupplier(Request $request, $id)
    {
        $supplier = Supplier::findOrFail($id);
//        $supplier->update($request->all());

        $path = '';
        if ($request->image) {
//            $path = Storage::disk('public')->put('supplier', $request->image);

            $file = $request->image;
            $path = $file->hashName('supplier');
            // supplier/bf5db5c75904dac712aea27d45320403.jpeg

            $image = Image::make($file);

            $image->resize(null, 350, function ($constraint) {
                $constraint->aspectRatio();
            });

//            $path = Storage::put($path, (string) $image->encode());
            Storage::disk('public')->delete($supplier->image);
            Storage::disk('public')->put($path, (string) $image->encode());

            $supplier->image = $path;
        }

        $supplier->name = $request->first_name . ' ' . $request->last_name;
        $supplier->land_area = $request->land_area;
        $supplier->address = $request->address;
        $supplier->planting_schedule = date('Y-m-d',strtotime($request->planting_schedule));
        $supplier->harvest_schedule = date('Y-m-d',strtotime($request->harvest_schedule));
        $supplier->quantity_plant = $request->quantity_plant;
        $supplier->name_plant = $request->name_plant;
        $supplier->long = $request->long;
        $supplier->lat = $request->lat;
        $supplier->description = $request->description;
        $supplier->certification = $request->certification;
        $supplier->save();

        $user = User::findOrFail($supplier->user_id);
//        $user->update($request->all());
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
//        $user->email = $request->email;
//        $user->phone_number = $request->phone_number;
//        $user->gender = $request->gender;
        $user->save();

        if ($user != null && $supplier != null)
            return redirect()->route('admin.supplier')->with('success', 'Update petani sukses!');

        return redirect()->route('admin.supplier')->with('error', 'Update petani gagal!');
    }

    public function showDetail($id) {

        return Supplier::find($id);
    }

}
