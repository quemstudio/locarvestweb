<?php

namespace App\Http\Controllers;

use App\ProductImg;
use App\Supplier;
use App\Customer;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Product;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
//use Auth;
use Maatwebsite\Excel\Facades\Excel;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Auth::check()) {
            $status = Customer::select('classification')->where('user_id', Auth::user()->id)->get();
            if ($status[0]->classification == 'individu') {
                $products = Product::where('category', 'individu')
                    ->where('status', 'on')
                    ->paginate(15);
            } else {
                $products = Product::where('category', 'wholesale')
                    ->where('status', 'on')
                    ->paginate(15);
            }
        } else {
            $products = Product::where('category', 'individu')
                ->where('status', 'on')
                ->paginate(15);
        }

        $countCat = Category::all();
//        $countCat = DB::table('products')
//            ->join('categories', 'categories.id', '=', 'products.category_id')
//            ->select(DB::raw('count(products.name) as count, categories.name, categories.id'))
//            ->groupBy('products.category_id')
//            ->get();
//      $books = Book::all();
        $response = [
            'products' => $products
        ];

        if ($request->ajax()) {
            return view('shopFiltered', compact('products'));
        }

        return view('shop', compact('products', 'countCat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("product.add");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Product::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
//        $product = Product::find($id);
        $product = DB::table('products')
            ->join('categories', 'categories.id', '=', 'products.category_id')
            ->join('suppliers', 'products.supplier_id', '=', 'suppliers.id')
            ->select(DB::raw('products.name as productName, suppliers.name as supplierName, categories.name as categoryName, categories.*, products.*'))
            ->where('products.id', $id)
            ->get();
        $images = DB::table('product_imgs')
            ->where('product_id', $id)
            ->get();
//        ProductImg::findOrFail($id);
        $images2 = ProductImg::where('product_id', $id)->get();
        $countCat = Category::all();
        // $countCat = DB::table('products')
        //     ->join('categories', 'categories.id', '=', 'products.category_id')
        //     ->select(DB::raw('count(products.name) as count, categories.name, categories.id'))
        //     ->groupBy('products.category_id')
        //     ->get();
        return view('shop_detail', compact('product', 'countCat', 'images', 'images2'));
//        return $images;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'admin') {
            $product = Product::findOrFail($id);
            $categories = Category::all();
            $suppliers = Supplier::all();
            return view("admin.detail-product", compact('product', 'categories', 'suppliers'));
        } else {
            echo 'nice try script kiddies';
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $product->update($request->all());

        return $product;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();

        return response()->json(null, 204);
    }

    public function productAdmin()
    {

        $product = Product::all();
        $no = 1;

        return view('admin.product', compact('product', 'no'));
    }

    public function importProductAdmin()
    {

        return view('admin.import-product');
    }

    public function bestProductAdmin()
    {
//        $product = Product::all();

        $best = DB::table('order_details')->selectRaw('product_id, SUM(qty) as total, SUM(total_price) as total_dapat')->groupBy('product_id')->orderBy('total', 'DESC')->get();

//        foreach ($best as $data) {
//            $product = Product::where('id', $data->product_id)->get();
//        }
        $no = 1;
        return view('admin.best-product', compact('best', 'no'));
    }

    public function addProductAdmin(Request $request)
    {

        $path = '';
        if ($request->image) {
//            $path = Storage::disk('public')->put('supplier', $request->image);

            $file = $request->image;
            $path = $file->hashName('product');
            // supplier/bf5db5c75904dac712aea27d45320403.jpeg

            $image = Image::make($file);

            $image->resize(null, 350, function ($constraint) {
                $constraint->aspectRatio();
            });

//            $path = Storage::put($path, (string) $image->encode());
            Storage::disk('public')->put($path, (string)$image->encode());
        }

        if ($request->name) {

            if ($request->status)
                $status = 'on';
            else
                $status = 'off';

            if ($request->best_offer)
                $best_offer = 'on';
            else
                $best_offer = 'off';

            $product = Product::create([
                'name' => $request->name,
                'planting_schedule' => date('Y-m-d', strtotime($request->planting_schedule)),
                'land_area' => $request->land_area,
                'seed_quantity' => $request->seed_quantity,
                'harvest_time' => date('Y-m-d', strtotime($request->harvest_time)),
                'harvest_quantity' => $request->harvest_quantity,
                'price' => $request->price,
                'category' => $request->category,
                'status' => $status,
                'description' => $request->description,
//                'amount_left' => $request->amount_left,
                'best_offer' => $best_offer,
//                'preorder_date' => date('Y-m-d', strtotime($request->preorder_date)),
                'category_id' => $request->category_id,
                'supplier_id' => $request->supplier_id,
                'image' => $path,
            ]);
            if ($product != null)
                return redirect()->route('admin.detail-product', $product->id)->with('success', 'Insert Data Success!');
            else
                return redirect()->route('admin.product')->with('error', 'Insert Data Failed!');
        }
        $cat = Category::all();
        $supplier = Supplier::all();
        return view('admin.add-product', compact('cat', 'supplier'));
    }

    public function productDetailAdmin(Request $request, $id)
    {

        $product = Product::find($id);
        $categories = Category::all();
        $suppliers = Supplier::all();

        return view('admin.detail-product', compact('product', 'categories', 'suppliers'));
    }

    public function filterPriceAllProduct(Request $request)
    {
        if (Auth::check()) {
            $status = Customer::select('classification')->where('user_id', Auth::user()->id)->get();
            if ($status[0]->classification == 'individu') {
                $products = DB::table('products')
                    ->where('category', 'individu')
                    ->where('status', 'on')
                    ->whereBetween('price', [$request->minPrice, $request->maxPrice])
                    ->paginate(15);
            } else {
                $products = DB::table('products')
                    ->where('category', 'wholesale')
                    ->where('status', 'on')
                    ->whereBetween('price', [$request->minPrice, $request->maxPrice])
                    ->paginate(15);
            }
        } else {
            $products = DB::table('products')
                ->where('category', 'individu')
                ->where('status', 'on')
                ->whereBetween('price', [$request->minPrice, $request->maxPrice])
                ->paginate(15);
        }

        //    return $status[0]->classification;
        return view('shopFiltered', compact('products'));

    }

    public function filterBestOffer()
    {
        if (Auth::check()) {
            $status = Customer::select('classification')->where('user_id', Auth::user()->id)->get();
            if ($status[0]->classification == 'individu') {
                $products = DB::table('products')
                    ->where('category', 'individu')
                    ->where('status', 'on')
                    ->where('best_offer', 'on')
                    ->paginate(15);
            } else {
                $products = DB::table('products')
                    ->where('category', 'wholesale')
                    ->where('status', 'on')
                    ->where('best_offer', 'on')
                    ->paginate(15);
            }
        } else {
            $products = DB::table('products')
                ->where('category', 'individu')
                ->where('status', 'on')
                ->where('best_offer', 'on')
                ->paginate(15);
        }

        //    return $status[0]->classification;
        return view('shopFiltered', compact('products'));

    }

    public function updateProduct(Request $request, $id)
    {
        $product = Product::findOrFail($id);

        $path = '';
        if ($request->image) {
//            $path = Storage::disk('public')->put('supplier', $request->image);

            $file = $request->image;
            $path = $file->hashName('product');
            // supplier/bf5db5c75904dac712aea27d45320403.jpeg

            $image = Image::make($file);

            $image->resize(null, 350, function ($constraint) {
                $constraint->aspectRatio();
            });

//            $path = Storage::put($path, (string) $image->encode());
            Storage::disk('public')->delete($product->image);
            Storage::disk('public')->put($path, (string)$image->encode());

            $product->image = $path;
        }

        if ($request->status)
            $status = 'on';
        else
            $status = 'off';

        if ($request->best_offer)
            $best_offer = 'on';
        else
            $best_offer = 'off';

        $product->name = $request->name;
        $product->planting_schedule = date('Y-m-d', strtotime($request->planting_schedule));
//            $product->planting_schedule = $request->planting_schedule;
        $product->land_area = $request->land_area;
        $product->seed_quantity = $request->seed_quantity;
        $product->harvest_time = date('Y-m-d', strtotime($request->harvest_time));
//            $product->harvest_time = $request->harvest_time;
        $product->harvest_quantity = $request->harvest_quantity;
        $product->price = $request->price;
        $product->category = $request->category;
        $product->status = $status;
        $product->description = $request->description;
        $product->amount_left = $request->amount_left;
        $product->best_offer = $best_offer;
        $product->preorder_date = date('Y-m-d', strtotime($request->preorder_date));
//            $product->preorder_date = $request->preorder_date;
        $product->category_id = $request->category_id;
        $product->supplier_id = $request->supplier_id;
        $product->save();


        if ($product != null)
            return redirect()->route('admin.product')->with('success', 'Update Data Success!');

        return redirect()->route('admin.product')->with('error', 'Update Data Failed!');
    }

    public function sortBy(Request $request)
    {

        if (Auth::check()) {
            $status = Customer::select('classification')->where('user_id', Auth::user()->id)->get();
            if ($status[0]->classification == 'individu') {
                if ($request->order == 'lh') {
                    $products = Product::where('category', 'individu')
                        ->where('status', 'on')
                        ->orderBy('price', 'asc')->paginate(15);
                } elseif ($request->order == 'hl') {
                    $products = Product::where('category', 'individu')
                        ->where('status', 'on')
                        ->orderBy('price', 'desc')->paginate(15);
                } else {
                    $products = Product::where('category', 'individu')
                        ->where('status', 'on')
                        ->orderBy('created_at', 'desc')->paginate(15);
                }
            } else {
                if ($request->order == 'lh') {
                    $products = Product::where('category', 'wholesale')
                        ->where('status', 'on')
                        ->orderBy('price', 'asc')->paginate(15);
                } elseif ($request->order == 'hl') {
                    $products = Product::where('category', 'wholesale')
                        ->where('status', 'on')
                        ->orderBy('price', 'desc')->paginate(15);
                } else {
                    $products = Product::where('category', 'wholesale')
                        ->where('status', 'on')
                        ->orderBy('created_at', 'desc')->paginate(15);
                }
            }
        } else {
            if ($request->order == 'lh') {
                $products = Product::where('category', 'individu')
                    ->where('status', 'on')
                    ->orderBy('price', 'asc')->paginate(15);
            } elseif ($request->order == 'hl') {
                $products = Product::where('category', 'individu')
                    ->where('status', 'on')
                    ->orderBy('price', 'desc')->paginate(15);
            } else {
                $products = Product::where('category', 'individu')
                    ->where('status', 'on')
                    ->orderBy('created_at', 'desc')->paginate(15);
            }
        }


        // return $products;
        return view('shopFiltered', compact('products'));
    }

    public function wholesaleShop()
    {
        $products = Product::where('category', 'wholesale')->get();
        $countCat = Category::all();
//        $countCat = DB::table('products')
//            ->join('categories', 'categories.id', '=', 'products.category_id')
//            ->select(DB::raw('count(products.name) as count, categories.name, categories.id'))
//            ->groupBy('products.category_id')
//            ->get();
//      $books = Book::all();
        $response = [
            'products' => $products
        ];
        return view('shop', compact('products', 'countCat'));
    }

    public function searchProduct(Request $request)
    {

        if (Auth::check()) {
            $status = Customer::select('classification')->where('user_id', Auth::user()->id)->get();
            if ($status[0]->classification == 'individu') {
                $products = Product::where('name', 'like', '%' . $request->search . '%')
                    ->where('category', 'individu')
                    ->where('status', 'on')
                    ->paginate(15);
            } else {
                $products = Product::where('name', 'like', '%' . $request->search . '%')
                    ->where('category', 'wholesale')
                    ->where('status', 'on')
                    ->paginate(15);
            }
        } else {
            $products = Product::where('name', 'like', '%' . $request->search . '%')
                ->where('category', 'individu')
                ->where('status', 'on')
                ->paginate(15);
        }

        return view('shopFiltered', compact('products'));
    }

    public function importProduct(Request $request)
    {

        if (Input::hasFile('import_file')) {
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function ($reader) {
            })->get();
            if (!empty($data) && $data->count()) {
                foreach ($data as $value) {
                    $insert[] = [
                        'name' => $value->name,
                        'planting_schedule' => $value->planting_schedule,
                        'land_area' => $value->land_area,
                        'seed_quantity' => $value->seed_quantity,
                        'harvest_time' => $value->harvest_time,
                        'harvest_quantity' => $value->harvest_quantity,
                        'price' => $value->price,
                        'price_category' => $value->price_category,
                        'status' => $value->status,
                        'description' => $value->description,
                        'best_offer' => $value->best_offer,
                        'category_id' => $value->id_product_category,
                        'supplier_id' => $value->id_petani
                    ];
                }
                if (!empty($insert)) {

                    foreach ($insert as $dataImport) {
                        $product = Product::create([

                            'name' => $dataImport['name'],
                            'planting_schedule' => Carbon::parse($dataImport['planting_schedule'])->format('Y/m/d'),
                            'land_area' => $dataImport['land_area'],
                            'seed_quantity' => $dataImport['seed_quantity'],
                            'harvest_time' => Carbon::parse($dataImport['harvest_time'])->format('Y/m/d'),
                            'harvest_quantity' => $dataImport['harvest_quantity'],
                            'price' => $dataImport['price'],
                            'category' => $dataImport['price_category'],
                            'status' => $dataImport['status'],
                            'description' => $dataImport['description'],
                            'best_offer' => $dataImport['best_offer'],
                            'category_id' => $dataImport['category_id'],
                            'supplier_id' => $dataImport['supplier_id']
                        ]);
                    }
                }
            }
        }

        if ($product)
            return redirect()->route('admin.import.product')->with('success', 'Import Data Success!');

        return redirect()->route('admin.import.product')->with('error', 'Import Data Failed!');
    }

    public function showDetail($id)
    {

        return Product::find($id);
    }

}
