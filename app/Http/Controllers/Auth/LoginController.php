<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Session;
use App\Customer;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm() {
        return view("auth/login-user");
  }

  protected function authenticated()
  {
    $status = Customer::select('classification')->where('user_id',Auth::user()->id)->get();
    Session::put('statusCat', $status[0]->classification);
  }

//    protected function credentials(Request $request)
//    {
////        return $request->only($this->username(), 'password');
//        return ['email' => $request->{$this->username()}, 'password' => $request->password, 'status' => 1];
//    }


}
