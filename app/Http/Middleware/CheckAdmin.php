<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CheckAdmin
{
    public function handle($request, Closure $next)
    {
            if (!empty(auth()->guard('admin')->id())) {
                $data = DB::table('admins')
                    ->select('admins.email', 'admins.id')
                    ->where('admins.id', auth()->guard('admin')->id())
                    ->get();

                if (!$data[0]->id) {
                    return redirect()->intended(route('admin.login'))->with('error', 'You do not have access to admin side');
                }
                return $next($request);
            } else {
                return redirect()->intended(route('admin.login'))->with('error', 'Please Login to access admin area');
            }
    }
}
