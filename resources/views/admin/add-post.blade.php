<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 16/01/18
 * Time: 19.25
 */
?>
@extends('layouts.appadmin')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-header ">
                    <h3><i class="icon-target"></i> <strong>Tambah</strong> Post</h3>
                </div>
                <div class="panel-content">
                    <div class="row">
                        <form class=" form-horizontal" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Judul</label>
                                    <div class="col-sm-9">
                                        <input class="form-control input-lg" type="text" placeholder="" name="title"
                                               required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Kategori </label>
                                    <div class="col-sm-9">
                                        <select class="form-control" style="width:100%;" name="categoryPost_id">
                                            @foreach($cat_posts as $data)
                                                <option value="{{$data->id}}">{{$data->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Gambar</label>
                                    <div class="col-sm-9">
                                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                            {{--<div class="form-control" data-trigger="fileinput">--}}
                                                {{--<i class="glyphicon glyphicon-file fileinput-exists"></i><span--}}
                                                        {{--class="fileinput-filename"></span>--}}
                                            {{--</div>--}}
                                            <input type="text" class="form-control" id="upload-file-info" readonly>
                                            <span class="input-group-addon btn btn-default btn-file"><span
                                                        class="fileinput-new">Pilih...</span><span
                                                        class="fileinput-exists">Ganti</span>
                                                <input type="file" name="cover" onchange="$('#upload-file-info').val($(this).val().split('\\').pop());">
                                            </span>
                                            <a href="#" class="input-group-addon btn btn-default fileinput-exists"
                                               data-dismiss="fileinput">Hapus</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Slider Landing Page</label>
                                    <div class="col-sm-9">
                                        <div class="onoffswitch2">
                                            <input name="status" class="onoffswitch-checkbox" id="myonoffswitch3" type="checkbox"
                                                   value="on">
                                            <label class="onoffswitch-label" for="myonoffswitch3">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <h3>Konten <strong>Editor</strong></h3>
                                        <textarea class="summernote bg-white" name="body"></textarea>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary" style="float:right">Submit
                                        </button>
                                        <a href="{{route('admin.post')}}">
                                            <button type="button" class="btn btn-danger" style="float:right">Batal
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection