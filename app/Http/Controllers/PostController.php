<?php

namespace App\Http\Controllers;

use App\CategoryPost;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::where('categoryPost_id', '!=' , 21)
            ->where('categoryPost_id', '!=' , 22)
            ->where('slide', '!=' , 'on')
            ->get();
        $countCat = DB::table('posts')
            ->join('category_posts','category_posts.id','=','posts.categoryPost_id')
            ->select(DB::raw('count(posts.id) as count, category_posts.name'))
//            ->where('posts.slide', '!=' , 'on')
            ->groupBy('posts.categoryPost_id')
            ->get();
//        $response = [
//            'posts' => $posts
//        ];
//        return $posts;
        return view('blog' , compact('posts','countCat'));
//        return response()->json($response,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("post.add");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Post::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = DB::table('posts')
            ->join('category_posts','category_posts.id','=','posts.categoryPost_id')
            ->where('posts.id',$id)
            ->get();
        $countCat = DB::table('posts')
            ->join('category_posts','category_posts.id','=','posts.categoryPost_id')
            ->select(DB::raw('count(posts.id) as count, category_posts.name'))
            ->groupBy('posts.categoryPost_id')
            ->get();
//        return $post;
        return view('blog-detail',compact('post','countCat'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        $cat_posts = CategoryPost::where('id', '!=' , 21)->where('id', '!=' , 22)->get();
        return view("admin.detail-post", compact('post', 'cat_posts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::findOrFail($id);
        $post->update($request->all());

        return $post;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        $post->delete();

        return response()->json(null,204);
    }

    public function postAdmin() {

        $post = Post::where('categoryPost_id', '!=' , 21)->where('categoryPost_id', '!=' , 22)->get();
        $no=1;
        return view('admin.post',compact('post','no'));
    }

    public function addPostAdmin(Request $request) {

        if ($request->title) {
//            $post = Post::create($request->all());


            $path = '';
            if ($request->cover) {
//            $path = Storage::disk('public')->put('supplier', $request->image);

                $file = $request->cover;
                $path = $file->hashName('post');
                // supplier/bf5db5c75904dac712aea27d45320403.jpeg

                $image = Image::make($file);

                $image->resize(null, 1000, function ($constraint) {
                    $constraint->aspectRatio();
                });

//            $path = Storage::put($path, (string) $image->encode());
                Storage::disk('public')->put($path, (string) $image->encode());
            }

            if($request->status)
                $status = 'on';
            else
                $status = 'off';

            $post = Post::create([
                'title' => $request->title,
                'body' => $request->body,
                'cover' => $path,
                'categoryPost_id' => $request->categoryPost_id,
                'slide' => $status,
                'admin_id' => Auth::user()->id
            ]);

            if ($post != null)
                return redirect()->route('admin.post')->with('success', 'Insert Data Success!');
            return redirect()->route('admin.post')->with('success', 'Insert Data Failed!');
        }

        $cat_posts = CategoryPost::where('id', '!=' , 22)->get();
        return view('admin.add-post', compact('cat_posts'));
    }

    public function updatePost(Request $request, $id)
    {
        $post = Post::findOrFail($id);

        $path = '';
        if ($request->cover) {
//            $path = Storage::disk('public')->put('supplier', $request->image);

            $file = $request->cover;
            $path = $file->hashName('post');
            // supplier/bf5db5c75904dac712aea27d45320403.jpeg

            $image = Image::make($file);

            $image->resize(null, 1000, function ($constraint) {
                $constraint->aspectRatio();
            });

//            $path = Storage::put($path, (string) $image->encode());
            Storage::disk('public')->delete($post->cover);
            Storage::disk('public')->put($path, (string) $image->encode());

            $post->cover = $path;
        }

        if($request->status)
            $status = 'on';
        else
            $status = 'off';

        $post->title = $request->title;
        $post->body = $request->body;
        $post->slide = $status;
        $post->categoryPost_id = $request->categoryPost_id;
        $post->admin_id = Auth::user()->id;
        $post->save();

        if ($post != null)
            return redirect()->route('admin.post')->with('success', 'Update data sukses!');

        return redirect()->route('admin.post')->with('error', 'Update data gagal!');
    }


    public function testimonyAdmin() {

        $post = Post::where('categoryPost_id','21')->get();
        $no=1;
        return view('admin.testimony',compact('post','no'));
//        return $post;
    }

    public function addTestimonyAdmin(Request $request) {

        if ($request->nama) {
//            $post = Post::create($request->all());

            $path = '';
            if ($request->cover) {
//            $path = Storage::disk('public')->put('supplier', $request->image);

                $file = $request->cover;
                $path = $file->hashName('post');
                // supplier/bf5db5c75904dac712aea27d45320403.jpeg

                $image = Image::make($file);

                $image->resize(null, 300, function ($constraint) {
                    $constraint->aspectRatio();
                });

//            $path = Storage::put($path, (string) $image->encode());
                Storage::disk('public')->put($path, (string) $image->encode());
            }

            $post = Post::create([
                'title' => $request->nama.'-'.$request->status,
                'body' => $request->body,
                'cover' => $path,
                'categoryPost_id' => 21,
                'admin_id' => Auth::user()->id
            ]);

            if ($post != null)
                return redirect()->route('admin.testimony')->with('success', 'Insert Data Success!');
            return redirect()->route('admin.testimony')->with('success', 'Insert Data Failed!');
        }

        return view('admin.add-testimony');
    }

    public function updateTestimony(Request $request, $id)
    {
        $post = Post::findOrFail($id);

        $path = '';
        if ($request->cover) {
//            $path = Storage::disk('public')->put('supplier', $request->image);

            $file = $request->cover;
            $path = $file->hashName('post');
            // supplier/bf5db5c75904dac712aea27d45320403.jpeg

            $image = Image::make($file);

            $image->resize(null, 300, function ($constraint) {
                $constraint->aspectRatio();
            });

//            $path = Storage::put($path, (string) $image->encode());
            Storage::disk('public')->delete($post->cover);
            Storage::disk('public')->put($path, (string) $image->encode());

            $post->cover = $path;
        }

        $post->title = $request->nama.'-'.$request->status;
        $post->body = $request->body;
//        $post->categoryPost_id = $request->categoryPost_id;
        $post->admin_id = Auth::user()->id;
        $post->save();

        if ($post != null)
            return redirect()->route('admin.testimony')->with('success', 'Update data sukses!');

        return redirect()->route('admin.testimony')->with('error', 'Update data gagal!');
    }




}
