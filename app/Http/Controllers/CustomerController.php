<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::all();
//      $books = Book::all();
        $response = [
            'customers' => $customers
        ];
        return response()->json($response,200);
//        return view('home' , compact('categories','books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("customer.add");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Customer::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Customer::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::findOrFail($id);
        return view("customer.edit", compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::findOrFail($id);
        $customer->update($request->all());

        return $customer;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::findOrFail($id);
        $customer->delete();

        return response()->json(null,204);
    }

    public function customer() {

        return view('admin.customer');
    }

    public function customerAdmin() {

        $customer = Customer::all();
        $no=1;
        return view('admin.customer',compact('customer','no'));
    }

    public function bestCustomerAdmin() {

        $best = DB::table('order_details')
            ->join('products', 'products.id', '=', 'order_details.product_id')
            ->join('orders', 'orders.id', '=', 'order_details.order_id')
            ->join('customers', 'customers.id', '=', 'orders.customer_id')
            ->join('users', 'users.id', '=', 'customers.user_id')
            ->selectRaw('customers.*, users.first_name, users.last_name, SUM(order_details.total_price) as total_keluar, SUM(order_details.qty) as total_beli')
            ->groupBy('customers.id')
            ->orderBy('total_beli', 'DESC')->get();

        $no = 1;

        return view('admin.best-customer', compact('best', 'no'));
    }

    public function addCustomerAdmin() {

        return view('admin.add-customer');
    }

    public function showDetail($id) {

        return Customer::find($id);
    }
}
