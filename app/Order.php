<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $primaryKey="id";
    protected $guarded = [];

    public function billings()
    {
        return $this->hasMany('App\Billing','order_id');
    }

    public function orderDetails()
    {
        return $this->hasMany('App\OrderDetail','order_id');
    }

    public function addresses()
    {
        return $this->belongsTo('App\Address','address_id');
    }

//    public function customers(){
//        return $this->belongsTo('App\Customer','customer_id');
//    }


}
