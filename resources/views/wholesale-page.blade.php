@extends('layouts.app')
@section('content')
    <div id="main">
        <div class="section pt-7 pb-7">
            <div class="container">
                <div class="row" id="user">
                        <div class="commerce col-md-6 col-md-offset-3">
                            <div class="row" >
                                <p align="center">
                                <font size="5px">
                                    Pilihan belanja sebagai grosir merupakan pilihan bagi anda konsumen untuk membeli barang dalam jumlah besar dengan harga yang disesuaikan.
                                </font>
                                </p>
                            </div>
                            <!-- <form class="commerce-login-form" action="{{route('switchwholesale')}}" method="get"> -->

                            @if($classification[0]->classification == 'individu')
                                <div class="row">
                                    <button class="col-md-12 col-sm-12 col-xs-12" onclick="switchWholesale()">BELANJA SEBAGAI GROSIR</button>

                                </div>
                            @else
                                <div class="row">
                                    <button class="col-md-12 col-sm-12 col-xs-12" onclick="switchWholesale()">BELANJA SEBAGAI INDIVIDU</button>
                                </div>
                                <!-- <div class="row pt-1">
                                    <button class="col-md-12 btn-danger btn-sm" onclick="switchWholesale()">STOP BECOME A WHOLESALER</a>
                                </div> -->
                            @endif
                            <!-- </form> -->


                        </div>
                </div>
            </div>
        </div>
    </div>
 @endsection