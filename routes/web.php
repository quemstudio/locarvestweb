<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//USER & CUSTOMER Route -------------------------------------




Route::group(['middleware' => 'auth'], function () {
    Route::get('/checkout', 'CartController@checkout')->name('checkout');
    Route::post('/checkout/calculateShipping', 'CartController@calculateShipping')->name('calculateShipping');
    Route::get('/payment', 'BillingController@checkout')->name('payment');
    Route::get('/user-profile', 'UserController@editUser')->name('userProfile');
    Route::get('/user-profile-form', 'UserController@userProfile')->name('userProfileForm');
    Route::get('/user-address', 'UserController@userAddress')->name('userAddress');
    Route::get('/user-history', 'UserController@userHistory')->name('userHistory');
    Route::get('/user-confirmPayment', 'UserController@userConfirmPayment')->name('userConfirmPayment');
    Route::post('/user-confirmPayment/detail', 'UserController@userDetailPayment')->name('userDetailPayment');
    Route::post('/user-confirmPayment/upload', 'UserController@userUploadPaymentPage')->name('userUploadPaymentPage');
    Route::put('/user-confirmPayment/uploadPaymentFile', 'UserController@uploadPaymentFile')->name('uploadPaymentFile');
    Route::get('/user-wholesale', 'UserController@userWholesale')->name('userWholesale');
    Route::get('/switchwholesale', 'UserController@switchwholesale')->name('switchwholesale');
    Route::put('/profile/updateProfile', 'UserController@updateProfile')->name('updateProfile');
    Route::get('/shop/wholesale', 'ProductController@wholesaleShop')->name('wholesaleShop');
    Route::post('/address/delAddress', 'UserController@delAddress')->name('delAddress');
    Route::post('/address/updateAddress', 'AddressController@updateAddress')->name('updateAddress');
    Route::post('/address/updateAddressSubmit', 'UserController@updateAddressSubmit')->name('updateAddressSubmit');
    Route::get('/user-payment', 'BillingController@userPayment')->name('user-payment');
    Route::post('/user-phoneshipping', 'BillingController@phoneShipping')->name('phoneShipping');
    Route::get('/confirm-order', 'OrderController@confirmOrder')->name('confirm-order');
    Route::get('/finish-order', 'OrderController@finishOrder')->name('finish-order');
    Route::get('/order-succeed', 'OrderController@orderSucceed')->name('order-succeed');

    //mail
    Route::get('/mail/send', 'MailController@invoiceMail');
    Route::get('/mail/test', function () {
        $invoice = App\Invoice::find(1);

        return new App\Mail\InvoicePaid($invoice);
    });

 
});


Route::get('/test/{lastOrder}/{userId}', 'MailController@send');
Route::get('/logout', 'Auth\LoginController@logout')->name('logoutGet');

//ini route yg td nya diluar
Route::get('/', 'HomeController@index')->name('welcome');
Route::get('/activation/{id}', 'UserController@changeStatus')->name('activationUser');
Route::get('/shop', 'ProductController@index')->name('shop');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/shop_detail/', 'ProductController@index')->name('shop_detail');
Route::get('/category-list', 'CategoryController@index')->name('category-list');
Route::get('/partner-petani', 'SupplierController@index')->name('partner-petani');
Route::get('/blog', 'PostController@index')->name('blog');
Route::get('/blog/{{posts}}', 'PostController@show')->name('blog.show');
//ini route yg td nya diluar
   //ini route yg td nya diluar
   Route::resource('users', 'UserController');
   Route::resource('suppliers', 'SupplierController');
   Route::resource('customers', 'CustomerController');
   Route::resource('carts', 'CartController');
   Route::resource('shippers', 'ShipperController');
   Route::resource('orders', 'OrderController');
   Route::resource('billings', 'BillingController');
   Route::resource('categories', 'CategoryController');
   Route::resource('products', 'ProductController');
   Route::resource('order_details', 'OrderDetailController');
//   Route::resource('products', 'ProductController');
   Route::resource('posts', 'PostController');
   Route::resource('addresses', 'AddressController');
   //ini route yg td nya diluar
   //ini route yg td nya diluar
   Route::post('/cart/addTemp', 'CartController@addCartTemp')->name('addCartTemp');
   Route::post('/cart/delTemp', 'CartController@deleteCartTemp')->name('deleteCartTemp');
   Route::post('/cart/delDetailTemp', 'CartController@deleteCartDetailTemp')->name('deleteCartDetailTemp');
   Route::post('/cart/addQtyTemp', 'CartController@addQtyCartTemp')->name('addQtyCartTemp');


   Route::any('/shop/filterPriceAll', 'ProductController@filterPriceAllProduct')->name('filterPriceAll');
   Route::any('/shop/filterBestOffer', 'ProductController@filterBestOffer')->name('filterBestOffer');
   Route::any('/shop/sortBy', 'ProductController@sortBy')->name('sortBy');
   Route::any('/shop/catSortBy', 'CategoryController@catSortBy')->name('catSortBy');
   Route::any('/shop/searchProduct', 'ProductController@searchProduct')->name('searchProduct');


   Route::get('/our-activities', 'PageController@ourActivities')->name('our-activities');
   Route::get('/about-us', 'PageController@aboutUs')->name('about-us');
   Route::get('/our-team', 'PageController@ourTeam')->name('our-team');
   Route::get('/faq', 'PageController@faq')->name('faq');
   Route::get('/terms-and-condition', 'PageController@termsCondition')->name('terms-and-condition');
   Route::get('/customer-service', 'PageController@customerService')->name('customer-service');


   Route::any('/category/filterPriceCategory', 'CategoryController@filterPriceCategory')->name('filterPriceCategory');
   //ini route yg td nya diluar
//Route::get('/categories', function () {
//    return view('category-list');
//})->name('category-list');

Route::get('/profile', function () {
    return view('profile');
})->name('profile');

Route::get('/our-goals', function () {
    return view('our-goals');
})->name('our-goals');

Route::get('/contact-us', function () {
    return view('contact-us');
})->name('contact-us');

//Route::get('/login', function () {
//    return view('login');
//})->name('login');

//Route::get('/register', function () {
//    return view('register');
//})->name('register');
//
//Route::get('/blog-detail', function () {
//    return view('blog-detail');
//})->name('blog-detail');
//
//Route::get('/cart', function () {
//    return view('cart');
//})->name('cart');
//
//
//Route::get('/checkout', function () {
//    return view('checkout');
//})->name('checkout');

//-------------------------------------------------

//Back Route -------------------------------------



Auth::routes();
Route::group(['middleware' => 'auth'], function () {
    //diisi routes buat halaman user yg harus login dulu
});



//ADMIN ROUTES
Route::get('/admin', 'AdminController@index')->name('admin');
Route::post('/admin/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
Route::get('/admin/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::group(['middleware' => ['auth:admin']], function () {

    //    Route::get('admin/coupon', 'BillingController@couponAdmin')->name('admin.coupon');

    //manage admin
    Route::get('admin/dashboard', 'AdminController@index')->name('admin.dashboard');
    Route::post('admin/logout', 'Auth\AdminLoginController@logout')->name('admin.logout.submit');
    Route::get('admin/admin_page', 'AdminController@adminPage')->name('admin.admin.page');
    Route::get('admin/configuration', 'AdminController@configurationAdmin')->name('admin.configuration');
    Route::get('admin/admin_page/add', 'AdminController@addAdminPage')->name('admin.add.show');
    Route::get('admin/admin_page/edit/{id}', 'AdminController@edit')->name('admin.edit.show');
    Route::post('admin/admin_page/add', 'AdminController@store')->name('admin.add.submit');
    Route::put('admin/admin_page/edit/{id}', 'AdminController@updateAdmin')->name('admin.update.submit');
    Route::delete('admin/admin_page/{id}', 'AdminController@destroy')->name('admin.delete.submit');

    //manage category
    Route::get('admin/category', 'CategoryController@categoryAdmin')->name('admin.category');
    Route::get('admin/best_categories', 'CategoryController@bestCategoryAdmin')->name('admin.best.categories');
    Route::get('admin/category/add', 'CategoryController@addCategoryAdmin')->name('category.add.show');
    Route::get('admin/category/edit/{id}', 'CategoryController@edit')->name('category.edit.show');
    Route::post('admin/category/add', 'CategoryController@store')->name('category.add.submit');
    Route::put('admin/category/edit/{id}', 'CategoryController@updateCategory')->name('category.update.submit');
    Route::delete('admin/category/delete/{id}', 'CategoryController@destroy')->name('category.delete');

    //manage category post
    Route::get('admin/category_post', 'CategoryPostController@categoryPostAdmin')->name('admin.category.post');
    Route::get('admin/category_post/add', 'CategoryPostController@addCategoryPostAdmin')->name('category_post.add.show');
    Route::get('admin/category_post/edit/{id}', 'CategoryPostController@edit')->name('category_post.edit.show');
    Route::post('admin/category_post/add', 'CategoryPostController@addCategoryPostAdmin')->name('category_post.add.submit');
    Route::put('admin/category_post/edit/{id}', 'CategoryPostController@updateCategoryPost')->name('category_post.update.submit');
    Route::delete('admin/category_post/{id}', 'CategoryPostController@destroy')->name('category_post.delete');

    //manage post
    Route::get('admin/post', 'PostController@postAdmin')->name('admin.post');
    Route::get('admin/post/add', 'PostController@addPostAdmin')->name('post.add.show');
    Route::get('admin/post/edit/{id}', 'PostController@edit')->name('post.edit.show');
    Route::post('admin/post/add', 'PostController@addPostAdmin')->name('post.add.submit');
    Route::put('admin/post/edit/{id}', 'PostController@updatePost')->name('post.update.submit');
    Route::delete('admin/post/{id}', 'PostController@destroy')->name('post.delete');

    //manage page
    Route::get('admin/page', 'PageController@pageAdmin')->name('admin.page');
    Route::get('admin/page/add', 'PageController@addPageAdmin')->name('page.add.show');
    Route::post('admin/page/add', 'PageController@store')->name('page.add.submit');
    Route::delete('admin/page/delete/{id}', 'PageController@destroy')->name('page.delete');
    Route::get('admin/page/edit/{id}', 'PageController@edit')->name('page.edit.show');
    Route::put('admin/page/edit/{id}', 'PageController@updatePageAdmin')->name('page.edit.submit');

    //manage supplier
    Route::get('admin/petani', 'SupplierController@supplierAdmin')->name('admin.petani');
    Route::get('admin/petani/show_detail/{id}', 'SupplierController@showDetail')->name('petani.detail.show');
    Route::get('admin/petani/add', 'SupplierController@addSupplierAdmin')->name('petani.add.show');
    Route::post('admin/petani/add', 'SupplierController@addSupplierAdmin')->name('petani.add.submit');

    //manage customer
    Route::get('admin/customer', 'CustomerController@customerAdmin')->name('admin.customer');
    Route::get('admin/best_customer', 'CustomerController@bestCustomerAdmin')->name('admin.best.customer');
    Route::get('admin/customer/add', 'CustomerController@addCustomerAdmin')->name('customer.add.show');
    Route::get('admin/customer/show_detail/{id}', 'CustomerController@showDetail')->name('customer.detail.show');
    Route::post('admin/customer/add', 'CustomerController@addCustomerAdmin')->name('customer.add.submit');

    //manage product
    Route::get('admin/product', 'ProductController@productAdmin')->name('admin.product');
    Route::get('admin/product/detail/{id}', 'ProductController@productDetailAdmin')->name('admin.detail-product');
    Route::get('admin/import_product', 'ProductController@importProductAdmin')->name('admin.import.product');
    Route::get('admin/best_product', 'ProductController@bestProductAdmin')->name('admin.best.product');
    Route::get('admin/product/add', 'ProductController@addProductAdmin')->name('product.add.show');
    Route::get('admin/product/edit/{id}', 'ProductController@edit')->name('product.edit.show');
    Route::get('admin/product/show_detail/{id}', 'ProductController@showDetail')->name('product.detail.show');
    Route::post('admin/product/add', 'ProductController@addProductAdmin')->name('product.add.submit');
    Route::post('admin/product/import', 'ProductController@importProduct')->name('product.import');
    Route::put('admin/product/edit/{id}', 'ProductController@updateProduct')->name('product.update.submit');
    Route::delete('admin/product/delete/{id}', 'ProductController@destroy')->name('product.delete');

    //manage product image
    Route::post('admin/product/edit/upload/{id}', 'ProductImgController@uploadImg')->name('productImg.upload.submit');
    Route::delete('admin/product/edit/delete/{id}', 'ProductImgController@destroy')->name('productImg.delete.submit');
//    Route::delete('admin/productImg/delete/{id}', 'ProductImgController@destroy')->name('productImg.delete.submit');

    //manage orders
    Route::get('admin/orders', 'OrderController@orderAdmin')->name('admin.orders');
    Route::get('admin/orders/add', 'OrderController@addOrderAdmin')->name('orders.add.show');
    Route::post('admin/orders/add', 'OrderController@addOrderAdmin')->name('orders.add.submit');
    Route::put('admin/orders/update/{id}', 'OrderController@updateProgress')->name('orders.update.submit');
    Route::put('admin/orders/retur', 'OrderController@catatRetur')->name('orders.retur.submit');

    //manage billing
    Route::get('admin/payment', 'BillingController@paymentAdmin')->name('admin.payment');
    Route::put('admin/payment/edit/{id}', 'BillingController@changeStatus')->name('payment.change.submit');
    Route::put('admin/payment/upload', 'BillingController@uploadPayment')->name('payment.upload.submit');

    //manage supplier
    Route::get('admin/supplier', 'SupplierController@supplierAdmin')->name('admin.supplier');
    Route::get('admin/best_supplier', 'SupplierController@bestSupplierAdmin')->name('admin.best.supplier');
    Route::get('admin/supplier/add', 'SupplierController@showAddSupplierAdmin')->name('supplier.add.show');
    Route::get('admin/supplier/edit/{id}', 'SupplierController@edit')->name('supplier.edit.show');
    Route::post('admin/supplier/add', 'SupplierController@addSupplierAdmin')->name('supplier.add.submit');
    Route::put('admin/supplier/edit/{id}', 'SupplierController@updateSupplier')->name('supplier.update.submit');
    Route::delete('admin/supplier/{id}/{user_id}', 'SupplierController@deleteSupplier')->name('supplier.delete.submit');


    //manage testimony
    Route::get('admin/testimony', 'PostController@testimonyAdmin')->name('admin.testimony');
    Route::any('admin/testimony/add', 'PostController@addTestimonyAdmin')->name('testimony.add.show');
    Route::any('admin/testimony/edit/{id}', 'PostController@updateTestimony')->name('testimony.update.submit');
});
