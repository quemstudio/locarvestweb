<?php

namespace App\Http\Controllers;

use App\Billing;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Session;
use Intervention\Image\Facades\Image;
use App\Http\Controllers\MailController as mail;

class BillingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $billings = Billing::all();
//      $books = Book::all();
        // $response = [
        //     'billings' => $billings
        // ];
        // return response()->json($response,200);
       return view('payment');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("billing.add");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Billing::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Billing::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $billing = Billing::findOrFail($id);
        return view("billing.edit", compact('billing'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $billing = Billing::findOrFail($id);
        $billing->update($request->all());

        return $billing;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $billing = Billing::findOrFail($id);
        $billing->delete();

        return response()->json(null,204);
    }

    public function paymentAdmin() {

        $billings = Billing::all();
        $no = 1;
        return view('admin.payment', compact('billings','no'));
    }

    public function couponAdmin() {

        return view('admin.coupon');
    }

    public function payment(){

        return view('payment');
    }

    public function changeStatus($id)
    {
        $billing = Billing::findOrFail($id);
        $billing->status = 'paid';
        $billing->save();

        $order = Order::findOrFail($billing->order_id);
        $order->status = 'confirmed';
        $order->save();

        //mail service-------------------------------------------
        $mail = new mail();
        $mail->paymentThanksMail($billing->order_id, $billing->bank, $billing->account_number);



        if ($billing != null)
            return response()->json(null,204);
//            return redirect()->route('admin.payment')->with('success', 'Ganti status sukses!');

//        return redirect()->route('admin.payment')->with('error', 'Ganti status gagal!');
        return response()->json('error',404);
    }

    public function userPayment()
    {
        return view('payment');
    }

    public function phoneShipping(Request $request)
    {
        Session::put('shipdate', $request->shipdate);
        Session::put('phonenumber', $request->phonenumber);
        Session::put('address', $request->address);

    }

    public function uploadPayment(Request $request)
    {
//        $billing = Billing::where('order_id',$request->order_id)->get();
        $path = '';
        if ($request->image) {
//            $path = Storage::disk('public')->put('supplier', $request->image);

            $file = $request->image;
            $path = $file->hashName('product');
            // supplier/bf5db5c75904dac712aea27d45320403.jpeg

            $image = Image::make($file);

            $image->resize(null, 800, function ($constraint) {
                $constraint->aspectRatio();
            });

//            $path = Storage::put($path, (string) $image->encode());
            Storage::disk('public')->put($path, (string) $image->encode());
        }

        if ($path) {
            Billing::where('order_id', $request->order_id)
                ->update(['bank' => $request->bank,
                    'account_number' => $request->account_number,
                    'confirmation_pic' => $path,
                    'status' => 'paid'
                ]);

            $order = Order::findOrFail($request->order_id);
            $order->status = 'confirmed';
            $order->save();
        } else {
            Billing::where('order_id', $request->order_id)
                ->update(['bank' => $request->bank,
                    'account_number' => $request->account_number,
                    'status' => 'paid'
                ]);

            $order = Order::findOrFail($request->order_id);
            $order->status = 'confirmed';
            $order->save();
        }
        return redirect()->route('admin.payment')->with('success', 'Update payment sukses!');
    }
}
