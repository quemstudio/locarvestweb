@extends('layouts.app')
@section('content')


    <div id="main">
        <div class="section section-bg-10 pt-11 pb-17">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="page-title text-center">Belanja</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="section border-bottom pt-2 pb-2">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <ul class="breadcrumbs">
                            <li><a href="{{URL::to('/')}}">Home</a></li>
                            <li>Belanja</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="section pt-7 pb-7">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-md-push-3">
                        <div class="shop-filter">
                            <div class="col-md-6">
                            </div>
                            <div class="col-md-6">
                                <div class="shop-filter-right">
                                    <form class="commerce-ordering">
                                        <select name="orderby" id="orderby" class="orderby" onchange="sortBy()">
                                            <option selected="true" disabled="disabled">Urut berdasarkan</option>
                                            <option value="new">Produk baru</option>
                                            <option value="lh">Harga: rendah ke tinggi</option>
                                            <option value="hl">Harga: tinggi ke rendah</option>
                                        </select>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <br><br>


                        <div class="product-grid" id="product-grid">

                            {{--<div id="addCart"></div>--}}
                            @foreach($products->chunk(3) as $chunk)
                                <div class="row">
                                    @foreach($chunk as $product)
                                <div class="col-md-4 col-sm-6 product-item text-center mb-3">
                                    <div class="product-thumb">
                                        <a href="{{route('products.show', $product->id)}}">
                                            <div class="badges">
                                                @if($product->best_offer == 'on')
                                                    <span class="hot">Best</span>
                                            @else
                                                <!--<span class="hot">Hot</span>
                                                <span class="onsale">Sale!</span>-->
                                                    @endif
                                            </div>
                                            @if(!empty($product->image))
                                                <img src="{{URL::asset('/storage/'.$product->image)}}" class="img-thumbnail" style="height: 180px; width: auto" alt=""/>
                                            @else
                                                <img src="{{URL::asset('/assets/images/test.jpg')}}" class="img-thumbnail" style="height: 180px; width: auto" alt=""/>
                                            @endif
                                        </a>

                                        <div class="product-action">
                                            {{--<form>--}}
                                            {{--<input name="id" type="hidden" data-value="{{ $product['id'] }}">--}}
                                            {{--<input name="name" type="hidden" value="{{ $product['name'] }}">--}}
                                            {{--<input name="qty" type="hidden" value="1">--}}
                                            {{--<input name="price" type="hidden" value="{{ $product['price'] }}">--}}
                                            <span class="add-to-cart" style="cursor: pointer" id="btn-addCart" onclick="addCartTemp('{{ $product->id }}','{{ $product->name }}',1,'{{ $product->price }}','{{ $product->image }}')">
													<a data-toggle="tooltip" data-placement="top" title="Tambah ke cart"
                                                       id="btn-addCart" ></a>
                                            </span>
                                            {{--</form>--}}
                                        </div>
                                    </div>
                                    <div class="product-info">
                                        <a href="{{route('products.show', $product->id)}}">
                                            <h2 class="title">{{ $product->name }}</h2>
                                            <span class="price">
													<ins>Rp. {{ str_replace(',', '.', number_format($product->price)) }}</ins>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                                </div>
                                    @endforeach
                            <div class="col-md-auto col-md-offset-4">
                                {{ $products->appends(request()->input())->links() }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-md-pull-9">
                        <div class="sidebar">
                            <div class="widget widget-product-search">
                                <div class="form-search">
                                    <input type="text" class="search-field" placeholder="Cari produk..." value="" id="searchProduct"
                                           name="s"/>
                                    <input type="submit" value="Search" id="searchBtn"/>
                                </div>
                            </div>
                            <div class="widget widget-product-categories">
                                <div class="text-center"><button class="button" onclick="filterBestOffer()">Penawaran Terbaik</button></div>

                            </div>
                            <div class="widget widget_price_filter">
                                <h3 class="widget-title">Filter berdasarkan harga</h3>
                                <div class="price_slider_wrapper" style="padding-left: 15px; padding-right: 15px;">
                                <div id="html5"></div>
                                    <div class="price_slider_amount">
                                        <button type="submit" class="button" onclick="filterPriceAllProduct()">Filter</button>
                                        <div class="price_label" style="display:block;">
                                            {{--IDR <input type="text" id="min_price"/> - IDR <input type="text" id="max_price"/>--}}
                                            IDR <span id="min_price"></span> &mdash; IDR <span id="max_price"></span>
                                        </div>
                                        <div class="clear"></div>
                                    </div>

                                </div>
                            </div>
                            {{--<div class="widget widget_price_filter">--}}
                                {{--<h3 class="widget-title">Filter berdasarkan harga</h3>--}}
                                {{--<div class="price_slider_wrapper">--}}
                                    {{--<div class="price_slider" style="display:none;"></div>--}}
                                    {{--<div class="price_slider_amount">--}}
                                        {{--<input type="text" id="min_price" name="min_price" value="" data-min="0" placeholder="Min price"/>--}}
                                        {{--<input type="text" id="max_price" name="max_price" value="" data-max="100000" placeholder="Max price"/>--}}
                                        {{--<button type="submit" class="button" onclick="filterPriceAllProduct()">Filter</button>--}}
                                        {{--<div class="price_label" style="display:none;">--}}
                                            {{--<span class="from"></span> &mdash; <span class="to"></span>--}}
                                        {{--</div>--}}
                                        {{--<div class="clear"></div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="widget widget-product-categories">
                                <h3 class="widget-title">Kategori Produk</h3>
                                <ul class="product-categories" style="font-weight: bolder;">
                                    @foreach($countCat as $category)
                                        <li><a href="{{ route('categories.show',$category->id) }}">>&nbsp&nbsp{{ $category->name }}</a></li>
                                    @endforeach
                                </ul>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script type="text/javascript">

    var html5Slider = document.getElementById('html5');

    noUiSlider.create(html5Slider, {
        start: [ 0, 50000 ],
        connect: true,
        range: {
            'min': 0,
            'max': 50000
        },
        format: wNumb({
            decimals: 0,
        })
    });

    var snapValues = [
        document.getElementById('min_price'),
        document.getElementById('max_price')
    ];
    html5Slider.noUiSlider.on('update', function( values, handle ) {
        snapValues[handle].innerHTML = values[handle];
    });

    //Cart--------------------------------------------------------------------------------

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function addCartTemp(id,name,qty,price,img) {
//            e.preventDefault();
//            alert("Your values are :"+ id);
//            var id = $("input[name='id']").val();
//            var name = $("input[name='name']").val();
//            var qty = $("input[name='qty']").val();
//            var price = $("textarea[name='price']").val();
//            alert(price);
//            console.log(id);
//            console.log(name);
//            console.log(qty);
//            console.log(price);
//            console.log(img);
            $.ajax({
                url: '/locarvestweb/public/cart/addTemp',
                type: 'POST',
                data: {id: id, name: name, qty: qty, price: price, img: img},
                success: function (data) {
                    if ($.isEmptyObject(data.error)) {
                        $('#addCart').html(data);
                        alert('Item added to cart');
                    } else {
                        printErrorMsg(data.error);
                    }
                }
            });
        };

    function deleteCartTemp(id) {
            console.log(id);
        $.ajax({
            url: '/locarvestweb/public/cart/delTemp',
            type: 'POST',
            data: {id: id},
            success: function (data) {
                if ($.isEmptyObject(data.error)) {
                    $('#addCart').html(data);
                    alert('Item deleted in cart');
                } else {
                    printErrorMsg(data.error);
                }
            }
        });
    };

        function printErrorMsg(msg) {
            $(".print-error-msg").find("ul").html('');
            $(".print-error-msg").css('display', 'block');
            $.each(msg, function (key, value) {
                $(".print-error-msg").find("ul").append('<li>' + value + '</li>');
            });
        }
    //Cart--------------------------------------------------------------------------------


</script>
@endpush