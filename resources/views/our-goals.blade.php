<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 05/01/18
 * Time: 19.56
 */
?>
@extends('layouts.app')
@section('content')
    <div id="main">
        <div class="section section-bg-10 pt-11 pb-17">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="page-title text-center">Our Goals</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="section border-bottom pt-2 pb-2">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <ul class="breadcrumbs">
                            <li><a href="index.html">Home</a></li>
                            <li>>Our Goals</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="section pt-7 pb-7">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="single-blog">
                            <div class="post-thumbnail">
                                <a href="#">
                                    <img src="./assets/images/blog/blog_detail.jpg" alt="" />
                                </a>
                            </div>
                            <div class="entry-meta">
										<span class="posted-on">
											<i class="ion-calendar"></i>
											<span>August 9, 2016</span>
										</span>
                                <span class="categories">
											<i class="ion-folder"></i>
											<a href="#">Nutrition Meal</a>
										</span>
                            </div>
                            <h1 class="entry-title">How to steam &amp; purée your sugar pie pumkin</h1>
                            <div class="entry-content">
                                <p>I don’t know about you, but everywhere I look is a pumpkin recipe – from pumpkin lattes to pumpkin pie. But to get in on the homemade pumpkin cooking action, you will need the skills to make a simple pumpkin purée. Once you have that mastered, you will be surprised how many recipes “fall” into your lap.</p><p>On another note, steaming your pumpkin is a great way to maintain the most nutritional value when you eat it. It’s also fairly quick and simple and makes for a better tasting pumpkin, whatever recipe you’re using it for.</p>
                                <p>
                                    <img class="alignright size-full" src="./assets/images/blog/blog_detail_2.jpg" alt="" />
                                </p>
                                <p><strong>Steamed Pumpkin Purée</strong></p>
                                <p>Cut the pumpkin in half. With a spoon or a scoop, scrape out the seeds and pulp from the center. Place all the seeds into a bowl (you can roast them later).</p>
                                <p>Cut the halves into smaller pieces and place in a large pot of water with a steam basket to keep the pumpkin pieces from touching the water. Boil the water and cover the pot to steam the pumpkin for about 15 to 20 minutes until the pumpkin is tender.</p>
                                <p>Lay the pumpkin pieces on a towel and let cool completely.</p>
                                <p>Once cooled, peel off and discard the rind. Place some of the pumpkin flesh into food processor or a blender. You can also use a potato ricer if you prefer. Do this in batches.</p>
                                <p>Pulse the pumpkin until smooth. Pour the purée into a bowl, and continue to purée until all the pumpkin is done. You can either use this immediately in whatever pumpkin recipe you’d like, or store it in the freezer for later use.</p>
                                <p><strong>Store in the freezer</strong></p>
                                <p>To store in the freezer, spoon about 1 cupful of pumpkin purée into a sealable plastic storage bag. Seal the bag with just a tiny bit of an opening remaining, then use your hands to flatten out the pumpkin inside the bag and push out the air and then seal. Store them in the freezer until you need them.</p>
                                <blockquote><p>Most pumpkin dishes involve scooping out the seeds, cutting off the skin, and chopping up the flesh before cooking. Make pumpkin bread as the default gift for everyone. It is cheap, it is beloved, it is carbs.</p></blockquote>
                                <p>Now that you know how to purée like a pro, we highly recommend that you try our delicious Rustic Pumpkin Pie. There is almost nothing that epitomizes fall more than homemade pumpkin pie. The fresh pumpkin purée in this recipe gives the pie an unbelievable flavor that will make you a believer in using the real deal stuff. This makes a great Thanksgiving and fall dessert — and dare we say breakfast?</p>
                            </div>
                            <!-- <div class="entry-footer">
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="share">
                                            <span> <i class="ion-android-share-alt"></i> Share this post </span>
                                            <span> <a target="_blank" href="#"><i class="fa fa-facebook"></i></a> </span>
                                            <span> <a target="_blank" href="#"><i class="fa fa-twitter"></i></a> </span>
                                            <span> <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a> </span>
                                        </div>
                                    </div>
                                </div>
                            </div> -->

                            <!-- <div class="entry-nav">
                                <div class="row">
                                    <div class="col-md-5 left">
                                        <i class="fa fa-angle-double-left"></i>
                                        <a href="#">How can salmon be raised organically in fish farms?</a>
                                    </div>
                                    <div class="col-md-2 text-center">
                                        <i class="ion-grid"></i>
                                    </div>
                                    <div class="col-md-5 right">
                                        <a href="#">How to steam &amp; purée your sugar pie pumkin</a>
                                        <i class="fa fa-angle-double-right"></i>
                                    </div>
                                </div>
                            </div> -->

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
