<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 16/01/18
 * Time: 19.34
 */
?>
@extends('layouts.appadmin')
@section('content')
    <!-- BEGIN PAGE CONTENT -->
    <div class="header">
        <h2>Data <strong>Post</strong></h2>
    </div>
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif(session('error'))
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    @endif
    <div class="row">
        <div class="col-lg-12 portlets">
            <div class="panel">
                <div class="panel-header ">
                    <h3><a href="{{route('post.add.show')}}">
                            <button type="button" class="btn btn-primary">+ Add Post</button>
                        </a></h3>
                </div>
                <div class="panel-content">
                    <div class="filter-left panel-content pagination2 table-responsive">
                        <table class="table table-hover table-dynamic table-tools" data-table-name="Data Posts">
                            <thead>
                            <tr>
                                <th width="40px">No</th>
                                <th>Kategori</th>
                                <th>Judul</th>
                                <th width="125px" style="min-width: 125px"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($post as $data)
                                @php
                                    $cat = \App\CategoryPost::findOrFail($data->categoryPost_id)
                                @endphp
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td>{{$cat->name}}</td>
                                    <td>
                                        {{$data->title}}
                                    </td>
                                    <td class='hidden-480'>
                                        <div class="btn-group">
                                            <a class="btn btn-success" href="{{route('post.edit.show' ,$data['id'])}}">
                                                <i style="padding:0px; margin:0px" class="fa fa-edit"></i>
                                            </a>
                                            <button class="btn btn-danger" type="button"
                                                    id="danger-alert"
                                                    data-tr="tr_{{$data->id}}"
                                                    onclick="deleteData('{{$data->id}}', '{{$data->title}}')">
                                                <i style="padding:0px; margin:0px" class="fa fa-trash-o"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        function deleteData(id, name) {
            swal({
                title: "Anda yakin ingin menghapus data " + name + " ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn btn-danger btn-custom waves-effect waves-light',
                confirmButtonText: "Ya, hapus!",
                cancelButtonText: "Tidak",
                closeOnConfirm: false,
                cancelButtonClass: 'btn',
                buttonsStyling: false,
                reverseButtons: true
            }, function () {
                setTimeout(function () {
                    $.ajax({
                        url: 'post/' + id,
                        type: 'DELETE',
                        dataType: 'json',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        success: function (data) {
                            if (data == null) {
                                swal("Terhapus!", "Data berhasil dihapus.", "success");
                                location.reload();
                            }
                            else
                                swal("Gagal!", "Gagal menghapus data.", "error");
                        }, error: function () {
                            swal("Error!", "Foreign key constraint fails.", "error");
//                            swal("Error!", "Failed to delete.", "error");
                        }
                    });
                }, 2000)
            });
        }
    </script>
@endpush