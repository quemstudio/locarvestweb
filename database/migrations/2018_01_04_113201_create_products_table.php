<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->date('planting_schedule');
            $table->integer('land_area');
            $table->integer('seed_quantity');
            $table->date('harvest_time');
            $table->integer('harvest_quantity');
            $table->integer('wholesale_price');
            $table->integer('individu_price');
            $table->string('status'); //status ready atau blm
            $table->text('description');
            $table->integer('amount_left'); //jumlah tersedia
            $table->integer('best_offer')->nullable(); //apakah termasuk ke penawaran terbaik atau tidak
            $table->date('preorder_date'); //harvest time ?? tanggal bisa mulai order (status ready),

            $table->string('image');

            $table->integer('category_id')->unsigned();
            $table->integer('supplier_id')->unsigned();
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('supplier_id')->references('id')->on('suppliers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
