<?php
/**
 * Created by PhpStorm.
 * User: Triand Mullik
 * Date: 1/30/2018
 * Time: 5:32 PM
 */ ?>



<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="modal-body">
            <div class="commerce">
                <form class="commerce-login-form" id="updateAddress" >
                    {{csrf_field()}} {{method_field('PUT')}}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-wrap">
                                <input type="text" name="city" id="city" placeholder="city" value="{{ $address->city }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-wrap">
                                <input type="text" name="kecamatan" id="kecamatan"  placeholder="Kecamatan" value="{{ $address->kecamatan }}" size="40">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-wrap">
                                <input type="text" name="kelurahan" id="kelurahan"  placeholder="Kelurahan" value="{{ $address->kelurahan }}" size="40">
                                <input type="hidden" id="rtrw" name="rtrw" value="00/00">
                                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                                <input type="hidden" name="id" id="id" value="{{$address->id}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-wrap">
                                <input type="text" name="zip_code" id="zip_code"   placeholder="Zip Code" value="{{ $address->zip_code }}" size="40">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-wrap">
                                <textarea name="addressUpdate" placeholder="Full Address" id="addressUpdate"  value="{{ $address->address }}">{{ $address->address }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="form-wrap">
                                    <!-- copy dari sini -->
                                    <div class="form-group input-group">
                                        <input type="text" id="search_location" class="form-control" placeholder="Nama Tempat/Jalan/Daerah">
                                        <div class="input-group-btn">
                                            <button class="btn btn-default get_map" type="submit">
                                                Cari Lokasi
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-md-offset-4">
                                        <p class="account-copyright">
                                            <span style="">Tarik dan pindahkan </span>
                                            <img style="vertical-align:middle; height: 25px;background-color: transparent;
border: none;" src="http://www.urltarget.com/images/location-pointer-pin-google-map-red.png">
                                            <span style="">ke lokasi anda</span>
                                        </p>
                                    </div>
                                    <div id="myMap2" class="col-md-12"></div>
                                    <input id="address2" class="form-control input-sm" type="text"/><br/>
                                    <input type="hidden" id="latitude2" name="lat" placeholder="Latitude" required/>
                                    <input type="hidden" id="longitude2" name="long" placeholder="Longitude"
                                           required/>
                                    <!-- copy sampe sini -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-wrap">
                                <button onclick="updateAddressSubmit()" class="col-md-12">Update</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>





