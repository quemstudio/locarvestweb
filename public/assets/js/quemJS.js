$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function addCartTemp(id, name, qty, price, img) {
//            e.preventDefault();
//            alert("Your values are :"+ id);
//            var id = $("input[name='id']").val();
//            var name = $("input[name='name']").val();
//            var qty = $("input[name='qty']").val();
//            var price = $("textarea[name='price']").val();
//            alert(price);
//            console.log(id);
//            console.log(name);
//            console.log(qty);
//            console.log(price);
//            console.log(img);
$('.load-bar').html('<div class="bar"></div>');
    $.ajax({
        url: '/locarvestweb/public/cart/addTemp',
        type: 'POST',
        data: {id: id, name: name, qty: qty, price: price, img: img},
        success: function (data) {
            if ($.isEmptyObject(data.error)) {
                $('#addCart').html(data);
                $.toast({
                    heading: 'Success',
                    text: 'Item added to cart',
                    showHideTransition: 'fade',
                    icon: 'success',
                    hideAfter: 2000,
                    position: 'bottom-center',
                    bgColor: '#5fbd74',
                    loader: false,
                });

            } else {
                printErrorMsg(data.error);
            }
        }
    });
};

function addMultiCartTemp(id, name, price, img) {
    var qty = $("#multiQty").val();
//        console.log(id);
//        console.log(name);
//        console.log(price);
//        console.log(img);
    $('.load-bar').html('<div class="bar"></div>');
    $.ajax({
        url: '/locarvestweb/public/cart/addTemp',
        type: 'POST',
        data: {id: id, name: name, qty: qty, price: price, img: img},
        success: function (data) {
            if ($.isEmptyObject(data.error)) {
                $('#addCart').html(data);
                alert('Item added to cart');
            } else {
                printErrorMsg(data.error);
            }
        }
    });
};

function deleteCartTemp(id) {
    console.log(id);
    $('.load-bar').html('<div class="bar"></div>');
    $.ajax({
        url: '/locarvestweb/public/cart/delTemp',
        type: 'POST',
        data: {id: id},
        success: function (data) {
            if ($.isEmptyObject(data.error)) {
                $('#addCart').html(data);
                $.toast({
                    heading: 'Success',
                    text: 'Item deleted in cart',
                    showHideTransition: 'fade',
                    icon: 'success',
                    hideAfter: 2000,
                    position: 'bottom-center',
                    bgColor: '#5fbd74',
                    loader: false,
                });
            } else {
                printErrorMsg(data.error);
            }
        }
    });
};

function deleteCartDetailTemp(id) {
    console.log(id);
    $('.load-bar').html('<div class="bar"></div>');
    $.ajax({
        url: '/locarvestweb/public/cart/delDetailTemp',
        type: 'POST',
        data: {id: id},
        success: function (data) {
            if ($.isEmptyObject(data.error)) {
                $('#deleteCart').html(data);
                $.toast({
                    heading: 'Success',
                    text: 'Item deleted in cart',
                    showHideTransition: 'fade',
                    icon: 'success',
                    hideAfter: 2000,
                    position: 'bottom-center',
                    bgColor: '#5fbd74',
                    loader: false,
                });
            } else {
                printErrorMsg(data.error);
            }
        }
    });
};

var timeout = null;

function addQty(id, idCart, price) {
    document.getElementById("amount-" + id).textContent = "Rp. " + $("#" + idCart).val() * price;
    if (timeout) {
        $('#loading').html('<img src="{{ URL::asset('/assets/images/ajax-loader.gif') }}">');
        clearTimeout(timeout);
    }
    timeout = setTimeout(function () {
        $('#loading').html('<img src="{{ URL::asset('/assets/images/ajax-loader.gif') }}">');
        var qty = $("#" + idCart).val();
        $.ajax({
            url: '/locarvestweb/public/cart/addQtyTemp',
            type: 'POST',
            data: {qty: qty, id: id},
            success: function (data) {
                if ($.isEmptyObject(data.error)) {
                    $('#updateCart').html(data);
                } else {
                    printErrorMsg(data.error);
                }
            }
        });
    }, 300);
};

function deleteAddress(id) {
    $('.load-bar').html('<div class="bar"></div>');
    if (timeout) {
        $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
        clearTimeout(timeout);
    }
    timeout = setTimeout(function () {
    $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
    $.ajax({
        url: '/locarvestweb/public/address/delAddress',
        type: 'POST',
        data: {id: id},
        success: function (data) {
            if ($.isEmptyObject(data.error)) {
                $('#user').html(data);
                $.toast({
                    heading: 'Success',
                    text: 'Address Deleted',
                    showHideTransition: 'fade',
                    icon: 'success',
                    hideAfter: 2000,
                    position: 'bottom-center',
                    bgColor: '#5fbd74',
                    loader: false,
                });
            } else {
                printErrorMsg(data.error);
            }
        }
    });
}, 300);
};

function updateAddressSubmit() {
    var city = $('#city').val();
    var id = $('#id').val();
    var kecamatan = $('#kecamatan').val();
    var kelurahan = $('#kelurahan').val();
    var address = $('#address').val();
    var zip_code = $('#zip_code').val();

    if (timeout) {
        $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
        clearTimeout(timeout);
    }
    timeout = setTimeout(function () {
        $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
        $.ajax({
            url: '/locarvestweb/public/address/updateAddressSubmit',
            type: 'POST',
            data: {zip_code: zip_code, city: city, id: id, kecamatan: kecamatan, kelurahan: kelurahan, address: address},
            success: function (data) {
                if ($.isEmptyObject(data.error)) {
                    $('#user').html(data);
                    $.toast({
                        heading: 'Success',
                        text: 'Address Updated',
                        showHideTransition: 'fade',
                        icon: 'success',
                        hideAfter: 2000,
                        position: 'bottom-center',
                        bgColor: '#5fbd74',
                        loader: false,
                    });
                } else {
                    printErrorMsg(data.error);
                }
            }
        });
    }, 300);
};

function addAddress() {
    $('.load-bar').html('<div class="bar"></div>');
    var city = $('#city').val();
    var id = $('#id').val();
    var kecamatan = $('#kecamatan').val();
    var kelurahan = $('#kelurahan').val();
    var address = $('#address').val();
    var zip_code = $('#zip_code').val();

    if (timeout) {
        $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
        clearTimeout(timeout);
    }
    timeout = setTimeout(function () {
        $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
        $.ajax({
            url: '/locarvestweb/public/address/updateAddressSubmit',
            type: 'POST',
            data: {zip_code: zip_code, city: city, id: id, kecamatan: kecamatan, kelurahan: kelurahan, address: address},
            success: function (data) {
                if ($.isEmptyObject(data.error)) {
                    $('#user').html(data);
                    $.toast({
                        heading: 'Success',
                        text: 'Address Updated',
                        showHideTransition: 'fade',
                        icon: 'success',
                        hideAfter: 2000,
                        position: 'bottom-center',
                        bgColor: '#5fbd74',
                        loader: false,
                    });
                } else {
                    printErrorMsg(data.error);
                }
            }
        });
    }, 300);
};

    


function updateAddress(id) {
    if (timeout) {
        $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
        clearTimeout(timeout);
    }
    timeout = setTimeout(function () {
        $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
        $.ajax({
            url: '/locarvestweb/public/address/updateAddress',
            type: 'POST',
            data: {id: id},
            success: function (data) {
                if ($.isEmptyObject(data.error)) {
                    $('#user').html(data);
                } else {
                    printErrorMsg(data.error);
                }
            }
        });
    }, 300);
};

function addAddressProfile(id) {
    if (timeout) {
        $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
        clearTimeout(timeout);
    }
    timeout = setTimeout(function () {
    $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
    $.ajax({
        url: '/locarvestweb/public/address/delAddress',
        type: 'POST',
        data: {id: id},
        success: function (data) {
            if ($.isEmptyObject(data.error)) {
                $('#user').html(data);
                $.toast({
                    heading: 'Success',
                    text: 'Address Deleted',
                    showHideTransition: 'fade',
                    icon: 'success',
                    hideAfter: 2000,
                    position: 'bottom-center',
                    bgColor: '#5fbd74',
                    loader: false,
                });
            } else {
                printErrorMsg(data.error);
            }
        }
    });
}, 300);
};

function myProfile() {
    if (timeout) {
        $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
        clearTimeout(timeout);
    }
    timeout = setTimeout(function () {
        $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
        $.ajax({
            url: '/locarvestweb/public/user-profile-form',
            type: 'GET',
            data: {},
            success: function (data) {
                if ($.isEmptyObject(data.error)) {
                    $('#user').html(data);
                } else {
                    printErrorMsg(data.error);
                }
            }
        });
    }, 300);
};

function myAddress() {
    console.log('test');
    if (timeout) {
        $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
        clearTimeout(timeout);
    }
    timeout = setTimeout(function () {
        $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
        $.ajax({
            url: '/locarvestweb/public/user-address',
            type: 'GET',
            data: {},
            success: function (data) {
                if ($.isEmptyObject(data.error)) {
                    $('#user').html(data);
                } else {
                    printErrorMsg(data.error);
                }
            }
        });
    }, 300);
};

function myHistory() {
    if (timeout) {
        $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
        clearTimeout(timeout);
    }
    timeout = setTimeout(function () {
        $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
        $.ajax({
            url: '/locarvestweb/public/user-history',
            type: 'GET',
            data: {},
            success: function (data) {
                if ($.isEmptyObject(data.error)) {
                    $('#user').html(data);
                } else {
                    printErrorMsg(data.error);
                }
            }
        });
    }, 300);
};

function filterPriceAllProduct() {
    $('.load-bar').html('<div class="bar"></div>');
    if (timeout) {
        $('.product-grid').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
        clearTimeout(timeout);
    }
    timeout = setTimeout(function () {
        $('.product-grid').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
        var minPrice = $("#min_price").val();
        var maxPrice = $("#max_price").val();

        console.log(minPrice);
        console.log(maxPrice);
        $.ajax({
            url: '/locarvestweb/public/shop/filterPriceAll',
            type: 'POST',
            data: {minPrice: minPrice, maxPrice: maxPrice},
            success: function (data) {
                if ($.isEmptyObject(data.error)) {
                    $('.product-grid').html(data);
                } else {
                    printErrorMsg(data.error);
                }
            }
        });
    }, 300);
};

function sortBy() {
    $('.load-bar').html('<div class="bar"></div>');
    var order = document.getElementById("orderby").value;
    console.log(order);
    if (timeout) {
        $('.product-grid').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
        clearTimeout(timeout);
    }
    timeout = setTimeout(function () {
        $('.product-grid').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
        $.ajax({
            url: '/locarvestweb/public/shop/sortBy',
            type: 'POST',
            data: {order: order},
            success: function (data) {
                if ($.isEmptyObject(data.error)) {
                    $('.product-grid').html(data);
                } else {
                    printErrorMsg(data.error);
                }
            }
        });
    }, 300);
};

function catSortBy(id) {
    $('.load-bar').html('<div class="bar"></div>');
    var order = document.getElementById("orderby").value;
    if (timeout) {
        $('.product-grid').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
        clearTimeout(timeout);
    }
    timeout = setTimeout(function () {
        $('.product-grid').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
        $.ajax({
            url: '/locarvestweb/public/shop/catSortBy',
            type: 'POST',
            data: {order: order, id: id},
            success: function (data) {
                if ($.isEmptyObject(data.error)) {
                    $('.product-grid').html(data);
                } else {
                    printErrorMsg(data.error);
                }
            }
        });
    }, 300);
};

function filterPriceCategoryProduct(catId) {
    $('.load-bar').html('<div class="bar"></div>');
    if (timeout) {
        $('.product-grid').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
        clearTimeout(timeout);
    }
    timeout = setTimeout(function () {
        $('.product-grid').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
        var minPrice = $("#min_price").val();
        var maxPrice = $("#max_price").val();

        console.log(minPrice);
        console.log(maxPrice);
        $.ajax({
            url: '/locarvestweb/public/category/filterPriceCategory',
            type: 'POST',
            data: {minPrice: minPrice, maxPrice: maxPrice, catId: catId},
            success: function (data) {
                if ($.isEmptyObject(data.error)) {
                    $('.product-grid').html(data);
                } else {
                    printErrorMsg(data.error);
                }
            }
        });
    }, 300);
};

function updateProfile(){
    timeout = setTimeout(function () {
        var first_name = $("#first_name").val();
        var last_name = $("#last_name").val();
        var phone_number = $("#phone_number").val();
        var gender = $('input[name=gender]:checked').val();
        

        $('#user').html('<div class="row"><div class="col-md-6 col-md-offset-3"><div class="pagination"><img src="{{ URL::asset('/assets/images/ajax-loader-3.gif') }}"></div></div><div>');
       

        console.log(first_name);
        console.log(last_name);
        console.log(phone_number);
        console.log(gender);

        $.ajax({
            url: '/locarvestweb/public/profile/updateProfile',
            type: 'PUT',
            data: {first_name: first_name, last_name: last_name, phone_number: phone_number, gender: gender},
            success: function (data) {
                if ($.isEmptyObject(data.error)) {
                    $('#user').html(data);
                    $.toast({
                    heading: 'Success',
                    text: 'Profile Updated',
                    showHideTransition: 'fade',
                    icon: 'success',
                    hideAfter: 2000,
                    position: 'bottom-center',
                    bgColor: '#5fbd74',
                    loader: false,
                });
                } else {
                    printErrorMsg(data.error);
                }
            }
        });
    }, 300);
}

function printErrorMsg(msg) {
    $(".print-error-msg").find("ul").html('');
    $(".print-error-msg").css('display', 'block');
    $.each(msg, function (key, value) {
        $(".print-error-msg").find("ul").append('<li>' + value + '</li>');
    });
}

//Cart--------------------------------------------------------------------------------


$("[type='number']").keypress(function (evt) {
    evt.preventDefault();
});