<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 16/01/18
 * Time: 19.26
 */
?>
    @extends('layouts.appadmin') @section('content')
    <!-- BEGIN PAGE CONTENT -->
    <div class="header">
        <h2>Data
            <strong>Kategori</strong>
        </h2>
    </div>
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif(session('error'))
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    @endif
    <div class="row">

        <div class="col-lg-12 portlets">
            <div class="panel">
                <div class="panel-header ">
                    <h3>
                        <a href="{{route('category.add.show')}}">
                            <button type="button" class="btn btn-primary">+ Tambah Kategori</button>
                        </a>
                    </h3>
                </div>
                <div class="panel-content">
                    <div class="panel-content pagination2 table-responsive">
                        <table class="table table-dynamic " data-table-name="Total Category">
                            <thead>
                                <tr>
                                    <th style="width: 20px">No</th>
                                    <th>Nama Kategori</th>
                                    <th></th>
                                    <th style="width:150px"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($cat as $data)
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td>
                                        {{$data->name}}
                                    </td>
                                    <td>
                                        {{--<img style="width: 50px;" src="{{URL::asset($data->image)}}" alt="image">--}}
                                        <img style="width: 50px;" src="{{Storage::url($data->image)}}" alt="image">
                                    </td>
                                    <td class='hidden-480'>
                                        <div class="btn-group">

                                            {{--<button class="btn btn-success" type="button">--}}
                                                {{--<i class="fa fa-edit"></i>--}}
                                            {{--</button>--}}
                                            <a class="btn btn-success" href="{{route('category.edit.show' , $data->id)}}">
                                                <i style="padding:0px; margin:0px" class="fa fa-edit"></i>
                                            </a>
                                            <button class="btn btn-danger" type="button" id="danger-alert" data-tr="tr_{{$data->id}}" onclick="deleteCat('{{$data->id}}', '{{$data->name}}')">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </div>

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection @push('script')
    <script>
        function deleteCat(id, name) {
            swal({
                title: "Anda yakin ingin menghapus kategori " + name + " ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn btn-danger btn-custom waves-effect waves-light',
                confirmButtonText: "Ya, hapus!",
                cancelButtonText: "Tidak",
                cancelButtonClass: 'btn',
                buttonsStyling: false,
                reverseButtons: true,
                closeOnConfirm: false
            }, function () {
                setTimeout(function () {
                    $.ajax({
                        url: 'category/delete/' + id,
                        type: 'DELETE',
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (data) {
                            if (data == null) {
                                swal("Deleted!", "Category has been deleted.", "success");
                                location.reload();
                            } else
                                swal("Not Deleted!", "Failed to delete.", "error");
                        },
                        error: function () {
                            swal("Error!", "Failed to delete.", "error");
                        }
                    });
                }, 2000)
            });
        }
    </script>
    @endpush