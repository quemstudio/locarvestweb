<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Category;
use App\Post;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $categories = Category::all();
        $testimonies = Post::where('categoryPost_id',21)->get();
        $slides = Post::where('categoryPost_id', '!=' , 21)
            ->where('categoryPost_id', '!=' , 22)
            ->where('slide','on')
            ->get();
        $posts = Post::where('categoryPost_id', '!=' , 21)
            ->where('categoryPost_id', '!=' , 22)
            ->where('slide', '!=' , 'on')
            ->orderBy('id', 'desc')->take(3)->get();
        $countCat = Category::all();
//        $countCat = DB::table('products')
//            ->join('categories','categories.id','=','products.category_id')
//            ->select(DB::raw('count(products.name) as count, categories.name'))
//            ->groupBy('products.category_id')
//            ->get();
//        return $testimonies;

        $array = [7,8,12,16,23];

        return view('welcome',compact('countCat','posts','testimonies','array','slides'));
       // return view('disable');

    }
}
