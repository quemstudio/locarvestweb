<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('address');
            $table->string('image');
            $table->string('name');
            $table->integer('land_area');
//            $table->string('commodities');
//            $table->date('planting_schedule');
//            $table->integer('land_area');
//            $table->integer('seed_quantity');
//            $table->date('harvest_time');
//            $table->integer('harvest_quantity');

            $table->integer('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supppliers');
    }
}
