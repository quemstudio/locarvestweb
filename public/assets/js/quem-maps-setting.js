var geocoder;
var map;
var map2;
var marker;

/*
 * Google Map with marker
 */
function initialize() {
    var initialLat = $('#latitude').val();
    var initialLong = $('#longitude').val();
    var myLatlng = new google.maps.LatLng(-6.902342,107.62180699999999);
    var infowindow = new google.maps.InfoWindow();
    // var infowindow
    initialLat = initialLat?initialLat:-6.902342;
    initialLong = initialLong?initialLong:107.62180699999999;

    var latlng = new google.maps.LatLng(initialLat, initialLong);
    var options = {
        zoom: 16,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById("myMap"), options);

    geocoder = new google.maps.Geocoder();

    marker = new google.maps.Marker({
        map: map,
        draggable: true,
        position: latlng
    });

    google.maps.event.addListener(marker, 'drag', function () {
        geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    $('#address').val(results[0].formatted_address);
                    $('#latitude').val(marker.getPosition().lat());
                    $('#longitude').val(marker.getPosition().lng());
                    infowindow.setContent(results[0].formatted_address);
                    infowindow.open(map, marker);
                }
            }
        });
    });

    // geocoder.geocode({'latLng': marker.getPosition() }, function(results, status) {
    //     if (status == google.maps.GeocoderStatus.OK) {
    //         if (results[0]) {
    //             $('#latitude,#longitude').show();
    //             $('#address').val(results[0].formatted_address);
    //             $('#latitude').val(marker.getPosition().lat());
    //             $('#longitude').val(marker.getPosition().lng());
    //             infowindow.setContent(results[0].formatted_address);
    //             infowindow.open(map, marker);
    //         }
    //     }
    // });
}

google.maps.event.addDomListener(window, 'load', initialize);



