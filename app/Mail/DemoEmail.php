<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;


class DemoEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The demo object instance.
     *
     * @var Demo
     */
    public $demo;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($demo)
    {
        $this->demo = $demo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        if($this->demo->emailType == 'invoice'){
            return $this->from('locarvest@gmail.com')
                ->subject('Informasi invoice anda')
                ->view('mails.invoice')
                ->with(
                    [
                        'dataProducts' => $this->demo->products,
                        'total_price' => $this->demo->total,
                        'sender' =>  $this->demo->sender,
                        'receiver' =>  $this->demo->receiver,
                        'date' =>  $this->demo->date,
                        'phone' =>  $this->demo->phone,
                        
                    ]);
        }else if($this->demo->emailType == 'paymentThanks'){
            return $this->from('locarvest@gmail.com')
                ->subject('Informasi konfirmasi pembayaran')
                ->view('mails.paymentThanks')
                ->with(
                    [
                        'receiver' => $this->demo->receiver,
                        'orderNo' => $this->demo->orderNo,
                        'bank' => $this->demo->bank,
                        'accountNumber' => $this->demo->accountNumber,
                        'nominal' => $this->demo->nominal,
                        'testVarOne' => '1',
                        'testVarTwo' => '50000',
                    ]);
        }else if($this->demo->emailType == 'register'){
            return $this->from('locarvest@gmail.com')
                ->subject('Terimakasih telah mendaftar di Locarvest')
                ->view('mails.register')
                ->with(
                    [
                        'receiver' => $this->demo->receiver,
                        'email' => $this->demo->email,
                        'password' => $this->demo->password,
                        'userId' => $this->demo->userId,
                    ]);
        }

    }
}
