<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
//      $books = Book::all();
        $response = [
            'categories' => $categories
        ];
//        return response()->json($response,200);
        return view('category-list' , compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("category.add");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $path = '';
        if ($request->image) {
//            $path = Storage::disk('public')->put('supplier', $request->image);

            $file = $request->image;
            $path = $file->hashName('category');
            // supplier/bf5db5c75904dac712aea27d45320403.jpeg

            $image = Image::make($file);

            $image->resize(null, 350, function ($constraint) {
                $constraint->aspectRatio();
            });

//            $path = Storage::put($path, (string) $image->encode());
            Storage::disk('public')->put($path, (string) $image->encode());
        }

        $cat= Category::create([
            'name' => $request->name,
            'image' => $path,
        ]);


        if($cat != null)
            return redirect()->route('admin.category')->with('success', 'Insert Data Success!');

        return redirect()->route('admin.category')->with('error', 'Insert Data Failed!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
//        return Category::find($id);
        if(Auth::check()){
        $status = Customer::select('classification')->where('user_id',Auth::user()->id)->get();
        if($status[0]->classification == 'individu'){
            $products = DB::table('products')
                ->join('categories','categories.id','=','products.category_id')
                ->select(DB::raw('products.name as productName, categories.name as categoryName, categories.*, products.*'))
                ->where('categories.id',$id)
                ->where('category','individu')
                ->where('status','on')
                ->paginate(15);
            }else{
            $products = DB::table('products')
                ->join('categories','categories.id','=','products.category_id')
                ->select(DB::raw('products.name as productName, categories.name as categoryName, categories.*, products.*'))
                ->where('categories.id',$id)
                ->where('category','wholesale')
                ->where('status','on')
                ->paginate(15);
            }
        }else{
            $products = DB::table('products')
                ->join('categories','categories.id','=','products.category_id')
                ->select(DB::raw('products.name as productName, categories.name as categoryName, categories.*, products.*'))
                ->where('categories.id',$id)
                ->where('category','individu')
                ->where('status','on')
                ->paginate(15);
        }
        $countCat = Category::all();
        // $countCat = DB::table('products')
        //     ->join('categories','categories.id','=','products.category_id')
        //     ->select(DB::raw('count(products.name) as count, categories.name, categories.id'))
        //     ->groupBy('products.category_id')
        //     ->get();
//        return $countCat;
        if ($request->ajax()) {
            return view('shopFiltered', compact('products'));  
        }
        return view('categoryDetail' , compact('products','countCat'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view("admin.detail-category", compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::findOrFail($id);
        $category->update($request->all());

        return $category;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();

        return response()->json(null,204);
    }

    public function categoryAdmin() {

        $cat = Category::all();
        $no = 1;
        return view('admin.category',compact('cat','no'));
    }

    public function bestCategoryAdmin() {

        $best = DB::table('order_details')
            ->join('products', 'products.id', '=', 'order_details.product_id')
            ->join('categories', 'categories.id', '=', 'products.category_id')
            ->selectRaw('categories.name, SUM(order_details.total_price) as total_dapat, SUM(order_details.qty) as total_jual')
            ->groupBy('categories.id')
            ->orderBy('total_jual', 'DESC')->get();

        $no = 1;

        return view('admin.best-categories', compact('best', 'no'));
    }

    public function addCategoryAdmin(Request $request) {

        return view('admin.add-category');
    }

    public function filterPriceCategory(Request $request){
        if(Auth::check()){
            $status = Customer::select('classification')->where('user_id',Auth::user()->id)->get();
            if($status[0]->classification == 'individu'){
                $products = DB::table('products')
                ->where('category_id',$request->catId)
                ->where('category','individu')
                    ->where('status','on')
                ->whereBetween('price', [$request->minPrice, $request->maxPrice])
                ->paginate(15);
            }else{
                $products = DB::table('products')
                ->where('category_id',$request->catId)
                ->where('category','wholesale')
                    ->where('status','on')
                ->whereBetween('price', [$request->minPrice, $request->maxPrice])
                ->paginate(15);
            }
        }else{
            $products = DB::table('products')
                ->where('category_id',$request->catId)
                ->where('category','individu')
                ->where('status','on')
                ->whereBetween('price', [$request->minPrice, $request->maxPrice])
                ->paginate(15);
        }

//        return $products[0]->individu_price;
        return view('shopFiltered',compact('products'));
    }

    public function catSortBy(Request $request){

    if(Auth::check()){
            $status = Customer::select('classification')->where('user_id',Auth::user()->id)->get();   
            if($status[0]->classification == 'individu'){
                if($request->order == 'lh'){
                    $products = DB::table('categories')
                    ->join('products','categories.id','=','products.category_id')
                    ->where('categories.id',$request->id)
                    ->where('category','individu')
                        ->where('status','on')
                    ->orderBy('price', 'asc')
                    ->paginate(15);
                }elseif($request->order == 'hl'){
                    $products = DB::table('categories')
                    ->join('products','categories.id','=','products.category_id')
                    ->where('categories.id',$request->id)
                    ->where('category','individu')
                        ->where('status','on')
                    ->orderBy('price', 'desc')
                    ->paginate(15);
                }else{
                    $products = DB::table('categories')
                    ->join('products','categories.id','=','products.category_id')
                    ->where('categories.id',$request->id)
                    ->where('category','individu')
                        ->where('status','on')
                    ->orderBy('categories.created_at', 'desc')
                    ->paginate(15);
                }
            }else{
                if($request->order == 'lh'){
                    $products = DB::table('categories')
                    ->join('products','categories.id','=','products.category_id')
                    ->where('categories.id',$request->id)
                    ->where('category','wholesale')
                        ->where('status','on')
                    ->orderBy('price', 'asc')
                    ->paginate(15);
                }elseif($request->order == 'hl'){
                    $products = DB::table('categories')
                    ->join('products','categories.id','=','products.category_id')
                    ->where('categories.id',$request->id)
                    ->where('category','wholesale')
                        ->where('status','on')
                    ->orderBy('price', 'desc')
                    ->paginate(15);
                }else{
                    $products = DB::table('categories')
                    ->join('products','categories.id','=','products.category_id')
                    ->where('categories.id',$request->id)
                    ->where('category','wholesale')
                        ->where('status','on')
                    ->orderBy('categories.created_at', 'desc')
                    ->paginate(15);
                }
            }
    }else{
        if($request->order == 'lh'){
            $products = DB::table('categories')
            ->join('products','categories.id','=','products.category_id')
            ->where('categories.id',$request->id)
            ->where('category','individu')
                ->where('status','on')
            ->orderBy('price', 'asc')
            ->paginate(15);
        }elseif($request->order == 'hl'){
            $products = DB::table('categories')
            ->join('products','categories.id','=','products.category_id')
            ->where('categories.id',$request->id)
            ->where('category','individu')
                ->where('status','on')
            ->orderBy('price', 'desc')
            ->paginate(15);
        }else{
            $products = DB::table('categories')
            ->join('products','categories.id','=','products.category_id')
            ->where('categories.id',$request->id)
            ->where('category','individu')
                ->where('status','on')
            ->orderBy('categories.created_at', 'desc')
            ->paginate(15);
        }
    }

        // return $products;
        return view('shopFiltered', compact('products'));
    }

    public function updateCategory(Request $request, $id)
    {
        $category = Category::findOrFail($id);
//        $category->update($request->all());

        if ($request->image) {
            //            $path = Storage::disk('public')->put('supplier', $request->image);

            $file = $request->image;
            $path = $file->hashName('category');
            // supplier/bf5db5c75904dac712aea27d45320403.jpeg

            $image = Image::make($file);

            $image->resize(null, 350, function ($constraint) {
                $constraint->aspectRatio();
            });

            //            $path = Storage::put($path, (string) $image->encode());
            Storage::disk('public')->delete($category->image);
            Storage::disk('public')->put($path, (string) $image->encode());

            $category->image = $path;
        }
        $category->name = $request->name;
        $category->save();

        if ($category != null)
            return redirect()->route('admin.category')->with('success', 'Update kategori sukses!');

        return redirect()->route('admin.category')->with('error', 'Update kategori gagal!');
    }
}
