<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 05/01/18
 * Time: 19.47
 */
?>
@extends('layouts.app')
@section('content')
    <div class="section pt-4 pb-7" id="user">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="display-3 text-center text-success">ORDER TERSIMPAN DALAM SISTEM</h3><br/>
                            <h3 class="panel-title text-center">Cek Email untuk melihat informasi invoice anda</h3><br/>
                            <font color="#f64336"> <h3 class="panel-title text-center text-danger">SEGERA LAKUKAN PEMBAYARAN DENGAN CARA BERIKUT :</h3> </font>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class=" col-md-12 col-lg-12 ">
                                    <table class="table table-user-information">
                                        <tbody>
                                        <tr>
                                            <td>1.</td>
                                            <td colspan="2">Login dan masuk ke profile anda, atau klik <a href="http://locarvest.com/user-profile"><font color="#3b7795"><b>disini</b></font></a></td>
                                        </tr>
                                        <tr>
                                            <td>2.</td>
                                            <td>Pilih tab Konfirmasi Pembayaran</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td colspan="2"><img style="display: block;" src="http://locarvest.com/assets/images/konfirmasipembayaran-2.png" alt="" /></td>
                                        </tr>
                                        <tr>
                                            <td>3.</td>
                                            <td>Tekan tombol Upload</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td colspan="2"><img style="display: block;" src="http://locarvest.com/assets/images/tombolupload-2.png" alt="" /></td>
                                        </tr>
                                        <tr>
                                            <td>4.</td>
                                            <td colspan="2">Masukkan informasi bank dan no. rekening pengirim dan upload gambar bukti transfer</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td colspan="2"><img style="display: block;" src="http://locarvest.com/assets/images/buktitransfer.png" alt="" /></td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
