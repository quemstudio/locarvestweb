<?php
/**
 * Created by PhpStorm.
 * User: AldiTahir
 * Date: 13/02/2018
 * Time: 8:45
 */
?>

<div class="row">
    <div class="col-md-6 col-md-offset-3">

        <div class="modal-body">
            <div class="commerce">
                <form class="commerce-login-form" id="updateOrder" action="{{route('uploadPaymentFile')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}} {{method_field('PUT')}}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-wrap">
                                No. Order
                                <input class="bg-secondary" type="text" name="order_number" id="order_number" placeholder="No. Order" value="{{ $orderNumber }}" readonly>
                                <input type="hidden" name="order_id" id="order_id" placeholder="Order Id" value="{{ $orderId }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-wrap">
                                Bank
                                <input type="text" name="bank" id="bank"  placeholder="" value="" size="40" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-wrap">
                                No. Rekening
                                <input type="text" name="account_number" id="account_number"  placeholder="" value="" size="40" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="transferFile" name="image" required>
                                <label class="custom-file-label" for="inputGroupFile01">Bukti Transfer</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-wrap">
                                <button class="col-md-12" type="submit">Confirm</button>
                                {{--<button onclick="updateBillingSubmit()" class="col-md-12">Confirm</button>--}}
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
