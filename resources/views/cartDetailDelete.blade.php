<?php
/**
 * Created by PhpStorm.
 * User: black
 * Date: 1/20/2018
 * Time: 5:13 PM
 */
?>
<div class="col-md-8" >
    <div class="table-responsive pre-scrollable">
        <table class="table shop-cart" style="table-layout: fixed;">
            <tbody>
            @foreach($carts as $row)
                <tr class="cart_item">
                    <td class="product-remove">
                        <a class="remove" onclick="deleteCartDetailTemp('{{ $row->id }}')" style="cursor: pointer">×</a>
                    </td>
                    <td class="product-thumbnail">

                            <img src="{{URL::asset($row->options->img)}}" alt="">

                    </td>
                    <td class="product-info">
                        {{ $row->name }}
                        {{--<span class="sub-title">Faucibus Tincidunt</span>--}}
                        <span class="amount" value="">Rp. {{ str_replace(',', '.', number_format($row->price )) }}</span>
                    </td>
                    <td class="product-quantity">
                        <div class="quantity">
                            <input id="qty-{{ $row->id }}" type="number" min="1" name="number" value="{{ $row->qty }}" class="input-text qty text" size="4"
                                   onmouseup="addQty('{{ $row->id }}', 'qty-{{ $row->id }}', '{{ $row->price }}')"
                                   onkeyup="addQty('{{ $row->id }}', 'qty-{{ $row->id }}', '{{ $row->price }}')"

                            >
                        </div>
                    </td>
                    <td class="product-subtotal">
                        <span class="amount" id="amount-{{ $row->id }}">Rp. {{ str_replace(',', '.', number_format($row->price*$row->qty )) }}</span>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="table-responsive">
        <table class="table shop-cart" style="table-layout: fixed;">
            <tr>
                <td colspan="5" class="actions">
                    <a class="" href="{{ route('products.index') }}">
                        <button class="col-md-3 ion-backspace" type="button" class="btn btn-primary" > Back to Shop
                        </button>
                    </a>
                </td>
            </tr>
        </table>
    </div>
</div>
<div class="col-md-4" id="updateCart">
    <div class="cart-totals">
        <table>
            <tbody>
            <tr class="cart-subtotal">
                <th>Subtotal</th>
                <td>Rp. <?php echo Cart::subtotal(); ?></td>
            </tr>
            </tbody>
        </table>
        <div class="proceed-to-checkout" onclick="cartCheck('{{Cart::count()}}')" style="cursor:pointer">
            <a >Lanjut ke Pengiriman</a>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('[data-toggle="tooltip"]').tooltip({
        trigger: 'manual'
    });

    function cartCheck(cart) {
        if (cart > 0) {
            var url = '{{URL::to('/checkout')}}';
            window.location.href = url;
        } else {
            $.toast({
                heading: 'Error',
                text: 'Cart Kosong',
                showHideTransition: 'fade',
                icon: 'error',
                hideAfter: 3000,
                position: 'bottom-center',
                loader: false,
            });
        }
    }

</script>