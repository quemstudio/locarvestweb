<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 16/01/18
 * Time: 19.25
 */
?>
@extends('layouts.appadmin')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-header ">
                    @if($post->categoryPost_id == 21)
                    <h3><i class="icon-target"></i> <strong>Data</strong> Testimoni</h3>
                    @else
                    <h3><i class="icon-target"></i> <strong>Data</strong> Post</h3>
                        @endif
                </div>
                @if($post->categoryPost_id == 21)
                    <div class="panel-content">
                        <div class="row">
                            <form class=" form-horizontal" method="post" enctype="multipart/form-data" action="{{route('testimony.update.submit', $post->id)}}">
                                {{csrf_field()}}
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Nama</label>
                                        <div class="col-sm-9">
                                            @php $splitName = explode('-',$post->title);@endphp
                                            <input class="form-control input-lg" type="text" placeholder="" name="nama" value="@php echo($splitName[0]); @endphp"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Status</label>
                                        <div class="col-sm-9">
                                            <input class="form-control input-lg" type="text" placeholder="" name="status" value="@php echo($splitName[1]); @endphp"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Testimoni</label>
                                        <div class="col-md-9">
                                            <textarea class="summernote bg-white" name="body" >{!! $post->body !!}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Cover</label>
                                        <div class="col-sm-9">
                                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                {{--<div class="form-control" data-trigger="fileinput">--}}
                                                {{--<i class="glyphicon glyphicon-file fileinput-exists"></i>--}}
                                                {{--<span class="fileinput-filename">{{$post->cover}}</span>--}}
                                                {{--</div>--}}
                                                <input type="text" class="form-control" id="upload-file-info" readonly value="{{$post->cover}}">
                                                <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Choose...</span><span class="fileinput-exists">Change</span>
                                                <input type="file" name="cover" onchange="$('#upload-file-info').val($(this).val().split('\\').pop());">
                                            </span>
                                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-primary" style="float:right">Submit
                                            </button>
                                            <a href="{{route('admin.testimony')}}">
                                                <button type="button" class="btn btn-danger" style="float:right">Batal
                                                </button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                @else
                <div class="panel-content">
                    <div class="row">
                        <form class=" form-horizontal" method="post" action="{{route('post.update.submit', $post->id)}}"
                              enctype="multipart/form-data">
                            {{csrf_field()}} {{method_field('PUT')}}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Judul</label>
                                    <div class="col-sm-9">
                                        <input class="form-control input-lg" type="text" placeholder="" name="title" value="{{$post->title}}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Kategori Post </label>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <select class="form-control" style="width:100%;" name="categoryPost_id">
                                                @foreach($cat_posts as $data)
                                                    @if($data->id == $post->categoryPost_id)
                                                        <option value="{{$data->id}}" selected>{{$data->name}}</option>
                                                    @else
                                                        <option value="{{$data->id}}">{{$data->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Slider Landing Page</label>
                                    <div class="col-sm-9">
                                        <div class="onoffswitch2">
                                            @if($post->slide == 'on')
                                                <input name="status" class="onoffswitch-checkbox"
                                                       id="status" type="checkbox" value="on" checked>
                                                <label class="onoffswitch-label" for="status">
                                                    <span class="onoffswitch-inner"></span>
                                                    <span class="onoffswitch-switch"></span>
                                                </label>
                                            @else
                                                <input name="status" class="onoffswitch-checkbox"
                                                       id="status" value="on" type="checkbox">
                                                <label class="onoffswitch-label" for="status">
                                                    <span class="onoffswitch-inner"></span>
                                                    <span class="onoffswitch-switch"></span>
                                                </label>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Gambar</label>
                                    <div class="col-sm-9">
                                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                            {{--<div class="form-control" data-trigger="fileinput">--}}
                                                {{--<i class="glyphicon glyphicon-file fileinput-exists"></i>--}}
                                                {{--<span class="fileinput-filename">{{$post->cover}}</span>--}}
                                            {{--</div>--}}
                                            <input type="text" class="form-control" id="upload-file-info" readonly value="{{$post->cover}}">
                                            <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Pilih...</span><span class="fileinput-exists">Ganti</span>
                                                <input type="file" name="cover" onchange="$('#upload-file-info').val($(this).val().split('\\').pop());">
                                            </span>
                                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <h3>Konten <strong>Editor</strong></h3>
                                        <textarea class="summernote bg-white"  name="body" required>{{$post->body}}</textarea>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary" style="float:right">Update Data</button>
                                        <a href="{{route('admin.post')}}"><button type="button" class="btn btn-danger" style="float:right">Batal</button></a>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>

                </div>

                @endif
            </div>
        </div>
    </div>

@endsection