<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 16/01/18
 * Time: 19.25
 */
?>
    @extends('layouts.appadmin') @section('content') @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @elseif(session('error'))
    <div class="alert alert-error">
        {{ session('error') }}
    </div>
    @endif
    <!-- BEGIN PAGE CONTENT -->

    <div class="header">
        <h2>Data
            <strong>Administrator</strong>
        </h2>
    </div>
    <div class="row">
        <div class="col-lg-12 portlets">
            <div class="panel">
                <div class="panel-header ">
                    <h3>
                        <a href="{{route('admin.add.show')}}">
                            <button type="button" class="btn btn-primary">+ Add Administrator</button>
                        </a>
                    </h3>
                </div>
                <div class="panel-content">
                    <div class="filter-left panel-content pagination2 table-responsive">
                        <table class="table table-hover table-dynamic table-tools" data-table-name="Data Admin">
                            <thead>
                                <tr>
                                    <th width="40px">No</th>
                                    <th>Email</th>
                                    <th class='hidden-350'>Nama</th>
                                    <th class='hidden-1024'>Role</th>
                                    <th width="125px" style="min-width: 125px"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($admins as $data)
                                <tr id="tr_{{$data->id}}">
                                    <td>{{$no++}}</td>
                                    <td>
                                        {{$data->email}}
                                    </td>
                                    <td class='hidden-350'>{{$data->first_name." ".$data->last_name}}</td>
                                    <td class='hidden-1024'>{{$data->role}}</td>
                                    <td class='hidden-480'>
                                        <div class="btn-group">
                                            @if($data->role != 'superadmin')
                                            <a class="btn btn-success" href="{{route('admin.edit.show' , $data->id)}}">
                                                <i style="padding:0px; margin:0px" class="fa fa-edit"></i>
                                            </a>
                                            <button class="btn btn-danger" type="button" id="danger-alert" data-tr="tr_{{$data->id}}" onclick="deleteUser('{{$data->id}}', '{{$data->first_name." ".$data->last_name}}')">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                            @endif
                                        </div>

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection @push('script')
    <script>
        function deleteUser(id, name) {
            swal({
                title: "Are you sure want to delete admin " + name + " ?",
                text: "You will not be able to recover this data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn btn-danger btn-custom waves-effect waves-light',
                confirmButtonText: "Yes, delete it!",
                cancelButtonClass: 'btn',
                buttonsStyling: false,
                reverseButtons: true,
                closeOnConfirm: false
            }, function () {
                setTimeout(function () {
                    $.ajax({
                        url: 'admin_page/' + id,
                        type: 'DELETE',
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (data) {
                            if (data == null) {
                                swal("Deleted!", "Admin has been deleted.", "success");
                                location.reload();
                            } else
                                swal("Not Deleted!", "Failed to delete.", "error");
                        },
                        error: function () {
                            swal("Error!", "Failed to delete.", "error");
                        }
                    });
                }, 2000)
            });
        }
    </script>
    @endpush