<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 05/01/18
 * Time: 19.54
 */
?>
@extends('layouts.app')
@section('content')
    <div id="main">
        <div class="section section-bg-10 pt-2 pb-2">
            <div class="container">
                <div class="row ">
                        <div class="commerce col-md-4 col-md-offset-4">
                            <form class="commerce-login-form" action="{{route('login')}}" method="post">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Email<span class="required">*</span></label>
                                        <div class="form-wrap">
                                            <input type="text" name="email" value="" size="40">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Password <span class="required">*</span></label>
                                        <div class="form-wrap">
                                            <input type="password" name="password" value="" size="40">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-wrap">
                                            <input type="submit" value="Login">
                                        </div>
                                    </div>
                                </div>
                                @if ($errors->any())
                                    <div class="alert alert-danger text-center">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="{{route('password.request')}}">Lupa Password ?</a>
                                    </div>
                                </div>
                            </form>


                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection
