<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryPost extends Model
{
    protected $primaryKey="id";
    protected $guarded = [];



    public function post()
    {
        return $this->hasMany('App\Post','id');
    }
}
