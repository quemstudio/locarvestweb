<?php
/**
 * Created by PhpStorm.
 * User: robseptian
 * Date: 05/01/18
 * Time: 19.54
 */
?>

<div class="container">
    <div class="row">
        <div class="table-responsive">
            <table class="table shop-cart">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Alamat</th>
                    <th scope="col">Kota</th>
                    <th scope="col">Kecamatan</th>
                    <th scope="col">Kelurahan</th>
                    <th scope="col">Kode Pos</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($users[0]->addresses as $address)
                    <tr>
                        <td>{{$address->address}}</td>
                        <td>{{$address->city}}</td>
                        <td>{{$address->kecamatan}}</td>
                        <td>{{$address->kelurahan}}</td>
                        <td>{{$address->zip_code}}</td>
                        <td>
                            <button onclick="deleteAddress('{{ $address->id }}')" class="ion-trash-a btn btn-danger"
                                    data-toggle="tooltip" data-placement="top" title="Hapus">
                                {{--<button onclick="updateAddress('{{ $address->id }}')" class="ion-edit btn btn-primary"--}}
                                        {{--data-toggle="tooltip" data-placement="top" title="Edit">--}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <button class="col-md-3" type="button" class="btn btn-primary" href="#exampleModalAddress"  data-toggle="modal">Tambah Alamat Baru
            </button>
        </div>
    </div>
</div>

<style>
    .modal-backdrop.in {
        z-index: 1000;
    }

    .modal-open {
        overflow: scroll;
    }

    .ui-autocomplete-input {
        z-index: 10000 !important;
    }

</style>
