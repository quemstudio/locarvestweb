<?php
/**
 * Created by PhpStorm.
 * User: AldiTahir
 * Date: 13/02/2018
 * Time: 5:41
 */
?>
<div class="container">
    <div class="row"  id="deleteCart">
        <div class="col-md-8" >
            <div class="table-responsive pre-scrollable">
                <table class="table shop-cart" style="table-layout: fixed;">
                    <th colspan="2">Produk</th>
                    <th>Qty</th>
                    <th>Harga</th>
                    <tbody>
                    @foreach($joined as $row)
                        <tr class="cart_item">
                            <td class="product-thumbnail">
                                <a href="#">
                                    <img src="{{URL::asset('/storage/'.$row->image)}}" alt="">
                                </a>
                            </td>
                            <td class="product-info">
                                <a href="#">{{ $row->name }}</a>
                                {{--<span class="sub-title">Faucibus Tincidunt</span>--}}
                                <span class="amount" value="">Rp. {{ str_replace(',', '.', number_format($row->price )) }}</span>
                            </td>
                            <td class="product-quantity">
                                <div class="quantity">
                                    {{ $row->qty }}
                                </div>
                            </td>
                            <td class="product-subtotal">
                                <span class="amount" id="amount-{{ $row->id }}">Rp. {{ str_replace(',', '.', number_format($row->orderDetailTotal )) }}</span>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-md-4" id="updateCart">
            <div class="table-responsive">
                <table class="table table-hover text-center">
                    <tbody>
                    <tr >
                        <th colspan="2">Subtotal</th>
                        <td>Rp. {{$total_price}},00</td>
                    </tr>
                    <tr >
                        <th colspan="2">Pengiriman</th>
                        <td>Rp. {{ $joined[0]->orderTotal - $total_price }},00</td>
                    </tr>
                    <tr >
                        <th colspan="2">Total</th>
                        <td><strong>Rp. {{ str_replace(',', '.', number_format($joined[0]->orderTotal)) }},00</strong></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
